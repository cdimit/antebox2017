require('./bootstrap');



Vue.component('CheckoutForm', require('./components/CheckoutForm.vue'));
Vue.component('SubscribeForm', require('./components/SubscribeForm.vue'));
Vue.component('RegisterForm', require('./components/RegisterForm.vue'));
Vue.component('UpdatecardForm', require('./components/UpdatecardForm.vue'));


const app = new Vue({
    el: '#app'
});
