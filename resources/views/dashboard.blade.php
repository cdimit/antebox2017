@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h3>Welcome {{Auth::user()->profile->name}}</h3>
				  		</div>


					</div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-users"></i>
							</div>
							@if(array_key_exists("total", $visits))
								<div class="count">{{$visits['total']}}</div>
							@else
								<div class="count">0</div>
							@endif
							<h3>Number of visitors</h3>
							<p>Number of visitors for your business on Shopguin.</p>
							</div>
						</div>
						<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-shopping-cart"></i>
							</div>
							<div class="count">{{Auth::user()->profile->openCarts()}}</div>
							<h3>New shopping carts</h3>
							<p>Number of open shopping carts currently with your business.</p>
							</div>
						</div>
						<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-gift"></i>
							</div>
							<div class="count">{{Auth::user()->profile->products->count()}}</div>
							<h3>Number of products</h3>
							<p>Number of products currently listed with your business on Shopguin.</p>
							</div>
						</div>
						<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<div class="tile-stats">
								<div class="icon"><i class="fa fa-map-marker"></i>
							</div>
							<div class="count">{{Auth::user()->profile->stores->count()}}</div>
							<h3>Number of stores</h3>
							<p>Number of stores listed for your business on Shopguin.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Overview of my business</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="demo-container" style="height:280px">
											<div id="totalProductSales" class="demo-placeholder"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Getting started</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Have a look at the list below of what you need to fill in, to get maximise the most out of Shopguin!
									</p>
									<div class="">
										<ul class="to_do">
											<li>
												Choose a theme for my business
											</li>
											<li>
												Customise the fields in my chosen theme
											</li>
											<li>
												List my products on Shopguin
											</li>
											<li>
												List my stores on Shopguin
											</li>
											{{-- @foreach($visits as $key => $value)
											<li>
												{{$key}}: {{($value)}}
												<br>
											</li>
											@endforeach --}}
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/dashboard/frontpage.js"></script>
	<script type="text/javascript" src="/js/assets/jquery.flot.min.js"></script>
	<script type="text/javascript" src="/js/assets/jquery.flot.time.min.js"></script>
</body>
</html>
