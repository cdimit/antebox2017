
{{-- Make bid --}}
<div class="modal fade meeting-point-modal" id="makeABid" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<i class="icon icon-hammer"></i>
					Make a bid on this shopping cart
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<form class="form-horizontal" role="form" method="POST" action="/delivery/{{$request->id}}/bid/create">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-6">
									<label for="delivery_fee" class="{{ $errors->has('delivery_fee') ? ' has-error' : '' }}">Delivery Fee (€)</label>
									<input id="delivery_fee" type="text" class="" value="{{ old('delivery_fee') }}" name="delivery_fee">
								</div>
								@if(Auth::user()->isClient())
								<div class="col-md-6">
									<label for="delivery_date" class="">Delivery Date</label>
									<input id="date" type="text" class="{{ $errors->has('delivery_date') ? ' has-error' : '' }}" value="{{ old('delivery_date') }}" name="delivery_date" placeholder="">
								</div>
								<input type="hidden" value="Crowd delivery" name="delivery_method">
								@endif
								@if(Auth::user()->isBusiness())
								<div class="col-md-6">
									<label for="delivery_method" class="">Delivery Method</label>
									<input id="delivery_method" type="text" class="{{ $errors->has('delivery_method') ? ' has-error' : '' }}" value="{{ old('delivery_method') }}" name="delivery_method" placeholder="">
								</div>
								<div class="col-md-12">
									<label for="delivery_date" class="">Delivery Date</label>
									<input type="text" class="{{ $errors->has('delivery_date') ? ' has-error' : '' }}" value="{{ old('delivery_date') }}" name="delivery_date" placeholder="Estimated between Thu. Apr. 27 and Thu. May. 4">
								</div>
								@endif
								<div class="col-md-12">
									<label for="message" class="">Message</label>
									<input id="message" type="text" class="{{ $errors->has('message') ? ' has-error' : '' }}" value="{{ old('message') }}" name="message">
									@if ($errors->has('message'))
										<span class="help-block">
											<strong>{{ $errors->first('message') }}</strong>
										</span>
									@endif

								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary btn-lg">
										<i class="icon icon-auction"></i>
										Make bid
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
