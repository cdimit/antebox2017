@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')

			
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Shopping carts</h2>
				  		</div>

						
					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-4">
							<div class="x_panel">
								<div class="x_title">
									<h2>Cart</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<div class="row">
										@foreach($request->items as $item)
										<div class="col-md-2 col-sm-3 col-xs-3">
											<img class="img-responsive" src="/storage/{{$item->product->picture}}">
										</div>
										<div class="col-md-10 col-sm-9 col-xs-9">
											<h4 style="margin: 0px">
												{{$item->product->name}}
											</h4>
											<h6>
												<span>
													Qty
												</span>
												{{$item->qty}} @
												€ {{$item->price}}
											</h6>
											<hr>
										</div>
										@endforeach
										<hr>
										<h4>
											Message
										</h4>
										<p>
											@if($request->message)
												<i>
													{{$request->message}}
												</i>
											@else
												<i>No message.</i>
											@endif
										</p>
										<hr>
										<h4>
											Delivery location											
										</h4>
										<p>
											<i>
												{{$request->meetingPoint['address1']}}, 
												{{$request->meetingPoint['address2']}}, 
												{{$request->meetingPoint['state']}}, 
												{{$request->meetingPoint['city']}}, 
												{{$request->meetingPoint['post_code']}}, 
												{{$request->meetingPoint['country']}}
											</i>
										</p>
										<hr>
										<h3>
											Total
										</h3>
										€ {{number_format($request->total_price, 2)}}
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="row">
								{{-- Current bids --}}
								<div class="col-md-12">
									<div class="x_panel">
										<div class="x_title">
											<h2>Bids made on cart</h2>
								 			
											<div class="clearfix"></div>
										</div>
										<div class="x_content">
											@if($request->status == 'open')

												<p class="text-muted font-13 m-b-30">
													Listed below are all the bids that have been made by crowd deliverers on this shopping cart so far:
												</p>

												@if($request->bids->count() > 0)
													<div class="row">
														@foreach($request->bids as $bid)
														<div class="col-md-4">
															<div class="x_panel">
																<div class="">
																	<h4>
																		Bid by <br>
																		<small>
																			{{$bid->biddable->name}}
																		</small>
																	</h4>
																	<p class="font-12 m-b-30 text-muted">
																		<h4>
																			<b>
																				Bid amount			
																			</b>
																		</h4> 
																		€ {{$bid->delivery_fee}}
																		<br>
																		<h4>
																			<b>
																				Message
																			</b>
																		</h4>
																		{{$bid->message}}
																		<br>
																		<h4>
																			<b>
																				Delivery date
																			</b>
																		</h4>
																		{{$bid->delivery_date}}
																	</p>
																</div>
															</div>
														</div>
														@endforeach
													</div>
												@elseif($request->bids->count() == 0)
													<h4>
														No bids made on this cart yet. Why not make a bid?
														<a href="/business/carts/{{$request->id}}/bid" class="btn btn-default"><i class="fa fa-gavel" aria-hidden="true"></i> Make a bid</a>
													</h4>
												@endif
											
											@elseif ($request->status == 'close')
												<p class="text-muted font-13 m-b-30">
													This cart has an accepted bid, shown below:
												</p>
												<div class="row">
													
													<div class="col-md-4">
														<div class="x_panel">
															<div class="">
																<h4>
																	Bid by <br>
																	<small>
																		{{$request->acceptedBid()[0]->biddable->first_name}}, {{$request->acceptedBid()[0]->biddable->last_name}}
																	</small>
																</h4>
																<p class="font-12 m-b-30 text-muted">
																	<h4>
																		<b>
																			Bid amount			
																		</b>
																	</h4> 
																	€ {{$request->acceptedBid()[0]->delivery_fee}}
																	<br>
																	<h4>
																		<b>
																			Message
																		</b>
																	</h4>
																	{{$request->acceptedBid()[0]->message}}
																	<br>
																	<h4>
																		<b>
																			Delivery date
																		</b>
																	</h4>
																	{{$request->acceptedBid()[0]->delivery_date}}
																</p>
															</div>
														</div>
													</div>
													
												</div>
											@endif
										</div>
									</div>									
								</div>
								{{-- Make a bid --}}
								<div class="col-md-12">
									<div class="x_panel">
										<div class="x_title">
											<h2>Make a bid</h2>
								 			
											<div class="clearfix"></div>
										</div>
										<div class="x_content">
											<p class="text-muted font-13 m-b-30">
												Have a look at the request items, and choose to make a bid.
											</p>
										
											
									
											<form class="form-horizontal" role="form" method="POST" action="/delivery/{{$request->id}}/bid/create" enctype="multipart/form-data">
												{{ csrf_field() }}

												<div class="form-group{{ $errors->has('delivery_fee') ? ' has-error' : '' }}">
												<label for="delivery_fee" class="col-md-4 control-label">Delivery Fee</label>
												<div class="col-md-6">
												<input id="delivery_fee" type="text" class="form-control" value="{{ old('delivery_fee') }}" name="delivery_fee">
												</div>
												</div>
												<div class="form-group{{ $errors->has('delivery_method') ? ' has-error' : '' }}">
												<label for="delivery_method" class="col-md-4 control-label">Delivery Method</label>
												<div class="col-md-6">
												<input id="delivery_method" type="text" class="form-control" value="{{ old('delivery_method') }}" name="delivery_method">
												</div>
												</div>

												<div class="form-group{{ $errors->has('delivery_date') ? ' has-error' : '' }}">
												<label for="delivery_date" class="col-md-4 control-label">Delivery Date</label>
												<div class="col-md-6">
												<input id="date" type="text" class="form-control" value="{{ old('delivery_date') }}" name="delivery_date" placeholder="">
												</div>
												</div>

												<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
												<label for="message" class="col-md-4 control-label">Message</label>
												<div class="col-md-6">
												<textarea name="message" id="" cols="30" rows="10"  value="{{ old('message') }}"></textarea>

												@if ($errors->has('message'))
												<span class="help-block">
												<strong>{{ $errors->first('message') }}</strong>
												</span>
												@endif
												</div>
												</div>


												<div class="form-group">
												<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Make a Bid
												</button>
												</div>
												</div>

											</form>
										</div>
									</div>
								</div>

							</div>
						</div>
						
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script src="/js/assets/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#date').datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d'
			});
		});
	</script>
</body>
</html>