@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h3 class="w300 c-black">
			<img src="/storage/{{$request->client->picture}}" class="profile-img-65">
			{{$request->client->first_name}} requested {{$request->created_at->diffForHumans()}}

		</h3>
		<div class="row">
			{{-- Shopping cart contents --}}
			<div class="col-md-4">
				<h3 class="w300 c-gray">
					Items requested
				</h3>
				<div class="box-light">
					<h3 class="w300" style="margin-top: 0;">
						<i class="icon icon-shopping-cart"></i>
						{{$request->business->name}}
					</h3>
					<hr>
					<ul class="products-as-list">
						<div class="row">
							@foreach($request->items as $item)
							<div class="col-md-12">
								<li>
									<div class="row">
										<div class="col-md-2 col-sm-3 col-xs-3">
											<img class="img-responsive" src="/storage/{{$item->product->picture}}">
										</div>
										<div class="col-md-10 col-sm-9 col-xs-9">
											<h4>
												{{$item->product->name}}
											</h4>
											<h6>
												<span>
													Qty
												</span>
												{{$item->qty}} @
												€ {{$item->price}}
											</h6>
										</div>
									</div>
								</li>
							</div>
							@endforeach

						</div>
						<h3>
							<span>Total</span> € {{$request->total_price}}
						</h3>
					</ul>
				</div>
			</div>
			{{-- Request details --}}
			<div class="col-md-8">
				<h3 class="w300 c-gray">
					Details about this request
				</h3>
				<div class="box-light">

					<h3 class="info" style="margin-top: 0px;">
						<span> <i class="icon icon-chat"></i> &nbsp; Message</span>
						</br>
					</h3>
						<p class="s-12em c-med-gray">
							{{$request->message}}
						</p>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<h3 class="info" style="margin-top: 0px;">
								<span><i class="icon icon-place"></i> &nbsp; Pickup location</span>
								</br>
								{{$request->business->address}},
								{{$request->business->state}},
								{{$request->business->zip_code}},
								{{$request->business->country}}
							</h3>
						</div>
						<div class="col-md-6">
							<h3 class="info" style="margin-top: 0px;">
								<span><i class="icon icon-place"></i> &nbsp;Delivery location</span>
								</br>
								{{-- {{$request->meetingPoint}} --}}
								{{$request->meetingPoint["address1"]}},
								{{$request->meetingPoint["address2"]}},
								{{$request->meetingPoint["city"]}},
								{{$request->meetingPoint["state"]}},
								{{$request->meetingPoint["post_code"]}},
								{{$request->meetingPoint["country"]}}
							</h3>
						</div>
					</div>

				</div>
			</div>

		</div>
		<hr>
		<div class="row text-center">
			<div class="col-md-12">
				<h3 class="w300">
					Why not make a bid for this request?
				</h3>

				<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#makeABid">
					<i class="icon icon-auction"></i>
					Make your bid
				</button>
				@include('bid.makebid')

			</div>


		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h3 class="w300">
					Bids made so far on this shopping cart:
				</h3>
				@if($request->bids->count() > 0)
				<div class="scrollable-container">
					<div class="row">
						@foreach($request->bids as $bid)
						<div class="col-md-3">
							<div class="box-light">
								<h3 class="info no-vm">
									<span>Bid by</span> &nbsp;
									<img src="/images/user-generic.png" alt="..." class="profile-img">
{{-- 									<img src="/storage/{{$bid->biddable->picture}}" alt="..." class="profile-img"> --}}
									{{$bid->biddable->name}}
								</h3>
								<hr>
								<h2 class="no-vm w300">€ {{$bid->delivery_fee}}</h2>
								<hr>
								<h3 class="info no-vm">
									<span>
										<i class="icon icon-delivery"></i>
										Delivery method
									</span>
										{{$bid->delivery_method}}
								</h3>
								<h3 class="info no-vm">
									<span>
										<i class="fa fa-calendar-o" aria-hidden="true"></i>
										Date
									</span>
									{{$bid->delivery_date}}
								</h3>
							</div>
						</div>
						@endforeach
					</div>
				</div>
				@else
					<h3 class="w300 c-med-gray">
						No bids yet..
					</h3>
				@endif
			</div>

		</div>
	</div>

</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#date').datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d'
			});
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
@endsection
