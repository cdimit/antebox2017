@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>My bid - Shopguin</title>
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-hammer"></i>
			My bid on shopping cart for London, GB
		</h1>
		<h4 class="w300 c-gray">
			<span>
				<i class="icon icon-small-calendar"></i> &nbsp;
			</span>
			27/01/2018
		</h4>
		
		<div class="row">
			<div class="col-md-3">	
				<div class="box-light">	
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<h4 class="w300 c-primary no-vm">
								YOUR <br> BID
							</h4>
						</div>
						<div class="col-sm-6 col-md-8">
							<h3 class="w300 c-med-gray no-vm">
								<i class="icon icon-cash"></i> &nbsp;
								£65.00
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">	
				<div class="box-light">	
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<h4 class="w300 c-primary no-vm">
								DELIVERY <br> DATE
							</h4>
						</div>
						<div class="col-sm-6 col-md-8">
							<h3 class="w300 c-med-gray no-vm">
								<i class="icon icon-small-calendar"></i> &nbsp;
								27/01/2018
							</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3 class="w300">
					Cart contents
				</h3>
				{{-- Shopping cart contents --}}
				<div class="box-light">
					<div class="row">
						<div class="col-md-12 col-xs-10">
							<h4 class="w300 no-vm">
								<i class="pe-7s-cart pe-lg pe-va"></i>
								Apple Ltd.
							</h4>
							<hr>
						</div>

					</div>
					<div class="shopping-cart-body">
						<ul class="products-as-list">
							<li>
								<div class="row">
									<div class=" col-xs-12 col-sm-6 col-md-6">
										<div class="row">
											<div class="col-md-2 col-xs-3">
												<img class="img-responsive" src="/storage/">
												</br>
											</div>
											<div class="col-md-10 col-xs-9">
												<h4>
													Apple iMac
												</h4>
												<h6>
													<span>
														Qty
													</span>
													5
												</h6>
												<h6>
													<span>
														Price
													</span>
													€ 2500
												</h6>
												<hr>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
						<div class="row">
							<div class="col-md-8 col-xs-12">
								<h3 class="info" style="margin-top: 0px;">
									<span><i class="icon icon-place"></i> &nbsp;Delivery location</span>
									</br>
									229 Mile End road <br>	
									London, E1 4AA <br>	
									United Kingdom
									
								</h3>
							</div>
							<div class="col-md-4 col-xs-12">	
								<h3 class="info no-vm">
									<span>
										Value
									</span>
									€ 2,500
								</h3>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="" class="btn btn-lg btn-primary">
					<i class="fa fa-ban" aria-hidden="true"></i>
					Withdraw my bid
				</a>
				<br>
				<br>
			</div>
		</div>
		
	</div>


</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script src="/js/app.js"></script>
@endsection
