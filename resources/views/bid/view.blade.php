@extends('layouts.boilerplate')

@section('head')
	<title>My shopping cart - Shopguin</title>
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">

	<script>
	 window.Stripe = <?php echo json_encode([
		 'csrfToken' => csrf_token(),
		 'stripeKey' => config('services.stripe.key'),
		 'user'			=> auth()->user()
	 ]); ?>
	</script>

@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-shopping-cart"></i>
			My shopping cart
		</h1>
		<h4 class="w300 c-gray">
			<span>
				<i class="icon icon-small-calendar"></i> &nbsp;
			</span>
			{{$request->created_at}}
		</h4>
		@if($request->status == 'close')
			@if($request->invoice->is_paid == 1)
				<div class="info-bar green">
					<h3 class="w300 c-black no-vm">
						<i class="icon icon-success"></i>
						Items due for delivery on {{$request->acceptedBid()->delivery_date}} by {{$request->acceptedBid()->biddable->name}}.
					</h3>

				</div>
			@else
				<div class="info-bar orange">
					<h3 class="w300 c-white no-vm">
						<i class="icon icon-cash"></i>
						Payment due for bid accepted at € {{$request->acceptedBid()->delivery_fee}}, for delivery on {{$request->acceptedBid()->delivery_date}} by {{$request->acceptedBid()->biddable->name}}.
					</h3>

				</div>
			@endif

		@endif

		<div class="row">
			<div class="col-md-12">
				{{-- Shopping cart contents --}}
				<div class="box-light">
					<div class="row">
						<div class="col-md-12 col-xs-10">
							<h4 class="w300 no-vm">
								<i class="pe-7s-cart pe-lg pe-va"></i>
								{{$request->business->name}}
							</h4>
							<hr>
						</div>

					</div>
					<div class="shopping-cart-body">
						<ul class="products-as-list">
							<li>
								<div class="row">
									@foreach($request->items as $item)
									<div class=" col-xs-12 col-sm-6 col-md-6">
										<div class="row">
											<div class="col-md-2 col-xs-3">
												<img class="img-responsive" src="/storage/{{$item->product->picture}}">
												</br>
											</div>
											<div class="col-md-10 col-xs-9">
												<h4>
													{{$item->product->name}}
												</h4>
												<h6>
													<span>
														Qty
													</span>
													{{$item->qty}}

												</h6>
												<h6>
													<span>
														Price
													</span>
													€ {{$item->price}}
												</h6>
												<hr>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</li>

						</ul>
						<div class="row">
							<div class="col-md-8 col-xs-12">

								<h3 class="info" style="margin-top: 0px;">
									<span><i class="icon icon-place"></i> &nbsp;Delivery location</span>
									</br>
									@if($request->meetingPoint)
									{{$request->meetingPoint->address1}},
									{{$request->meetingPoint->address2}}, <br>
									{{$request->meetingPoint->city}},
									{{$request->meetingPoint->state}},<br>
									{{$request->meetingPoint->post_code}}, <br>
									{{$request->meetingPoint->country}}
									@endif
								</h3>

							</div>
							<div class="col-md-4 col-xs-12">
								@if($request->status == 'close')
									@if(!$request->invoice->is_paid)
										<h3 class="info no-vm">
											<span>
												Sub-total
											</span>
											€ {{$request->invoice->clientRequest->total_price}}
										</h3>
										<h3 class="info no-vm">
											<span>
												Delivery fee
											</span>
											€ {{$request->invoice->delivery_fee}}
										</h3>
										<h3 class="info no-vm">
											<span>
												Service
											</span>
											{{$request->invoice->service_value}}%
										</h3>
										<h3 class="info no-vm">
											<span>
												Service fee
											</span>
											€ {{$request->invoice->service_fee}}
										</h3>
										<h2 class="info no-vm">
											<span>
												<i class="icon icon-euro"></i>
												Grand total
											</span>
											€ {{$request->invoice->final_price}}
										</h2>
										<br>
										<div id="app">
											<checkout-form :invoice="{{ $request->invoice }}"></checkout-form>
										</div>


									@elseif ($request->invoice->is_paid)
										<a href="/deal/{{$request->invoice->id}}" class="btn btn-primary">
											<i class="icon icon-invoice"></i> &nbsp;
											View invoice
										</a>
									@endif
								@else
									<h3 class="info no-vm">
										<span>
											<i class="icon icon-euro"></i>
											Total
										</span>
										€{{$request->total_price}}
									</h3>
								@endif
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
		@if($request->status == 'open')
			<hr>
			<div class="row">
				<div class="col-md-12">
					<h3 class="w300">
						Bids made for your shopping cart:
					</h3>
					<div class="scrollable-container">
						<div class="row">
							@if($request->bids->count() > 0)
								@foreach($request->bids as $bid)
									<div class="col-md-3">
										<div class="box-light">
											<h3 class="info no-vm">
												<span>Bid by</span>
												<img src="https://randomuser.me/api/portraits/men/20.jpg" alt="..." class="profile-img">
												{{$bid->biddable->name}}
											</h3>
											<hr>
											<h2 class="no-vm w300">£{{$bid->delivery_fee}}</h2>
											<hr>
											<h3 class="info no-vm">
												<span>
													<i class="icon icon-delivery"></i>
													Delivery method
												</span>
													{{$bid->delivery_method}}
											</h3>
											<h3 class="info no-vm">
												<span>
													<i class="fa fa-calendar-o" aria-hidden="true"></i>
													Date
												</span>
												{{$bid->delivery_date}}
											</h3>
											<h3 class="info no-vm">
												<span>
													<i class="icon icon-chat"></i>
													Message
												</span>
												{{$bid->message}}
											</h3>
											@if($request->isOwner(Auth::user()))
											<div class="text-center">
												<hr>
												<form method="POST" action="/bid/{{$bid->id}}/accept">
													{{csrf_field()}}
													<button class="btn btn-success">
														<i class="icon icon-checked"></i>
														Accept bid
													</button>
												</form>

											</div>
											@endif
										</div>
									</div>
								@endforeach
							@else
								<h3 class="w300 c-med-gray text-center">
									:( No bids on your cart yet unfortunately...
									</br></br>
								</h3>
							@endif
						</div>
					</div>
				</div>
			</div>
		@elseif ($request->status == 'close')
			<div class="row">
				<div class="col-md-12">
					<h3 class="w300">
						Accepted bid:
					</h3>
					<div class="scrollable-container">
						<div class="row">

							<div class="col-md-3">
								<div class="box-light">
									<h3 class="info no-vm">
										<span>Bid by</span>
										<img src="https://randomuser.me/api/portraits/men/20.jpg" alt="..." class="profile-img">
										{{$request->acceptedBid()->biddable->first_name}}, {{$request->acceptedBid()->biddable->last_name}}
									</h3>
									<hr>
									<h2 class="no-vm w300">£{{$request->acceptedBid()->delivery_fee}}</h2>
									<hr>
									<h3 class="info no-vm">
										<span>
											<i class="icon icon-delivery"></i>
											Delivery method
										</span>
										{{$request->acceptedBid()->delivery_method}}
									</h3>
									<h3 class="info no-vm">
										<span>
											<i class="fa fa-calendar-o" aria-hidden="true"></i>
											Date
										</span>
										{{$request->acceptedBid()->delivery_date}}
									</h3>
									<h3 class="info no-vm">
										<span>
											<i class="icon icon-chat"></i>
											Message
										</span>
										{{$request->acceptedBid()->message}}
									</h3>
									@if($request->isOwner(Auth::user()))
									<div class="footer green">
										<h4 class="w300 c-black no-vm text-center">
											<i class="icon icon-success"></i>
											Accepted
										</h4>
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
		@if($request->status == 'close')
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<h2 class="w300">
						Chat with your deliverer.
					</h2>
					<p class="s-12em">
						Please use the box below to chat with your deliverer.
					</p>
					<div class="box-light">
						<div class="row">
							<div class="col-md-12 col-xs-12">
								<ul class="chat-window">
									@foreach($conversation->messages as $message)
										@if($message->isAuth())
											@if($message->isNote())
											<li class="announcement">
												<div class="message">
													<i class="pe-7s-info"></i>
													{{$message->message}}
												</div>
												<div class="timestamp">
													{{$message->created_at}}
												</div>
											</li>
											@else
											<li class="right">
												<div class="message">
													{{$message->message}}

												</div>
												<div class="timestamp">
													{{$message->created_at}}
												</div>
											</li>
											@endif
										@elseif(!$message->isNote())
											<li class="left">
												<div class="message">
													{{$message->message}}
												</div>
												<div class="timestamp">
													{{$message->created_at}}
												</div>
											</li>
										@endif
									@endforeach
								</ul>
								<div class="msg-window">
									<form method="POST" action="/conversations/{{$conversation->id}}/sendMessage" style="width: 100%;">
										{{ csrf_field() }}
										<div class="row">
											<div class="col-md-10 col-xs-8">
												<input type="text" value="" name="message" required>
											</div>
											<div class="col-md-2 col-xs-4">
												<button type="submit" class="btn btn-primary">
													<i class="icon icon-send"></i>
													Send
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12">
					<h2 class="w300">
						Delivery process
					</h2>
					<p class="s-12em">
						Please let us know how your delivery went by confirming the delivery of your products, as well offering any feedback.
					</p>
					<a href="" class="btn btn-lg btn-success">
						<i class="icon icon-success"></i>
						Confirm receipt of products
					</a>
					<br><br>
				</div>

			</div>
	</div>
	@endif

</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script src="/js/app.js"></script>
@endsection
