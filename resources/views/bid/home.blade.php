@extends('layouts.boilerplate')

@section('head')
	<title>My bids - Shopguin</title>
	@include('partials.head')
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')

	<div class="container first">
		<div class="row intro">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>
					<i class="icon icon-hammer"></i> My bids
				</h1>
				<p class="s-12em">
					Listed below are all the bids you've made on delivery requests. If somebody accepts one of your bids, the associated delivery will become available in the 'My deliveries' section, and the bid will disappear from this page.
				</p>
			</div>
		</div>
		<div class="row delivery-requests">
		
				<div class="col-xs-12 col-sm-12 col-md-5">
					<div class="panel" >
						<div class="row body">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details">
								<div class="row delivery-details">
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										<h6>
											Pick up from
										</h6>
										<h4 class="">
											<i class="icon icon-place"></i>
											London, GB  
										</h4>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										<h6>
											Business
										</h6>
										<h4 class="">
											Apple Ltd.					
										</h4>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
										<h6>
											Deliver to
										</h6>
										<h4>
											<i class="icon icon-place"></i>
											Manchester, GB
										</h4>
									</div>
								</div>
								<div class="row delivery-details">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h6>
											Delivery for
										</h6>
										<h4>
											<img src="https://randomuser.me/api/portraits/men/20.jpg" alt="..." class="request-profile-img">
											Johnanthan Doughwater
										</h4>
									</div>
								</div>
								<div class="row cart-images">
										<div class="col-md-3 col-xs-4 img">
											<img class="img-responsive" src="/storage/">
										</div>
								</div>
								<h4 class="summary">
									<span>
										Items
									</span>
									5
									<span>
										Cart value
									</span>
									€ 65.00
								</h4>
								<div class="row delivery-details">
									<div class="col-lg-2">
										<h4 class="w300 c-primary">
											YOUR BID
										</h4>
									</div>
									<div class="col-lg-10">
										<h3 class="w300 c-med-gray">
											<i class="icon icon-cash"></i> &nbsp;
											€ 65.00
										</h3>
									</div>
								</div>
								<div class="earn row">
									<div class="text-center  col-xs-12 ">
										<a href="/client/bids/2" class="btn btn-delivery"><i class="icon icon-hammer" style="font-size:1.5em;"></i> &nbsp; View my bid &nbsp;</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
@endsection
