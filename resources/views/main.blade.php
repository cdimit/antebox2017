@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>Shopguin</title>
	<link rel="stylesheet" href="/css/assets/unslider.css">
	<link rel="stylesheet" href="/css/assets/unslider-dots.css">
	<link rel="stylesheet" type="text/css" href="/css/assets/slick.css"/>
	<link rel="stylesheet" href="/css/assets/slick-theme.css">

@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/599857bb1b1bed47ceb0583b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

@endsection

@section('navbar')
	@include('partials.navbar')
	{{-- @include('partials.quick-action') --}}
@endsection

@section('body')
	{{-- Top slider --}}
	<div class="first mt-20">
		<div class="top-slider">
			<div class="slider">
				<img class="img-responsive" src="/img/frontpage/slider/frontpage-slider-1.png" alt="">
				<div class="slider-content">
					<h1 class="no-vm">
						Shopguin
					</h1>
					<br>
					<h2 class="no-vm">
						Cyprus's first online shopping <br> marketplace for all your needs, <br> has now launched!
						<br>
						<a href="/products" class="btn btn-primary btn-lg btn-sq">
							Start shopping now!
						</a>
					</h2>
						
				</div>
				
			</div>
			<div class="slider">
				<img class="img-responsive" src="/img/frontpage/slider/frontpage-slider-2.png" alt="">
				<div class="slider-content">
					<h1 class="no-vm">
						Shop when you want
					</h1>
					<br>
					<h2 class="no-vm">
						Don't be put off by shop opening times! <br> With Shopguin, you can do your <br> shopping at your own pleasure!
						<br>
						<a href="/products" class="btn btn-primary btn-lg btn-sq">
							Find something to buy!
						</a>
					</h2>
						
				</div>
				
			</div>
		</div>
	</div>
		
	</div>
	{{-- Key points --}}
	<section>
		<div class="container">
			<div class="row featured-blocks">
				<div class="col-md-4 block">
					<div class="row">
						<div class="col-xs-3 col-md-3">
							<h1>
								<i class="icon icon-gift"></i>
								
							</h1>
						</div>
						<div class="col-xs-9 col-md-9">
							<h2>
								Add products to your cart
							</h2>
							<h4>
								A shopping cart is automatically created for every seller
								
							</h4>
						</div>
					</div>
				</div>
				<div class="col-md-4 block">
					<div class="row">
						<div class="col-xs-3 col-md-3">
							<h1>
								<i class="icon icon-shopping-cart"></i>
								
							</h1>
						</div>
						<div class="col-xs-9 col-md-9">
							<h2>
								Check out your cart
							</h2>
							<h4>
								Choose a preferred delivery time and location for delivery
								
							</h4>
						</div>
					</div>
				</div>
				<div class="col-md-4 block">
					<div class="row">
						<div class="col-xs-3 col-sm-4 col-md-3">
							<h1>
								<i class="icon icon-delivery"></i>
								
							</h1>
						</div>
						<div class="col-xs-9 col-sm-8 col-md-9">
							<h2>
								Accept a delivery bid
							</h2>
							<h4>
								Choose the best delivery bid that meets your preferences
								
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	{{-- Featured ads --}}
	<section class="featured-ads">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12 ">
					<div class="row">
						<div class="col-md-12">
							<div class="ad" style="margin-bottom: 25px;">
								<img class="img-responsive" src="/img/frontpage/featured-ad/featured-ad-1.png" alt="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 ">
							<div class="row">
								<div class="col-md-6">
									<div class="ad">
										<img class="img-responsive" src="/img/frontpage/featured-ad/featured-ad-2.jpg" alt="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="ad">
										<img class="img-responsive" src="/img/frontpage/featured-ad/featured-ad-3.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="ad">
						<img class="img-responsive" src="/img/frontpage/featured-ad/featured-ad-lg.png" alt="">
					</div>
				</div>
			</div>
			
		</div>
	</section>
		
	<section class="section-img text-center">
		<div class="section-img-overlay">
			<h2 class="">
				Shopping works differently on Shopguin!
			</h2>
			<br>
			<h3>
				We offer both conventional and crowd delivery!	
			</h3>
			<br>
			{{-- <a class="btn btn-white" href="/shopping">Learn about shopping with Shopguin</a> --}}
		</div>
	</section>


	{{-- Featured products --}}
	<section class="grey">
		<div class="container ">
			<div class="row">
				<div class="col-md-12 fpage">
					<h2 class="w500 text-center">
						Featured products
					</h2>
					<p class="s-13em c-gray text-center">
						Have a browse through some of the top products selling on Shopguin right now, from various sellers around the world.
					</p>
				</div>
				<div class="col-md-12">
					<div id="featured-products" class="featured owl-carousel owl-theme ">
						@foreach($featured_products as $product)
						
								<div class="item product-grid">
									<div class="col-xs-12 ">

										<div class="box-light hover-fx">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-4 product-img">
													<a href="/products/{{$product->id}}">
														<img class="img-responsive" src="/storage/{{$product->picture}}">
														
													</a>
												</div>
												<div class="col-md-12 col-sm-12 col-xs-8 product-details" ">
													<a href="/products/{{$product->id}}">
														<span class="category">
															{{$product->category->name}}
														</span>
														<h3 class="product-title">
															{{str_limit($product->name, $limit = 15, $end = '...')}}
															<span class="flag-icon flag-icon-{{$product->business->country}}"></span> 
														</h3>
														<h6 class="company">
															{{$product->business->name}}
														</h6>
														<h2 class="price">
															@if($product->discount)
																@if($product->discount->off_method=="percent")
																	<strike>
																		€ {{number_format($product->price, 2)}}		
																	</strike>
																	&nbsp;
																	€ {{number_format($product->discount->new_price, 2)}}	
																	&nbsp;
																	<div class="discount">
																		{{$product->discount->value}} % OFF
																	</div> 
																	&nbsp;
																@endif
																@if($product->discount->off_method=="amount")
																	<strike>
																		€ {{number_format($product->price, 2)}}		
																	</strike>
																	&nbsp;
																	€ {{number_format($product->discount->new_price, 2)}}	
																	&nbsp;
																	<div class="discount">
																		€ {{$product->discount->value}} OFF
																	</div> 
																	&nbsp;
																@endif
															@else
																€ {{number_format($product->price, 2)}}
															@endif
														</h2>
														<!-- <div class="product-info rating" style="padding-left: 0px;">
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star-o no" aria-hidden="true"></i>
															&nbsp;
															<a href="">
																(13 reviews)					
															</a>
														</div> -->
													</a>
													<hr>
												</div>

												<div class="col-md-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-xs-8 col-xs-offset-4 product-buy">
													<div class="text-center">
														<a href="/products/{{$product->id}}/addItemToCart" class="btn btn-primary">
															<i class="icon icon-shopping-basket hidden-xs"></i> &nbsp;
															Add to cart
														</a>
														<a href="/wishlist/{{$product->id}}/add" class="btn btn-red">
															<i class="icon icon-heart"></i>
														</a>
													</div>
												</div>
											</div>
										</div>	
									</div>
									
								</div>
							
					
						@endforeach
							
					</div>
				</div>
				<hr>
			</div>
		</div>
	</section>
	
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="//stephband.info/jquery.event.move/js/jquery.event.move.js"></script>
	<script src="//cdn.jsdelivr.net/velocity/1.2.3/velocity.min.js"></script>
	<script src="//stephband.info/jquery.event.swipe/js/jquery.event.swipe.js"></script>
	<script src="/js/assets/parallax.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			// $('.parallax').parallax();

		

			$('.top-slider').slick({
				autoplay: true,
				adaptiveHeight: true,
				appendDots: $('.arrows'),
				autoplaySpeed: 2500
			});

					

			$('#featured-products').owlCarousel({
				loop:true,
				center: false,
				nav:false,
				// margin: 25,
				autoPlay: true,
				autoWidth: true,
				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:true,
						margin: 0,
						stagePadding: 0
					},
					600:{
						items:2,
						nav:false
					},
					1000:{
						items:5,
						nav:true,
						loop:false
					}
				}
			});

		});
	</script>
@endsection
