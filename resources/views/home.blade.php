@extends('layouts.app')

@section('content')
<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Dashboard</div>
					 <div class="panel-body">
						  You are logged in!
						  <br><br>
						  <strong>Businesses</strong>
						  <br>
						  @foreach($business as $b)
							 <a href="/{{$b->slug}}">{{$b->name}}</a><br>
						  @endforeach

					 </div>
				</div>
		  </div>
	 </div>
</div>

<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Admin (Permission only for admins later)</div>
					 <div class="panel-body">
						<a href="/admin/categories/" class="btn btn-default">Categories</a><br>
					 </div>
				</div>
		  </div>
	 </div>
</div>
@endsection
