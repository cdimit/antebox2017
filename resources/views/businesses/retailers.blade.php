@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
	@include('partials.quick-action')
@endsection


@section('body')
	<div class="container first">
		<div class="row intro">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>
					<i class="icon icon-store"></i> Retailers
				</h1>
				<p class="s-12em">
					Have a look at all the retailers selling at Shopguin.
				</p>
			</div>
		</div>
		<div class="row">
			@foreach($businesses as $business)
				<div class="col-md-4 col-lg-3 col-sm-6 col-xs-12">
					<div class="box-light" style="height: 150px;">
						<a href="/{{$business->slug}}">
							<h3 class="w300 no-vm">
								{{$business->name}}
								<span  class="flag-icon flag-icon-{{$business->country}}" style="font-size:1.2em;"></span>
								<input type="hidden" id="country_name" name="country_name" value="{{$business->country}}">

							</h3>
							<h4 class="w300 c-gray">
								{{$business->address1}}, {{$business->address2}}
							</h4>
						</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
