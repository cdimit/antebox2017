@include('partials.head')
<link rel="stylesheet" type="text/css" href="/css/biz_tpl1.css">
</head>

<button type="button" class="quick-action-btn visible-xs">
	<i class="pe-7s-keypad pe-3x pe-va"></i>
</button>

<body>
@include('partials.navbar-slim')
@include('biz_templates.header')
<div class="template">
	<div class="container" style="margin-bottom: 50px;">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
				<h1 class="intro">True Italian Food</h1>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. 
				</p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				</p>
				
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<img src="https://source.unsplash.com/y3aP9oo9Pjc/900x900" class="img-responsive">
			</div>
			
		</div>
	</div>
	<div class="featured">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h1 class="text-center">
						Featured products
					</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
					
					<div  id="featured-products " class="featured-products owl-carousel owl-theme">
						<div class="item">
							<img src="https://source.unsplash.com/jpkfc5_d-DI/900x900">
							<h3>
								Antipasta
							</h3>
							<p>
								Lorem ipsum dolor sit amet consectetur
							</p>
							<h4>
								£60.00
							</h4>
							<a href="" class="btn btn-primary">Add to cart</a>
						</div>
						<div class="item">
							<img src="https://source.unsplash.com/KK1hlAI2lgE/900x900">
							<h3>
								Antipasta
							</h3>
							<p>
								Lorem ipsum dolor sit amet consectetur
							</p>
							<h4>
								£60.00
							</h4>
							<a href="" class="btn btn-primary">Add to cart</a>
						</div>
						<div class="item">
							<img src="https://source.unsplash.com/sHG1dCUXgPY/900x900">
							<h3>
								Antipasta
							</h3>
							<p>
								Lorem ipsum dolor sit amet consectetur
							</p>
							<h4>
								£60.00
							</h4>
							<a href="" class="btn btn-primary">Add to cart</a>
						</div>
						<div class="item">
							<img src="https://source.unsplash.com/gUBJ9vSlky0/900x900">
							<h3>
								Antipasta
							</h3>
							<p>
								Lorem ipsum dolor sit amet consectetur
							</p>
							<h4>
								£60.00
							</h4>
							<a href="" class="btn btn-primary">Add to cart</a>
						</div>
						<div class="item">
							<img src="https://source.unsplash.com/TO69trRWlrI/900x900">
							<h3>
								Antipasta
							</h3>
							<p>
								Lorem ipsum dolor sit amet consectetur
							</p>
							<h4>
								£60.00
							</h4>
							<a href="" class="btn btn-primary">Add to cart</a>
						</div>
						<div class="item">
							<img src="https://source.unsplash.com/gUBJ9vSlky0/900x900">
							<h3>
								Antipasta
							</h3>
							<p>
								Lorem ipsum dolor sit amet consectetur
							</p>
							<h4>
								£60.00
							</h4>
							<a href="" class="btn btn-primary">Add to cart</a>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>



</body>
@include('partials.footer')
@include('partials.scripts')
<script type="text/javascript">
	$('.main-slider').owlCarousel({
		loop:true,
		nav:true,
		items:1,
	});
	$('.featured-products').owlCarousel({
		loop:true,
		nav:true,
		margin: 25,
		autoPlay: true,
		
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:5,
				nav:true,
				stagePadding: 50,
				loop:false
			}
		}
	});
</script>

</html>