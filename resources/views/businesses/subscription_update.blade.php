@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">

          <h1>Your Subscription Plan: <span class="label label-warning">{{auth()->user()->profile->subscribePlan->category}}</span>
						@if($end = auth()->user()->profile->trialEndAt())
							<span class="label label-info">Trial Ends in: {{$end->diffForHumans()}}</span>
						@endif
					</h1>
					<br>
					<hr>

					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Update Plan</h2>
				  		</div>
					</div>

					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					@if(auth()->user()->profile->isActive())
						<div class="row">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
                    <strong>Want to Modify Your Subscription?</strong>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
                    <form method="POST" action="/business/subscription/change">
                      {{ method_field('PATCH') }}
                      {{ csrf_field() }}

                      <select name="plan" class="form-control">
                        @foreach($plans as $plan)
                        <option value="{{$plan->id}}">{{$plan->category}} (€{{$plan->humanprice}})</option>
                        @endforeach
                      </select>

                      <button type='submit' class="btn btn-primary">Update</button>
                    </form>
									</div>
								</div>
							</div>
						</div>

					@endif




				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>



	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

	<script src="/js/app.js"></script>
</body>
</html>
