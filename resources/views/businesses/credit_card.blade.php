@include('dashboard.head')
<script>
 window.Stripe = <?php echo json_encode([
	 'csrfToken' => csrf_token(),
	 'stripeKey' => config('services.stripe.key'),
	 'user'			=> auth()->user()
 ]); ?>
</script>


<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Update Your Credit Card</h2>
				  		</div>
					</div>

					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

						<div class="row">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      Want to update the credit card that we have on file? Provide the new details here. And don't worry; your card information will never directly touch our servers.
                    </p>
                    <p>
                      <strong>{{$source->brand}}: ••••{{$source->last4}} ({{$source->exp_month}} / {{$source->exp_year}})</strong>
                    </p>
										<div id="app">
											<updatecard-form></updatecard-form>

										</div>
									</div>
								</div>
							</div>
						</div>





				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>



	@include('dashboard.scripts')
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-payments').DataTable({});
		});
	</script>
	<script src="/js/app.js"></script>
</body>
</html>
