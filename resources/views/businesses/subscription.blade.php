@include('dashboard.head')
<script>
 window.Stripe = <?php echo json_encode([
	 'csrfToken' => csrf_token(),
	 'stripeKey' => config('services.stripe.key'),
	 'user'			=> auth()->user()
 ]); ?>
</script>


<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Subscription</h2>
				  		</div>
					</div>

					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					@if(!auth()->user()->profile->isActive())
						<div class="row">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										Subscription plans
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div id="app">
											<subscribe-form :plans="{{ $plans }}"></subscribe-form>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif

					@if(auth()->user()->profile->isOnGracePeriod())

						<div class="row">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										Grace period
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<p>
											Your subscription will fully expire on {{auth()->user()->profile->subscription_end_at->format('d-m-Y')}} ({{auth()->user()->profile->subscription_end_at->diffForHumans()}})
										</p>

										<form method="POST" action="/stripe/subscribe">
											{{ method_field('PATCH') }}
											{{ csrf_field() }}

											<div class="checkbox">
												<label>
													<input type="checkbox" name="resume">
													Resume My {{auth()->user()->profile->subscribePlan->packet }} {{auth()->user()->profile->subscribePlan->category}} Subscription
												</label>
											</div>

											<button type='submit' class="btn btn-primary">Update</button>
										</form>
									</div>
								</div>
							</div>
						</div>

					@endif

					@if (count(auth()->user()->payments))
						<div class="row">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										Payments
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div class="table_scroll">
											<table id="all-payments" class="table table-striped table-bordered no-wrap dashboard-table" >
												<thead>
													<tr>
														<th>Payment date</th>
														<th>Payment amount</th>
													</tr>
												</thead>
												<tbody>
													@foreach (auth()->user()->payments as $payment)
													<tr>
														<td>
															{{ $payment->created_at->diffForHumans() }}
														</td>
														<td>
															€ {{ $payment->formatAmount() }}
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif

					@if(auth()->user()->profile->isSubscribed())
						<div class="row">
							<div class="col-md-8 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										Cancel subscription
										<div class="clearfix"></div>
									</div>

									<div class="x_content">
										<form method="POST" action="/stripe/subscribe">
											{{ csrf_field() }}
											{{ method_field('DELETE') }}

											<button class="btn btn-danger">
												Cancel my subscription
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					@endif


				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>



	@include('dashboard.scripts')
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-payments').DataTable({});
		});
	</script>
	<script src="/js/app.js"></script>
</body>
</html>
