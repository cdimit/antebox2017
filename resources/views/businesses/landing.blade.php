@extends('layouts.boilerplate')

{{-- Head tags --}}
@section('head')
	@include('partials.head')
	{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css"> --}}

@endsection

{{-- OpenGraph tags --}}
@section('og-scripts')
@endsection

{{-- Head scripts --}}
@section('head-scripts')
@endsection

{{-- Navbar --}}
@section('navbar')
	@include('partials.navbar-slim')
@endsection

{{-- Body --}}
@section('body')
<div class="business">
	<section class="welcome">
		<div class="container text-center">
			<h1 class="page-title ">
				Take your business online with Shopguin.
			</h1>
			<h4 class="page-subtitle text-center">
				Reach millions of customers globally with your own customised online store.
			</h4>
			<a href="#start" class="btn btn-inverted">
				Get started now
			</a>
		</div>

	</section>
	<section>
		<div class="container">
			<h1 id="start" class="text-center section-title">
				What is Shopguin?
			</h1>
			<p >
				We want to make online selling easy and accessible to everyone. That is why are easy to use, top of the art technology, offers you a complete and affordable solution so that you can start selling online within minutes. Fill in the forms, add pictures and prices and bam you are all set up! Yes that easy.				
			</p>
		</div>
	</section>
	<section class="parallax-container" id="mobile-usage">
		<div class="parallax">
			<img src="/images/mobile-usage-sm.jpg"  alt="">
		</div>
	</section>
	<section>
		<div class="container">
			<h1 class="text-center section-title">
				Benefits of Shopguin
			</h1>
			<div class="row">
				<div class="col-md-4 text-center">
					<i class="icon icon-worldwide"></i>
					<h3>
						Sell to a global market
					</h3>
					<p>
						With 1.6 billion online buyers you can expand your business and reach the growth levels you always wanted. Domestically or internationally, you choose!
					</p>
				</div>
				<div class="col-md-4 text-center">
					<i class="icon icon-customer"></i>
					<h3>
						Online presence
					</h3>
					<p>
						81% of buyers conduct research online before buying products in stores. If your products are there, they wont find them! Don't worry, we got your back.
					</p>
				</div>
				<div class="col-md-4 text-center">
					<i class="icon icon-stats"></i>
					<h3>
						Increase your traffic and sales
					</h3>
					<p>
						Play in two fields, online and offline, attract more leads and more sales. The game has now changed.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="parallax-container" id="phone-usage">
		<div class="parallax">
			<img src="/images/header-phone-sm.jpg"   alt="">
		</div>
	</section>
	<section>
		<div class="container">
			<h1 class="text-center section-title">
				Why us?
			</h1>
			<p>
				We offer you the latest technology, advanced mobile friendly design and most importantly everything is already pre-built for you. No need to install anything else! Just fill in the data, and you have the most advanced technology for your business.
			</p>
			<div class="row s-11em">
				<div class="col-md-4">
					<ul>
						<li>
							<div class="row">
								<div class="col-xs-2">
									<i class="icon icon-price-tag"></i>
								</div>
								<div class="col-xs-10">
									Most economical solution
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="col-xs-2">
									<i class="icon icon-cash"></i>
								</div>
								<div class="col-xs-10">
									Built in payment processing, review and messaging system
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul>
						<li>
							<div class="row">
								<div class="col-xs-2">
									<i class="icon icon-online-shop"></i>
								</div>
								<div class="col-xs-10">
									Complete solution with hosting included
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="col-xs-2">
									<i class="icon icon-chat"></i>
								</div>
								<div class="col-xs-10">
									Code-free and easy to use
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-md-4">
					<ul>
						<li>
							<div class="row">
								<div class="col-xs-2">
									<i class="icon icon-support"></i>
								</div>
								<div class="col-xs-10">
									Dedicated technical support team
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="col-xs-2">
									<i class="icon icon-high-heels"></i>
								</div>
								<div class="col-xs-10">
									Stress free sales
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<h1 class="text-center section-title">
				Pricing
			</h1>
			<p>
				Everything is included in our pricing memberships and we offer the lowest prices in the market as we negotiate better deals for hosting for all of our business users together. You are not alone in this. Choose the plan that fits your business!
			</p>
			<div class="row">
				<div class="col-md-12">
					<div class="pricing-table-container text-center">
						<div class="pricing-table">
							<div class="header">
								Basic
								<span>
									All the basics for starting a new business
								</span>
							</div>
							<div class="footer">
								<h2 class="w500 no-vm">
									€9.90
									<span>
										/mo
									</span>
								</h2>
							</div>
							<div class="body">
								<ul>
									<li>
										Up to 10 products
									</li>
									<li>
										<span>
											Transaction fee:
										</span>
										4.00%
									</li>
								</ul>
							</div>
							
						</div>
						<div class="pricing-table">
							<div class="header">
								Professional
								<span>
									Everything you need for a growing business
								</span>
							</div>
							<div class="footer">
								<h2 class="w500 no-vm">
									€19.90
									<span>
										/mo
									</span>
								</h2>
							</div>
							<div class="body">
								<ul>
									<li>
										Up to 30 products
									</li>
									<li>
										<span>
											Transaction fee:
										</span>
										4.00%
									</li>
								</ul>
							</div>
							
						</div>

						<div class="pricing-table">
							<div class="header">
								Advanced
								<span>
									Advanced features for scaling your business
								</span>
							</div>
							<div class="footer">
								<h2 class="w500 no-vm">
									€39.90
									<span>
										/mo
									</span>
								</h2>
							</div>
							<div class="body">
								<ul>
									<li>
										Up to 100 products
									</li>
									<li>
										<span>
											Transaction fee:
										</span>
										4.00%
									</li>
								</ul>
							</div>
							
						</div>

						
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="sign-up text-center sticky">
		<div class="container">
			<h1>
				Ready to get started? Try your first month with us for free!
			</h1>
			<a class="btn btn-white" href="/business/register">Register your business now</a>
		</div>
	</section>
	
</div>
@endsection

{{-- Footer --}}
@section('footer')
	@include('partials.footer')
@endsection

{{-- Footer scripts --}}
@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/parallax.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
      		$('.parallax').parallax();
      		$(window).scroll(function(){
      			var currentScroll = $(window).scrollTop();
      			console.log(currentScroll);
      		});
    	});
	</script>
@endsection


