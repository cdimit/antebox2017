@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
	<link rel="stylesheet" href="/css/assets/jquery.timepicker.css">
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
	<div class="container first">
		<h1 class="w300">
			Request from {{$request->client->first_name}}
		</h1>
		<p class="s-13em">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus maiores nostrum quibusdam. Molestiae ex, aut animi optio non quo perferendis blanditiis doloremque, rerum officiis commodi ducimus, accusamus dolor tempore mollitia?
		</p>
	</div>
	<div class="container">
		<div class="row">
			{{-- Current product for shopping cart --}}
			<div class="row delivery-requests">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<div class="panel">
						<div class="row body">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 product-img">
								<div class="contain">
									<img class="img-responsive product-img" src="https://placeimg.com/640/640/tech">
								</div>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 details">
								<div class="request-profile">
									<img src="https://randomuser.me/api/portraits/men/20.jpg" alt="..." class="request-profile-img">
									{{$request->client->first_name}} requested
								</div>
								@foreach($request->items as $item)
								<h3 class="product-title">
									{{$item->product->name}}
									<span class="flag-icon flag-icon-cy"></span>
								</h3>
								<h4 class="value">
									<span>
										Value
									</span>
									€ {{$item->price}}
								</h4>
								<h4 class="value">
									<span>
										Total
									</span>
									€ {{$request->total_price}}
								</h4>
								@endforeach
								<div class="row delivery-details">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<h6>
											Pick up from
										</h6>
										<h4 class="">
											<i class="icon icon-place"></i>  {{$request->business->city}}, {{$request->business->country}}
										</h4>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<h6>
											Deliver to
										</h6>
										<h4 class="">
											<i class="icon icon-place"></i>
											{{$request->meetingPoint->city}}, {{$request->meetingPoint->country}}
										</h4>
									</div>
								</div>
								<div class="earn row">
									<div class="text-center col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<a href="" class="btn btn-delivery"><i class="icon icon-details"></i> &nbsp; See details</a>
									</div>
									<div class="text-center  col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<a href="" class="btn btn-delivery"><i class="icon icon-delivery" style="font-size:1.5em;"></i> &nbsp; Make an offer</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			{{-- Shopping cart overview --}}
			<div class="col-lg-4">

			{{-- --}}

			</div>
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/bootstrap-datepicker.js"></script>
	<script src="/js/assets/jquery.timepicker.min.js"></script>
	<script type="text/javascript" src="/js/products/products.js"></script>
	<script type="text/javascript" src="/js/products/request.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&libraries=places&callback=initAutocomplete" async defer></script>
@endsection
