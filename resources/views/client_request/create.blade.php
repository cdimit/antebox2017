@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
	<link rel="stylesheet" href="/css/assets/jquery.timepicker.css">
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
	@include('partials.quick-action')
@endsection

@section('body')
	<div class="container first">
		<h1 class="w300">
			Shopping cart for {{$shopping_cart->business->name}}
		</h1>
		<p class="s-13em">
			Use this page to choose a delivery address for your shopping cart, and also leave a message for your deliverer if you like. You will then receive bids on your shopping cart, for you to choose from. Once you've found your ideal bid, you can accept it and then pay for it.
		</p>
	</div>
	<div class="container">
		<div class="row">
			{{-- Current product for shopping cart --}}
			<div class="col-md-8 col-md-offset-2 box-light">
				{{-- Timeline steps --}}
				<div style="margin-bottom: 75px; margin-top: 15px;">
					<ul class="timeline-steps">
						<li class="active">
							<i class="icon icon-price-tag"></i>
							<span>Products</span>
						</li>
						<li>
							<i class="icon icon-placeholder"></i>
							<span>Location</span>
						</li>
						<li>
							<i class="icon icon-route"></i>
							<span>Finalise</span>
						</li>

						<div class="progress-bar" style="width: 0%;"></div>
						<div class="bar"></div>
					</ul>
				</div>
				<form role="form" id="clientRequest" method="POST" action="/client/request/{{$shopping_cart->id}}/create" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-multistep">
						{{-- Products --}}
						<div class="step" id="step1">
							<h3 class="w500">
								Shopping cart overview
							</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="box-light">
										<h4 class="w500" style="margin-top: 0;">
											{{$shopping_cart->business->name}} ({{$shopping_cart->items->count()}} items)
										</h4>
										<hr>
										<ul class="products-as-list">
											<div class="row">
												@foreach($shopping_cart->items as $item)
												<div class="col-md-12">
													<li>
														<div class="row">
															<div class="col-md-2 col-sm-3 col-xs-3">
																<img class="img-responsive" src="/storage/{{$item->product->picture}}">
															</div>
															<div class="col-md-10 col-sm-9 col-xs-9">
																<h4>
																	<div class="row">
																		<div class="col-md-9 col-xs-12">
																			{{$item->product->name}}
																		</div>
																		<div class="col-md-3 text-right hidden-xs">
																			<a href="/item/{{$item->id}}/remove" class="delete">
																				<i class="icon icon-rubbish-bin"></i> Remove
																			</a>
																		</div>
																	</div>
																</h4>
																<h6>
																	<b>Qty:</b>
																	{{$item->qty}}
																	{{-- <label for="qty"></label>
																	<input  class="input-sm" type="number" min="1" step="1" value="{{$item->qty}}" name="qty" style="display: inline-block; width: 75px;"> --}}
																</h6>
																<h6>
																	€ {{$item->price}}
																	<a href="/item/{{$item->id}}/remove" class="btn btn-xs btn-red hidden-md hidden-lg" style="position: absolute; right: 5px;">
																		<i class="icon icon-rubbish-bin"></i> Remove
																	</a>
																</h6>
															</div>
														</div>
													</li>
												</div>
												@endforeach
											</div>

										</ul>
										<h3 class="info no-vm">
											<span>
												<i class="icon icon-euro"></i>
												Total
											</span>
											€ {{$shopping_cart->price}}
										</h3>
									</div>

								</div>

							</div>

							<button type="button" id="to-location" class="btn btn-primary" data-dismiss="alert">
								Next
								<i class="icon icon-right-chevron"></i>
							</button>
							</br></br>
						</div>
						{{-- Location --}}
						<div class="step" id="step2" style="display: none;">
							<h3 class="w500">
								Delivery location
							</h3>
							<p class="s-12em">
								Please either choose from one of your meeting points saved in your profile, or enter a new delivery location.
							</p>
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<h4 class="w500 accordion" data-toggle="collapse" data-target="#newaddress">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
										Choose a new address
									</h4>
									<div id="newaddress" class="collapse">
										<label for="address" class="">Search for address</label>
										<input type="text" name="address" class="" id="address" placeholder="" onFocus="geolocate()">
										<hr>

										<div class="row">
											<div class="col-md-6">
												<label for="street_number" class="">Address line 1</label>
												<input name="address1" type="text" class="" id="street_number" placeholder="" value="Enter address here">
											</div>
											<div class="col-md-6">
												<label for="route" class="">Address line 2</label>
												<input name="address2" type="text" class="" id="route" placeholder="">
											</div>
											<div class="col-md-6">
												<label for="locality" class="">City</label>
												<input name="city" type="text" class="" id="locality" placeholder="">
											</div>
											<div class="col-md-6">
												<label for="administrative_area_level_1" class="">State</label>
												<input name="state" type="text" class="" id="administrative_area_level_1" placeholder="">
											</div>
											<div class="col-md-6">
												<label for="country" class=" ">Country</label>
												<input name="country" type="text" class="" id="country" placeholder="">
											</div>
											<div class="col-md-6">
												<label for="postal_code" class="">Post code</label>
												<input name="post_code" type="text" class="" id="postal_code" placeholder="">
											</div>
										</div>
										</br>
									</div>
									<h4 class="w500 accordion" data-toggle="collapse" data-target="#meetingpoints">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
										Choose a saved meeting point
									</h4>
									<div id="meetingpoints" class="collapse">
										 <div class="form-group{{ $errors->has('meeting_point') ? ' has-error' : '' }}">
										 	<div class="row">
											 	<div class="col-md-6">
													<label for="meeting_point" class="">Meeting Point</label>
													<select name="meeting_point" class="">
														<option value="">Choose Meeting Point</option>
														@foreach(auth()->user()->profile->meetingPoints as $mp)
															<option value="{{$mp->id}}">{{$mp->address1}}</option>
														@endforeach
													</select>
												</div>

										 	</div>
										 </div>
									</div>
								</div>
							</div>
							<hr>
							<button type="button" id="back-to-products" class="btn btn-primary" data-dismiss="alert">
								<i class="icon icon-left-chevron"></i>
								Previous
							</button>
							<button type="button" id="to-final" class="btn btn-primary" data-dismiss="alert">
								Next
								<i class="icon icon-right-chevron"></i>
							</button>
							</br></br>
						</div>

						<div class="step" id="step3" style="display: none;">
							<h3 class="w500">
								Ready to get your products!
							</h3>
							<p class="s-12em">
								Your shopping cart is now ready to be finalised. You can see an overview of what's in your cart below, as well as leave a message regarding your delivery. After finalising your cart, you will either be offered a delivery by {{$shopping_cart->business->name}}, or a delivery via our crowd delivery platform.
							</p>
							<div class="row">
								<div class="col-md-6">
									<div class="box-light">
										<h4 class="w500 no-vm">
											Shopping cart overview
										</h4>
										<ul class="products-as-list">
											<hr>
											<div class="row">
												@foreach($shopping_cart->items as $item)
												<div class="col-md-12">
													<li>
														<div class="row">
															<div class="col-md-2 col-sm-3 col-xs-3">
																<img class="img-responsive" src="/storage/{{$item->product->picture}}">
															</div>
															<div class="col-md-10 col-sm-9 col-xs-9">
																<h4>
																	{{$item->product->name}}

																</h4>
																<h6>
																	<b>Qty:</b> {{$item->qty}} | € {{$item->price}}
																</h6>
															</div>
														</div>
													</li>
												</div>
												@endforeach
											</div>
										</ul>
										<h3 class="info no-vm">
											<span>
												<i class="icon icon-euro"></i>
												Total
											</span>
											€ {{$shopping_cart->price}}
										</h3>
									</div>
								</div>
								<div class="col-md-6">
									<p class="s-12em">
										Feel free to leave a message regarding your delivery below:
									</p>
									<textarea name="message" id="" cols="30" rows="4"></textarea>
								</div>
								<br>
							</div>
							<div class="text-center">
								
								<button type="submit" class="btn  btn-primary s-14em">
									<i class="icon icon-success"></i>
									Finalise shopping cart
								</button>

							</div>
							<hr>
							<button type="button" id="back-to-location" class="btn btn-primary" data-dismiss="alert">
								<i class="icon icon-left-chevron"></i>
								Previous
							</button>
							</br></br>
						</div>
					</div>
				</form>
			</div>
			{{-- Shopping cart overview --}}
			<div class="col-lg-4">


			</div>
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/bootstrap-datepicker.js"></script>
	<script src="/js/assets/jquery.timepicker.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
	<script type="text/javascript" src="/js/products/products.js"></script>
	<script type="text/javascript" src="/js/products/request.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&libraries=places&callback=initAutocomplete" async defer></script>
@endsection
