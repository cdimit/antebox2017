@extends('layouts.app')

@section('content')
<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Make Request</div>
					 <div class="panel-body">
						 <strong>{{$shopping_cart->business->name}}</strong>
						 <hr>
						 items: {{$shopping_cart->items->count()}}
						 <br>
						 @foreach($shopping_cart->items as $item)
						 	{{$item->product->name}} | qty: {{$item->qty}} | price {{$item->price}}
							<br>
						 @endforeach
						 <br>
						 Total Price: {{$shopping_cart->price}}

						 <form class="form-horizontal" role="form" method="POST" action="/client/request/{{$shopping_cart->id}}/create">
						 {{ csrf_field() }}

						 <div class="form-group{{ $errors->has('meeting_point') ? ' has-error' : '' }}">
							 <label for="meeting_point" class="col-md-4 control-label">Meeting Point</label>
							 <div class="col-md-6">
								 <select name="meeting_point" class="form-control">
								 @foreach(auth()->user()->profile->meetingPoints as $mp)
								 <option value="{{$mp->id}}">{{$mp->address}}</option>
								 @endforeach
								 </select>
							 </div>
						 </div>

						 <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
							 <label for="message" class="col-md-4 control-label">Message</label>
							 <div class="col-md-6">
								 <input id="message" type="text" class="form-control" value="{{ old('message') }}" name="message">

								 @if ($errors->has('message'))
								 <span class="help-block">
								 <strong>{{ $errors->first('message') }}</strong>
								 </span>
								 @endif
							 </div>
						 </div>


						 <div class="form-group">
							 <div class="col-md-8 col-md-offset-4">
								 <button type="submit" class="btn btn-primary">
								 Make Request
								 </button>
							 </div>
						 </div>

					 </form>

					 </div>
				</div>
		  </div>
	 </div>
</div>

@endsection
