{{-- Side navigation --}}
<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<a href="/" class="site_title">

				<span>
					<img src="/img/logo-and-text-white.png" class="img-responsive nav-logo" style="margin: 10px;">
					{{-- Shopguin Dashboard --}}
				</span>
			</a>
		</div>

		<div class="clearfix"></div>

		{{-- Profile information --}}
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="/storage/{{Auth::user()->profile->logo}}" class="img-circle profile_img">
			</div>
	  		<div class="profile_info">
				<span>Welcome,</span>
		 		<h2>{{Auth::user()->profile->name}}</h2>
	  		</div>
	  		<div class="clearfix"></div>
		</div>

		<br />

		{{-- Sidebar menu --}}
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>General</h3>
				<ul class="nav side-menu">
					<li><a href="/{{Auth::user()->profile->slug}}"><i class="fa fa-shopping-bag"></i>Go to my store</a></li>
					<li><a href="/dashboard"><i class="fa fa-home"></i>Home</a></li>
					<li><a><i class="fa fa-user"></i> Account <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="/business/profile/edit">Edit profile</a></li>
							<li><a href="/business/account/payout">Payout methods</a></li>
							<li><a href="/business/account/email">Change email</a></li>
							<li><a href="/business/account/password">Change password</a></li>
						</ul>
					</li>
					<li><a><i class="fa fa-calendar"></i> Subscription <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="/business/subscription/change">My Subscription</a></li>
							<li><a href="/business/card">Credit Card</a></li>
							<li><a href="/business/subscription">Cancel</a></li>
						</ul>
					</li>
					<li><a><i class="fa fa-paint-brush" aria-hidden="true"></i>Theme <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="/business/themes">Choose your theme</a></li>
							<li><a href="/business/themes/customise">Customise theme</a></li>
						</ul>
					</li>
					<li><a><i class="fa fa-gift" ></i> Products <span class="fa fa-chevron-down"></span></a>
				  		<ul class="nav child_menu">
							<li><a href="/business/products">All products</a></li>
							<li><a href="/business/products/create">Add a new product</a></li>
							<li><a href="/business/discount/create">Add a product discount</a></li>
						</ul>
					</li>

					<li><a><i class="fa fa-map-marker" aria-hidden="true"></i>Stores <span class="fa fa-chevron-down"></span></a>
				  		<ul class="nav child_menu">
							<li><a href="/business/stores">All stores</a></li>
							<li><a href="/business/stores/create">Add a new store</a></li>
						</ul>
					</li>
					<li><a><i class="fa fa-shopping-cart" ></i> Shopping carts <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="/business/carts">View all carts</a></li>
						</ul>
					</li>

				</ul>
			</div>
		</div>

		{{-- /menu footer buttons --}}

	</div>
</div>


{{-- Top Navigation --}}
<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="/storage/{{Auth::user()->profile->logo}}" >{{Auth::user()->profile->name}}
						<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="/business/profile/edit"> Profile</a></li>
						<li>
							<a href="{{ route('logout') }}"
									onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
									<i class="fa fa-sign-out pull-right"></i> Log Out</a>
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</li>
				<li role="presentation" class="dropdown">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-envelope-o"></i>
						@if(($numNot = Auth::user()->unreadNotifications->count()) != 0 )
						<span class="badge bg-green">{{$numNot}}</span>
						@endif
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
						@foreach(Auth::user()->unreadNotifications as $notification)
						<li>

							<a href="/notifications/read/{{$notification->id}}">
								<span class="glyphicon glyphicon-off"></span>
								<span>
									<span>{{$notification->type}}</span>
									
								</span>
								<span class="message">
									{{$notification->data['message']}}
									<span class="time">{{$notification->created_at->diffForHumans()}}</span>
								</span>
							</a>
						</li>
						@endforeach
						<li>
							<div class="text-center">
								<a href="/notifications/markasread">
									<strong>Mark as Read</strong>
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</li>
						<li>
							<div class="text-center">
								<a href="/notifications/test">
									<strong>Create Test Notification</strong>
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
	</div>
</div>
