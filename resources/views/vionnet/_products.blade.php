@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<link rel="stylesheet" type="text/css" href="/css/vionnet.css">
	{{-- AngularJS --}}
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.js"></script>
	<script type="text/javascript" src="/js/assets/angular.route.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.3/lodash.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/restangular/1.5.1/restangular.min.js"></script>
	<script type="text/javascript" src="/js/assets/ui-bootstrap-tpls-2.5.0.min.js"/></script>
	<script type="text/javascript" src="/js/assets/angular.utils/pagination/dirPagination.js"></script>
	<script type="text/javascript" src="/js/templates/vionnet/ng-products.js"></script>
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection


@section('body')
	<div class="template" ng-app="anteboxVionnet">
		@include('vionnet.header')
		<div ng-view></div>


	</div>

@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="/js/templates/vionnet/main.js"></script>
	<script>
		var _config= {
			key1: '{{$url}}'
		};
	</script>
@endsection
