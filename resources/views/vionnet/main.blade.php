@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>{{$business['name']}} - Shopguin</title>
	<link rel="stylesheet" type="text/css" href="/css/vionnet.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	{{-- AngularJS --}}
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.js"></script>
	<script type="text/javascript" src="/js/assets/angular.route.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.3/lodash.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/restangular/1.5.1/restangular.min.js"></script>
	<script type="text/javascript" src="/js/assets/ui-bootstrap-tpls-2.5.0.min.js"/></script>
	<script type="text/javascript" src="/js/assets/angular.utils/pagination/dirPagination.js"></script>
	<script type="text/javascript" src="/js/templates/vionnet/app.js"></script>
@endsection

@section('og-scripts')
	<meta property="og:title" content="{{$business['name']}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://www.shopguin.com/{{$business['slug']}}" />
	<meta property="og:image" content="https://www.shopguin.com/storage/{{$business['logo']}}" />
	<meta property="og:description" content="{{$business['motto']}}" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:site" content="@shopguin">
	<meta name="twitter:creator" content="@shopguin">
	<meta name="twitter:title" content="{{$business['name']}} - Shopguin">
	<meta name="twitter:image" content="https://www.shopguin.com/storage/{{$business['logo']}}">
	<meta name="description" content="{{$business['motto']}}">
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim-nofixed')

	@include('partials.quick-action')
@endsection


@section('body')

	<div class="template" ng-app="anteboxVionnet">
		{{-- Navbar --}}
		<div class="header clearfix">
			<div class="header-container">
				<button type="button" class="burger burger--htx"  aria-expanded="false" id="open-nav">
					<span>Toggle menu</span>
				</button>
			</div>

			<div class="header-container">
				<div class="text-left">
					<h1 class="brand-name ">
						{{$business['name']}}
					</h1>
					<div class="sub-title hidden-xs">
						{{str_limit($business['themeFields']['motto'], $limit = 80, $end = '...')}}
					</div>
				</div>
			</div>
			<div class="slide-in-nav closed">
				<ul>
					<li>
						<a onClick="$('.slide-in-nav').toggleClass('closed')" href="#!/">Home</a>
					</li>
					<li>
						<a onClick="$('.slide-in-nav').toggleClass('closed')" href="#!/products">Products</a>
					</li>
					<li>
						<a onClick="$('.slide-in-nav').toggleClass('closed')" href="#!/about">About</a>
					</li>
					<li>
						<a onClick="$('.slide-in-nav').toggleClass('closed')" href="#!/stores">Stores</a>
					</li>
					{{-- <li>
						<div class="accordion"  data-toggle="collapse" data-target="#slidedown" href="">
							<i class="fa fa-angle-right fa-lg"></i> &nbsp;
							Categories
						</div>
						<ul id="slidedown" class="collapse">
							<li>
								<a href="">
									Hi
								</a>
							</li>
							<li>
								<a href="">
									Hi
								</a>
							</li>
						</ul>
					</li> --}}
				</ul>
			</div>
		</div>
		<div ng-view></div>


	</div>
@endsection

@section('footer')
	@include('partials.slim_footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/assets/bootstrap-notify.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
	<script src="/js/templates/vionnet/main.js"></script>
	<script>
		var _config= {
			id: '{{$id}}'
		};
		@if (session('status'))
			$.notify({
					message: '{{ session('status') }}' 
				},{
					type: 'success'
				}
			);
		@endif
	</script>
@endsection
