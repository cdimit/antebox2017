<div class="header clearfix">
	<div class="header-container">
		<button type="button" class="burger burger--htx"  aria-expanded="false" id="open-nav">
			<span>Toggle menu</span>
		</button>
	</div>

	<div class="header-container ">
		<div class="text-left">
			<h1 class="brand-name ">
				Vionnet
			</h1>
			<div class="sub-title">
				Fashion redefined.
			</div>
		</div>
	</div>
	<div class="slide-in-nav closed">
		<ul>
			<li>
				<a href="#!/">Home</a>
			</li>
			<li>
				<a href="#!/products">Products</a>
			</li>
			<li>
				<a href="#!/page">About</a>
			</li>
			<li>
				<div class="accordion"  data-toggle="collapse" data-target="#slidedown" href="">
					<i class="fa fa-angle-right fa-lg"></i> &nbsp;
					Categories
				</div>
				<ul id="slidedown" class="collapse">
					<li>
						<a href="">
							Hi
						</a>
					</li>
					<li>
						<a href="">
							Hi
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>

<div id="main-slider" class="main-slider owl-carousel owl-theme">
	<div class="item">
		<div class="row">
			<div class="col-md-9 col-xs-12">
				<img src="https://source.unsplash.com/U2ymajzuqFk/1600x900">
			</div>
			<div class="col-md-3 col-xs-12">
				<div class="row">
					<div class="col-md-12 col-xs-4">
						<img src="https://source.unsplash.com/dyPw2pc4Rnc/900x500">
					</div>
					<div class="col-md-12 col-xs-4">
						<img src="https://source.unsplash.com/WcXxxQYp_aM/900x500">
					</div>
					<div class="col-md-12 col-xs-4">
						<img src="https://source.unsplash.com/O3hC__qmVCo/900x500">
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="item">
		<img src="https://source.unsplash.com/L0RvwcsiSFw/1600x900">
	</div>
</div>

