<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                    <p style=3D"font-family: Avenir, Helvetica, sans-serif;=
 box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE;=
 font-size: 12px; text-align: center;">If you need any help contact us directly at contact@shopguin.com or call us at 00447761488380
 </p>
                </td>
            </tr>
        </table>
    </td>
</tr>
