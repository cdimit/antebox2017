@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Change Password</div>
                <div class="panel-body">

                  @if (session('error_password'))
                    <div class="alert alert-danger">
                      {{ session('error_password') }}
                    </div>
                  @endif

                  @if (session('status_password'))
                    <div class="alert alert-success">
                      <strong>{{ session('status_password') }}</strong>
                    </div>
                  @endif

                    <form class="form-horizontal" role="form" method="POST" action="/account/settings/password/update" >
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                            <label for="old_password" class="col-md-4 control-label">Old Password</label>

                            <div class="col-md-6">
                                <input id="old_password" type="password" class="form-control" name="old_password" required>

                                @if ($errors->has('old_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">New Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm New Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Change Email</div>
                <div class="panel-body">

                  @if (session('status_email'))
                    <div class="alert alert-success">
                      <strong>{{ session('status_email') }}</strong>
                    </div>
                  @endif

                    <form class="form-horizontal" role="form" method="POST" action="/account/settings/email/update" >
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-md-4 control-label">Current Email</label>

                            <div class="col-md-6">
                              <input type="text" class="form-control" value="{{$user->email}}" readonly>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">New Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Change Email
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Payout Methods</div>
                <div class="panel-body">

                  @if (session('status_payout'))
                    <div class="alert alert-success">
                      <strong>{{ session('status_payout') }}</strong>
                    </div>
                  @endif

                    <form class="form-horizontal" role="form" method="POST" action="/account/settings/payout/update" >
                        {{ csrf_field() }}

                        <div class="panel panel-default">
              						<div class="panel-heading">PayPal</div>
                          <div class="form-group{{ $errors->has('paypal') ? ' has-error' : '' }}">
                              <label for="paypal" class="col-md-4 control-label">Paypal Account Email</label>

                              <div class="col-md-6">
                                <input type="email" class="form-control" name="paypal" @if($user->payout('paypal')) value="{{$user->payout('paypal')->data}}" @endif>

                                  @if ($errors->has('paypal'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('paypal') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                        </div>

            						<div class="panel panel-default">
              						<div class="panel-heading">IBAN</div>
                          <div class="form-group{{ $errors->has('iban') ? ' has-error' : '' }}">
                              <label for="iban" class="col-md-4 control-label">IBAN Electronic Formatl</label>

                              <div class="col-md-6">
                                <input type="text" class="form-control" name="iban" placeholder="ex: CY17002001280000001200527600" @if($user->payout('iban')) value="{{$user->payout('iban')->data}}" @endif>

                                  @if ($errors->has('iban'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('iban') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Payout Methods
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
