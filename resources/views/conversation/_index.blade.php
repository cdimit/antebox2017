@extends('layouts.app')

@section('content')
<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Conversations</div>
					 <div class="panel-body">
						 <h3>As Requester</h3>
						 @foreach($user->requestConversations as $conversation)
						 <a href="/conversations/{{$conversation->id}}" class="btn btn-default">Conversation with {{$conversation->bidder->profile->name}}</a>
						 @endforeach

						 <h3>As Bidder</h3>
						 @foreach($user->bidConversations as $conversation)
							<a href="/conversations/{{$conversation->id}}" class="btn btn-default">Conversation with {{$conversation->requester->profile->name}}</a>
						 @endforeach
					 </div>
				</div>
		  </div>
	 </div>
</div>

@endsection
