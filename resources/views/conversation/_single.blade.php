@extends('layouts.app')

@section('content')
<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Conversations with {{$conversation->getOtherUser($user)->profile->name}}</div>
					 <div class="panel-body">
             @foreach($conversation->messages as $message)
              @if($message->isNote())
                <strong>{{$message->message}}</strong>
              @elseif($message->isAuth())
                {{$message->created_at}}, {{$message->message}}
              @else
                {{$message->message}}, {{$message->created_at}}
              @endif
              <br>
             @endforeach
             <form method="POST" action="/conversations/{{$conversation->id}}/sentMessage" style="display: inline-block;">
               {{ csrf_field() }}
               <input type="text" value="{{ old('message') }}" class="{{ $errors->has('message') ? ' has-error' : '' }}" name="message" required>
               <button type="submit" class="btn btn-primary">
                 Sent
               </button>
             </form>
					 </div>
				</div>
		  </div>
	 </div>
</div>

@endsection
