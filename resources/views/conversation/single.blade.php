@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-chat"></i>
			Conversations
		</h1>
		<p class="s-12em">
			View all your conversations with businesses and deliverers on this page.
		</p>
		<div class="box-light">
			<div class="row">
				<div class="col-md-4 col-xs-12">

					<ul class="contacts-list hidden-xs hidden-sm">
						<h4 class="w300 no-vm c-med-gray">
							<i class="icon icon-delivery"></i>
							Deliverers
						</h4>
						@foreach($user->requestConversations as $contact)
							<li class="">
								<a href="/conversations/{{$contact->id}}">
									{{$contact->bidder->profile->name}}
								</a>
							</li>
						@endforeach
						<h4 class="w300 no-vm c-med-gray">
							<i class="icon icon-shopping-bags"></i>
							Delivery requests
						</h4>
						@foreach($user->bidConversations as $contact)
						<li>
							<a href="/conversations/{{$contact->id}}">
								{{$contact->bidder->profile->name}}
							</a>
						</li>
						@endforeach
					</ul>
					<div class="visible-sm visible-xs">
						<a href="/conversations" class="btn btn-primary">
							<i class="pe-7s-users pe-lg"></i>
							Back to contact list
						</a>
					</div>
				</div>
				<div class="col-md-8 col-xs-12">
					<ul class="chat-window">
						@foreach($conversation->messages as $message)
							@if($message->isAuth())
								@if($message->isNote())
								<li class="announcement">
									<div class="message">
										<i class="pe-7s-info"></i>
										{{$message->message}}
									</div>
									<div class="timestamp">
										{{$message->created_at}}
									</div>
								</li>
								@else
								<li class="right">
									<div class="message">
										{{$message->message}}

									</div>
									<div class="timestamp">
										{{$message->created_at}}
									</div>
								</li>
								@endif
							@elseif(!$message->isNote())
								<li class="left">
									<div class="message">
										{{$message->message}}
									</div>
									<div class="timestamp">
										{{$message->created_at}}
									</div>
								</li>
							@endif

						@endforeach
					</ul>
					<div class="msg-window">
						<form method="POST" action="/conversations/{{$conversation->id}}/sendMessage" style="width: 100%;">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-10 col-xs-8">
									<input type="text" value="{{ old('message') }}" class="{{ $errors->has('message') ? ' has-error' : '' }}" name="message" required>
								</div>
								<div class="col-md-2 col-xs-4">
									<button type="submit" class="btn btn-primary">
										<i class="icon icon-send"></i>
										Send
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#date').datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d'
			});
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
@endsection
