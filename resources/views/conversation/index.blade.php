@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-chat"></i>
			Conversations
		</h1>
		<p class="s-12em">
			View all your conversations with businesses and deliverers on this page.
		</p>
		<div class="box-light">
			<div class="row">
				<div class="col-md-4 col-xs-12">
					<ul class="contacts-list">
						<h4 class="w300 no-vm c-med-gray" style="padding: 10px;">
							<i class="icon icon-delivery"></i>
							Deliverers
						</h4>
						@foreach($user->requestConversations as $conversation)
						<li>
							<a href="/conversations/{{$conversation->id}}">
								{{$conversation->bidder->profile->name}}
							</a>
						</li>
						@endforeach
						<h4 class="w300 no-vm c-med-gray" style="padding: 10px;">
							<i class="icon icon-shopping-bags"></i>
							Delivery requests
						</h4>
						@foreach($user->bidConversations as $conversation)
						<li>
							<a href="/conversations/{{$conversation->id}}">
								{{$conversation->bidder->profile->name}}
							</a>
						</li>
						@endforeach
					</ul>
				</div>
				<div class="col-md-8 col-xs-12">
					<div class="text-center hidden-xs hidden-sm">
						<h1 class="c-gray">
							Select a person to resume your conversation
						</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#date').datepicker({
				format: 'yyyy-mm-dd',
				startDate: '+1d'
			});
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
@endsection
