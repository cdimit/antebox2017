@extends('layouts.boilerplate')

@section('head')
	<title>My wishlist - Shopguin</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
@endsection

@section('body')
	<div class="container first">
		<h1 class="w300">
			<i class="pe-7s-magic-wand pe-lg pe-va" ></i>
			My Wishlist
		</h1>
		<p class="s-13em">
			Everything you want to buy in the future in one place. When you are ready just add them to your cart!
		</p>
		<div class="row">
			<div class="col-lg-7">
				<ul class="products-as-list">
					<li>
						<div class="row">
							@foreach($wishlist as $product)
							<div class=" col-xs-12 col-sm-6 col-md-6">
								<div class="row">
									<div class="col-md-2 col-xs-3">
									<img class="img-responsive" src="/storage/{{$product->picture}}">
										</br>
									</div>
									<div class="col-md-10 col-xs-9">
										<h4>
											<a href="/products/{{$product->id}}">
											{{$product->name}}
											</a>
											<a href="/wishlist/{{$product->id}}/remove" class="delete">
												<i class="icon icon-rubbish-bin"></i>
											</a>
										</h4>
										<h6>
											<i class="icon icon-euro"></i>
											€ {{$product->currentprice}}
										</h6>
										<div class="row">
											<div class="col-xs-6">
												<form method="POST" action="/products/{{$product->id}}/addItemToCart">
													{{ csrf_field() }}
													<button type="submit" class="btn btn-xs btn-primary">Add to cart</button>
												</form>
											</div>
										</div>
										<hr>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</li>
				</ul>
			</div>
			<div class="col-lg-2"></div>
			<div class="col-lg-3 side-info">
					<h3 class="w300">
						Instructions
					</h3>
					<p class="s-11em">
						You can add as many products as you want in your personal wishlist. Simply press on the heart shaped icon below the product. You can then access all the products in your wishlist here.
					</p>
				</div>
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection
