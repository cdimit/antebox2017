<!DOCTYPE html>
<html>
	<head>
		@yield('head')
		@yield('og-scripts')
		@yield('head-scripts')
	</head>

	<body>
		@yield('navbar')
		@yield('body')
		@yield('footer')
	</body>
	
	@yield('scripts')
</html>