<!DOCTYPE html>
<html lang="en">
<head>
	 <meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1">

	 <!-- CSRF Token -->
	 <meta name= "csrf-token" content="{{ csrf_token() }}">

	 <title>{{ config('app.name', 'Laravel') }}</title>

	 <!-- Styles -->
	 <link href="/css/dashboard.css" rel="stylesheet">

	 <!-- Scripts -->
	 <script>
	 	window.Stripe = <?php echo json_encode([
	 		'csrfToken' => csrf_token(),
	 		'stripeKey' => config('services.stripe.key'),
	 		'user'			=> auth()->user()
	 	]); ?>
	 </script>
</head>
<body>
	 <div id="app">

		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<!-- Collapsed Hamburger -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!-- Branding Image -->
					<a class="navbar-brand" href="{{ url('/home') }}">
						<img src="/img/antebox-logo-text-white@2x.png" class="img-responsive nav-logo" >
						&nbsp;
						<span>
						My dashboard
						</span>
					</a>
				</div>

					 <div class="collapse navbar-collapse" id="app-navbar-collapse">


						<!-- Right Side Of Navbar -->
						<ul class="nav navbar-nav navbar-right">
							<!-- Authentication Links -->
							<li>
								<a href="/">
									Home
								</a>
							</li>
							@if (Auth::guest())
								<li><a href="{{ url('/login') }}">Login</a></li>
								<li><a href="{{ url('/register') }}">Register</a></li>
								<li><a href="{{ url('/business/register') }}">Register Business</a></li>
							@else
								@if(Auth::user()->isClient())
								{{-- Client menu links --}}
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										{{ Auth::user()->profile->first_name }} <span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href="/client/{{Auth::user()->profile->id}}">
												Show my profile
											</a>
										</li>
										<li>
											<a href="/client/profile/edit">
												Edit my profile
											</a>
										</li>
										<li>
											<a href="/account/settings">
												Account Settings
											</a>
										</li>
										<li>
											<a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
															  Logout
											</a>

											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										</li>
									</ul>
								</li>
								@elseif(Auth::user()->isBusiness())
								{{-- Business profile links --}}
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
										{{ Auth::user()->profile->name }} <span class="caret"></span>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="/{{Auth::user()->profile->slug}}">Show Business Profile</a></li>
										<li><a href="/business/profile/edit">Edit Business Profile</a></li>
										<li><a href="/business/products">Products</a></li>
										<li>
											<a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
												Logout
											</a>

											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
											  {{ csrf_field() }}
											</form>
										</li>
									</ul>
								</li>
								{{-- Notifications possibly? --}}
								{{-- Prepare fullscreen notifcation menu? --}}
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Action</a></li>
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li class="divider"></li>
										<li><a href="#">Separated link</a></li>
										<li class="divider"></li>
										<li><a href="#">One more separated link</a></li>
									</ul>
								</li>
								@endif
							 @endif
						  </ul>
					 </div>
				</div>
		</nav>



		  @yield('content')
	 </div>

	 <!-- Scripts -->
	 <script src="https://checkout.stripe.com/checkout.js"></script>
	 <script src="/js/app.js"></script>
</body>
</html>
