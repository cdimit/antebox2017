@extends('layouts.app')

@section('content')

@if(!auth()->user()->profile->isActive())
<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Dashboard</div>
					 <div class="panel-body">
						 <subscribe-form :plans="{{ $plans }}"></subscribe-form>
					 </div>
				</div>
		  </div>
	 </div>
</div>
@endif

@if(auth()->user()->profile->isOnGracePeriod())
	<div class="container">
		 <div class="row">
			  <div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
			<div class="panel-heading">Grace Period</div>

			<div class="panel-body">
				<p>
					Your subscription will fully expire on {{auth()->user()->profile->subscription_end_at->format('d-m-Y')}} ({{auth()->user()->profile->subscription_end_at->diffForHumans()}})
				</p>

				<form method="POST" action="/stripe/subscribe">
					{{ method_field('PATCH') }}
					{{ csrf_field() }}

					<div class="checkbox">
						<label>
							<input type="checkbox" name="resume">
							Resume My subscription
						</label>
					</div>

					<button type='submit' class="btn btn-primary">Update</button>
				</form>
			</div>
	</div>
</div>
</div>
</div>
@endif

@if (count(auth()->user()->payments))
	<div class="container">
		 <div class="row">
			  <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Payments</div>

                        <div class="panel-body">
                            <ul class="list-group">
                                @foreach (auth()->user()->payments as $payment)
                                    <li class="list-group-item">{{ $payment->created_at->diffForHumans() }}:
                                        <strong>${{ $payment->formatAmount() }}</strong>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
									</div>
							 </div>
						</div>
@endif

@if(auth()->user()->profile->isSubscribed())
										<div class="container">
											 <div class="row">
												  <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Cancel</div>

                        <div class="panel-body">
                            <form method="POST" action="/stripe/subscribe">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <button class="btn btn-danger">Cancel My Subscription</button>
                            </form>
                        </div>
                    </div>
									</div>
							 </div>
						</div>
@endif




@endsection
