@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>{{$business['name']}} - Shopguin</title>
	<link rel="stylesheet" type="text/css" href="/css/capsule.css">
	{{-- AngularJS --}}
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.js"></script>
	<script type="text/javascript" src="/js/assets/angular.route.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.3/lodash.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/restangular/1.5.1/restangular.min.js"></script>
	<script type="text/javascript" src="/js/assets/ui-bootstrap-tpls-2.5.0.min.js"/></script>
	<script type="text/javascript" src="/js/assets/angular.utils/pagination/dirPagination.js"></script>
	<script type="text/javascript" src="/js/templates/capsule/app.js"></script>
@endsection

@section('og-scripts')
	<meta property="og:title" content="{{$business['name']}}" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="https://www.shopguin.com/{{$business['slug']}}" />
	<meta property="og:image" content="https://www.shopguin.com/storage/{{$business['logo']}}" />
	<meta property="og:description" content="{{$business['motto']}}" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:site" content="@shopguin">
	<meta name="twitter:creator" content="@shopguin">	
	<meta name="twitter:title" content="{{$business['name']}} - Shopguin">
	<meta name="twitter:image" content="https://www.shopguin.com/storage/{{$business['logo']}}">
	<meta name="description" content="{{$business['motto']}}">
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim-nofixed')

	@include('partials.quick-action')
@endsection


@section('body')
	<div class="template" ng-app="anteboxCapsule">
		{{-- Navbar --}}
		<div class="header clearfix text-center">
			<div class="header-container ">
				<img class="nav-logo" src="/images/themes/capsule/capsule-logo.png" alt="">
			</div>
			<div class="menu">
				<div class="container">
					<div class="nav-menu">
						<ul>
							<li><a href="#!/">Home</a></li>
							<li><a href="#!/products">Products</a></li>
							<li><a href="#!/stores">Stores</a></li>
							<li><a href="#!/about">About us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div ng-view></div>


	</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/assets/bootstrap-notify.min.js"></script>
	<script type="text/javascript" src="/js/assets/bootstrap-notify.min.js"></script>
	<script src="/js/templates/agnolotti/main.js"></script>
	<script>
		var _config= {
			id: '{{$id}}'
		};
		@if (session('status'))
			$.notify({
					message: '{{ session('status') }}' 
				},{
					type: 'success'
				}
			);
		@endif
	</script>

@endsection
