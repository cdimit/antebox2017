@extends('layouts.boilerplate')

@section('head')
	<title>Make a delivery - Shopguin</title>
	@include('partials.head')
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
	<div id="deliveries-map"></div>

	<div class="container ">
		<div class="row intro">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h1>
					<i class="icon icon-delivery"></i> Make a delivery
				</h1>
				<p>
					Have a look at some of these recently posted requests on Shopguin, which you could offer to deliver. The map above shows these delivery locations with markers.
				</p>
				<h2 class="w300">
					Recently posted requests
				</h2>
			</div>
		</div>
		<div class="row delivery-requests">
			@if($requests)
			@foreach($requests as $req)
				<div class="col-xs-12 col-sm-12 col-md-5 ">
					<div class="panel" >
						<div class="row body">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 details">
								<div class="request-profile">
									<img src="/storage/{{$req->client->picture}}" alt="..." class="request-profile-img">
									{{$req->client->first_name}} requested {{$req->created_at->diffForHumans()}}
								</div>
								<h3 class="business-name">
									{{$req->business->name}}
									<span class="flag-icon flag-icon-cy"></span>
								</h3>
								<div class="row cart-images">
									@foreach($req->items->take(3) as $product)
										<div class="col-md-3 col-xs-4 img">
											<img class="img-responsive" src="/storage/{{$product->product->picture}}">
										</div>
									@endforeach
								</div>
								<h4 class="summary">
									<span>
										Items
									</span>
									{{count($req->items)}}
									<span>
										Total
									</span>
									€ {{$req->total_price}}
								</h4>
								<div class="row delivery-details">
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<h6>
											Pick up from
										</h6>
										<h4 class="">
											<i class="icon icon-place"></i>  {{$req->business->city}}, {{$req->business->country}}
										</h4>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<h6>
											Deliver to
										</h6>
										<h4 class="">
											<i class="icon icon-place"></i>
											@if($req->meetingPoint)
											{{$req->meetingPoint->city}}, {{$req->meetingPoint->country}}
											@endif
										</h4>
									</div>
								</div>
								<div class="earn row">
									<div class="text-center  col-xs-12 ">
										<a href="/delivery/{{$req->id}}/bid/create" class="btn btn-delivery"><i class="icon icon-delivery" style="font-size:1.5em;"></i> &nbsp; View delivery request</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			@endif
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
@endsection
