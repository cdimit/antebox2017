@extends('layouts.boilerplate')

@section('head')
	<title>Delivery - Shopguin</title>
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-delivery"></i>
			Delivery from Apple Ltd.
		</h1>
		<div class="details">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<h5 class="w300 spread">
						Deliver to
					</h5>
					<h4 class="w300 c-med-gray">
						<img src="https://randomuser.me/api/portraits/men/20.jpg" alt="..." class="profile-img-65">
						John Doe  
					</h4>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<h5 class="w300 spread">
						Pick up from
					</h5>
					<h4 class="w300 c-med-gray">
						<i class="icon icon-place"></i>
						London, GB  
					</h4>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<h5 class="w300 spread">
						Dropoff location
					</h5>
					<h4 class="w300 c-med-gray">
						<i class="icon icon-place"></i>
						London, GB  
					</h4>
				</div>

			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<h2 class="w300">
					<i class="icon icon-shopping-cart"></i>
					Shopping cart contents
				</h2>
				{{-- Shopping cart contents --}}
				<div class="box-light">
					<div class="row">
						<div class="col-md-12 col-xs-10">
							<h4 class="w300 no-vm">
								<i class="pe-7s-cart pe-lg pe-va"></i>
							</h4>
							<hr>
						</div>

					</div>
					<div class="shopping-cart-body">
						<ul class="products-as-list">
							<li>
								<div class="row">
									<div class=" col-xs-12 col-sm-6 col-md-6">
										<div class="row">
											<div class="col-md-2 col-xs-3">
												<img class="img-responsive" src="/storage/">
												<br>
											</div>
											<div class="col-md-10 col-xs-9">
												<h4>
													
												</h4>
												<h6>
													<span>
														Qty
													</span>
													

												</h6>
												<h6>
													<span>
														Price
													</span>
													€ 
												</h6>
												<hr>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>

						<div class="row">
							<div class="col-md-8 col-xs-12">
								<h3 class="info" style="margin-top: 0px;">
									<span><i class="icon icon-place"></i> &nbsp;Delivery location</span>
									<br>
								</h3>
							</div>
							<div class="col-md-4 col-xs-12">
								<h3 class="info no-vm">
									<span>
										Delivery fee (earnings)
									</span>
									€ 605.00
								</h3>
								<br>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-6 col-xs-12">
				
				<h3 class="w300">
					Chat with the cart owner
				</h3>
				<div class="box-light">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<ul class="chat-window">
								<li class="announcement">
									<div class="message">
										<i class="pe-7s-info"></i>
										Major announcement
									</div>
									<div class="timestamp">
										20170/04/12
									</div>
								</li>
								
								<li class="right">
									<div class="message">
										Hey, bring me my stuff!
									</div>
									<div class="timestamp">
										2017/04/02
									</div>
								</li>
								
						
								<li class="left">
									<div class="message">
										Ok mate, hold on!
									</div>
									<div class="timestamp">
										2019/04/11
									</div>
								</li>		
							</ul>
							<div class="msg-window">
								<form method="POST" action="/conversations//sendMessage" style="width: 100%;">
									
									<div class="row">
										<div class="col-md-10 col-xs-8">
											<input type="text" value="" name="message" required>
										</div>
										<div class="col-md-2 col-xs-4">
											<button type="submit" class="btn btn-primary">
												<i class="icon icon-send"></i>
												Send
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-xs-12">
				<h3 class="w300">
					Delivery process
				</h3>
				<p class="s-12em">
					Let your customer know how their delivery is progressing below:
				</p>
				<a href="" class="btn btn-lg btn-success">
					<i class="icon icon-success"></i>
					Confirm receipt of items
				</a>
				<br><br>
				<a href="" class="btn btn-lg btn-success">
					<i class="icon icon-delivery"></i>
					Confirm delivery of items
				</a>
				<br><br>
			</div>
		</div>
		
		

</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			$(".nav-lower-tabs > li:nth-child(3)").addClass("active");
			$(".nav-lower-tabs > li:nth-child(2)").removeClass("active");
			$("#buy").removeClass("active in");
			$("#deliver").addClass("active in");
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="/js/app.js"></script>
@endsection
