@extends('layouts.boilerplate')

@section('head')
	<title>Search results ({{count($products)}}) - Shopguin</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
	@include('partials.quick-action')
@endsection


@section('body')
	<div  class="container all-products">
		<h1 class="w300">
			Search results
		</h1>

			<h3 class="w300 c-gray" style="margin-top: 0px;">
				<small>
				@if(count($products) > 1)
					{{count($products)}} products found.
				@elseif(count($products) == 1)
					{{count($products)}} product found.
				@elseif(count($products) == 0)
					No products found.
				@endif
				</small>
			</h3>
			<div class="row is-flex product-grid">
				<!-- Product card for each product -->
				
					@foreach($products as $product)
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
						<div class="box-light" style="margin-left: -10px; margin-right: -10px;">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-5">
									<div class="product-img">
										<a href="/products/{{$product->id}}">
											<img class="img-responsive" src="/storage/{{$product->picture}}">
										</a>
									</div>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-7 product-details">
									<a href="/products/{{$product->id}}">
										<span class="category">
											{{$product->category->name}}
										</span>
										<h3 class="product-title">
											{{$product->name}}
											<span class="flag-icon flag-icon-{{$product->business->country}}"></span> 
										</h3>
										<h6 class="company">
											{{$product->business->name}}
										</h6>
										<h2 class="price">
											@if($product->discount)
												@if($product->discount->off_method=="percent")
													<strike>
														€ {{number_format($product->price, 2)}}
													</strike>
													&nbsp;
													€ {{number_format($product->discount->new_price, 2)}}
													&nbsp;
													<div class="discount">
														-€{{$product->discount->value}} % OFF
													</div>
												@elseif($product->discount->off_method=="amount")
													<strike>
														€ {{number_format($product->price, 2)}}
													</strike>
													&nbsp;
													€ {{number_format($product->discount->new_price, 2)}}
													&nbsp;
													<div class="discount">
														-€{{$product->discount->value}} OFF
													</div>
												@endif
											@else
												€ {{number_format($product->price, 2)}}
											@endif
											
										</h2>
										<div class="product-info rating" style="padding-left: 0px;">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star-o no" aria-hidden="true"></i>
											&nbsp;
											<a href="">
												(13 reviews)					
											</a>
										</div>
									</a>
									<hr>
								</div>

								<div class="col-md-12 col-sm-12 col-sm-offset-0 col-md-offset-0 col-xs-8 col-xs-offset-4">
									<div class="text-center">
										<form method="POST" action="/products/{{$product->id}}/addSearchedItemToCart" style="display: inline-block;">
											{{ csrf_field() }}
											<button class="btn btn-primary">
												<i class="icon icon-shopping-basket"></i> &nbsp; Add to cart &nbsp; 
											</button>
										</form>
										<form method="POST" action="/wishlist/{{$product->id}}/add" style="display: inline-block;">
											{{ csrf_field() }}
											<button class="btn btn-red">
												<i class="icon icon-heart"></i>
											</button>
										</form>
									</div>
								</div>
							</div>
						</div>	
					</div>
					@endforeach
			</div>	
		
			<h3>
			</h3>
		
		
	</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
	
@endsection


