

<!-- Modal -->
<div class="modal fade" id="addImages" tabindex="-1" role="dialog" aria-labelledby="addImagesLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="addImagesLabel">View / add more images</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="/business/products/{{$product->id}}/images/add" enctype="multipart/form-data">
					{{ csrf_field() }}


					<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
						<label for="picture" class="col-md-4 col-xs-12 control-label">Image</label>
						<div class="col-md-6 col-xs-12">
							<input id="picture" type="file" name="picture">

							@if ($errors->has('picture'))
								<span class="help-block">
									<strong>{{ $errors->first('picture') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-8 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Add Image
							</button>
						</div>
					</div>
				</form>


				<hr>
				<p class="text-muted font-13 m-b-30">
					Images of the product
				</p>
				
				<div class="row">
					@foreach($product->images() as $img)
						<div class="col-md-4">
							<img class="img-responsive" src="/storage/{{$img}}" >
							<br>
							<form method="POST" action="/business/products/{{$product->id}}/images/remove">
								{{ csrf_field() }}
								<input type="hidden" name="image" value="{{$img}}">
								<button type="submit" class="btn btn-xs btn-danger">Remove</button>
							</form>
							
						</div>
					@endforeach
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
			</div>
		</div>
	</div>
</div>