@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Products</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All products from {{Auth::user()->profile->name}}</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the products associated with your company.
									</p>
									<div class="table_scroll">
										<table id="all-products" class="table table-striped table-bordered no-wrap dashboard-table" >
											<thead>
												<tr>
													<th>Image</th>
													<th>Name</th>
													<th>Category</th>
													<th>Stores</th>
													{{-- <th>Total visits</th> --}}
													{{-- <th>Unique visits</th> --}}
													<th>Price</th>
													<th>Discount</th>
													<th></th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach($products as $product)
												<tr>
													<td>
														<img src="/storage/{{$product->picture}}" class="img-responsive" style="width: 80px; height: auto;"><br>
													</td>
													<td>
														{{$product->name}}
													</td>
													<td>
														{{$product->category->name}}
													</td>
													<td>
														{{$product->stores()->count()}}
														<a href="/business/products/{{$product->id}}/stores" class="btn btn-default btn-xs"><i class="fa fa-plus-square" aria-hidden="true"></i></a>
													</td>
													{{-- <td> --}}
														{{-- 100 --}}
														{{-- {{$product->visits}} --}}
													{{-- </td> --}}
													{{-- <td> --}}
														{{-- 150 --}}
														{{-- {{$product->visits_unique}} --}}
													{{-- </td> --}}
													<td>
														@if($product->discount)
														<strike>
															{{$product->price}}
														</strike>
														{{$product->discount->new_price}}
														@else
														{{$product->price}}
														@endif
													</td>
													<td>
														@if($product->discount)
														{{$product->discount->value}}
															@if($product->discount->off_method == 'percent')
															%
															@else
															(value)
															@endif
														<a href="/business/discount/{{$product->discount->id}}/remove" class="btn btn-xs btn-danger"><i class="fa fa-minus-square" aria-hidden="true"></i> Remove</a>
														@else
												  		<a href="/business/discount/create" class="btn btn-xs btn-default"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Discount</a>
														@endif
													</td>
													<td >
														<a href="/business/products/{{$product->id}}/images" class="btn btn-xs btn-default"><i class="fa fa-plus-square" aria-hidden="true"></i> Add More Images</a>
													</td>
													<td>
														<a href="/products/{{$product->id}}" class="btn btn-xs btn-default"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
													</td>
													<td>
      													<a href="/business/products/{{$product->id}}/edit" class="btn btn-xs btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
													</td>
													<td>
           										      	<a href="/business/products/{{$product->id}}/delete" class="btn btn-xs btn-danger"><i class="fa fa-times-circle" aria-hidden="true"></i> Delete</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-products').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
