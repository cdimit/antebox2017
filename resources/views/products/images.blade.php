@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Products</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Add more images to product</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<form class="form-horizontal" role="form" method="POST" action="/business/products/{{$product->id}}/images/add" enctype="multipart/form-data">
									{{ csrf_field() }}


									<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
										<label for="picture" class="col-md-4 col-xs-12 control-label">Image</label>
										<div class="col-md-6 col-xs-12">
											<input id="picture" type="file" name="picture">

											@if ($errors->has('picture'))
											<span class="help-block">
											<strong>{{ $errors->first('picture') }}</strong>
											</span>
											@endif
										</div>
									</div>

									<div class="form-group">
										<div class="col-md-8 col-md-offset-4">
											<button type="submit" class="btn btn-primary">
											Add Image
											</button>
										</div>
									</div>
								</form>


										<hr>
										<p class="text-muted font-13 m-b-30">
											Images of the product
										</p>

										@foreach($product->images() as $img)
											<img class="img-responsive col-md-4" src="/storage/{{$img}}" >
											<form method="POST" action="/business/products/{{$product->id}}/images/remove">
												{{ csrf_field() }}
												<input type="hidden" name="image" value="{{$img}}">
												<button type="submit" class="btn btn-xs btn-danger">Remove</button>
											</form>
										@endforeach


								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Adding discounts to products</h3>
							<p  class="text-muted font-13 m-b-30">
								Use this page to add a discount to your products. You can choose whether your discount is by percentage, or by value, and also add an expiry for your discount.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script type="text/javascript">
		$(function(){

			$('.datepicker').datepicker({
				format: 'yyyy-mm-dd'
			});
		});

	</script>
</body>
</html>
