@extends('layouts.boilerplate')

@section('head')
	<title>All products - Shopguin</title>
	@include('partials.head')
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('og-scripts')
	<meta property="og:title" content="All products - Shopguin" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.shopguin.com/products" />
	<meta property="og:description" content="All products on Shopguin.com" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:site" content="@shopguin">
	<meta name="twitter:creator" content="@shopguin">
@endsection

@section('head-scripts')
	{{-- AngularJS --}}
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.3/lodash.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/restangular/1.5.1/restangular.min.js"></script>
	<script type="text/javascript" src="/js/assets/ui-bootstrap-tpls-2.5.0.min.js"/></script>
	<script type="text/javascript" src="/js/assets/angular.utils/pagination/dirPagination.js"></script>
	<script type="text/javascript" src="/js/products/ng-products.js"></script>
@endsection

@section('navbar')
	@include('partials.navbar')



	@include('partials.quick-action')
@endsection


@section('body')
<section class="grey mt--20">
	<div ng-app="anteboxAllProducts" class="container all-products">
		<div class="row" ng-controller="ProductsController as p">
			{{-- Mobile Sort Filter --}}
			<div class="m-sortfilter-bar hidden-xs hidden-md hidden-sm hidden-lg">
				<div class="row">
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="filterProducts">
						<i class="fa fa-filter" aria-hidden="true"></i> Refine
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="sortProducts">
						<i class="icon icon-sort" aria-hidden="true"></i> Sort
					</div>
					<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="viewProducts">
						<i class="icon icon-view" aria-hidden="true"></i> View
					</div>
				</div>
				<div id="sortProductsBar" class="row sortFilterLowerBar sortFilter" style="display:none;">
					<a href="" ng-click="p.sortType = 'price'; p.sortReverse = !p.sortReverse">
						<i class="fa fa-sort-amount-asc" aria-hidden="true" ng-show="p.sortType == 'price' && !p.sortReverse"></i>
						<i class="fa fa-sort-amount-desc" aria-hidden="true" ng-show="p.sortType == 'price' && p.sortReverse"></i>
						<i class="fa fa-eur" aria-hidden="true"></i>
						Price
					</a>
					<a href="" ng-click="p.sortType = 'business.name'; p.sortReverse = !p.sortReverse">
						<i class="fa fa-sort-amount-asc" aria-hidden="true" ng-show="p.sortType == 'business.name' && !p.sortReverse"></i>
						<i class="fa fa-sort-amount-desc" aria-hidden="true" ng-show="p.sortType == 'business.name' && p.sortReverse"></i>
						<i class="fa fa-building-o" aria-hidden="true"></i>
						Company
					</a>
					<a href="" ng-click="p.sortType = 'category.name'; p.sortReverse = !p.sortReverse">
						<i class="fa fa-sort-amount-asc" aria-hidden="true" ng-show="p.sortType == 'category.name' && !p.sortReverse"></i>
						<i class="fa fa-sort-amount-desc" aria-hidden="true" ng-show="p.sortType == 'category.name' && p.sortReverse"></i>
						<i class="fa fa-gift" aria-hidden="true"></i>
						Category
					</a>
				</div>
				<div id="filterProductsBar" class="row sortFilterLowerBar sortFilter" style="display:none;">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						{{-- Categories filter --}}
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="m-categories" data-toggle="collapse" data-parent="#accordion" href="#m-categories-filter" aria-expanded="true" aria-controls="m-categories-filter">
								<h4 class="panel-title">
									<a role="button" >
										<i class="icon icon-categories"></i> &nbsp; Categories
									</a>
								</h4>
							</div>
							<div id="m-categories-filter" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="m-categories">
								<div class="panel-body">
									<div class="checkbox" ng-repeat="category in p.categoriesList">
										<label>
											<input ng-click="p.addToFilter(category, p.categoryFilter)" type="checkbox" ng-checked="p.exists(category, p.categoryFilter)"> [[category]]
										</label>

									</div>
									<div ng-show="p.categoriesList.length == 0">
										No categories found...
									</div>
								</div>
							</div>
						</div>
						{{-- Businesses filter --}}
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="m-companies" data-toggle="collapse" data-parent="#accordion" href="#m-companies-filter" aria-expanded="true" aria-controls="m-companies-filter">
								<h4 class="panel-title">
									<a role="button" >
										<i class="icon icon-companies"></i> &nbsp; Companies
									</a>
								</h4>
							</div>
							<div id="m-companies-filter" class="panel-collapse collapse " role="tabpanel" aria-labelledby="m-companies">
								<div class="panel-body">
									<div class="checkbox" ng-repeat="company in p.companiesList">
										<label>
											<input ng-click="p.addToFilter(company, p.companyFilter)" type="checkbox" ng-checked="p.exists(company, p.companyFilter)"> [[company]]
										</label>
									</div>
									<div ng-show="p.companiesList.length == 0">
										No companies found...
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="viewProductsBar" class="row sortFilterLowerBar sortFilter" style="display:none;">
					<h4>
						Products per page:
					</h4>
					<select class="form-control"  ng-model="p.itemsPerPage" ng-options="number for number in p.viewByOptions"></select>
				</div>
			</div>
			{{-- Sidebar --}}
			<products-filter></products-filter>
			{{-- Products grid --}}
			<products-grid ></products-grid>
		</div>
	</div>
</section>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
	<script type="text/javascript" src="/js/assets/bootstrap-notify.min.js"></script>
	<script type="text/javascript">
		var sortFilter = $('.m-sortfilter-bar')
		var fixMeTop = sortFilter.offset().top;
		$(window).scroll(function(){
			var currentScroll = $(window).scrollTop();
			if (currentScroll >= 25) {
				sortFilter.addClass('stuck');
			} else {
				sortFilter.removeClass('stuck');
			}

			if (currentScroll >= 181) {
				$('.sidebar-product-filter').addClass('scroll-md');
			} else if (currentScroll <= 181) {
				$('.sidebar-product-filter').removeClass('scroll-md');
			}
		});
		@if (session('status'))
			$.notify({
					message: '{{ session('status') }}' 
				},{
					type: 'success'
				}
			);
		@endif
	</script>
@endsection
