@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Products</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Edit product - {{$product->name}} </h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Use this form to edit any information about your product.
									</p>
									<form class="form-horizontal" role="form" method="POST" action="/business/products/{{$product->id}}/edit" enctype="multipart/form-data">
									{{ csrf_field() }}

										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 col-xs-12 control-label">Name</label>
											<div class="col-md-6 col-xs-12">
												<input id="name" type="text" class="form-control" name="name" value="{{ $product->name }}" required autofocus>

												@if ($errors->has('name'))
												<span class="help-block">
												<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 col-xs-12 control-label">Price</label>
											<div class="col-md-6 col-xs-12">
												<input id="price" type="text" class="form-control" name="price" value="{{ $product->price }}" required="required"  autofocus>

												@if ($errors->has('price'))
												<span class="help-block">
												<strong>{{ $errors->first('price') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
											<label for="category_id" class="col-md-4 col-xs-12 control-label">Category</label>
											<div class="col-md-6 col-xs-12">
												<select name="category_id" class="form-control">
												@foreach($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
												@endforeach
												</select>
												@if ($errors->has('category_id'))
												<span class="help-block">
												<strong>{{ $errors->first('category_id') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
											<label for="description" class="col-md-4 col-xs-12 control-label">Description</label>
											<div class="col-md-6 col-xs-12">
												<input id="description" type="text" class="form-control" value="{{ $product->description }}" name="description">

												@if ($errors->has('description'))
												<span class="help-block">
												<strong>{{ $errors->first('description') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('long_description') ? ' has-error' : '' }}">
											<label for="long_description" class="col-md-4 col-xs-12 control-label">Long Description</label>
											<div class="col-md-6 col-xs-12">
												<input id="description" type="text" class="form-control" value="{{ $product->long_description }}" name="long_description">

												@if ($errors->has('long_description'))
												<span class="help-block">
												<strong>{{ $errors->first('long_description') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
											<label for="picture" class="col-md-4 col-xs-12 control-label">Picture</label>
											<div class="col-md-6 col-xs-12">
												<input  id="picture" type="file" name="picture">
												<br>
												<img class="img-responsive col-md-4" src="/storage/{{$product->picture}}" >
												@if ($errors->has('picture'))
												<span class="help-block">
												<strong>{{ $errors->first('picture') }}</strong>
												</span>
												@endif
												<br>
												<!-- Button trigger modal -->
												<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#addImages">
													Add more images
												</button>
											</div>

										</div>

										<div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
											<label for="video" accept="video/*" class="col-md-4 col-xs-12 control-label">Video</label>
											<div class="col-md-6 col-xs-12">
												<input id="video" type="file" name="video">

												@if ($errors->has('video'))
												<span class="help-block">
												<strong>{{ $errors->first('video') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('bag') ? ' has-error' : '' }}">
											<label for="bag" class="col-md-4 col-xs-12 control-label">Bag (e.g. cabin)</label>
											<div class="col-md-6 col-xs-12">
												<input id="bag" type="text" class="form-control" name="bag" value="{{ $product->bag }}">

												@if ($errors->has('bag'))
												<span class="help-block">
												<strong>{{ $errors->first('bag') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
											<label for="weight" class="col-md-4 col-xs-12 control-label">Weight (g)</label>
											<div class="col-md-6 col-xs-12">
												<input id="weight" type="text" class="form-control" name="weight" value="{{ $product->weight }}">

												@if ($errors->has('weight'))
												<span class="help-block">
												<strong>{{ $errors->first('weight') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('height') ? ' has-error' : '' }}">
											<label for="height" class="col-md-4 col-xs-12 control-label">Height (cm)</label>
											<div class="col-md-6 col-xs-12">
												<input id="height" type="text" class="form-control" name="height" value="{{ $product->height }}">

												@if ($errors->has('height'))
												<span class="help-block">
												<strong>{{ $errors->first('height') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('width') ? ' has-error' : '' }}">
											<label for="width" class="col-md-4 col-xs-12 control-label">Width (cm)</label>
											<div class="col-md-6 col-xs-12">
												<input id="width" type="text" class="form-control" name="width" value="{{ $product->width }}">

												@if ($errors->has('width'))
												<span class="help-block">
												<strong>{{ $errors->first('width') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('depth') ? ' has-error' : '' }}">
											<label for="depth" class="col-md-4 col-xs-12 control-label">Depth (cm)</label>
											<div class="col-md-6 col-xs-12">
												<input id="depth" type="text" class="form-control" name="depth" value="{{ $product->depth }}">

												@if ($errors->has('depth'))
												<span class="help-block">
												<strong>{{ $errors->first('depth') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
											<label for="tags" class="col-md-4 col-xs-12 control-label">Add Tags(seperate with comma)</label>
											<div class="col-md-6 col-xs-12">
												<input id="tags" type="text" class="form-control" name="tags" value="{{$product->tagsstring}}">

												@if ($errors->has('tags'))
												<span class="help-block">
												<strong>{{ $errors->first('tags') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Update product
												</button>
											</div>
										</div>
									</form>
									@include('products.modal_add_images')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#tags').tagsInput();
		});
	</script>
</body>
</html>
