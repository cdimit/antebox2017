@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>{{$product->name}} - Shopguin</title>
@endsection

@section('og-scripts')
	<meta property="og:title" content="{{$product->name}} - Shopguin" />
	<meta property="og:type" content="product" />
	<meta property="og:image" content="https://www.shopguin.com/storage/{{$product->picture}}" />
	<meta property="og:url" content="https://www.shopguin.com/products/{{$product->id}}/{{$product->slug}}" />
	<meta property="og:description" content="{{$product->description}}" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:site" content="@shopguin">
	<meta name="twitter:creator" content="@shopguin">
	<meta name="twitter:title" content="{{$product->name}} - Shopguin">
	<meta name="twitter:image" content="https://www.shopguin.com/storage/{{$product->picture}}">
	<meta name="description" content="{{$product->description}}">
	
@endsection


@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
	{{-- Product title, price, description --}}
	<div class="container first">

		<div class="row product">
			<div class="product-info col-xs-12 col-sm-6 col-md-6 col-lg-4">
				<div id="product-images" class="owl-carousel owl-theme visible-xs">
					<div class="item m-product-img">
						<img class="img-responsive" src="/storage/{{$product->picture}}">
					</div>
					@foreach($product->images() as $img)
						<div class="item m-product-img">
							<img class="img-responsive" src="/storage/{{$img}}">
						</div>
					@endforeach
				</div>
				<div class="hidden-xs">
					<div id="product-tabs" class="tab-content">
						@php($img = $product->images())
						@for($i=0; $i < count($product->images()); $i++)
							<div class="tab-pane fade" id="img{{$i+1}}">
								<div class="product-img">
									<img class="img-responsive" src="/storage/{{$img[$i]}}">
								</div>
							</div>
						@endfor
						<div class="tab-pane fade active in" id="img0">
							<div class="product-img">
								<img class="img-responsive" src="/storage/{{$product->picture}}">
							</div>
						</div>
					</div>
					<ul class="nav nav-product-img-tabs">
						<li class="active">
							<a href="#img0" data-toggle="tab">
								<img class="img-responsive" src="/storage/{{$product->picture}}">
							</a>
						</li>
						@for($i=0; $i < count($product->images()); $i++)
							<li class="">
								<a href="#img{{$i+1}}" data-toggle="tab">
									<img class="img-responsive" src="/storage/{{$img[$i]}}">
								</a>
							</li>
						@endfor
						
					</ul>
				</div>

			</div>
			<div class="product-info col-xs-12 col-sm-6 col-md-6 col-lg-5">
				<h5>
					<span>{{$product->category->name}}</span> / <a href="/{{$product->business->slug}}">{{$product->business->name}}</a>
					
				</h5>
				<h1>
					{{$product->name}}

				</h1>
				{{-- <div class="rating">
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star" aria-hidden="true"></i>
					<i class="fa fa-star-o no" aria-hidden="true"></i>
					&nbsp;
					<a href="">
						(13 reviews)
					</a>

				</div> --}}
				<div class="countryFlagName">
					<span  class="flag-icon flag-icon-{{$product->business->country}}" style="font-size:1.8em;"></span>
					<h3 id="countryName" style="display:inline-block;">
						<input type="hidden" id="country_name" name="country_name" value="{{$product->business->country}}">
					</h3>

				</div>
				<hr>
				<p class="short-description">
					{{str_limit($product->description, $limit = 200, $end = '...')}}
					<br>
					@foreach($product->tags as $tag)
					<span>{{$tag->name}}</span>
					@endforeach
				</p>
				<h2>
					@if($product->discount)
					<strike>
						€ {{number_format($product->price, 2)}}
					</strike>
					&nbsp;
						€ {{number_format($product->discount->new_price, 2)}}
					&nbsp;
					</br>
					<span>
						@if($product->discount->off_method=="percent")
						{{$product->discount->value}}% OFF
						@elseif($product->discount->off_method=="amount")
						-€{{$product->discount->value}} OFF
						@endif
					</span>
					@else
						€ {{number_format($product->price, 2)}}
					&nbsp;
					@endif
				</h2>

				
				<form method="POST" action="/products/{{$product->id}}/addItemToCart" style="display: inline-block;">
					{{ csrf_field() }}
					<button class="btn btn-primary s-13em">
						&nbsp; <i class="icon icon-shopping-basket"></i> &nbsp; Add to cart &nbsp; 
					</button>				
				</form>
				<form method="POST" action="/wishlist/{{$product->id}}/add" style="display: inline-block;">
					{{ csrf_field() }}
					<button class="btn btn-red s-13em" >
						<i class="icon icon-heart"></i>
					</button>
				</form>
				<br><br>
				<p class="short-description">
					Share on social media:
					<br>
				</p>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) return;
					js = d.createElement(s); js.id = id;
					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=414466878904860";
					fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-share-button" data-href="http://www.shopguin.com/products/{{$product->id}}/{{$product->slug}}" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
					
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 recommendations">
				
			</div>
		</div>
	</div>
	<!-- Product details -->
	<div style="background-color: #fff;">
		<hr>
		<div class="container">
			<div class="visible-xs">
				<p class="hint">
					<i class="fa fa-exclamation-circle" aria-hidden="true"></i> &nbsp; <b>Hint:</b> Slide the tabs to scroll
				</p>
			</div>
			<div class="row product-details-lg">
				<ul class="nav nav-flex-tabs">
					<li class="active"><a href="#description" data-toggle="tab">Description</a></li>
					<li class=""><a href="#details" data-toggle="tab">Product details</a></li>
					<li class=""><a href="#company" data-toggle="tab">About the company</a></li>
				</ul>
				<div id="product-details-panes" class="tab-content">
					<div class="tab-pane fade active in" id="description">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<p>
									{{$product->description}}
								</p>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<video poster="/img/placeholders/video-placeholder.png" class="img-responsive" width="640" height="480"  controls>
									<source src="/storage/{{$product->video}}" type="video/mp4">
									Your browser does not support the video tag.
								</video>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="details">
						<p>
							<div class="dimensions">
								<ul>
									<li>
										<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-arrows-h fa-stack-1x fa-inverse"></i>
										</span>
										<span class="property">Width:</span> &nbsp; {{$product->width}} cm
									</li>
									<li>
										<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-arrows-v fa-stack-1x fa-inverse"></i>
										</span>
										<span class="property">Height:</span> &nbsp; {{$product->height}} cm
									</li>
									<li>
										<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-cube fa-stack-1x fa-inverse"></i>
										</span>
										<span class="property">Depth:</span> &nbsp; {{$product->depth}} cm
									</li>
									<li>
										<span class="fa-stack fa-lg">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-balance-scale fa-stack-1x fa-inverse"></i>
										</span>
										<span class="property">Weight:</span> &nbsp; {{$product->weight}} g
									</li>
								</ul>
							</div>
						</p>
					</div>
					<div class="tab-pane fade" id="company">
						<h3 class="w500">{{$product->business->name}}</h3>
						<p>
							{{$product->business->about}}
						</p>
						<p>
							<a href="/{{$product->business->slug}}" class="btn btn-primary">Learn more</a>
							&nbsp;
							<span class="fa-stack fa-lg" >
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-globe fa-stack-1x fa-inverse"></i>
							</span>
							<a href="">
								{{$product->business->website}}
							</a>
						</p>
						<h3 class="w500">This product is available at {{$product->business->name}}'s following stores:</h3>
						<div class="row">
							@foreach($product->stores as $store)
								<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 store">
									<h3>{{$store->name}}</h3>
									<p>
										{{$store->address}}<br>
										{{$store->city}}<br>
										{{$store->state}}<br>
										{{$store->zip_code}}<br>
										{{$store->country}}
									</p>
									<p>
										<span class="fa-stack fa-lg" style="font-size:0.8em;">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-phone fa-stack-1x fa-inverse"></i>
										</span> {{$store->phone}}
										<br>

									</p>
									@if($store->map_x and $store->map_y) {{-- if exist --}}
										<?php
											$link = 'https://maps.google.com/maps?&z=10&q='.$store->map_x.'+'.$store->map_y.'&ll='.$store->map_x.'+'.$store->map_y;
										?>
										<a href="{{$link}}" class="btn btn-primary" style="font-size:0.8em;"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Map</a>
									@endif
								</div>
							@endforeach

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
	<script type="text/javascript" src="/js/assets/bootstrap-notify.min.js"></script>
	<script type="text/javascript">
		@if (session('status'))
			$.notify({
					message: '{{ session('status') }}' 
				},{
					type: 'success'
				}
			);
		@endif
	</script>
@endsection
