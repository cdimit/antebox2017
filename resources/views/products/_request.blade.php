@include('partials.head')

<button type="button" class="quick-action-btn visible-xs">
	<i class="pe-7s-keypad pe-3x pe-va"></i>
</button>

@include('partials.quick-action')

@include('partials.navbar')

{{-- Main --}}
<div class="container first">
	<div class="row">
		{{-- Request steps form --}}
		<div class="col-xs-12 col-md-8 col-lg-8">
			<div class="row">
				<div style="margin-bottom: 150px;">
					<ul class="steps-timeline">
						<li class="active">
							<i class="fa fa-shopping-basket fa-2x" aria-hidden="true"></i>
							<span>
								Quantity
							</span>
						</li>
						<li class="">
							<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
							<span>
								Location
							</span>
						</li>
						<li class="">	
							<i class="fa fa-calendar fa-2x" aria-hidden="true"></i>
							<span>
								Time/Date
							</span>
						</li>
						<div class="progress-bar">
						</div>
						<div class="full-bar">
						</div>
					</ul>
					
				</div>

				<div class="steps-form active" id="step1">
					<h2>How many would you like?</h2>
					<p>
						Select how many of 'Product title' you would like to request.
					</p>
					<div class="row product-bar ">
						<div class="col-xs-12 col-lg-3">
							<img class="img-responsive" src="https://placeimg.com/240/240/tech">
							
						</div>
						<div class="col-xs-12 col-lg-9 ">
							<h3>
								<span class="flag-icon flag-icon-cy"></span> &nbsp;
								Really long product title goes here and wraps around too 
								<br>
								<small>
								<i class="fa fa-shopping-bag" aria-hidden="true"></i> Monagri Grape Farm &nbsp; | &nbsp; <i class="fa fa-balance-scale" aria-hidden="true"></i> 300 g
								</small>
								<h2 id="total-price">
									£60.00
								</h2>
							</h3>
							<label for="qty">Quantity:</label>
							<form action="" name="delivery-address">
							<input type="number" min="1" step="1" value="1" name="qty">
							
						</div>
							
					</div>
					<br>
					<div class="alert alert-dismissible alert-info">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						Since you're buying a product from <strong>Monagri Grape Farm</strong>, why not consider adding another product from this company to your shopping cart?
						<br><br>
						<div class="row">
							<div class="col-xs-3 col-md-2 col-lg-1">
								<i class="pe-7s-cart pe-3x pe-va"></i>
							</div>
							<div class="col-xs-9 col-md-10 col-lg-11">
								Products belonging to the same company automatically get added into one shopping cart, so your deliverer can pick them up in one go and deliver them to you!
							</div>
						</div>
						<br>
						<a href="#" class="btn btn-info">Keep shopping</a>
					</div>
					<button type="button" id="to-delivery" class="btn btn-primary" data-dismiss="alert">Next</button>
				</div>
				<div class="steps-form" id="step2">
					<h2>Where would you like it delivered?</h2>
					<p>
						Please fill in the address of where you would like your items delivered.
					</p>
					
					<br>
					
						
					
					<label for="address" class=" control-label">Search for address</label>
					<input type="text" class="form-control" id="address" placeholder="" onFocus="geolocate()">
					<hr>

					<label for="street_number" class=" control-label">Address line 1</label>
					<input type="text" class="form-control" id="street_number" placeholder="">
		
					<label for="route" class=" control-label">Address line 2</label>
					<input type="text" class="form-control" id="route" placeholder="">
					
					<label for="locality" class=" control-label">City</label>
					<input type="text" class="form-control" id="locality" placeholder="">

					<label for="administrative_area_level_1" class=" control-label">State</label>
					<input type="text" class="form-control" id="administrative_area_level_1" placeholder="">

					<label for="country" class=" control-label">Country</label>
					<input type="text" class="form-control" id="country" placeholder="">

					<label for="postal_code" class=" control-label">Post code</label>
					<input type="text" class="form-control" id="postal_code" placeholder="">
					</form>
					<button type="button" id="to-qty" class="btn btn-primary" data-dismiss="alert">Back</button>

					<button type="button" id="to-delivery" class="btn btn-primary" data-dismiss="alert">Next</button>
				</div>
			</div>
			
		</div>
		<!-- Shopping cart overview -->
		<div class="col-xs-12 col-md-4 col-lg-4">
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
		</div>
		
	</div>
</div>

<div class="container">
	
</div>
	


</body>
<script type="text/javascript" src="/js/assets/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="/js/assets/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/products/products.js"></script>
<script type="text/javascript" src="/js/products/product-request.js"></script>
<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&libraries=places&callback=initAll" async defer></script>
</html>