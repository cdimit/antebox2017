@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>{{$product->name}} stores</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Stores listing this product</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the stores that currently stock this product.
									</p>
									<div class="table_scroll">
										<table id="all-products" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>Name</th>
													<th>Country</th>
													<th>City</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach($product->stores as $store)
												<tr>
													<td>
														{{$store->name}}
													</td>
													<td>
													{{$store->country}}
													</td>
													<td>
													{{$store->city}}
													</td>
													<td>
														<a href="/business/products/{{$product->id}}/stores/{{$store->id}}/remove" class="btn btn-danger"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Remove</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All stores from {{Auth::user()->profile->name}}</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Select a store from the list below, to add your product to.
									</p>
									<div class="table_scroll">
										<table id="all-stores" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>Name</th>
													<th>Country</th>
													<th>City</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach($stores as $store)
												<tr>
													<td>
														{{$store->name}}
													</td>
													<td>
													{{$store->country}}
													</td>
													<td>
													{{$store->city}}
													</td>
													<td>
														<a href="/business/products/{{$product->id}}/stores/{{$store->id}}/add" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> &nbsp; Add</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>


				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-stores').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
