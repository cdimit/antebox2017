@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Store products</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>



					<div class="row">
						<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Products listed in {{$store->name}} store</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are all the products assigned to your '<b>{{$store->name}}</b>' store.
									</p>
									<div class="table_scroll">
										<table id="products-in-store" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>Name</th>
													<th>Category</th>
													<th>Remove product from this store</th>
												</tr>
											</thead>
											<tbody>
												@foreach($store->products as $product)
												<tr>
													<td>
														{{$product->name}}
													</td>
													<td>
													{{$product->category->name}}
													</td>
													<td>
														<a href="/business/stores/{{$store->id}}/products/{{$product->id}}/remove" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Remove</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>

							<div class="x_panel">
								<div class="x_title">
									<h2>Products not belonging to {{$store->name}} store</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the stores associated with your company.
									</p>
									<div class="table_scroll">
										<table id="products-not-in-store" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>Name</th>
													<th>Category</th>
													<th>Add product to this store</th>
												</tr>
											</thead>
											<tbody>
												@foreach($products as $product)
												<tr>
													<td>
														{{$product->name}}
													</td>
													<td>
													{{$product->category->name}}
													</td>
													<td>
														<a href="/business/stores/{{$store->id}}/products/{{$product->id}}/add" class="btn btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Add</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<h3>Adding products to your store</h3>
							<p  class="text-muted font-13 m-b-30"> 
								In the first table, all the products from your company that belong to the <b>{{$store->name}}</b> store are listed. To remove a product from this store, press the orange remove button in the row of that product.
							</p>
							<p>
								In the second table, you can see all the products from your company that are currently not assigned to the <b>{{$store->name}}</b> store . Click the green 'Add' button next to any product to add it to this store.
							</p>
						</div>

					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#products-in-store').DataTable({});
			$('#products-not-in-store').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
