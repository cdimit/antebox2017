@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Stores</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All stores from {{Auth::user()->profile->name}}</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are all the stores associated with your company.
									</p>
									<div class="table_scroll">
										<table id="all-products" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>Name</th>
													<th>Products</th>
													<th></th>
													<th></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												@foreach($stores as $store)
												<tr>
													<td>
														{{$store->name}}
													</td>
													<td>
														{{$store->products->count()}}
													</td>
													<td>
														<a href="/business/stores/{{$store->id}}/products" class="btn btn-default btn-xs"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Add products to this store</a>
													</td>
													<td>
		                      	<a href="/business/stores/{{$store->id}}/edit" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit store</a>
													</td>
													<td>
		               					<a href="/business/stores/{{$store->id}}/delete" class="btn btn-xs btn-danger"><i class="fa fa-times-circle" aria-hidden="true"></i> Delete store</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>

									<a href="/business/stores/create" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add store</a>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-products').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
