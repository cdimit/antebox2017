	@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Stores</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-9 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Add a new store</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Add a new store for your company, to which you can assign products to.
									</p>
									<form class="form-horizontal" role="form" method="POST" action="/business/stores/create" >
									{{ csrf_field() }}

										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 col-xs-12 control-label">Name</label>
											<div class="col-md-6 col-xs-12">
												<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

												@if ($errors->has('name'))
												<span class="help-block">
												<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 col-xs-12 control-label">Phone</label>
											<div class="col-md-6 col-xs-12">
												<input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" autofocus>

												@if ($errors->has('phone'))
												<span class="help-block">
												<strong>{{ $errors->first('phone') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<hr>

										<div class="form-group">
											<label for="address_search" class="col-md-4 col-xs-12 control-label">Search for address</label>
											<div class="col-md-6 col-xs-12">
												<input type="text" class="form-control" id="address_search" placeholder="" onFocus="geolocate()">

											</div>
										</div>
										
										<hr>

										<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
											<label for="address" class="col-md-4 col-xs-12 control-label">Address</label>
											<div class="col-md-6 col-xs-12">
												<input id="route" type="text" class="form-control" value="{{ old('address') }}" name="address" required>
												<input name="" type="hidden" class="" id="street_number" placeholder="">
												
												@if ($errors->has('address'))
												<span class="help-block">
												<strong>{{ $errors->first('address') }}</strong>
												</span>
												@endif
											</div>
										</div>
										
										<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
											<label for="city" class="col-md-4 col-xs-12 control-label">City</label>
											<div class="col-md-6 col-xs-12">
												<input id="locality" type="text" class="form-control" name="city" value="{{ old('city') }}" required>

												@if ($errors->has('city'))
												<span class="help-block">
												<strong>{{ $errors->first('city') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
											<label for="state" class="col-md-4 col-xs-12 control-label">State</label>
											<div class="col-md-6 col-xs-12">
												<input id="administrative_area_level_1" type="text" class="form-control" name="state" value="{{ old('state') }}">

												@if ($errors->has('state'))
												<span class="help-block">
												<strong>{{ $errors->first('state') }}</strong>
												</span>
												@endif
											</div>
										</div>

										

										
										

										<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
											<label for="zip_code" class="col-md-4 col-xs-12 control-label">Postcode</label>
											<div class="col-md-6 col-xs-12">
												<input id="postal_code" type="text" class="form-control" name="zip_code" value="{{ old('zip_code') }}">

												@if ($errors->has('zip_code'))
												<span class="help-block">
												<strong>{{ $errors->first('zip_code') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
											<label for="country" class="col-md-4 col-xs-12 control-label">Country</label>
											<div class="col-md-6 col-xs-12">
												<input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}" required>

												@if ($errors->has('country'))
												<span class="help-block">
												<strong>{{ $errors->first('country') }}</strong>
												</span>
												@endif
											</div>
										</div>

										

										

										<div class="form-group">
											<div class="row">
												<div class="col-md-4"></div>
												<div class="col-md-6 col-xs-12 ">
													<div id="map-canvas" style="width:400px;height:300px;">
													</div>
												</div>
												
											</div>
										</div>

										<div class="form-group{{ $errors->has('map_x') ? ' has-error' : '' }}">
											<label for="map_x" class="col-md-4 col-xs-12 control-label">Latitude</label>

											<div class="col-md-6 col-xs-12 ">
												<input id="_map_x" type="text" class="form-control" name="_map_x" value="" disabled>
												<input id="map_x" type="hidden" class="form-control" name="map_x" value="" >

												@if ($errors->has('map_x'))
												<span class="help-block">
													<strong>{{ $errors->first('map_x') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('map_y') ? ' has-error' : '' }}">
											<label for="map_y" class="col-md-4 col-xs-12 control-label">Longititude</label>

											<div class="col-md-6 col-xs-12">
												<input id="_map_y" type="text" class="form-control" name="_map_y" value="" disabled>
												<input id="map_y" type="hidden" class="form-control" name="map_y" value="" >


												@if ($errors->has('map_y'))
												<span class="help-block">
													<strong>{{ $errors->first('map_y') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Add store
												</button>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')

	<script type="text/javascript" src="/js/gmaps_autocomplete.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&libraries=places&callback=initAutocomplete" async defer></script>
</body>
</html>
