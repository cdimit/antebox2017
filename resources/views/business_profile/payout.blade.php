@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>My business details</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Payout methods</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<form class="form-horizontal" role="form" method="POST" action="/account/settings/payout/update" >
										{{ csrf_field() }}

										<div class="form-group  {{ $errors->has('paypal') ? ' has-error' : '' }}">
											<label for="paypal" class="col-md-4 control-label">Paypal Account Email</label>

											<div class="col-md-6">
												<input type="email" class="form-control  " name="paypal" @if($user->payout('paypal')) value="{{$user->payout('paypal')->data}}" @endif>

												@if ($errors->has('paypal'))
													<span class="help-block">
														<strong>{{ $errors->first('paypal') }}</strong>
													</span>
												@endif
											</div>

										</div>

										<div class="form-group{{ $errors->has('iban') ? ' has-error' : '' }}">
											<label for="iban" class="col-md-4 control-label">IBAN Electronic Formatl</label>

											<div class="col-md-6">
												<input type="text" class="form-control" name="iban" placeholder="ex: CY17002001280000001200527600" @if($user->payout('iban')) value="{{$user->payout('iban')->data}}" @endif>

												@if ($errors->has('iban'))
													<span class="help-block">
														<strong>{{ $errors->first('iban') }}</strong>
													</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-6 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
													Update Payout Methods
												</button>
											</div>
										</div>
									</form>
									

								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Payout methods</h3>
							<p  class="text-muted font-13 m-b-30">
								These are the methods for receiving payouts.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')

</body>
</html>
