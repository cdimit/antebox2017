@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Themes</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										Choose a theme for your business
									</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Please choose the theme you would like for your business, and fill in the fields below for your storefront.
									</p>
									<form class="form-horizontal" role="form" method="POST" action="/business/themes/" enctype="multipart/form-data">
										{{ csrf_field() }}

										<div class="row">
											<div class="col-md-3">
												<label for="agnolotti" class="large-radio">
													<input class="large-radio" type="radio" name="theme" value='1' id="agnolotti" @if($fields->theme->id == '1')checked @endif}}> Agnolotti<br>
													<span>
														Suitable for businesses selling food items.
														<img src="/images/agnolotti.jpg" class="img-responsive" alt="">
													</span>
													<br>
													<a href="/demo/agnolotti" class="btn btn-default btn-xs">Try demo</a>
												</label>
											</div>
											<div class="col-md-3">
												<label for="vionnet" class="large-radio">
													<input class="large-radio" type="radio" name="theme" value='2' id="vionnet" @if($fields->theme->id == '2')checked @endif}}> Vionnet<br>
													<span>
														Modern, minimalistic theme for businesses in the fashion industry.
														<img src="/images/vionnet.jpg" class="img-responsive" alt="">
													</span>
													<br>
													<a href="/demo/vionnet" class="btn btn-default btn-xs">Try demo</a>
												</label>
											</div>
										</div>
										<div class="col-md-3">
												<label for="belgravia" class="large-radio">
													<input class="large-radio" type="radio" name="theme" value='4' id="belgravia" @if($fields->theme->id == '4')checked @endif}}> Belgravia<br>
													<span>
														Bold, classy and stylish theme ideal for the clothing industry!
														<img src="/images/belgravia.jpg" class="img-responsive" alt="">
													</span>
													<br>
													<a href="/demo/belgravia" class="btn btn-default btn-xs">Try demo</a>
												</label>
										</div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Change Theme
												</button>
											</div>
										</div>
									</form>

									<hr>

								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>
								Choosing a theme for your storefront
							</h3>
							<p  class="text-muted font-13 m-b-30">
								Use this page to choose the theme for your storefront.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')

</body>
</html>
