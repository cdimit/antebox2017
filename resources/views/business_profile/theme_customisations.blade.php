@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Themes</h2>
					 		<p class="text-muted font-13 m-b-30">
					 			Use this page to customise your selected theme.
					 		</p>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-9 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										Customise your theme
									</h2>

									<div class="clearfix"></div>
								</div>
								<p class="text-muted font-13 m-b-30">
									Complete the steps in the wizard below to customise your theme, and get up and running!
								</p>
								<div class="x_content">
									<div id="wizard" class="">
										<ul class="">
											<li>
												<a href="#step-1">
													Step 1 <br>
													<small>
														Slider images and text
													</small>
												</a>
											</li>
											<li>
												<a href="#step-2">
													Step 2 <br>
													<small>
														Homepage content
													</small>
												</a>
											</li>
											<li>
												<a href="#step-3">
													Step 3 <br>
													<small>
														About page
													</small>
												</a>
											</li>
										</ul>
										<div class="">
											<div id="step-1" class="step-content">

												<p class="text-muted font-13 m-b-30">
													This refers to the slider of 3 images on the home page of your storefront. Add three pictures to advertise your company or a certain product. Make it look nice by giving it a title and some words describing each picture in the slider.
												</p>


												<form class="form-horizontal" role="form" method="POST" action="/business/themes/customise/frontpageslider" enctype="multipart/form-data">
													{{ csrf_field() }}

													<div class="form-group{{ $errors->has('slider_1_img') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #1 image</label>
														<div class="col-md-6">
															<input id="slider_1_img" type="file" name="slider_1_img">
															<br>
															<img class="img-responsive col-md-4" src="/storage/{{$fields->slider_1_img}}" >
															@if ($errors->has('slider_1_img'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_1_img') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('slider_2_img') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #2 image</label>
														<div class="col-md-6">
															<input id="slider_2_img" type="file" name="slider_2_img">
															<br>
															<img class="img-responsive col-md-4" src="/storage/{{$fields->slider_2_img}}" >
															@if ($errors->has('slider_2_img'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_2_img') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('slider_3_img') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #3 image</label>
														<div class="col-md-6">
															<input id="slider_3_img" type="file" name="slider_3_img">
															<br>
															<img class="img-responsive col-md-4" src="/storage/{{$fields->slider_3_img}}" >
															@if ($errors->has('slider_3_img'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_3_img') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<hr>

													<div class="form-group{{ $errors->has('slider_1_heading') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #1 title</label>
														<div class="col-md-6">
															<input id="slider_1_heading" type="text" class="form-control" name="slider_1_heading" value="{{ $fields->slider_1_heading }}"    autofocus>

															@if ($errors->has('slider_1_heading'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_1_heading') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('slider_1_p') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #1 - Short description (10 words)</label>
														<div class="col-md-6">
															<input id="slider_1_p" type="text" class="form-control" name="slider_1_p" value="{{ $fields->slider_1_p }}"    autofocus>

															@if ($errors->has('slider_1_p'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_1_p') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<hr>

													<div class="form-group{{ $errors->has('slider_2_heading') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #2 title</label>
														<div class="col-md-6">
															<input id="slider_2_heading" type="text" class="form-control" name="slider_2_heading" value="{{ $fields->slider_2_heading }}"    autofocus>

															@if ($errors->has('slider_2_heading'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_2_heading') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('slider_2_p') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #2 - Short description (10 words)</label>
														<div class="col-md-6">
															<input id="slider_2_p" type="text" class="form-control" name="slider_2_p" value="{{ $fields->slider_2_p}}"    autofocus>

															@if ($errors->has('slider_2_p'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_2_p') }}</strong>
																</span>
															@endif
														</div>
													</div>
													<hr>

													<div class="form-group{{ $errors->has('slider_3_heading') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #3 title</label>
														<div class="col-md-6">
															<input id="slider_3_heading" type="text" class="form-control" name="slider_3_heading" value="{{ $fields->slider_3_heading }}"    autofocus>

															@if ($errors->has('slider_3_heading'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_3_heading') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('slider_3_p') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Slider #3 - Short description (10 words)</label>
														<div class="col-md-6">
															<input id="slider_3_p" type="text" class="form-control" name="slider_3_p" value="{{ $fields->slider_3_p }}"    autofocus>

															@if ($errors->has('slider_3_p'))
																<span class="help-block">
																	<strong>{{ $errors->first('slider_3_p') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-8 col-md-offset-4">
															<p class="text-muted font-13 m-b-30">
																Please ensure you save these fields, before pressing next.
															</p>
															<button type="submit" class="btn btn-primary">
															Save theme settings
															</button>
														</div>
													</div>
												</form>
											</div>
											<div id="step-2" class="step-content">

												<p class="text-muted font-13 m-b-30">
													The below fields refer to content found on the homepage.
												</p>

												<form class="form-horizontal" role="form" method="POST" action="/business/themes/customise/homepage" enctype="multipart/form-data">
													{{ csrf_field() }}
													<h4>
														Introductory text
													</h4>
													<p class="text-muted font-13 m-b-30">
														The below text appears just below the slider on the home page.
													</p>
													<div class="form-group{{ $errors->has('intro_heading') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Heading</label>
														<div class="col-md-6">
															<input id="intro_heading" type="text" class="form-control" name="intro_heading" value="{{ $fields->intro_heading }}" autofocus>
															<p class="help">
																Heading for introductory paragraph on home page.
															</p>

															@if ($errors->has('intro_heading'))
																<span class="help-block">
																	<strong>{{ $errors->first('intro_heading') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('intro') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Introductory paragraph</label>
														<div class="col-md-6">
															<textarea name="intro" id="intro" cols="30" rows="3">{{ $fields->intro }}</textarea>

															@if ($errors->has('intro'))
																<span class="help-block">
																	<strong>{{ $errors->first('intro') }}</strong>
																</span>
															@endif
														</div>
													</div>
													<hr>
													<h4>
														Featured ads
													</h4>
													<p class="text-muted font-13 m-b-30">
														These ads appear below the introductory paragraph on the homepage.
													</p>
													<div class="form-group{{ $errors->has('ad_1_img') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #1 image</label>
														<div class="col-md-6">
															<input id="ad_1_img" type="file" name="ad_1_img">
															<br>
															<img class="img-responsive col-md-4" src="/storage/{{$fields->ad_1_img}}" >
															@if ($errors->has('ad_1_img'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_1_img') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_2_img') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #2 image</label>
														<div class="col-md-6">
															<input id="ad_2_img" type="file" name="ad_2_img">
															<br>
															<img class="img-responsive col-md-4" src="/storage/{{$fields->ad_2_img}}" >
															@if ($errors->has('ad_2_img'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_2_img') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_3_img') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #3 image</label>
														<div class="col-md-6">
															<input id="ad_3_img" type="file" name="ad_3_img">
															<br>
															<img class="img-responsive col-md-4" src="/storage/{{$fields->ad_3_img}}" >
															@if ($errors->has('ad_3_img'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_3_img') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_1_text') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #1 text</label>
														<div class="col-md-6">
															<input id="ad_1_text" type="text" class="form-control" name="ad_1_text" value="{{ $fields->ad_1_text }}"  autofocus>

															@if ($errors->has('ad_1_text'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_1_text') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_2_text') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #2 text</label>
														<div class="col-md-6">
															<input id="ad_2_text" type="text" class="form-control" name="ad_2_text" value="{{ $fields->ad_2_text }}"    autofocus>

															@if ($errors->has('ad_2_text'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_2_text') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_3_text') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #3 text</label>
														<div class="col-md-6">
															<input id="ad_3_text" type="text" class="form-control" name="ad_3_text" value="{{ $fields->ad_3_text }}"    autofocus>

															@if ($errors->has('ad_3_text'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_3_text') }}</strong>
																</span>
															@endif
														</div>
													</div>


													<div class="form-group{{ $errors->has('ad_1_link') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #1 link</label>
														<div class="col-md-6">
															<input id="ad_1_link" type="text" class="form-control" name="ad_1_link" value="{{ $fields->ad_1_link }}"  autofocus>

															@if ($errors->has('ad_1_link'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_1_link') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_2_link') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #2 link</label>
														<div class="col-md-6">
															<input id="ad_2_link" type="text" class="form-control" name="ad_2_link" value="{{ $fields->ad_2_link }}"    autofocus>

															@if ($errors->has('ad_2_link'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_2_link') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('ad_3_link') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Featured ad #3 link</label>
														<div class="col-md-6">
															<input id="ad_3_link" type="text" class="form-control" name="ad_3_link" value="{{ $fields->ad_3_link }}"    autofocus>

															@if ($errors->has('ad_3_link'))
																<span class="help-block">
																	<strong>{{ $errors->first('ad_3_link') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<hr>
													<h4>
														Opening hours
													</h4>
													<p class="text-muted font-13 m-b-30">
														The opening hours paragraph is a short paragraph to describe your opening hours, which appears on the bottom of the home page.
													</p>
													<div class="form-group{{ $errors->has('openingclosing') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Opening times paragraph</label>
														<div class="col-md-6">

															<textarea name="openingclosing" id="openingclosing" value="" cols="40" rows="3">{{ $fields->openingclosing }}</textarea>

															@if ($errors->has('openingclosing'))
																<span class="help-block">
																	<strong>{{ $errors->first('openingclosing') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-8 col-md-offset-4">
															<p class="text-muted font-13 m-b-30">
																Please ensure you save these fields, before pressing next.
															</p>
															<button type="submit" class="btn btn-primary">
															Save theme settings
															</button>
														</div>
													</div>
												</form>
											</div>
											<div id="step-3" class="step-content">

												<p class="text-muted font-13 m-b-30">
													About page
												</p>

												<form class="form-horizontal" role="form" method="POST" action="/business/themes/customise/aboutpage" enctype="multipart/form-data">
													{{ csrf_field() }}

													<div class="form-group{{ $errors->has('about_text') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">About paragraph</label>
														<div class="col-md-6">
															<textarea name="about_text" id="about_text" cols="40" rows="3" value="">{{ $fields->about_text }}</textarea>

															@if ($errors->has('about_text'))
																<span class="help-block">
																	<strong>{{ $errors->first('about_text') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group{{ $errors->has('motto') ? ' has-error' : '' }}">
														<label for="name" class="col-md-4 control-label">Motto</label>
														<div class="col-md-6">
															<input id="motto" type="text" class="form-control" name="motto" value="{{ $fields->motto }}"    autofocus>

															@if ($errors->has('motto'))
																<span class="help-block">
																	<strong>{{ $errors->first('motto') }}</strong>
																</span>
															@endif
														</div>
													</div>

													<div class="form-group">
														<div class="col-md-8 col-md-offset-4">
															<p class="text-muted font-13 m-b-30">
																Please ensure you save these fields, before pressing next.
															</p>
															<button type="submit" class="btn btn-primary">
															Save theme settings
															</button>
														</div>
													</div>
												</form>
											</div>

										</div>
									</div>

								</div>
							</div>
						</div>

					</div>

				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.smartWizard.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// Initialize Smart Wizard
			$('#wizard').smartWizard({
				toolbarSettings: {
					 toolbarExtraButtons: [
						$('<button></button>').text('Finish')
					    .addClass('btn btn-info')
					    .on('click', function(){
							window.location.href = '/dashboard';
					    })]
				}
			});
		});
	</script>

</body>
</html>
