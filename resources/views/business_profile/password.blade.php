@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>My business details</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Change password</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									
									@if (session('error_password'))
										<div class="alert alert-danger">
											{{ session('error_password') }}
										</div>
									@endif

									@if (session('status_password'))
										<div class="alert alert-success">
											<strong>{{ session('status_password') }}</strong>
										</div>
									@endif

									<form class="form-horizontal" role="form" method="POST" action="/account/settings/password/update" >
									{{ csrf_field() }}

										<div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
											<label for="old_password" class="col-md-4 control-label">Old Password</label>

											<div class="col-md-6">
												<input id="old_password" type="password" class="form-control" name="old_password" required>

												@if ($errors->has('old_password'))
													<span class="help-block">
														<strong>{{ $errors->first('old_password') }}</strong>
													</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
											<label for="password" class="col-md-4 control-label">New Password</label>

											<div class="col-md-6">
												<input id="password" type="password" class="form-control" name="password" required>

												@if ($errors->has('password'))
													<span class="help-block">
														<strong>{{ $errors->first('password') }}</strong>
													</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<label for="password-confirm" class="col-md-4 control-label">Confirm New Password</label>

											<div class="col-md-6">
												<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
											</div>
										</div>


										<div class="form-group">
											<div class="col-md-6 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
													Change Password
												</button>
											</div>
										</div>
									</form>
	

								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Password</h3>
							<p  class="text-muted font-13 m-b-30">
								Ensure your password is set to a secure value, that cannot be guessed by others.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')

</body>
</html>
