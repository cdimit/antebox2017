@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>My business details</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Change email</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									@if (session('status_email'))
										<div class="alert alert-success">
											<strong>{{ session('status_email') }}</strong>
										</div>
									@endif

									<form class="form-horizontal" role="form" method="POST" action="/account/settings/email/update" >
										{{ csrf_field() }}

										<div class="form-group">
											<label class="col-md-4 control-label">Current Email</label>

											<div class="col-md-6">
												<input type="text" class="form-control" value="{{$user->email}}" readonly>
											</div>
										</div>

										<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
											<label for="email" class="col-md-4 control-label">New Email</label>

											<div class="col-md-6">
												<input id="email" type="text" class="form-control" name="email" required>

												@if ($errors->has('email'))
													<span class="help-block">
														<strong>{{ $errors->first('email') }}</strong>
													</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-6 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
													Change Email
												</button>
											</div>
										</div>
									</form>
									
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Email</h3>
							<p  class="text-muted font-13 m-b-30">
								Use this page to change your business email associated with your Shopguin account for loggin in.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')

</body>
</html>
