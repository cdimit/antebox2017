@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>My business details</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Edit details</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">

									<form class="form-horizontal" role="form" method="POST" action="/business/profile/edit" enctype="multipart/form-data">
									{{ csrf_field() }}

										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 col-xs-12 control-label">Business name</label>
											<div class="col-md-6 col-xs-12">
												<input id="name" type="text" class="form-control" name="name" value="{{ $business->name }}" required autofocus>

												@if ($errors->has('name'))
												<span class="help-block">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
											<label for="logo" class="col-md-4 col-xs-12 control-label">
												Logo
											</label>

											<div class="col-md-6 col-xs-12">
												<input id="logo" type="file" name="logo">
												<img class="img-responsive" src="/storage/{{$business->logo}}" >

												@if ($errors->has('logo'))
												<span class="help-block">
													<strong>{{ $errors->first('logo') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<hr>

										<div class="form-group">
											<label for="address_search" class="col-md-4 col-xs-12 control-label">Search for address</label>
											<div class="col-md-6 col-xs-12">
												<input type="text" class="form-control" id="address_search" placeholder="" onFocus="geolocate()">

											</div>
										</div>
										
										<hr>

										{{-- Address --}}

										<div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
											<label for="address1" class="col-md-4 col-xs-12 control-label">
												Address 1
											</label>

											<div class="col-md-6 col-xs-12">
												<input id="street_number" type="text" class="form-control" name="address1" value="{{ $business->address1 }}">

												@if ($errors->has('address1'))
												<span class="help-block">
												<strong>{{ $errors->first('address1') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
											<label for="address2" class="col-md-4 col-xs-12 control-label">
												Address 2
											</label>

											<div class="col-md-6 col-xs-12">
												<input id="route" type="text" class="form-control" value="{{ $business->address2 }}" name="address2" required>
												<input id="address2" type="hidden">

												@if ($errors->has('address2'))
												<span class="help-block">
												<strong>{{ $errors->first('address2') }}</strong>
												</span>
												@endif
											</div>
										</div>

										
										<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
											<label for="city" class="col-md-4 col-xs-12 control-label">
												City
											</label>

											<div class="col-md-6 col-xs-12">
												<input id="locality" type="text" class="form-control" name="city" value="{{ $business->city }}">

												@if ($errors->has('city'))
												<span class="help-block">
													<strong>{{ $errors->first('city') }}</strong>
												</span>
												@endif
											</div>
										</div>
										
					
										<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
											<label for="state" class="col-md-4 col-xs-12 control-label">
												State
											</label>

											<div class="col-md-6 col-xs-12">
												<input id="administrative_area_level_1" type="text" class="form-control" name="state" value="{{ $business->state }}">

												@if ($errors->has('state'))
												<span class="help-block">
												<strong>{{ $errors->first('state') }}</strong>
												</span>
												@endif
											</div>
										</div>

										

										<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
											<label for="zip_code" class="col-md-4 col-xs-12 control-label">
												Postal code
											</label>

											<div class="col-md-6 col-xs-12">
												<input id="postal_code" type="text" class="form-control" name="zip_code" value="{{ $business->zip_code }}">

												@if ($errors->has('zip_code'))
												<span class="help-block">
													<strong>{{ $errors->first('zip_code') }}</strong>
												</span>
												@endif
											</div>
										</div>
										
										<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
											<label for="country" class="col-md-4 col-xs-12 control-label">Country</label>
											<div class="col-md-3 col-xs-6">
												<input id="country" type="text" class="form-control"  disabled>

												@if ($errors->has('country'))
												<span class="help-block">
												<strong>{{ $errors->first('country') }}</strong>
												</span>
												@endif
											</div>
											<div class="col-md-3 col-xs-6">
												<input type="text" id="country_code" class="form-control" name="country_code" value="{{ $business->country }}" required>
												
											</div>
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-4"></div>
												<div class="col-md-6 col-xs-12 ">
													<div id="map-canvas" style="width:400px;height:300px;">
													</div>
												</div>

											</div>
										</div>

										<div class="form-group{{ $errors->has('map_x') ? ' has-error' : '' }}">
											<label for="map_x" class="col-md-4 col-xs-12 control-label">Latitude</label>

											<div class="col-md-6 col-xs-12 ">
												<input id="_map_x" type="text" class="form-control" name="_map_x" value="{{ $business->map_x }}" disabled>
												<input id="map_x" type="hidden" class="form-control" name="map_x" value="{{ $business->map_x }}" >

												@if ($errors->has('map_x'))
												<span class="help-block">
													<strong>{{ $errors->first('map_x') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('map_y') ? ' has-error' : '' }}">
											<label for="map_y" class="col-md-4 col-xs-12 control-label">Longititude</label>

											<div class="col-md-6 col-xs-12">
												<input id="_map_y" type="text" class="form-control" name="_map_y" value="{{ $business->map_y }}" disabled>
												<input id="map_y" type="hidden" class="form-control" name="map_y" value="{{ $business->map_y }}" >


												@if ($errors->has('map_y'))
												<span class="help-block">
													<strong>{{ $errors->first('map_y') }}</strong>
												</span>
												@endif
											</div>
										</div>


										<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
											<label for="phone" class="col-md-4 col-xs-12 control-label">Phone</label>

											<div class="col-md-6 col-xs-12">
												<input id="phone" type="text" class="form-control" name="phone" value="{{ $business->phone }}">

												@if ($errors->has('phone'))
												<span class="help-block">
													<strong>{{ $errors->first('phone') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<hr>

										<div class="form-group{{ $errors->has('since') ? ' has-error' : '' }}">
											<label for="since" class="col-md-4 col-xs-12 control-label">Since (year)</label>

											<div class="col-md-6 col-xs-12">
												<input id="since" type="text" class="form-control" name="since" value="{{ $business->since }}">

												@if ($errors->has('since'))
												<span class="help-block">
												<strong>{{ $errors->first('since') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
													Update profile
												</button>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Editting your profile</h3>
							<p  class="text-muted font-13 m-b-30">
								Please ensure you fill in your company's country, website and address.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script src="/js/assets/countrySelect.min.js"></script>
	<script type="text/javascript" src="/js/gmaps_autocomplete.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&libraries=places&callback=initAutocomplete" async defer></script>

	
</body>
</html>
