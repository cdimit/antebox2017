<div class="header">
	<div class="brand-name text-center">
		<h1>
			Agnolotti
		</h1>
		<div class="sub-title text-center">
			True Italian Food
		</div>
	</div>
</div>
<div class="menu">
	<div class="container">
		<div class="nav-menu">
			<ul>
				<li><a href="">Home</a></li>
				<li><a href="">Products</a></li>
				<li><a href="">Categories</a></li>
				<li><a href="">Stores</a></li>
				<li><a href="">About us</a></li>
			</ul>
		</div>
		
	</div>
</div>
<div id="main-slider" class="main-slider owl-carousel owl-theme">
	<div class="item">
		<img src="https://source.unsplash.com/jpkfc5_d-DI/1600x900">
	</div>
	<div class="item">
		<img src="https://source.unsplash.com/KK1hlAI2lgE/1600x900">
	</div>
</div>

