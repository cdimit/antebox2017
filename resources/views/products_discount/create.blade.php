@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Products</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Add discount to product</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Please choose which product you would like to add a discount to.
									</p>
									<form class="form-horizontal" role="form" method="POST" action="/business/discount/create">
									{{ csrf_field() }}


										<div class="form-group{{ $errors->has('product_id') ? ' has-error' : '' }}">
											<label for="product_id" class="col-md-4 control-label">Product</label>
											<div class="col-md-6">
												<select name="product_id" class="form-control">
													@foreach($products as $product)
														<option value="{{$product->id}}">{{$product->name}} | {{$product->price}}</option>
													@endforeach
												</select>
												@if ($errors->has('product_id'))
													<span class="help-block">
														<strong>{{ $errors->first('product_id') }}</strong>
													</span>
												@endif
											</div>
										</div>
									
										<hr>
										<p class="text-muted font-13 m-b-30">
											Please add the discount below:
										</p>

										<div class="form-group{{ $errors->has('off_method') ? ' has-error' : '' }}">
											<label for="off_method" class="col-md-4 control-label">
												Discount method
											</label>
											<div class="col-md-6">
												<select name="off_method" class="form-control">
													<option value="percent">Percentage (%)</option>
													<option value="amount">Specified amount</option>
												</select>

												@if ($errors->has('off_method'))
												<span class="help-block">
												<strong>{{ $errors->first('off_method') }}</strong>
												</span>
												@endif
											</div>
										</div>


										<div class="form-group{{ $errors->has('value') ? ' has-error' : '' }}">
											<label for="value" class="col-md-4 control-label">Discount value</label>
											<div class="col-md-6">
												<input id="value" type="text" class="form-control" name="value" value="{{ old('value') }}" required autofocus>

												@if ($errors->has('value'))
												<span class="help-block">
													<strong>{{ $errors->first('value') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('valid_until') ? ' has-error' : '' }}">
											<label for="valid_until" class="col-md-4 control-label">Valid Until (Date)</label>
											<div class="col-md-6">
												<input id="valid_until" name="valid_until" value="{{ old('valid_until') }}" autofocus type="text" class="datepicker" id="date" style="width: 200px;">

												@if ($errors->has('valid_until'))
												<span class="help-block">
													<strong>{{ $errors->first('valid_until') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Create Discount
												</button>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Adding discounts to products</h3>
							<p  class="text-muted font-13 m-b-30">
								Use this page to add a discount to your products. You can choose whether your discount is by percentage, or by value, and also add an expiry for your discount.
							</p>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script type="text/javascript">
		$(function(){
			
			$('.datepicker').datepicker({
				format: 'yyyy-mm-dd'
			});
		});

	</script>
</body>
</html>
