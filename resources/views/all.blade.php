@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-shopping-cart"></i> My shopping carts
		</h1>
		<p class="s-13em">
			Shopping carts work differently in Shopguin. For every company that sells on Shopguin, you get to make a shopping cart specifically with them. You can then individually request a deliverer or conventional delivery method, for the delivery of your items. If you need some more help, click here.
		</p>
		<div class="row">
			<div class="col-lg-9">
				<div class="row">
					@foreach($shopping_carts as $cart)
						<div class="col-md-6">
							<div class="shopping-cart-panel box-shadow">
								<div class="shopping-cart-heading">
									<div class="row">
										<div class="col-md-10 col-xs-10">
											{{$cart->business->name}} ({{$cart->items->count()}})
										</div>
										<div class="col-md-2 col-xs-2 text-right">
											<a href="/shopping_cart/{{$cart->id}}/remove" class="c-red">
												<i class="icon icon-rubbish-bin"></i>

											</a>
										</div>
									</div>
								</div>
								<div class="shopping-cart-body">
									<ul class="products-as-list">
										@foreach($cart->items as $item)
											<li>
												<div class="row">
													<div class="col-md-3 col-xs-3">
														<img class="img-responsive" src="https://placeimg.com/790/640/tech">
													</div>
													<div class="col-md-9 col-xs-9">
														<div class="row">
															<div class="col-md-10 col-xs-9">
																<h4>
																	{{$item->product->name}}
																</h4>
																<h6>
																	<b>Qty:</b> {{$item->qty}} | € {{$item->price}}
																</h6>

															</div>
															<div class="col-md-2 col-xs-3 text-right">
																<a href="/item/{{$item->id}}/remove" style="font-size:1.2em; margin-right:10px;">
																	<i class="fa fa-times-circle fa-1x " aria-hidden="true"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</li>

										@endforeach
									</ul>
									<div class="row">
										<div class="col-md-9 col-xs-7">
											<h3 class="" style="margin:1px;">
												Total:
												<span class="c-black">
													€{{$cart->price}}
												</span>
												&nbsp; &nbsp;

											</h3>
										</div>
										<div class="col-md-3 col-xs-5">
											<a href="/client/request/{{$cart->id}}/create" class="btn btn-primary" style="display: inline-block;">
												Request
												<i class="icon icon-right-chevron"></i>
											</a>

										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="col-lg-3 side-info">
				<h3>
					Instructions
				</h3>
				<p class="s-13em">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</p>
			</div>
		</div>
	</div>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection
