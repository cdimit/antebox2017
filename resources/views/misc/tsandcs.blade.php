@extends('layouts.boilerplate')

{{-- Head tags --}}
@section('head')
	<title>Terms and Conditions - Shopguin</title>
	@include('partials.head')
	{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css"> --}}

@endsection

{{-- OpenGraph tags --}}
@section('og-scripts')
@endsection

{{-- Head scripts --}}
@section('head-scripts')
@endsection

{{-- Navbar --}}
@section('navbar')
	@include('partials.navbar-slim')
@endsection

{{-- Body --}}
@section('body')
<div class="business">
	<section >
		<div class="container text-center">
			<h1>
				Terms and Conditions
			</h1>
			
		</div>

	</section>
	<section>
		<div class="container">
			<div class="col-md-8 col-md-offset-2">
				<p>
					Shopguin Ltd (company number) whose registered office is at:  "Shopguin"; and
					"Seller".
				</p>
				<h3>
					Background
				</h3>
				<p>
					Shopguin is the owner and operator of Shopguin.com, a platform offering a complete solution for online selling. The Seller is the owner of a premium brand.  The parties wish to work together under the Conditions set out here to develop and maximize the sales of the brand on shopguin.com, develop the brand’s image, facilitate and encourage the growth of both parties.
				</p>
				<h3>
					1.	INTRODUCTION
				</h3>
				<p>
					These terms and conditions (the “Conditions”) apply between you (the “Seller”) and Shopguin Limited (“Shopguin”“we”, “us”), in connection with the provision of the Service to you by us.
					Shopguin shall be entitled to amend the Conditions or the Charges or both of them from time to time by providing you with reasonable prior written notice. 
				</p>
					2.	DEFINITIONS AND INTERPRETATION
					In these Conditions, the following words shall have the following meanings only and shall not affect the interpretation or construction of the Conditions:
					 "Shopguin Website" means http://www.shopguin.com or such other worldwide web address that Shopguin in its sole discretion selects as a replacement;

					“Product Page” means the particular web page on which an individual Product is displayed and the relevant information relating to that Product is provided;

					"Charges" means the charges detailed in clause 13 of these Conditions and as may be adjusted and notified to you by Shopguin in writing;

					"CMS" means the content management system provided by Shopguin to each Seller for management of its interaction with Shopguin and the Shopguin Website;

					"Conditions" means these terms and conditions and any document referred to in them, or any amended version of them brought into effect from time to time in accordance with these Conditions;

					“Confidential Information” means any information that would be regarded as confidential by a reasonable business person relating to the business, affairs, customers, clients, suppliers, plans, operations, processes, product information, know-how, designs, trade secrets or software of either party;

					"Customer" means a person(s), firm or company who enters into or is invited to enter into any transaction to purchases Product(s) from the Seller through the Site;

					"Customer Terms" means the terms and conditions relating to a Customer a copy of which is attached;

					 "IPR" means all the intellectual property rights conferred by the law of any country or jurisdiction in the world (including by statute) as amended or re-enacted (by common law, civil law, equity or otherwise) in relation to any invention, discovery, literary work, dramatic work, musical work, artistic work, copyright, database, trade mark, service mark, design (whether two dimensional or three dimensional), patents, semiconductor topography, confidential information, know-how, trade secret, and in each case whether or not it has been reduced to a material form, and howsoever it may be recorded, stored or embodied (including in an electronic or transient medium), including all applications for such rights as well as all extensions and renewals of such rights;

					"Site" means the online marketplace provided by Shopguin through the Shopguin Website to facilitate the promotion and sale of Sellers' Products;

					"Policies" means any policy (including any guides relating to content and style) which may be notified and made available to the Seller by Shopguin from time to time;

					"Products" means the goods that Sellers wish to promote and sell through the Site;

					"Renewal Date" means twelve months from the commencement of these Conditions in accordance with Clause 3, or such other period as may be advised by Shopguin in writing, and each anniversary thereafter;

					"Returns & Refunds Procedures" means the procedures set out in Clause 8 (Returns and Refunds) of these Conditions or as may be updated by Shopguin from time to time and displayed within the Customer Terms;

					"Seller Information" means information, data or content provided by the Seller in any form or medium, whether or not such information is owned by the Seller, about the Seller or the Products, given by the Seller to Shopguin for whatever purpose, whether directly or on the Seller’s behalf;

					"Service" means the website Shopguin.com and other services provided by Shopguin, as further described in these Conditions;

					"Single Commission Fee percentage" is X% and “Single Commission Fee” has the meaning ascribed to it in Clause 13.2 of these Conditions;

					"Software" means any software installed by or on behalf of Shopguin that permits Sellers to access and trade through the Site;

					"Term" means a period of twelve months, unless another period is agreed in writing by Shopguin, from either (a) the date when the contract is concluded between Shopguin and the Seller in accordance with Clause 3 or (b) a subsequent Renewal Date;

					"Value Added Tax" means value added, sales or services tax, or any similar tax imposed in any jurisdiction;

					"Virus" means any computer virus, macro virus, trojan horse, worm or anything else designed to interfere with, interrupt or disrupt the normal operating procedures of a computer or network, or to intercept or access without authority or expropriate any system, information or data;

					“Working Day” means any day other than a Saturday or Sunday on which banks are open for business in London.

					The headings to clauses are inserted for convenience only and shall not affect the interpretation or construction of these Conditions.
					Words expressed in the singular shall include the plural and vice versa. Words referring to a particular gender include every gender.
					References to a person include an individual, company, corporation, firm or partnership.
					References to any statute or statutory provision shall include:
					2.1	any subordinate legislation made under it;
					2.2	any provision which it has modified or re-enacted (whether with or without modification); and
					2.3	any provision which subsequently supersedes it or re-enacts it (whether with or without modification).
					All references in these Conditions to clauses are to the clauses in these Conditions unless otherwise stated.  
					References to the words "include", "includes", "including", "in particular" or any similar words do not limit the words proceeding or following.
					In this agreement, a reference to "writing" includes typing, printing, lithography, photography, fascimile, email and other modes of representing or reproducing words in a visible form.
					3.	TERM AND TERMINATION
					3.1	The Term of these Conditions (the “Term”) shall commence on the date of these Conditions.
					3.2	Without prejudice to either party's right to terminate the Conditions under the remainder of this clause 3, these Conditions shall continue in force unless and until either party notifies the other in writing at least 30 days prior to the expiry of the current Term.
					3.3	Shopguin may immediately suspend provision of the Service or terminate the Conditions without liability to Shopguin by notifying the Seller in writing if:
					3.3.1	the Seller commits a material breach of the Conditions (including a material breach of any of the Policies) and, if capable of remedy, fails to remedy the breach within fourteen (14) days of a written notice to do so; 
					3.3.2	the Seller fails to pay any Charges payable to Shopguin within seven working days of its due date for payment under these Conditions;
					3.3.3	the Seller is the subject of a bankruptcy order, or becomes insolvent, or makes any arrangement or composition with or assignment for the benefit of its creditors, or goes into voluntary liquidation (otherwise than for the purposes of reconstruction or amalgamation) or compulsory liquidation, or a receiver or administrator is appointed over its assets, or if the equivalent of any such events under the laws of any relevant jurisdiction occurs to the Seller; or in Shopguin's sole discretion, a Seller's eligibility or suitability to be listed on the Site, or otherwise receive the Service, changes.
					3.4	Either party may at any time terminate the Conditions by giving the other not less than 30 days’ written notice. 
					3.5	Notwithstanding any such termination or suspension in accordance with the foregoing clause:
					3.5.1	the Seller shall pay Shopguin all Charges due up to and including the date of suspension or termination; and
					3.5.2	termination of this agreement shall not affect any accrued rights or liabilities of either party nor shall it affect the coming into force, or the continuance in force, of any provision hereof which is expressly or by implication intended to come into or continue in force after such termination.
					4.	SHOPGUIN’S OBLIGATIONS
					4.1	Following commencement of the Term, Shopguin will:
					4.1.1	work closely and in good faith with the Seller, and use all reasonable endeavours to position and manage the online placement of the Seller’s brand and products, make a complementary and relevant product selection around the placement of the Seller’s products on the Shopguin Website, promote the Brand and assist with its marketing and facilitate and encourage the sale of the Products via the Shopguin Website,
					4.1.2	provide the Service with reasonable skill and care; and
					4.1.3	use reasonable endeavours to restore any faults in the Service as soon as reasonably practicable. The Seller acknowledges that the transmission of information via the internet is not completely secure, there is always a risk that communications by electronic means may not reach their intended destination, or may do so much later than intended, for reasons outside Shopguin's control, and that it is technically impossible to provide the Service entirely free of fault at all times.
					4.2	Shopguin reserves the right to revise or alter the Service at any time. Any variation in the Service will be subject to the Conditions.
					4.3	The Site provides a platform to allow Sellers to offer and sell their Products directly to Customers. In doing so, the Seller authorises and appoints Shopguin as its commercial agent to directly negotiate and/or conclude the sale and/or purchase of Products between the Seller and Customers via the Shopguin Services and Shopguin accepts this appointment on the terms of these Conditions. As part of this process:
					4.3.1	any contract to sell and buy Products is made only between the Seller and Customer concerned and Shopguin is not a party to any such contract;
					4.3.2	Shopguin facilitates the negotiation of the sale of Products between Customers and Sellers through the use of the Shopguin Website, together with the Shopguin Services that contribute to increasing the Seller’s goodwill, promote the Seller’s Products and generally encourage Customers to place orders with Sellers.
					4.3.3	Products offered for sale through the Shopguin Website are neither owned nor come into the possession of Shopguin at any time.
					4.4	The Seller hereby acknowledges that Shopguin has sole and complete discretion whether to invite or select other prospective Sellers to subscribe to use of the Service.

					5.	SELLER’S OBLIGATIONS 
					5.1	SECURITY
					5.1.1	The Seller:
					5.1.1.1	is responsible for the security and proper use of all passwords, or other security devices used in connection with the use of the Shopguin Website and shall ensure that they remain confidential, secure, used properly and not disclosed to unauthorised third parties;
					5.1.1.2	shall inform Shopguin immediately if there is any reason to believe that a password or any other security device has or is likely to become known to someone not authorised to use it, or is being or is likely to be used in an unauthorised way;
					5.1.1.3	shall ensure that the CMS and its content remains entirely confidential, and that no other person beside those in the Seller's employment has sight of the CMS or any of its content.
					5.1.2	Shopguin reserves the right:
					5.1.2.1	to suspend access to the Service if at any time Shopguin considers that there is or is likely to be a breach of security, in which event Shopguin will notify the Seller of the suspension and any steps to be taken by it as soon as reasonably practicable; and
					5.1.2.2	to require the Seller to change any or all of the passwords used by the Seller in connection with the provision of the Service, in which event Shopguin will notify the Seller of the requirement to change passwords and any further steps to be taken by the Seller as soon as reasonably practicable.
					5.1.3	The Seller hereby undertakes to ensure that it has up-to-date and virus free software and hardware for the purposes of interacting with the Site and that any information submitted to Shopguin and the Site is free from Viruses.
					5.1.4	Shopguin has no responsibility for the provision, support and maintenance of any of the Seller's hardware or software used to provide the Seller with access to the internet or the Shopguin Website, or any related hardware or software (including any IP router, proxy server, firewall or anti-virus software), the responsibility for which shall remain exclusively with the Seller.
					5.2	SELLER’S PRODUCT AND BRAND PAGES
					5.2.1	Shopguin has absolute discretion as to:
					5.2.1.1	the look, feel and content of the Shopguin Website (including all Product and Brand Pages);
					5.2.1.2	the inclusion, positioning, content, location and all other presentation of Seller Information (including in Shopguin’s sole discretion the right to remove any Seller Information from the Shopguin Website at any time during the Term); and
					5.2.1.3	the Products on the Shopguin Website (including in Shopguin’s sole discretion the right to remove any Product from the Shopguin Website, or not allow a Product to be for sale on the Shopguin Website).
					5.2.2	The Seller shall at all times comply with Shopguin policies as regards the content of the Shopguin Website and their Product Page as it may be updated from time to time, and shall work with Shopguin to ensure that the Product Page maintains a high standard of presentation including in relation to the form and content of copy and product imagery;
					5.2.3	Without prejudice to the Seller's obligation to comply with any Policies, the Seller undertakes and agrees that none of its Seller Information nor any of the Seller's activities or use of the Shopguin Website will:
					5.2.3.1	be false, inaccurate or misleading;
					5.2.3.2	be offensive, indecent, obscene, pornographic, menacing, abusive or defamatory;
					5.2.3.3	be in breach of any applicable law or regulation;
					5.2.3.4	adversely affect the reputation of Shopguin or the Shopguin brand;
					5.2.3.5	create, or be likely to create, liability for Shopguin or cause Shopguin to lose (in whole or in part) the services of its internet service or other suppliers;
					5.2.3.6	contain any Virus; and
					5.2.3.7	cause the Shopguin Website or its functionality to be interrupted, damaged or impaired in any way.
					5.2.4	The Seller shall:
					5.2.4.1	Provide Shopguin at all times with up-to-date, accurate and relevant information about the Products to enable Shopguin to develop and maintain the Product Page, including where applicable, 
					a.	where a Product is made-to-order, or a personalised or specially-made Product, 
					b.	where a Product requires a Customer approval of proof prior to its production, or otherwise cannot be cancelled;
					c.	estimated Product delivery times, 
					d.	Product postage and packing costs (both national and international).
					5.2.5	Stock Information
					5.2.5.1	The Seller shall ensure stock is available to fill Customer orders and if stock availability is low or compromised, is obliged to inform Shopguin immediately so as to allow Customer communication and avoid backorders.
					5.2.5.2	The Seller must update such stock availability regularly using the link provided by Shopguin.
					5.2.5.3	Once the final piece of stock of any Product has been sold and will no longer be available, the Seller must notify Shopguin immediately using the link provided by Shopguin.
					5.2.5.4	If a Customer places an order for an item which is in fact out of stock and the Seller has not so notified Shopguin in advance of the order, and the Customer consequently requires a refund, then the Seller may be charged the Single Commission Fee on that order by Shopguin.
					5.2.6	Pricing

					5.2.6.1	The Seller must notify Shopguin of its recommended retail prices and, In addition, postage and packing charges.
					5.2.6.2	If the Seller is VAT registered, the Seller should notify Shopguin of the set  VAT rate at the level which is currently in force in the UK with respect to the Seller's Products.
					5.2.6.3	The Seller is solely responsible for ensuring that it fully complies with current VAT regulations and accounting for VAT correctly.
					5.2.6.4	The Seller agrees that it will not price its Products at a higher level than they are priced either on its own website or on any other sales channel, without Shopguin’s prior written consent.
					5.2.6.5	Shopguin reserves the right to apply discounts to the price of the Seller’s Products at the point of sale for promotional or marketing purposes at its own discretion.
					5.2.7	General
					5.2.7.1	The Seller will ensure that a link to the Shopguin Website is incorporated into the Seller’s website as reasonably required by Shopguin. This link cannot be a “no-follow” link.
					5.2.7.2	The Seller will in good faith use best endeavours to comply and co-operate with Shopguin’s activities in promoting, selling or marketing the Seller’s products whether on the Shopguin Website or elsewhere. 
					5.3	COMPLIANCE WITH LAWS

					5.3.1	The Seller shall comply at all times with all applicable laws, regulations (including product safety and product marking laws and regulations) and Trading Standards requirements in respect of the manufacture, packaging, marking, certification (including, without limitation, CE marking) and delivery of the Products it sells.
					5.3.2	Where required by applicable laws and regulations, appropriate instructions shall be included with the Product to ensure the safe use of the Products.
					5.3.3	Products promoted on the Shopguin Website that are perishable or edible, and cosmetics designed for topical application to the skin, may not be delivered to any address outside of the UK.
					5.3.4	The Seller shall inform Shopguin as soon as reasonably practicable upon becoming aware of any claim against Shopguin or the Seller arising out of or in connection with any defect in its Products, or any failure by the Seller to ensure that the Products are appropriately marked or certified in accordance with applicable laws or regulations.
					5.3.5	The Seller shall indemnify and hold harmless Shopguin against any and all liabilities, costs, expenses, fines, damages and losses (including any direct, indirect or consequential losses) it incurs in connection with any claim envisaged under this Clause 5.3 or paid or agreed to be paid by Shopguin in settlement of the claim and all legal or other expenses incurred by Shopguin in or about the defence or settlement of the claim. Shopguin shall notify the Seller in writing as soon as practicable after becoming aware of the claim.
					5.3.6	The Seller shall maintain at all times, at its own expense and with reputable insurers appropriate insurance in relation to its business. The Seller shall, upon written request from Shopguin, provide Shopguin with any information it reasonably requires concerning the scope of such insurance together with any relevant certificates of currency.
					5.3.7	The Seller shall comply with Shopguin' reasonable instructions relating to any product recall and in any event Shopguin reserves the right to take immediate and exclusive conduct of the product recall on notice to the Seller, in which case the Seller shall give such assistance as Shopguin may reasonably require.
					5.3.8	The Seller shall maintain appropriate, up to date and accurate records to enable the immediate recall of any Products.
					5.4	SUSPENSION OF SERVICE
					Shopguin may immediately suspend or terminate the Seller's subscription and use of the Service in the event Shopguin reasonably believes or suspects that any Seller Information does not comply with the provisions of this clause 5.
					6.	EXCLUSIVITY
					The Seller agrees that for the Term of these Conditions, it will sell the Products on the Shopguin Website only, and not through any other online or offline channel in the UK, excluding only the Seller’s own website (if applicable).


					7.	 ORDERS AND YOUR RELATIONSHIP WITH THE CUSTOMER
					7.1	THE CONTRACT OF SALE
					7.1.1	The Seller acknowledges and agrees that following acceptance of an order through the CMS, such an acceptance is also deemed to be an irrevocable instruction by the Seller to Shopguin to conclude a contract of sale between the Seller and the Customer. The contract of sale between the Seller and the Customer is concluded when Shopguin (acting as the commercial agent of the Seller) sends a shipping confirmation email to the Customer, and Shopguin has no responsibility for the performance of any such contract.
					7.1.2	The Seller acknowledges and agrees that the terms and conditions relating to any such contract shall comprise of the Customer Terms, the email confirmation relating to the Customer's order and the applicable details on the relevant Product Page. The Seller agrees to be bound by all such provisions.
					7.2	PROCESSING CUSTOMER ORDERS
					7.2.1	Shopguin shall notify the Seller by email of any order. The Seller acknowledges that Shopguin does not warrant the reliability of email communications. 
					7.2.2	Following receipt of such notification the Seller shall, within a maximum of two (2) Working Days, and as a matter of best practice within twenty four (24) hours, confirm its acceptance or rejection of each and every order, using the CMS, and confirm an estimated dispatch date. The Seller shall use its best endeavours to accept every order.
					7.2.3	Following acceptance of an order through the CMS, the Seller shall:
					7.2.4	fulfil the Customer order as soon as reasonably practicable and in any event within two (2) Working Days of receiving the order and within the estimated delivery times provided by the Seller to Shopguin;
					7.2.5	once the Customer pays for the order, an automated confirmation email will be sent to the Customer. Then, once the Seller dispatches the item and updates Shopguin’s CMS with the parcel tracking ID, the Customer will receive an automated shipping details confirmation email from Shopguin with tracking ID and estimated delivery times;
					7.2.6	include with all orders the appropriate Shopguin packing slip, and such additional documentation or material as may be required and/or provided by Shopguin.
					7.2.7	confirm shipment by updating the CMS with the relevant tracking number. The Customer will then automatically be notified of shipment and tracking number
					7.3	COMMUNICATION WITH CUSTOMERS
					7.3.1	All communications with the Customer other than those specifically required by these Conditions shall be managed by Shopguin, including shipment confirmation, product delivery information, returns and after-sales services.
					7.3.2	 If for any reason the Seller is required to communicate with the Customer, then The Seller shall ensure that any and all communication with any Customer shall:
					7.3.2.1	be solely for the purposes of processing and/or progressing a Customer order as required by these Conditions; 
					7.3.2.2	be approved in advance by Shopguin; 
					7.3.2.3	not include any reference to the Seller's own website, email address, other correspondence address or any other promotion of services outside those offered through or by Shopguin.
					7.3.3	The obligations under this Clause 7.3 shall include any material included with the dispatch of a Customer’s order, but shall exclude swing tickets and sewn on labels. 
					7.3.4	Any breach of these Clauses 7.3.1 or 7.3.2 shall constitute a material breach of these Conditions and, further, may constitute a breach of data protection legislation laws.
					8.	RETURNS AND REFUNDS
					8.1	A Customer may request a refund or return for any Product by completing the requisite form on the Shopguin website within 14 days of receipt of the Product.  Shopguin will ensure that the completion of this form will trigger the sending of an automated notification through the CMS to both Shopguin and the Seller.
					8.2	Upon receipt of the returned Product, and in any event within 5 days of its receipt, the Seller will confirm its approval of the return to Shopguin by email.
					8.3	Once the Seller has confirmed the return, Shopguin will also approve such refund and initiate the refund with the Customer directly in accordance with Returns & Refunds Procedures, as displayed on the Shopguin Website from time to time.
					8.4	the Seller must only process returns, and refunds will only be issued by Shopguin through the payment platform. Refunds will not be issued by any other means. 
					8.5	The Seller and Shopguin shall work together to ensure that returns and refunds to the Customer shall be processed in accordance with the following terms and conditions:
					8.5.1	by law, the Customer may not cancel, return or obtain a refund in relation to the following Products, unless such Product is faulty:
					8.5.2	made-to-order or personalised items that are specifically made to a Customer's specification, except where the Customer has chosen items from pre-determined upgrade options or standard off-the-shelf components. Shopguin shall determine in its sole discretion as to whether a Product is or is not a made-to-order or personalized item. For the avoidance of doubt, items with hygiene seals are a Cancellable Product unless they are personalised items;
					8.5.3	perishable items, including food and flowers;
					8.5.4	audio or video recordings or computer software which a Customer has unsealed;
					8.5.5	newspapers, periodicals or magazines; and
					8.5.6	items that by their nature cannot be returned, such as where it is physically impossible to return items or where items cannot be restored to the same physical state as they were supplied,
					8.5.7	the Seller shall provide Customers with a 14 days period from the receipt of a Cancellable Product in which Customers can cancel their order and return the Product to the Seller and obtain a full refund, such refund to be issued by Shopguin in accordance with the terms of this clause. if the Customer requests a refund for a product (which is not prohibited by the terms of clause 8.5 above) then such refund shall be processed by Shopguin once the returned item has been accepted by the Seller in line with paragraph 8.A.j below) as soon as possible and in any case no later than:
					8.5.7.1	14 days after the day the returned product is received by the Seller; or
					8.5.7.2	If earlier, 14 days after the date the Customer provides evidence that they have returned the product to the Seller
					8.5.8	if a Customer rejects a Product because it is faulty, the parties shall follow the relevant provisions of the ‘Returns & Refunds Policy’, as may be updated from time to time;
					8.5.9	 the Seller shall provide an address in the United Kingdom or European Union, to which a Customer may return a Cancellable Product.
					9.	SHIPPING AND DELIVERY 
					9.1	The Seller agrees to provide Shopguin with the following information:
					9.1.1	the registered courier company which the Seller intends to use for shipping to the Products to the Customer;
					9.1.2	the estimated shipping times for each item to be shipped to Customers to the UK and to other countries worldwide;
					9.1.3	the shipping charges payable in relation to each country for each item;
					9.1.4	any changes to the above charges promptly upon such change.
					9.2	The Seller agrees to pay all shipping costs for shipping Products to Customers within the UK and worldwide.
					9.3	Shopguin agrees to refund the Seller for the costs of shipping products outside the UK, such refund to be made at the time of payment by Shopguin to the Seller under Clause 13 below.
					9.4	For the avoidance of doubt, a Customer will not be charged for shipping within the UK.
					9.5	 Shopguin will pay the cost of a UK Customer’s return of a Product to the Seller, unless such return is, in Shopguin’s reasonable opinion, by reason of the fault of the Seller or its Product (such as damaged, defective or incorrect Product.), in which case the cost of the Customer return will be added to the invoice and charged to the Seller during the relevant Payment cycle. 
					10.	YOUR USE OF THE CMS
					10.1	The Seller shall not:
					10.1.1	use the CMS or Shopguin Website beyond the scope of use set out in these Conditions;
					10.1.2	access the CMS unlawfully, modify or make derivative works based on the CMS nor attempt to reverse engineer or access the CMS with the intention of creating a competitive product or service nor to copy or build any concepts, features, functions or graphics based on the CMS. The Seller acknowledges that damages may not provide an adequate remedy for breach of this clause and that Shopguin shall be entitled to seek injunctive relief to prevent the occurrence or continuance of any alleged breach of this clause.
					10.2	The Seller fully acknowledges that the IPR in the CMS are vested exclusively in Shopguin and nothing in the Conditions shall be deemed to vest any rights in the CMS in the Seller.
					11.	INTELLECTUAL PROPERTY RIGHTS
					11.1	SHOPGUIN’S IPR
					11.1.1	Any IPR in the Shopguin name, logo or branding are owned entirely by Shopguin, and the Seller agrees that it may only use the Shopguin name, logo or branding on any promotional material, packaging or elsewhere, whether in hard or electronic format, in accordance with these Conditions or with Shopguin’s prior written consent.
					11.1.2	Any IPR created by Shopguin (either alone or in connection with others, including, but not limited to the Seller) in the course of the performance of these Conditions or otherwise in the provision of the Service shall remain the Shopguin’s property of Shopguin..
					11.1.3	Where photographs or images of the Seller's Products are produced by Shopguin or its agents, all IPR in such photographs or images shall be the sole property of Shopguin and may not be used for any other purpose other than for display on the Shopguin website or in printed material produced by Shopguin or unless otherwise agreed in writing. 
					11.2	SELLER’S IPR
					11.2.1	The Seller warrants that:
					11.2.1.1	it is the legal beneficial owner of all of the IPR in and relating to the Products (which includes the data and information, including Seller Information, relating to such Products), photographs, logos, images and copy that it provides or uploads to Shopguin, and/or that it possesses a valid licence to use any and all such IPR; and
					11.2.1.2	the making of Products available for sale on the Site, and consequent use of the Seller’s IPR by Shopguin as referred to in Clause 11.2.2 will not infringe any IPR owned by any third party, and there is and will be no claim against Shopguin by any third party arising in relation to the use of such IPR;
					11.2.1.3	all items offered for sale by the Seller are not replica or design copies of any other brand, designer or manufacturer.
					11.2.2	The Seller grants to Shopguin a non-exclusive, perpetual, irrevocable, royalty-free and worldwide licence to use, license, disclose and distribute any information (including Seller Information), data, comments or images provided by the Seller to Shopguin for any purpose (including disclosing information to third party service providers so that they can contact you directly about their products and/or services). The Seller hereby waives their rights to be acknowledged as the author of their Seller Information and to object to the use, in whatever form, of their Seller Information by Shopguin..
					11.2.3	The Seller shall indemnify and hold Shopguin harmless against any and all damages, liabilities, costs, expenses and/or losses arising out of or relating to any breach of Clause 11 in respect of any claim or action that the normal operation, possession or use of those IPR by Shopguin infringes a third party's rights ("IPR Infringement Claim").
					11.2.4	In the event of an IPR Infringement Claim the Seller shall forthwith make without charge to Shopguin such alterations, modifications or adjustments to the IPR as shall be necessary to make them non-infringing.
					11.2.5	Shopguin shall notify the Seller as soon as reasonably practicable if it becomes aware of any IPR Infringement Claim by a third party.
					11.2.6	 Shopguin shall be entitled to take sole conduct of the defence to any claim or action in respect of any IPR Infringement Claim and may settle or compromise such claim or action at is sole discretion. The Seller shall give Shopguin such assistance as it shall reasonably require in respect of the conduct of the said defence including, without prejudice to the generality of the foregoing, the filing of all pleadings and other court process and the provision of all relevant documents.
					11.2.7	At the request of Shopguin, the Seller shall take the conduct of the defence to any claim or action in respect of any IPR Infringement Claim. The Seller shall not at any time admit liability or otherwise settle or compromise, or attempt to settle or compromise, such claim or action except upon the express written instructions of Shopguin, such instructions not to be unreasonably withheld or delayed.

					11.3	SURVIVAL OF TERMINATION
					This Clause 11 shall survive termination or expiry of these Conditions howsoever arising.
					12.	MARKETING AND PROMOTIONS
					12.1	From time to time Shopguin may run promotions on all or part of the Shopguin Website. Any such promotions shall be separate, and in addition to, any promotions operated by Sellers in their Storefronts, and may involve offering Customers either free delivery, discounted prices or other promotional activity relating to some or all Products on the Shopguin Website. Shopguin shall, where relevant and applicable to the Seller and/or its Products, inform the Seller of the nature and terms of any promotion and, at Shopguin’ sole discretion:
					12.1.1	Shopguin shall bear the costs of any such promotion; or
					12.1.2	where Shopguin requires the Seller to bear the costs of any promotion, the Seller shall inform Shopguin in writing whether or not it wishes to participate in the relevant promotion.
					12.2	The Seller shall permit, comply and co-operate with all activities undertaken by Shopguin to promote, sell or market the Seller’s Products, in such form and manner as Shopguin in its sole discretion deems appropriate, whether directly through the Site or the Shopguin Website, through any offline publications produced by Shopguin, or through websites or offline publications not produced, owned or operated by Shopguin.
					13.	CHARGES AND PAYMENTS
					13.1	GENERAL
					13.1.1	The Seller will pay Shopguin any and all Charges in accordance with these Conditions.
					13.1.2	The relevant Charges shall be as notified to the Seller and updated by Shopguin from time to time in accordance with these Conditions.
					13.1.3	Unless specified otherwise, all Charges are subject to VAT or other similar taxes or levies, all of which amounts the Seller shall pay, where appropriate, at the rate prevailing at the relevant tax point, and in addition to the Charges themselves.
					13.1.4	The Seller shall make all payments to Shopguin due under the Conditions without any deduction whether by way of set-off, withholding, counterclaim, discount, abatement or otherwise.
					13.1.5	If any sum due from the Seller to Shopguin under these Conditions is not paid on or before the due date for payment, then all sums then owing by the Seller to Shopguin shall become due and payable immediately and, without prejudice to any other right or remedy available to Shopguin, Shopguin shall be entitled to:
					13.1.5.1	cancel or suspend its performance of the Conditions or any order, including suspending provision of the Service, until arrangements as to payment or credit have been established which are satisfactory to Shopguin; and
					13.1.5.2	charge the Seller the cost of obtaining judgment or payment, to include all reasonable professional costs (including legal fees) and other costs of issuing proceedings or otherwise pursuing a debt recovery procedure.
					13.1.6	If Shopguin’s payment to the Seller as set out below involves a currency conversion, it will be completed at a foreign exchange rate determined by a financial institution, which is adjusted regularly based on market conditions and which will be applied immediately by Shopguin and without notice to the Seller.
					13.2	SINGLE COMMISSION FEE
					13.2.1	A Single Commission Fee is payable by the Seller on the value of the total amount payable by a Customer in relation to a Product sold through the Shopguin Website by the Seller. The Single Commission Fee is calculated as the Single Commission Percentage of the value of a Product as advertised on the Shopguin Website as charged to the Customer, excluding the delivery charges applying to that Product.
					13.2.2	Products returned by the Customer and refunded by Shopguin will not be subject to the Single Commission Fee. 
					13.3	PAYMENT
					13.3.1	As part of the Seller’s appointment of Shopguin as its commercial agent (as set out in clause 4.3 of these Conditions), the Seller agrees that Shopguin also acts as the Seller's exclusive agent for the purpose of accepting, refunding and/or otherwise processing payment(s) related to the sale of Product(s) by the Seller via the Site. The Seller and Shopguin acknowledge and agree that except as otherwise provided in these Conditions (and in acknowledgement that both parties are businesses), that the duties of an agent implied under the common law are expressly excluded. For the avoidance of doubt, Shopguin, acting as the Seller's commercial agent, is neither the buyer nor the seller of the Product(s) and is not a party to the contract of sale of Product(s) between the Seller and the Customer.
					13.3.2	Payments for Product(s) will be made directly by a Customer to Shopguin (acting as commercial agent of the applicable Seller). The Seller agrees that the Customer’s obligation to pay the Seller for Product(s) is satisfied when the Customer validly pays Shopguin for the applicable Product(s). The Seller further agrees that it will not seek recourse (legal or otherwise) against a Customer or any third party for payment of a Product if the Customer has validly paid Shopguin in the manner referred to in this clause.
					13.3.3	Payment for Products by Customers through the Shopguin Website will be through payment methods made available from time to time, using Shopguin's current online payment processing system (known as Stripe).
					13.3.4	Shopguin will pay the Seller for the relevant transaction less the Single Commission Fee and any applicable payment processing or currency conversion fee in accordance with the following timetable (NET 15):
					13.3.4.1	Bi-monthly, Shopguin will issue an invoice to the Seller indicating the value of the total amount payable by Customers in relation to the Seller’s Products sold through the Shopguin Website, for which the period for Customer refunds has expired.
					13.3.4.2	The invoice will indicate the amount received from Customers for the Seller’s Products during the relevant period for which the Customer refunds notification period of 14 days has expired, the Single Commission Fee and any applicable processing or currency conversion, and any refunds claimed by Customers and paid by Shopguin during that period, and any shipping fees charged to Customers for shipping outside the UK. 
					13.3.4.3	Payment will be made to the Seller within 5 Working Days of the date of the invoice to the bank account that has been nominated by the Seller. 

					14.	CONFIDENTIALITY
					14.1	Both parties will keep in confidence any Confidential Information and, except in accordance with these Conditions, will not disclose that Confidential Information to anyone (other than their employees, professional advisers or suppliers who need to know the information) without the written consent of the other party
					14.2	The obligations of confidentiality under the Conditions shall not extend to any matter which either party can show:
					14.2.1	is in or has become part of the public domain, other than through a breach of the Conditions or other confidentiality obligations;
					14.2.2	was lawfully in the possession of the recipient before the disclosure under the Conditions took place;
					14.2.3	was independently disclosed to it by a third party entitled to disclose the same;
					14.2.4	is required to be disclosed under any applicable law, or by order of a court or governmental body, or by authority of competent jurisdiction.
					14.3	The obligations of confidentiality under the Conditions shall remain in effect for two (2) years after the termination or expiry of the Conditions, howsoever arising.
					15.	INDEMNITY AND LIABILITY
					15.1	Subject to the terms of these Conditions, the Seller shall indemnity Shopguin (and its respective employees, directors, agents and representatives) and hold harmless against any and all damages, liabilities, costs, judgements, expenses and losses arising out of any claim that arises directly out of or relating to 
					15.1.1	any actual or alleged breach of these Conditions by the Seller, or
					15.1.2	the Seller’s own website or other sales channels, products, contents or sale or return of any product.
					15.2	Subject to the liabilities which cannot be excluded by law, Shopguin's total liability in contract, tort (including negligence) or breach of duty, misrepresentation or otherwise, arising in connection with these Conditions, shall be limited in aggregate to a maximum of £10,000. 
					15.3	Shopguin shall not be liable to the Seller for any loss of profit or other economic loss (direct or indirect), indirect or consequential loss or damage, costs, expenses or other claims for consequential compensation whatsoever (howsoever caused) or loss or damage (contractual, tortious, breach of statutory duty or otherwise) that arises out of or in connection with the Conditions, or for any liability incurred by the Seller to a Customer, or to any other person howsoever, arising from the provision of the Service or otherwise.
					16.	TRANSFER
					16.1	The Seller is not entitled to assign, charge, subcontract or transfer this agreement or any part of it without the prior written consent of Shopguin. 
					16.2	Shopguin may assign, charge, subcontract or transfer this agreement or any part of it to any person.
					17.	GENERAL
					17.1	Shopguin reserves the right to suspend or to cancel the Conditions in whole or in part (without liability to Shopguin) if it is prevented from or delayed in the carrying on of its business and its obligations under the Conditions due to circumstances beyond its reasonable control, including acts of God, fire, flood, lightning, severe weather conditions, war, revolution, acts of terrorism, IT or internet outage, industrial disputes (whether of its own employees or others) or acts of local or central government (including the imposition of legal or regulatory restrictions). If any such event beyond the reasonable control of Shopguin continues for a continuous period of more than 30 days, either party shall be entitled to give notice in writing to the other to terminate the Conditions.
					17.2	A waiver by either party of any breach of the Conditions, or delay in enforcing any breach, shall not prevent the subsequent enforcement of that breach and shall not be deemed to be a waiver of any subsequent breach of that or any other provision.
					17.3	If at any time any one or more of these Conditions (or any part of one or more of these Conditions) is held to be or becomes void or otherwise unenforceable, for any reason under any applicable law, the same shall be deemed omitted from the Conditions and the validity and/or enforceability of the remaining provisions of the Conditions shall not in any way be affected or impaired as a result of that omission.
					17.4	Notices given under the Conditions must be in writing and may be delivered by hand or by courier, or sent by prepaid first class or registered mail to, in the case of Shopguin, to its registered address, or any alternative address notified by Shopguin to the Seller in accordance with this provision; and, in the case of the Seller, to the address which it provides on the CMS, or any alternative address notified by it to Shopguin in accordance with this provision.
					17.5	Notices may be validly served by email provided that, to be effective, such email is sent to the email address most recently provided by the addressee to the sender of the relevant notice.
					17.6	This agreement constitutes the entire agreement between the parties and supersedes and extinguishes all previous drafts, arrangements, understandings or agreements between them, whether written or oral, relating to the subject matter of this agreement.
					17.7	This agreement shall be governed by English law, and the parties irrevocably submit to the exclusive jurisdiction of the English courts in respect of any dispute relating to or arising under it.

				</p>
				
			</div>
		</div>
	</section>
	
</div>
@endsection

{{-- Footer --}}
@section('footer')
	@include('partials.footer')
@endsection

{{-- Footer scripts --}}
@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/parallax.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
      		$('.parallax').parallax();
    	});
	</script>
@endsection


