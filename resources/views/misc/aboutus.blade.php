@extends('layouts.boilerplate')

{{-- Head tags --}}
@section('head')
	<title>About us - Shopguin</title>
	@include('partials.head')
	{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.1/css/materialize.min.css"> --}}

@endsection

{{-- OpenGraph tags --}}
@section('og-scripts')
	<meta property="og:title" content="About Shopguin" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.shopguin.com/aboutus" />
	<meta property="og:description" content="About Shopguin" />
	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:site" content="@shopguin">
	<meta name="twitter:creator" content="@shopguin">
@endsection

{{-- Head scripts --}}
@section('head-scripts')
@endsection

{{-- Navbar --}}
@section('navbar')
	@include('partials.navbar-slim')
@endsection

{{-- Body --}}
@section('body')
<div class="business">
	<section >
		<div class="container text-center">
			<h1>
				About us
			</h1>
			
		</div>

	</section>
	<section>
		<div class="container">
			<h1 id="start" class="text-center section-title">
				About Shopguin
			</h1>
			<p >
				Founded in June of 2017 and based in Limassol, Cyprus, Shopguin is a unique platform for people and businesses to create easily a powerful online store - online or from a mobile phone or tablet.

				Whether a small bakery or a large supermarket, Shopguin allows people and businesses to start selling their products online. All this supported with a world-class customer service and technical support. Shopguin is the easiest way for people and businesses to innovate and grow online and showcase their products to an audience of billions.
			</p>
		</div>
	</section>
	
</div>
@endsection

{{-- Footer --}}
@section('footer')
	@include('partials.footer')
@endsection

{{-- Footer scripts --}}
@section('scripts')
	@include('partials.scripts')
	<script src="/js/assets/parallax.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
      		$('.parallax').parallax();
    	});
	</script>
@endsection


