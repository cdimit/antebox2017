@extends('layouts.boilerplate')

@section('head')
	<title>Shopguin - How customise your theme</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<div class="row intro">
			<div class="col-md-12">
				<br>
				<h4 class="w500 c-med-gray">
					Shopguin :: 
					<a href="/howto" style="text-decoration: none;">
						Getting started guides
					</a>
				</h4>
				<h1 class="">
					How to customise your online store's theme
				</h1>
				<p class="s-13em">
					Find out how how you can customise your online store's theme, and reflect your business's branding. Your theme represents how your online store looks to your customers. You can currently choose among two themes and switch between them as you wish.
					<br>
					<br>
					<a href="/themes" class="btn btn-lg btn-primary">
						View the themes 
					</a>
				</p>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				{{-- Step 1 --}}

				<div class="row">
					<div class="col-md-4">
						<h2 class="w300 qs">
							Step 1 - Log in to your dashboard
						</h2>
						<br>
						<p class="s-13em">
							First things first, visit shopguin.com and if you already have a business account, simply login with your email and password.
							<br>
							<br>
							If you do not have a business account learn how to quickly make one <a href="/howto/account">here</a>.
						</p>
					</div>
					<div class="col-md-8">
						<img src="/img/howto/addproduct/login.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 2 --}}
				<div class="row">
					<div class="col-md-7">
						<img src="/img/howto/themes/thememenu.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
					<div class="col-md-5">
						<h2 class="w300 qs">
							Step 2 - Navigate to the 'Theme' menu
						</h2>
						<br>
						<p class="s-13em">
							Use the sidebar to find the <b>Theme</b> submenu, from where you can choose the theme for your online store, as well as customise it.
						</p>
						<br>
						<p class="s-13em">
							To choose which theme your store will have, simply choose 'Choose your theme' and make your choice. If you want to customise your theme, choose 'Customise theme' and read on below!
						</p>
						<br>
						
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 3 --}}
				<div class="row">
					<div class="col-md-4">
						<h2 class="w300 qs">
							Step 3 - Customise the slider
						</h2>
						<br>
						<p class="s-13em">
							We've made it super easy to customise your theme, through a step by step process.
						</p>
						<br>
						<p class="s-13em">
							The first step involves uploading images for the slider on your front page, and the text that appears on it. 
						</p>
						<br>
						<p class="s-13em">
							For each slider, you get a <b>title</b> and a <b>short descrption</b> , both of which appear above your text.
						</p>
						<br>
						<h4>
							Remember!
						</h4>
						<p class="s-13em">
							Before moving on from each step, remember to save your settings by pressing <b>Save theme settings</b> at the bottom of each step!
						</p>
					</div>
					<div class="col-md-8">
						<img src="/img/howto/themes/sliderstep.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 4 --}}
				<div class="row">
					<div class="col-md-7">
						<img src="/img/howto/themes/featuredads.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
					<div class="col-md-5">
						<h2 class="w300 qs">
							Step 4 - Customise your featured ads
						</h2>
						<br>
						<p class="s-13em">
							The next step in the process is customising the introductory text below the slider, and the featured ads that will appear on your online store, as well some other text.
						</p>
						<br>
						<p class="s-13em">
							Just like the sliders, each image can feature a small bit of text that appears above it.
						</p>
						<br>
						<p class="s-13em">
							You can also add your opening hours paragraph at the bottom of this step. This can be a short paragraph listing which days and what hours you're open on.
						</p>
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 5 --}}
				<div class="row">
					<div class="col-md-4">
						<h2 class="w300 qs">
							Step 5 - Final step!
						</h2>
						<br>
						<p class="s-13em">
							The last step involves adding text for your <b>About page</b> and a <b>motto</b> for your company! 
						</p>
						<br>
						<p class="s-13em">
							Remember to press <b>Save theme settings</b> at the end of each step! Once you're done, you can press <b>Finish</b> and you're done!
						</p>
						
					</div>
					<div class="col-md-8">
						<img src="/img/howto/themes/finalstep.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>
				<hr>
				<br>

			</div>
		</div>
	</div>
</section>
<section class="sign-up text-center">
	<div class="container">
		<h1>
			Congrats! You've learnt how to customise your theme!
		</h1>		
		<a class="btn btn-white" href="/business/register">Register your business now</a>
	</div>
	<br>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
