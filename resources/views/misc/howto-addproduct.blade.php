@extends('layouts.boilerplate')

@section('head')
	<title>Shopguin - How to add a product</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<div class="row intro">
			<div class="col-md-12">
				<br>
				<h4 class="w500 c-med-gray">
					Shopguin :: 
					<a href="/howto" style="text-decoration: none;">
						Getting started guides
					</a>
				</h4>
				<h1 class="">
					How to add products to your online store
				</h1>
				<p class="s-13em">
					Follow along this guide to learn how to add products to your store!
				</p>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				{{-- Step 1 --}}

				<div class="row">
					<div class="col-md-4">
						<h2 class="w300 qs">
							Step 1 - Log in to your dashboard
						</h2>
						<br>
						<p class="s-13em">
							Visit shopguin.com and if you already have a business account, simply login with your email and password.
							<br>
							<br>
							If you do not have a business account learn how to quickly make one <a href="/howto/account">here</a>.
						</p>
					</div>
					<div class="col-md-8">
						<img src="/img/howto/addproduct/login.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 2 --}}
				<div class="row">
					<div class="col-md-7">
						<img src="/img/howto/addproduct/addproduct.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
					<div class="col-md-5">
						<h2 class="w300 qs">
							Step 2 - Click on 'Add product'
						</h2>
						<br>
						<p class="s-13em">
							Use the sidebar to find the 'Products' sub-category, and select 'Add new product' from that menu.
							<br>
							<br>
							<h4 class="c-med-gray">
								Did you know?
							</h4>
						</p>
						<p class="s-13em">
							The dashboard gives you the ability to make changes to your theme, products, stores, but also check your customers’ shopping carts.

						</p>
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 3 --}}
				
				<div class="row">
					<div class="col-md-7">
						<h2 class="w300 qs">
							Step 3 - Fill in the details of your product
						</h2>
						<br>
						<p class="s-13em">
							Fill in the form with your product’s details.
							<br>
							<br>
							Then, press “Add product” to publish it to your store and make it available for your customers.
						</p>
						
					</div>
					<div class="col-md-5">
						<img src="/img/howto/addproduct/fillinform.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>

			</div>
		</div>
	</div>
</section>
<section class="sign-up text-center">
	<div class="container">
		<h1>
			That's it! Now go add some products!
		</h1>
		
	</div>
	<br>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
