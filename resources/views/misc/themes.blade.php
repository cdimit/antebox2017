@extends('layouts.boilerplate')

@section('head')
	<title>Themes - Shopguin</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">

	<div class="container first">
		<div class="row intro">
			<div class="col-md-12">
				<h1>
					Shopguin themes <i>for businesses</i> 
				</h1>
				<p class="s-13em">
					Shopguin offers businesses a selection of high quality, customisable themes to enabling you to build your brand and identity online through our platform. Our theme selection is continuously growing, with new themes being added to reflect the various industries which your business may cater to.
				</p>
				
			</div>
		</div>
	</div>
</section>
<hr>
<section class="grey">
	<div class="container">
		<div class="row ">
			<div class="col-md-12">
				<h2 class="w300">
					All themes
				</h2>
				
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="row theme-grid is-flex">
					<div class="col-lg-3 col-md-4 col-sm-6 theme text-center" style="min-height: 415px">
						<div class="box-light">
							<img src="/images/agnolotti.jpg" class="img-responsive" alt="">
							<h3 class="w300">
								Agnolotti
							</h3>
							<p class="s-11em c-med-gray">
								Ideal for businesses in the food industry, but equally suitable for other companies which prefer a more darker, saturated colour pallette.
							</p>
							<a href="/demo/agnolotti" target="_blank" class="btn btn-primary btn-xs">View demo</a>
						</div>
					</div>

					<div class="col-lg-3 col-md-4 col-sm-6 theme text-center" style="min-height: 415px">
						<div class="box-light">
							<img src="/images/vionnet.jpg" class="img-responsive" alt="">
							<h3 class="w300">
								Vionnet
							</h3>
							<p class="s-11em c-med-gray">
								A modern & minimalistic theme, Vionnet appeals to businesses in practically any industry. With a subdued colour pallete, its likely to be your best first choice as a theme. 
							</p>
							<a href="/demo/vionnet" target="_blank" class="btn btn-primary btn-xs">View demo</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6 theme text-center" style="min-height: 415px">
						<div class="box-light">
							<img src="/images/belgravia.jpg" class="img-responsive" alt="">
							<h3 class="w300">
								Belgravia
							</h3>
							<p class="s-11em c-med-gray">
								A bold, stylish and classy theme suitable for businesses in the clothing industry. Dark colours, accented with bright highlights creates for a memorable design.
							</p>
							<a href="/demo/belgravia" target="_blank" class="btn btn-primary btn-xs">View demo</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-xs-12 sidebar-info white">
				<h4 class="w300">
					About our themes
				</h4>
				<p class="s-11em c-med-gray">
					None of the themes fancy your interest? If you think we could offer something more unique or different, please get in touch with us as we would love to hear your feedback.
				</p>
			</div>
		</div>
	</div>
</section>
<section class="sign-up text-center">
	<div class="container">
		<h1>
			All our themes are free! Get started now!
		</h1>
		<a class="btn btn-white" href="/business/register">Register your business now</a>
	</div>
	<br>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
