@extends('layouts.boilerplate')

@section('head')
	<title>Shopguin - How to guides</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<div class="row intro">
			<div class="col-md-12">
				<h1>
					How To guides for Shopguin
				</h1>
				<p class="s-13em">
					Getting your online business up and running with Shopguin is really easy! We've put together a series of guides, which you can refer to anytime to see how simple and intuitive it is to use your website. Have a look at our guides below:
				</p>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="w300">
					How To guides
				</h2>
				<br>
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="row is-flex">
					<div class="col-lg-3 col-md-4 col-sm-6" style="min-height: 415px">
						<a href="/howto/account" style="text-decoration: none;">
							<div class="box-light">
								<div class="blue-header text-center">
									<h2 class="">
										<i class="icon icon-team" style="font-size: 4em;"></i>
										<br>
										Make a business account
									</h2>
								</div>
								<p class="desc">
									Learn how to make a business account in under 5 minutes.
								</p>
							</div>
						</a>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6  " style="min-height: 415px">
						<a href="/howto/addproduct" style="text-decoration: none;">
							<div class="box-light">
								<div class="blue-header text-center">
									<h2 class="">
										<i class="icon icon-basket" style="font-size: 4em;"></i>
										<br>
										Add products to your business
									</h2>
								</div>
								<p class="desc">
									Learn how to add products to your online store.
								</p>
							</div>
						</a>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6  " style="min-height: 415px">
						<a href="/howto/themes" style="text-decoration: none;">
							<div class="box-light">
								<div class="blue-header text-center">
									<h2 class="">
										<i class="icon icon-browser" style="font-size: 4em;"></i>
										<br>
										Customise your store theme
									</h2>
								</div>
								<p class="desc">
									Learn how to customise the theme for your online store.
								</p>
							</div>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
