@extends('layouts.boilerplate')

@section('head')
	<title>Shopguin - How to make an account</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<div class="row intro">
			<div class="col-md-12">
				<br>
				<h4 class="w500 c-med-gray">
					Shopguin :: 
					<a href="/howto" style="text-decoration: none;">
						Getting started guides
					</a>
				</h4>
				<h1 class="">
					How to make a business account
				</h1>
				<p class="s-13em">
					Follow the steps below, and you'll be up and running with your online store on Shopguin in no time!
				</p>
			</div>
		</div>
	</div>
</section>

<section class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="w300 qs">
					Quick overview
				</h2>
				<br>
				<p class="s-13em">
					Online selling is made easy through Shopguin! With our easy to use software you can build a professional store and leave everyone wondering how you did it

				</p>
				<br>
				<div class="row">
					<div class="col-md-4 text-center">
						<i class="icon icon-team" style="font-size: 6em;"></i>
						<h2 class="c-med-gray w500">
							Create your account
						</h2>
					</div>
					<div class="col-md-4 text-center">
						<i class="icon icon-listing" style="font-size: 6em;"></i>
						<h2 class="c-med-gray w500">
							Fill in your details
						</h2>
					</div>
					<div class="col-md-4 text-center">
						<i class="icon icon-online-shopping" style="font-size: 6em;"></i>
						<h2 class="c-med-gray w500">
							Start selling
						</h2>
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 1 --}}

				<div class="row">
					<div class="col-md-4">
						<h2 class="w300 qs">
							Step 1 - Login details
						</h2>
						<br>
						<p class="s-13em">
							Create your account <a href="/business/register">here</a>
							<br>
							<br>
							You can create your account in 4 steps. 
						</p>
						<br>
						<br>
						<p class="s-13em">
							Use an email and password that you would like to use when logging in.
							<br>
							<br>
							You will receive all orders and notifications on this email address, so make sure you have constant access to it.
						</p>
					</div>
					<div class="col-md-8">
						<img src="/img/howto/account/login.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 2 --}}
				<div class="row">
					<div class="col-md-7">
						<img src="/img/howto/account/details.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
					<div class="col-md-5">
						<h2 class="w300 qs">
							Step 2 - Your address
						</h2>
						<br>
						<p class="s-13em">
							The next step is simply filling in your business address.
							<br>
							<br>
							You'll be also be to add other addresses later on, for your different stores when you're fully signed up.
						</p>
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 3 --}}
				
				<div class="row">
					<div class="col-md-4">
						<h2 class="w300 qs">
							Step 3 - Choose your membership plan
						</h2>
						<br>
						<p class="s-13em">
							All plans include hosting, payment processing, messaging and the website.
						</p>
						<br>
						<br>
						<p class="s-13em">
							Choose the Basic plan for selling up to 10 products, the Professional plan for up to 30 products and the Advanced one for 100 products. 
							<br>
							<br>
							To find out how to add a product, click <a href="/howto/addproduct">here</a>.
						</p>
					</div>
					<div class="col-md-8">
						<img src="/img/howto/account/plan.png" alt="" class="img-responsive" style="max-height: 500px;">
					</div>
				</div>
				<br>
				<hr>
				<br>
				{{-- Step 4 --}}
				<div class="row">
					<div class="col-md-5">
						<img src="/img/howto/account/theme.png" alt="" class="img-responsive" style="max-height: 500px;">
						
					</div>
					<div class="col-md-7">
						<h2 class="w300 qs">
							Step 4 - Choose your theme
						</h2>
						<br>
						<p class="s-13em">
							We have two themes for you to choose from. 
							<br>
							<br>
							Choose the Vionnet for simple colours or the Agnolotti with earth colours. 
							<br>
							<br>
							View the themes <a href="/themes">here</a>.
							<br>
							<br>
							Learn how easy it is to customise your theme <a href="/howto/themes">here</a>.
						</p>
					</div>
				</div>
				<br>
				<hr>
				<br>

			</div>
		</div>
	</div>
</section>
<section class="sign-up text-center">
	<div class="container">
		<h1>
			You're done! Now what are you waiting for, sign up!
		</h1>
		<a class="btn btn-white" href="/business/register">Register your business now</a>
	</div>
	<br>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
