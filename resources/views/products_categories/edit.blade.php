@include('dashboard_admin.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard_admin.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Products</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Edit category {{$category->name}}</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the products associated with your company.
									</p>
									<form class="form-horizontal" role="form" method="POST" action="/admin/categories/{{$category->id}}/edit" enctype="multipart/form-data">
									{{ csrf_field() }}

										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 control-label">Name</label>
											<div class="col-md-6">
												<input id="name" type="text" class="form-control" name="name" value="{{ $category->name }}" required autofocus>

												@if ($errors->has('name'))
												<span class="help-block">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
											<label for="description" class="col-md-4 control-label">Description</label>
											<div class="col-md-6">
												<input id="description" type="text" class="form-control" name="description" value="{{ $category->description }}" autofocus>

												@if ($errors->has('description'))
												<span class="help-block">
													<strong>{{ $errors->first('description') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
											<label for="icon" class="col-md-4 control-label">Icon</label>

											<div class="col-md-3">
												<input id="icon" type="text" class="form-control" name="icon" value="{{ $category->icon }}" autofocus>

												@if ($errors->has('icon'))
												<span class="help-block">
													<strong>{{ $errors->first('icon') }}</strong>
												</span>
												@endif
											</div>
											<div class="col-md-1">
												<i class="fa {{$category->icon}}" aria-hidden="true"></i>

											</div>
											<div class="col-md-2">
												Use code from <a href="http://fontawesome.io/icons/"><strong>here</strong></a>
											</div>
                    									</div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Edit category
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<h3>Editting product categories</h3>
							<p  class="text-muted font-13 m-b-30">
								Please ensure you fill in your company's country, website and address.
							</p>
						</div>

					</div>
				</div>
			</div>


			@include('dashboard_admin.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#tags').tagsInput({
				'delimiter': [','],
			});
		});
	</script>
</body>
</html>
