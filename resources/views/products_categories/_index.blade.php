@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>
                <div class="panel-body">
                    @if (session('status'))
                      <div class="alert alert-success">
                        <strong>{{ session('status') }}</strong>
                      </div>
                    @endif

                    @foreach($categories as $category)
                      <strong>{{$category->name}}</strong><br>
                      {{$category->description}}<br>
                      <img src="{{Storage::url($category->icon)}}"><br>
                      <a href="/admin/categories/{{$category->id}}/edit" class="btn btn-primary">Edit</a>
                      <a href="/admin/categories/{{$category->id}}/delete" class="btn btn-danger">Delete</a>
                      <hr>
                    @endforeach
                    <a href="/admin/categories/create" class="btn btn-success">Create</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
