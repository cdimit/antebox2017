@include('dashboard_admin.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard_admin.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Product categories</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All product categories from</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the product categories associated with your company.
									</p>
									<div class="table_scroll">
										<table id="all-products" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>ID</th>
													<th>Icon</th>
													<th>Name</th>
													<th>Description</th>
													<th>Products</th>
													<th></th>
													<th></th>
													<th></th>
												</tr>

											</thead>
											<tbody>
												@foreach($categories as $category)
												<tr>
													<td>
														{{$category->id}}
													</td>
													<td>
														<i class="fa {{$category->icon}}" aria-hidden="true"></i>
													</td>
													<td>
														{{$category->name}}
													</td>
													<td>
														{{$category->description}}
													</td>
													<td>
														{{$category->products->count()}}
													</td>
													<td >
														<a href="/admin/categories/{{$category->id}}" class="btn btn-xs btn-default"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
													</td>
													<td>
		                      							<a href="/admin/categories/{{$category->id}}/edit" class="btn btn-xs btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
													</td>
													<td>
		                      							<a href="/admin/categories/{{$category->id}}/delete" class="btn btn-xs btn-danger"><i class="fa fa-times-circle" aria-hidden="true"></i> Delete</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All Deleted product categories from </h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the product categories associated with your company.
									</p>
									<div class="table_scroll">
										<table id="all-products" class="table table-striped table-bordered no-wrap" >
											<thead>
												<tr>
													<th>ID</th>
													<th>Icon</th>
													<th>Name</th>
													<th>Description</th>
													<th>Products</th>
													<th></th>
													<th></th>
													<th>Deleted At</th>
													<th></th>
												</tr>

											</thead>
											<tbody>
												@foreach($trashed_categories as $category)
												<tr>
													<td>
														{{$category->id}}
													</td>
													<td>
													  <i class="fa {{$category->icon}}" aria-hidden="true"></i>
													</td>
													<td>
														{{$category->name}}
													</td>
													<td>
														{{$category->description}}
													</td>
													<td>
														{{$category->products->count()}}
													</td>
													<td >
														<a href="/admin/categories/{{$category->id}}" class="btn btn-default">View</a>
													</td>
													<td>
		                      	<a href="/admin/categories/{{$category->id}}/edit" class="btn btn-default">Edit</a>
													</td>
													<td>
														{{$category->deleted_at}}
													</td>
													<td>
		                      	<a href="/admin/categories/{{$category->id}}/restore" class="btn btn-default">Restore</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>



				</div>
			</div>


			@include('dashboard_admin.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-products').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
