@include('dashboard_admin.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard_admin.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Products</h2>
				  		</div>


					</div>
					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Add product category</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the products associated with your company.
									</p>
									<form class="form-horizontal" role="form" method="POST" action="/admin/categories/create">
									{{ csrf_field() }}

										<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
											<label for="name" class="col-md-4 col-xs-12 control-label">Name</label>
											<div class="col-md-6 col-xs-12">
												<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

												@if ($errors->has('name'))
												<span class="help-block">
													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
											<label for="tags" class="col-md-4 col-xs-12 control-label">
												Fields
											</label>
											<div class="col-md-6 col-xs-12">
												<input id="tags" type="text" class="form-control" name="fields" value="{{ old('fields') }}">
												<p class="help">
													Separate fields via comma.
												</p>
												@if ($errors->has('tags'))
												<span class="help-block">
												<strong>{{ $errors->first('fields') }}</strong>
												</span>
												@endif
											</div>
										</div>


										<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
											<label for="description" class="col-md-4 col-xs-12 control-label">Description</label>
											<div class="col-md-6 col-xs-12">
												<input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus>

												@if ($errors->has('description'))
												<span class="help-block">
													<strong>{{ $errors->first('description') }}</strong>
												</span>
												@endif
											</div>
										</div>

										<div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
											<label for="icon" class="col-md-4 col-xs-12 control-label">Icon</label>

											<div class="col-md-4 col-xs-12">
												<input id="icon" type="text" class="form-control" name="icon" value="{{ old('icon') }}" autofocus>

												@if ($errors->has('icon'))
												<span class="help-block">
													<strong>{{ $errors->first('icon') }}</strong>
												</span>
												@endif
											</div>
											<div class="col-md-2">
												Use code from <a href="http://fontawesome.io/icons/"><strong>here</strong></a>
											</div>
                    </div>

										<div class="form-group">
											<div class="col-md-8 col-md-offset-4">
												<button type="submit" class="btn btn-primary">
												Create category
												</button>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard_admin.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="/js/assets/jquery.tagsinput.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#tags').tagsInput({
				'delimiter': [','],
			});
		});
	</script>
</body>
</html>
