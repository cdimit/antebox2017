@include('dashboard_admin.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard_admin.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Payments</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All Payments on Shopguin</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the Payments on Shopguin.
									</p>
									<div class="table_scroll">
										<table id="all-businesses" class="table table-striped table-bordered no-wrap dashboard-table" >
											<thead>
												<tr>
													<th>
														Id
													</th>
													<th>
														Name
													</th>
													<th>
														Account
													</th>
													<th>
														Activity
													</th>
													<th>
														Amount
													</th>
													<th>
														Date
													</th>
												</tr>
											</thead>
											<tbody>
												@foreach($payments as $payment)
												<tr>
													<td>
														{{$payment->id}}
													</td>
													<td>
														{{$payment->user->profile->name}}
													</td>
													<td>
														{{$payment->user->role}}
													</td>
													<td>
														{{$payment->paymentable_type}}
													</td>
													<td>
														{{$payment->formatAmount()}}
													</td>
													<td>
														{{$payment->created_at->diffForHumans()}} ({{$payment->created_at}})
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard_admin.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-businesses').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
