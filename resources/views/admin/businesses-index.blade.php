@include('dashboard_admin.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard_admin.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Businesses</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>

					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All businesses registered on Shopguin</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the businesses signed up on Shopguin.
									</p>
									<div class="table_scroll">
										<table id="all-businesses" class="table table-striped table-bordered no-wrap dashboard-table" >
											<thead>
												<tr>
													<th>
														Id
													</th>
													<th>
														Business name
													</th>
													<th>
														Country
													</th>
													<th>
														Membership plan
													</th>
													<th>
														Member since
													</th>
													<th>
														Edit
													</th>
												</tr>
											</thead>
											<tbody>
												@foreach($businesses as $business)
												<tr>
													<td>
														{{$business->id}}
													</td>
													<td>
														{{$business->name}}
														@if($business->isTrial())
															<span class="label label-info">Trial Ends in: {{$business->trialEndAt()->diffForHumans()}}</span>
														@endif
													</td>
													<td>
														{{$business->country}}
													</td>
													<td>
														{{$business->subscribePlan->category}}
														@if($business->isActive())
															(Active)
														@else
															(Not Active)
														@endif
													</td>
													<td>
														{{$business->created_at->diffForHumans()}}
													</td>
													<td>
														<a href="" class="btn btn-xs btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
													</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			@include('dashboard_admin.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-businesses').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
