@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Shopping carts</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>
					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>All shopping carts made with {{Auth::user()->profile->name}}</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										Listed below are below are all the shopping carts customers have made with your products.
									</p>
									<div class="accordion" id="cartsaccordion" role="tablist" aria-multiselectable="true">
										<div class="panel">
											<a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#cartsaccordion" href="#allcarts" aria-expanded="true" aria-controls="collapseOne">
												<h4 class="panel-title">All carts</h4>
											</a>
											<div id="allcarts" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body">
													<p class="text-muted font-13 m-b-30">
														Listed below are below are all the shopping carts customers have made with your products.
													</p>
													<div class="table_scroll">
														<table id="all-requests" class="table table-striped table-bordered no-wrap dashboard-table" >
															<thead>
																<tr>
																	<th>Customer name</th>
																	<th>Date</th>
																	<th>Items</th>
																	<th>Cart value</th>
																	<th>Delivery location</th>
																	<th>Status</th>
																	<th>Bids</th>
																	<th>View cart</th>
																	<th>Make a bid</th>
																	<th>View invoice</th>
																</tr>
															</thead>
															<tbody>
																@if($allRequests)
																@foreach($allRequests as $req)
																<tr>
																	<td>
																		{{$req->client->first_name}}, {{$req->client->last_name}}
																	</td>
																	<td>
																		{{$req->created_at}}
																	</td>
																	<td>
																		{{count($req->items)}}

																	</td>
																	<td>
																		€ {{number_format($req->total_price, 2)}}
																	</td>
																	<td>
																		{{$req->meetingPoint['city']}}, {{$req->meetingPoint['country']}}
																	</td>
																	<td>
																		@if($req->status == 'close')
																			@if($req->invoice->is_paid == 1)
																				<i class="fa fa-check" aria-hidden="true"></i>
																				Payment made
																			@else
																				Payment pending.
																			@endif
																		@else 
																			Payment pending.
																		@endif
																	</td>
																	<td>
																		{{$req->bids->count()}}
																	</td>
																	<td>
																		<a href="/business/carts/{{$req->id}}/view" class="btn btn-xs btn-default"><i class="fa fa-eye" aria-hidden="true"></i> View cart</a>																	
																	</td>
																	<td>
																		@if($req->status == 'open')
																			<a href="/business/carts/{{$req->id}}/bid" class="btn btn-xs btn-default"><i class="fa fa-gavel" aria-hidden="true"></i> Make a bid</a>
																		@elseif($req->status == 'close')
																			<a href="" class="btn btn-xs btn-default disabled"><i class="fa fa-gavel" aria-hidden="true"></i> Bid accepted</a>
																		@endif
																	</td>
																	<td>
																		@if($req->status == 'close')
																			@if($req->invoice->is_paid == 1)
																				<a href="/business/carts/{{$req->invoice->id}}/invoice" class="btn btn-default btn-xs">
																					<i class="fa fa-newspaper-o" aria-hidden="true"></i>
																					View invoice
																				</a>
																			@endif
																		@endif
																	</td>
																</tr>
																@endforeach
																@endif
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>

										<div class="panel">
											<a class="panel-heading collapsed"  role="tab" id="headingOne1" data-toggle="collapse" data-parent="#cartsaccordion" href="#opencarts" aria-expanded="true" aria-controls="collapseOne">
												<h4 class="panel-title">Open carts</h4>
											</a>
											<div id="opencarts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body">
													<p class="text-muted font-13 m-b-30">
														This is a list of all the carts with your company, that have not had accepted a bid or had their products delivered yet.
													</p>
													<div class="table_scroll">
														<table id="open-requests" class="table table-striped table-bordered no-wrap dashboard-table" >
															<thead>
																<tr>
																	<th>Customer name</th>
																	<th>Date</th>
																	<th>Items</th>
																	<th>Cart value</th>
																	<th>Delivery location</th>
																	<th>Status</th>
																	<th>Bids</th>
																	<th>View cart</th>
																	<th>Make a bid</th>
																</tr>
															</thead>
															<tbody>
																@if($openRequests)
																@foreach($openRequests as $req)
																<tr>
																	<td>
																		{{$req->client->first_name}}, {{$req->client->last_name}}
																	</td>
																	<td>
																		{{$req->created_at}}
																	</td>
																	<td>
																		{{count($req->items)}}
																	</td>
																	<td>
																		€ {{number_format($req->total_price, 2)}}
																	</td>
																	<td>
																		{{$req->meetingPoint['city']}}, {{$req->meetingPoint['country']}}
																	</td>
																	<td>
																		@if($req->status == 'close')
																			@if($req->invoice->is_paid == 1)
																				<i class="fa fa-check" aria-hidden="true"></i>
																				Payment made
																			@else
																				Payment pending.
																			@endif
																		@else 
																			Payment pending.
																		@endif
																	</td>
																	<td>
																		{{$req->bids->count()}}
																	</td>
																	<td>
																		<a href="/business/carts/{{$req->id}}/view" class="btn btn-xs btn-default"><i class="fa fa-eye" aria-hidden="true"></i> View cart</a>																
																	</td>
																	<td>
																		<a href="/business/carts/{{$req->id}}/bid" class="btn btn-xs btn-default"><i class="fa fa-gavel" aria-hidden="true"></i> Make a bid</a>
																	</td>
																</tr>
																@endforeach
																@endif
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>

										<div class="panel">
											<a class="panel-heading collapsed" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#cartsaccordion" href="#completedcarts" aria-expanded="true" aria-controls="collapseOne">
												<h4 class="panel-title">Completed carts</h4>
											</a>
											<div id="completedcarts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body">
													<p class="text-muted font-13 m-b-30">
														This is a list of all completed sales with your company, that have been delivered and paid for.
													</p>
													<div class="table_scroll">
														<table id="closed-requests" class="table table-striped table-bordered no-wrap dashboard-table" >
															<thead>
																<tr>
																	<th>Customer name</th>
																	<th>Date</th>
																	<th>Items</th>
																	<th>Cart value</th>
																	<th>Delivery location</th>
																	<th>Status</th>
																	<th>Bids</th>
																	<th>View invoice</th>
																</tr>
															</thead>
															<tbody>
																@if($closedRequests)
																@foreach($closedRequests as $req)
																<tr>
																	<td>
																		{{$req->client->first_name}}, {{$req->client->last_name}}
																	</td>
																	<td>
																		{{$req->created_at}}
																	</td>
																	<td>
																		{{count($req->items)}}
																	</td>
																	<td>
																		€ {{number_format($req->total_price, 2)}}
																	</td>
																	<td>
																		{{$req->meetingPoint['city']}}, {{$req->meetingPoint['country']}}
																	</td>
																	<td>
																		@if($req->status == 'close')
																			@if($req->invoice->is_paid == 1)
																				<i class="fa fa-check" aria-hidden="true"></i>
																				Payment made
																			@else
																				Payment pending.
																			@endif
																		@else 
																			Payment pending.
																		@endif
																	</td>
																	<td>
																		{{$req->bids->count()}}
																	</td>
																	<td>
																		@if($req->status == 'close')
																			@if($req->invoice->is_paid == 1)
																				<a href="/business/carts/{{$req->invoice->id}}/invoice" class="btn btn-default btn-xs">
																					<i class="fa fa-newspaper-o" aria-hidden="true"></i>
																					View invoice
																				</a>
																			@endif
																		@endif
																	</td>
																</tr>
																@endforeach
																@endif
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									


								</div>
							</div>
						</div>
					</div>



				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-requests').DataTable({});
			$('#open-requests').DataTable({});
			$('#closed-requests').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
