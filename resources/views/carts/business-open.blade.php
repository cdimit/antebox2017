@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Shopping carts</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>
					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Open shopping carts</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										This page will list all open shopping carts created with items from your business. These carts are either awaiting bids for delivery, or have accepted bids and are awaiting delivery of their items. Once the items have been delivered and paid for, you will find them in your 'Completed sales' pages under 'Shopping Carts'.
									</p>
									<div class="table_scroll">
										<table id="all-requests" class="table table-striped table-bordered no-wrap dashboard-table" >
											<thead>
												<tr>
													<th>Customer name</th>
													<th>Items</th>
													<th>Cart value</th>
													<th>Delivery location</th>
													<th>View</th>
													<th>Bid</th>
													<th>Payment</th>
												</tr>
											</thead>
											<tbody>
												@if($requests)
												@foreach($requests as $req)
												<tr>
													<td>
														{{$req->client->first_name}}, {{$req->client->last_name}}
													</td>
													<td>
														{{count($req->items)}}
													</td>
													<td>
														€ {{number_format($req->total_price, 2)}}
													</td>
													<td>
														{{$req->meetingPoint['city']}}, {{$req->meetingPoint['country']}}
													</td>
													<td>
														@if($req->status == 'open')
														<a href="/dev5" class="btn btn-xs btn-default"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
														@elseif($req->status == 'close')
														Bid already accepted.
														@endif
														
													</td>
													<td>
														<a href="/business/carts/{{$req->id}}/bid" class="btn btn-xs btn-default"><i class="fa fa-gavel" aria-hidden="true"></i> Make a bid</a>
													</td>
													<td>
														@if($req->status == 'close')
															@if($req->invoice->is_paid == 1)
																<i class="fa fa-check" aria-hidden="true"></i>
																Payment made
															@else
																Payment pending.
															@endif
														@else 
															Payment pending.
														@endif
													</td>
												</tr>
												@endforeach
												@endif
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>



				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-requests').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
