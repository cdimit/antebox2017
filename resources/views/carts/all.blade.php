@extends('layouts.boilerplate')

@section('head')
	<title>My shopping carts - Shopguin</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey mt--20">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-shopping-cart"></i> My shopping carts
		</h1>
		<p class="s-13em">
			Shopping carts work differently in Shopguin. For every company that sells on Shopguin, you get to make a shopping cart specifically with them. You can then individually request a deliverer or conventional delivery method, for the delivery of your items.
		</p>
		<div class="row">
			<div class="col-lg-9">
				<div class="row">
					<div class="col-md-12">
						<h3 class="w300 c-gray accordion" data-toggle="collapse" data-target="#checkedout">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							Checked out carts
						</h3>
					</div>
					@if(Auth::user()->profile->requests)
						@php ($open_carts = Auth::user()->profile->requests)
						@foreach($open_carts as $cart)
							<div id="checkedout" class="col-md-12 collapse">
								<div class="box-light">
									<div class="header {{$cart->status == 'close' ? 'green' : 'blue'}}">
										<div class="row">
											<div class="col-md-12 col-xs-12 ">
												
												@if($cart->status == 'close')
													<h4 class="w300 no-vm {{$cart->status == 'close' ? 'c-black' : ''}}" >
														<i class="pe-7s-cart pe-lg pe-va"></i>
														{{$cart->business->name}} ({{$cart->items->count()}})
													</h4>
													<h4 class="w300 c-black">
														<i class="icon icon-success"></i> &nbsp;
														Bid accepted at €{{$cart->bids[0]->delivery_fee}}, for delivery on {{$cart->bids[0]->delivery_date}} by {{$cart->bids[0]->biddable->name}}.
													</h4>
												@else
													<h4 class="w300 c-white">
														<i class="pe-7s-cart pe-lg pe-va"></i>
														{{$cart->business->name}} ({{$cart->items->count()}})   |  &nbsp;
														Awaiting bids on this cart
													</h4>
												@endif
											</div>
										</div>
									</div>
									@if($cart->status == 'close' && $cart->invoice->is_paid == '0')
										<div class="info-bar ">
											<h4 class="c-white w300 no-vm">
												<i class="icon icon-cash"></i> &nbsp;
												Payment outstanding for cart
											</h4>

										</div>
									@endif
									<div class="shopping-cart-body">
										<ul class="products-as-list">
											<li>
												<div class="row">
													@foreach($cart->items as $item)
													<div class=" col-xs-12 col-sm-6 col-md-6">
														<div class="row">
															<div class="col-md-2 col-xs-3">
																<img class="img-responsive" src="/storage/{{$item->product->picture}}">
																</br>
															</div>
															<div class="col-md-10 col-xs-9">
																<h4>
																	{{$item->product->name}}
																</h4>
																<h6>
																	<span>
																		Qty
																	</span>
																	{{$item->qty}} @
																	€ {{$item->price}}
																</h6>
																<hr>
															</div>
														</div>
													</div>
													@endforeach
												</div>
											</li>
										</ul>
										<div class="row">
											<div class="col-md-5 col-xs-12">	
												<h3 class="info no-vm">
													<span>
														<i class="icon icon-euro"></i>
														Total
													</span> 
													€{{$cart->total_price}}
												</h3>
												<h3 class="info no-vm">
													<span>
														<i class="icon icon-small-calendar"></i>
														Created on
													</span>
													{{$cart->created_at}}
												</h3>								
												<h3 class="info" style="margin-top: 0px;">
													<span><i class="icon icon-place"></i> &nbsp;Delivery location</span>
													<br>
													@if($cart->meetingPoint)
													{{$cart->meetingPoint->address1}},
													{{$cart->meetingPoint->address2}},
													{{$cart->meetingPoint->city}},
													{{$cart->meetingPoint->state}},
													{{$cart->meetingPoint->post_code}},
													{{$cart->meetingPoint->country}}
													@endif
												</h3>
											</div>
											<div class="col-md-5">
												<h3 class="info no-vm">
													<span>
														<i class="icon icon-hammer"></i>
														Bids
													</span>
													{{$cart->bids->count()}}
												</h3>
											</div>
											<div class="col-md-2 col-xs-5 text-right">
												@if($cart->status == 'close')
													<a href="/client/request/{{$cart->id}}/view" class="btn btn-primary" style="display: inline-block;">
														View cart
														<i class="icon icon-right-chevron"></i>
													</a>
													<br></br>
													@if($cart->invoice->is_paid == 1)
													<a href="/deal/{{$cart->invoice->id}}" class="btn btn-primary">
														<i class="icon icon-invoice"></i> &nbsp;
														View invoice
													</a>
													@endif
												
												@else
													<a href="/client/request/{{$cart->id}}/view" class="btn btn-primary" style="display: inline-block;">
														View bids
														<i class="icon icon-right-chevron"></i>
													</a>
												
												@endif
												
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@endif
					<div class="col-md-12">
						<h3 class="w300 c-gray accordion" data-toggle="collapse" data-target="#opencarts">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							Open carts
						</h3>		
					</div>
					@foreach($shopping_carts as $cart)
						<div id="opencarts" class="collapse col-md-12">
							<div class="box-light">
								<div class="row">
									<div class="col-md-12 col-xs-10">
										<h4 class="w300 no-vm">
											<i class="pe-7s-cart pe-lg pe-va"></i>
											{{$cart->business->name}} ({{$cart->items->count()}})
										</h4>
										<hr>
									</div>
								</div>
								<div class="shopping-cart-body">
									<ul class="products-as-list">
										<li>
											<div class="row">
												@foreach($cart->items as $item)
												<div class=" col-xs-12 col-sm-6 col-md-6">
													<div class="row">
														<div class="col-md-2 col-xs-3">
															<img class="img-responsive" src="/storage/{{$item->product->picture}}">
															<br>
														</div>
														<div class="col-md-10 col-xs-9">
															<h4>
																{{$item->product->name}}
															</h4>
															<h6>
																<span>
																	Qty
																</span>
																{{$item->qty}} @
																€ {{$item->price}}
															</h6>
															<hr>
														</div>
													</div>
												</div>
												@endforeach
											</div>
										</li>
									</ul>
									<div class="row">
										<div class="col-md-10 col-xs-12">	
											<h3 class="info no-vm">
												<span>
													<i class="icon icon-euro"></i>
													Total
												</span> 
												€{{$cart->price}}
											</h3>
											<h3 class="info no-vm">
												<span>
													<i class="icon icon-small-calendar"></i>
													Created on
												</span>
												{{$cart->created_at}}
											</h3>
										</div>
										<div class="col-md-2 col-xs-12 text-right">
											<a href="/client/request/{{$cart->id}}/create" class="btn btn-primary" style="display: inline-block;">
												Checkout
												<i class="icon icon-right-chevron"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="col-lg-3 side-info">
				<h3>
					Instructions
				</h3>
				<p class="s-12em">
					Browse through the products, and if you want some start adding them to your cart. Every product will be automatically added to a shopping cart that you can then complete with every business.
				</p>
			</div>
		</div>
	</div>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection
