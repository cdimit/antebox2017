@extends('layouts.boilerplate')

@section('head')
	<title>My shopping carts - Shopguin</title>
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')
	<button type="button" class="quick-action-btn visible-xs">
		<i class="pe-7s-keypad pe-3x pe-va"></i>
	</button>

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<div class="row">
			<div class="col-md-12">
				<h1 class="w300">
					<i class="icon icon-shopping-cart"></i> Item successfully added to cart!
				</h1>
				<p class="s-13em">
					Your product was successfully added to your cart! Would you like to continue shopping, or check out your cart now?
				</p>
				
			</div>

		</div>
		<div class="row">
			<div class="col-md-6 col-xs-12 text-center">
				<a href="/client/request/{{$shopping_cart->id}}/create" class="btn btn-primary btn-lg">
					<i class="icon icon-shopping-basket"></i>
					Checkout my cart
				</a>
				<br>
				<br>

			</div>
			<div class="col-md-6 col-xs-12 text-center">
				<a href="/products" class="btn btn-primary btn-lg">
					<i class="icon icon-shopping-cart"></i>
					Continue shopping
				</a>
			</div>
		</div>
		<br>
	</div>
</section>
@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection
