@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Shopping carts</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>
					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Completed sales</h2>

									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<p class="text-muted font-13 m-b-30">
										This page will list all completed transactions made with your business on Shopguin, that have been paid for.
									</p>
									<div class="table_scroll">
										<table id="all-requests" class="table table-striped table-bordered no-wrap dashboard-table" >
											<thead>
												<tr>
													<th>Customer name</th>
													<th>Items</th>
													<th>Delivery location</th>
													<th>Cart value</th>
													<th>Total value</th>
													<th>View</th>
												</tr>
											</thead>
											<tbody>
												
												<tr>
													<td>
														John Doe
													</td>
													<td>
														56
													</td>
													<td>
														London, UK
													</td>
													<td>
														£65.00
													</td>
													<td>
														£72.00
														
													</td>
													<td>
														<a href="/business/carts/2/invoice" class="btn btn-default btn-xs">
															<i class="fa fa-newspaper-o" aria-hidden="true"></i>
															View invoice
														</a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>


								</div>
							</div>
						</div>
					</div>



				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-requests').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
