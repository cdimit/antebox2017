<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Shopguin - Dashboard</title>
	<!-- Font Awesome -->
	<script src="https://use.fontawesome.com/67b15a84b6.js"></script>
	<!-- NProgress -->
	<link href="/css/assets/nprogress.css" rel="stylesheet">
	{{-- DataTables --}}
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
	{{-- Country selector --}}
	<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
	<!-- Custom Theme Style -->
	<link href="/css/gent-dashboard.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/assets/datepicker.css">
	<link rel="stylesheet" href="/css/assets/datepicker.css">
	<link rel="stylesheet" href="/css/assets/smart_wizard.min.css">
	{{-- Favicon --}}
	<link rel="icon" type="img/ico" href="icon/favicon.ico">
	<link rel="icon" sizes="1024x1025" href="icon/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="1024x1025" href="img/ios-favicon.png" />
	<!-- Scripts -->
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>

</head>
