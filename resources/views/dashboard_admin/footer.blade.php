{{-- Footer --}}
<footer>
	<div class="pull-right">
		&copy; 2017, Shopguin Ltd. | 
		Template by <a href="https://colorlib.com">Colorlib</a>
	</div>
	<div class="clearfix"></div>
</footer>