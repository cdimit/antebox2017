@component('mail::message')
# Hello!

Welcome to Shopguin! In order to get started, you need to confirm your email address.

@component('mail::button', ['url' => $url])
Confirm Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
