@extends('layouts.boilerplate')

@section('head')
	<title>Invoice - Shopguin</title>
	@include('partials.head')
	<link rel="stylesheet" href="/css/assets/datepicker.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar')

	@include('partials.quick-action')
@endsection

@section('body')
<section class="grey">
	<div class="container first">
		<h1 class="w300">
			<i class="icon icon-shopping-cart"></i>
			Invoice for {{$invoice->clientRequest->business->name}} shopping cart
		</h1>
		<h4 class="w300 c-gray">
			<span>
				<i class="icon icon-small-calendar"></i> &nbsp;
			</span>
			Invoice for {{$invoice->clientRequest->created_at}}
		</h4>
		<div class="row">
			<div class="col-md-12">
				{{-- Shopping cart contents --}}
				<div class="box-light">
					<div class="row">
						<div class="col-md-12 col-xs-10">
							<h4 class="w300 no-vm">
								<i class="pe-7s-cart pe-lg pe-va"></i>
								Cart items
							</h4>
							<hr>
						</div>
					</div>
					<div class="shopping-cart-body">
						<ul class="products-as-list">
							<li>
								<div class="row">
									@foreach($invoice->clientRequest->items as $item)
									<div class=" col-xs-12 col-sm-4 col-md-6">
										<div class="row">
											<div class="col-md-2 col-xs-3 col-sm-3">
												<img class="img-responsive" src="/storage/{{$item->product->picture}}">
												<br>
											</div>
											<div class="col-md-10 col-xs-9 col-sm-9">
												<h4>
													{{$item->product->name}}
												</h4>
												<h6>
													<span>
														Qty
													</span>
													{{$item->qty}} @
													€ {{$item->price}}
												</h6>
												<hr>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</li>

						</ul>
						<div class="row">
							<div class="col-md-7 col-xs-12">
								<h3 class="info" style="margin-top: 0px;">
									<span><i class="icon icon-place"></i> &nbsp;Delivery location</span> <br>
									{{$invoice->clientRequest->meetingPoint->address1}}, <br>
									{{$invoice->clientRequest->meetingPoint->address2}}, <br>
									{{$invoice->clientRequest->meetingPoint->city}}, 
									{{$invoice->clientRequest->meetingPoint->state}},<br>
									{{$invoice->clientRequest->meetingPoint->post_code}}, 
									{{$invoice->clientRequest->meetingPoint->country}}
									<br>
								</h3>
								
								@if($invoice->deal->isClientDeal())
									<h3 class="info no-vm">
										<span>
											<i class="pe-7s-users pe-lg pe-va" ></i>
											Deliverer
										</span>
										{{$invoice->deal->deliverer->name}}<br>
									</h3>
								@else
									<h3 class="info no-vm">
									<span>
										<i class="pe-7s-users pe-lg pe-va" ></i>
										Deliverery method
									</span>
									{{$invoice->clientRequest->bids[0]->delivery_method}}<br>
								</h3>
									<h3 class="info no-vm">
										<span>
											<i class="icon icon-delivery"></i>
											Tracking number <br>
										</span>
										{{$invoice->deal->tracking_number}}
									</h3>
								@endif
							</div>
							<div class="col-md-5 col-xs-12">
								<h3 class="info no-vm">
									<span>
										
										Sub-total
									</span> 
									€ {{$invoice->clientRequest->total_price}}
								</h3>
								<h3 class="info no-vm">
									<span>
										Delivery fee
									</span> 
									€ {{$invoice->delivery_fee}}
								</h3>
								<h3 class="info no-vm">
									<span>
										Service
									</span> 
									{{$invoice->service_value}}%
								</h3>
								<h3 class="info no-vm">
									<span>
										Service fee
									</span> 
									€ {{$invoice->service_fee}}
								</h3>

								<h2 class="info no-vm">
									<span>
										<i class="icon icon-euro"></i>
										Grand total
									</span> 
									€ {{$invoice->final_price}}
								</h2>						
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<button onclick="window.print();" class="btn btn-primary">
					<i class="icon icon-printer"></i> &nbsp;
					Print invoice
				</button>
				<br>
				<br>
			</div>
		</div>
	</div>

</section>



@endsection


@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		$(document).ready(function(){
		});
	</script>
	<script type="text/javascript" src="/js/delivery/delivery-home.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&callback=initMap" async defer></script>
@endsection
