@include('dashboard.head')

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			@include('dashboard.nav')


			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
				 		<div class="title_left">
					 		<h2>Invoice</h2>
				  		</div>
					</div>
					<div class="clearfix"></div>
					@if (session('status'))
						<div class="alert alert-success">
							<strong>{{ session('status') }}</strong>
						</div>
					@endif

					<div class="row">
						<div class="col-md-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>
										Invoice for
										{{$invoice->clientRequest->client->first_name}}
										{{$invoice->clientRequest->client->last_name}}
									</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<section class="content invoice">
										<!-- title row -->
										<div class="row">
											<div class="col-xs-12 invoice-header">
												<h1>
													{{$invoice->clientRequest->business->name}}
													<small class="pull-right">
														Date:
														{{$invoice->clientRequest->created_at}}
													</small>
												</h1>
											</div>
											<!-- /.col -->
										</div>
										<!-- info row -->
										<div class="row invoice-info">
											<div class="col-sm-4 invoice-col">
												From
												<address>
													@php($biz = $invoice->clientRequest->business)
													<strong>
														{{$invoice->clientRequest->business->name}}
													</strong>
													<br>
													{{$biz->address1}} <br>
													{{$biz->address2}} <br>
													{{$biz->city}} <br>
													{{$biz->zip_code}} <br>
													{{$biz->country}} <br>
												</address>
											</div>
											<!-- /.col -->
											<div class="col-sm-4 invoice-col">
												Delivery to
												<address>
													<strong>
														{{$invoice->clientRequest->client->first_name}}
														{{$invoice->clientRequest->client->last_name}}
													</strong>
													@php($add = $invoice->clientRequest->meetingPoint)
													<br>
													{{$add->address1}} <br>
													{{$add->address2}} <br>
													{{$add->city}} <br>
													{{$add->post_code}} <br>
													{{$add->country}} <br>
												</address>
											</div>
											<!-- /.col -->

											<div class="col-sm-4 invoice-col">
												<b>Invoice</b> #{{$invoice->id}}
												<br>
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->

										<!-- Table row -->
										<div class="row">
											<div class="col-xs-12 table">
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Qty</th>
															<th>Product name</th>
															<th style="width: 59%">Description</th>
															<th>Subtotal</th>
														</tr>
													</thead>
													<tbody>
														@foreach($invoice->clientRequest->items as $item)
														<tr>
															<td>
																{{$item->qty}}
															</td>
															<td>
																{{$item->product->name}}
															</td>
															<td>
																{{$item->product->description}}
															</td>
															<td>
																€ {{number_format($item->price, 2)}}
															</td>
														</tr>
														@endforeach
													</tbody>
												</table>
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->

										<div class="row">
											<!-- accepted payments column -->
											<div class="col-xs-6">
												<p class="lead">Delivery method</p>
													@if($invoice->deal->isClientDeal())
														<div class="x_panel">
															<h5>Deliverer (crowd delivery):</h5>
															{{$invoice->deal->deliverer->name}}
															<br>
															<h5>Expected delivery date:</h5>
															{{$invoice->clientRequest->bids[0]->delivery_date}}
														</div>
														
													@else
														<div class="x_panel">
															<h5>Delivery method:</h5>
															{{$invoice->clientRequest->bids[0]->delivery_method}}
															<br>
															<h5>Expected delivery date:</h5>
															{{$invoice->clientRequest->bids[0]->delivery_date}}
															<br>
															<h5>Tracking number:</h5>
															{{$invoice->deal->tracking_number}}
														</div>
													@endif
											</div>
											<!-- /.col -->
											<div class="col-xs-6">
												<p class="lead">Amount Due 2/22/2014</p>
												<div class="table-responsive">
													<table class="table">
														<tbody>
															<tr>
																<th style="width:50%">Subtotal:</th>
																<td>
																	€ {{number_format($invoice->clientRequest->total_price, 2)}}
																</td>
															</tr>
															<tr>
																<th>Delivery cost</th>
																<td>
																	€ {{number_format($invoice->delivery_fee, 2)}}
																</td>
															</tr>
															<tr>
																<th>Service</th>
																<td>
																	{{$invoice->service_value}} %
																</td>
															</tr>
															<tr>
																<th>Service fee</th>
																<td>
																	€ {{number_format($invoice->service_fee, 2)}}
																</td>
															</tr>
															<tr>
																<th>Total:</th>
																<td>€ {{number_format($invoice->final_price, 2)}}</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->

										<!-- this row will not appear when printing -->
										<div class="row no-print">
											<div class="col-xs-12">
												<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>



				</div>
			</div>


			@include('dashboard.footer')
		</div>
	</div>

	@include('dashboard.scripts')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#all-requests').DataTable({});
			console.log("Initialised");
		});
	</script>
</body>
</html>
