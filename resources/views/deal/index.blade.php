@extends('layouts.app')

@section('content')
<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Deal Invoice for client</div>
					 <div class="panel-body">
						 Products:<br>
						 @foreach($invoice->clientRequest->items as $item)
							 {{$item->product->name}} | Value: € {{$item->price}}<br>
						 @endforeach
						 <br>
						 Products Total Price: € {{$invoice->clientRequest->total_price}}<br>
						 <br>
						 Delivery Fee: € {{$invoice->delivery_fee}}<br>
						 <br>
						 Service Value: {{$invoice->service_value}}%<br>
						 <br>
						 Service Fee: € {{$invoice->service_fee}}<br>
						 <br>
						 Final Price: € {{$invoice->final_price}}
					 </div>
				</div>
		  </div>
	 </div>
</div>

<div class="container">
	 <div class="row">
		  <div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					 <div class="panel-heading">Deal</div>
					 <div class="panel-body">
						 Client: {{$invoice->clientRequest->client->name}}<br>
						 <br>
						 Deliverer: {{$invoice->deal->id}}<br>
						 <br>
						 Is client paid: {{$invoice->is_paid}}
						 <a href ="#" class="btn btn-success">Pay</a>
						 <br>
						 <br>
						 @if($invoice->deal->isClientDeal())
						 Is deliverer bought the products: {{$invoice->deal->is_bought}}<br>
						 <br>
						 Is client recive the products: {{$invoice->deal->is_received}}<br>
						 <br>
						 Is deliverer deliver the products: {{$invoice->deal->is_delivered}}<br>
						 <br>
						 @else
						 Tracking Number: {{$invoice->deal->tracking_number}}<br>
						 <br>
						 Is client recive the products: {{$invoice->deal->is_received}}<br>
						 @endif
					 </div>
				</div>
		  </div>
	 </div>
</div>


@endsection
