@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>My dashboard - Shopguin</title>
	<link rel="stylesheet" type="text/css" href="/css/assets/pikaday.css">
	<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
<div class="container">
	<div class="row dashboard">
		@include('client_profile.sidebar')
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 main-content">
			<h2>
				My meeting points
			</h2>
			<p>
				Add your favourite meeting points here, which you can choose from when requesting delivery of your items.
			</p>
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">

					@if (session('status'))
						<div class="alert alert-success alert-dismissable">
							<strong>{{ session('status') }}</strong>
							<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
					@endif

					<div class="row">
						@foreach($mpoints as $mpoint)
							<div class="col-md-6 col-xs-12">
								<div class="box-light">
									<h4 class="w300 no-vm" style="padding-bottom: 5px;">
										<i class="icon icon-place"></i> 
										Location One
									</h4>
									<p class="s-11em">
										
										{{$mpoint->address1}},<br>
										{{$mpoint->address2}},<br>
										{{$mpoint->state}},<br>
										{{$mpoint->city}},
										{{$mpoint->zip_code}}<br>
										{{$mpoint->country}},<br>
										
									</p>
									<div class="footer">
										<button type="button" onclick="editMeetingPointMap({{$mpoint->id}})" id="newMeetingPoint" class="btn btn-info btn-xs" data-toggle="modal" data-target="#meetingPointEdit-{{$mpoint->id}}">
											<i class="icon icon-pencil-edit"></i>
											Edit
										</button>
										<a href="/client/meeting_points/{{$mpoint->id}}/delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
										
									</div>

									{{-- Edit meeting point --}}
									@include('client_profile.edit_meeting_point')

								</div>
							</div>
						@endforeach

					</div>

					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#meetingPointCreate">
						<i class="pe-7s-users pe-lg pe-va"></i> &nbsp;
						Add a meeting point
					</button>

					{{-- Add meeting point --}}
					@include('client_profile.add_meeting_point')

				</div>

				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 side-info">
					<h3>
						Instructions
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript">
		var positionDoubleClick;

		$('#meetingPointCreate').on('shown.bs.modal', function(){
			initialise(0);
			console.log('Worked');
		});

		function editMeetingPointMap(id) {
			$('#meetingPointEdit-' + id).on('shown.bs.modal', function(){
				initialise(id);
				console.log('Worked');
			});
		}


		function initialise(id) {
			var initialCentre = new google.maps.LatLng(50.5, 4.5);
			var map = new google.maps.Map(document.getElementById('meetingPointMap-' + id), {
				center: initialCentre,
				disableDoubleClickZoom: true,
				zoom: 5
			});
			var marker = new google.maps.Marker({
				position: initialCentre,
				map: map
			});

			google.maps.event.addListener(map, 'dblclick', function(e){
				positionDoubleClick = e.latLng;
				marker.setPosition(positionDoubleClick);
				var _x = "#_map_x-" + id;
				var x = "#map_x-" + id;

				var _y = "#_map_y-" + id;
				var y = "#map_y-" + id;

				$(x).val(positionDoubleClick.lat());
				$(y).val(positionDoubleClick.lng());

				$(_x).val(positionDoubleClick.lat());
				$(_y).val(positionDoubleClick.lng());
			});
		}
		// google.maps.event.addDomListener(window, 'load', initialise)
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyMYcybGUzLrw4_suuPY-6XdmOk4JZMQY&libraries" async defer></script>
@endsection
