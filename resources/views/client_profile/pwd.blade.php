@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>My dashboard - Shopguin</title>
	<link rel="stylesheet" type="text/css" href="/css/assets/pikaday.css">
	<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection


@section('body')
<div class="container">
	<div class="row dashboard">
		@include('client_profile.sidebar')
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 main-content">
			<h2>
				Change account settings
			</h2>
			<p>
				Change your account settings here.
			</p>
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
					@if (session('error_password'))
						<div class="alert alert-danger">
							{{ session('error_password') }}
						</div>
					@endif

					@if (session('status_password'))
						<div class="alert alert-success">
							<strong>{{ session('status_password') }}</strong>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="/account/settings/password/update" >
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
					<label for="old_password" class="col-md-4 control-label">Old Password</label>

					<div class="col-md-6">
					<input id="old_password" type="password" class="form-control" name="old_password" required>

					@if ($errors->has('old_password'))
					<span class="help-block">
					<strong>{{ $errors->first('old_password') }}</strong>
					</span>
					@endif
					</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password" class="col-md-4 control-label">New Password</label>

					<div class="col-md-6">
					<input id="password" type="password" class="form-control" name="password" required>

					@if ($errors->has('password'))
					<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
					</span>
					@endif
					</div>
					</div>

					<div class="form-group">
					<label for="password-confirm" class="col-md-4 control-label">Confirm New Password</label>

					<div class="col-md-6">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
					</div>
					</div>


					<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="btn btn-primary">
					Change Password
					</button>
					</div>
					</div>
					</form>
					<p>
						*Required
					</p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 side-info">
					<h3>
						Instructions
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
