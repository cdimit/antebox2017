@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
	<title>My dashboard - Shopguin</title>
	<link rel="stylesheet" type="text/css" href="/css/assets/pikaday.css">
	<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
<div class="container">
	<div class="row dashboard">
		@include('client_profile.sidebar')
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 main-content">
			<h2>
				Change account settings
			</h2>
			<p>
				Change your account settings here.
			</p>
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
					<form class="form-horizontal" role="form" method="POST" action="/account/settings/payout/update" >
						{{ csrf_field() }}

						<div class="form-group  {{ $errors->has('paypal') ? ' has-error' : '' }}">
							<label for="paypal" class="col-md-4 control-label">Paypal Account Email</label>

							<div class="col-md-6">
								<input type="email" class="form-control  " name="paypal" @if($user->payout('paypal')) value="{{$user->payout('paypal')->data}}" @endif>

								@if ($errors->has('paypal'))
									<span class="help-block">
										<strong>{{ $errors->first('paypal') }}</strong>
									</span>
								@endif
							</div>

						</div>

						<div class="form-group{{ $errors->has('iban') ? ' has-error' : '' }}">
							<label for="iban" class="col-md-4 control-label">IBAN Electronic Formatl</label>

							<div class="col-md-6">
								<input type="text" class="form-control" name="iban" placeholder="ex: CY17002001280000001200527600" @if($user->payout('iban')) value="{{$user->payout('iban')->data}}" @endif>

								@if ($errors->has('iban'))
									<span class="help-block">
										<strong>{{ $errors->first('iban') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Update Payout Methods
								</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 side-info">
					<h3>
						Instructions
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection
