{{-- Add meeting point --}}
<div class="modal fade meeting-point-modal" id="meetingPointCreate" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add a new meeting point</h4>
			</div>

			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="/client/meeting_points/create" >
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-6">
							<label for="address1" class="">Address line 1</label>
							<input id="address1" type="text" class="{{ $errors->has('address1') ? ' has-error' : '' }}" value="{{ old('address1') }}" name="address1" required>
							@if ($errors->has('address1'))
								<span class="help-block">
									<strong>{{ $errors->first('address1') }}</strong>
								</span>
							@endif

							<label for="address2" class="">Address line 2</label>
							<input id="address2" type="text" class="{{ $errors->has('address2') ? ' has-error' : '' }}" value="{{ old('address2') }}" name="address2" required>
							@if ($errors->has('address2'))
								<span class="help-block">
									<strong>{{ $errors->first('address2') }}</strong>
								</span>
							@endif
						
							<label for="city" class="">City</label>
							<input id="city" type="text" class="{{ $errors->has('city') ? ' has-error' : '' }}" name="city" value="{{ old('city') }}" required>
							@if ($errors->has('city'))
								<span class="help-block">
									<strong>{{ $errors->first('city') }}</strong>
								</span>
							@endif

							<label for="state" class="">State</label>
							<input id="state" type="text" class="{{ $errors->has('state') ? ' has-error' : '' }}" name="state" value="{{ old('state') }}">
							@if ($errors->has('state'))
								<span class="help-block">
									<strong>{{ $errors->first('state') }}</strong>
								</span>
							@endif

							<label for="post_code" class="">Post Code</label>
							<input id="post_code" type="text" class="{{ $errors->has('post_code') ? ' has-error' : '' }}" name="post_code" value="{{ old('post_code') }}">
							@if ($errors->has('post_code'))
								<span class="help-block">
									<strong>{{ $errors->first('post_code') }}</strong>
								</span>
							@endif
						
							<label for="country" class="">Country</label>
							<input id="country" type="text" class="{{ $errors->has('country') ? ' has-error' : '' }}" name="country" value="{{ old('country') }}" required>
							@if ($errors->has('country'))
								<span class="help-block">
									<strong>{{ $errors->first('country') }}</strong>
								</span>
							@endif
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<div class="col-xs-12">
									<div class="googleMaps" id="meetingPointMap-0" style="width:100%;height:300px;">
									</div>

								</div>

							</div>

							<div class="form-group{{ $errors->has('map_x') ? ' has-error' : '' }}">
								<label for="map_x" class="col-md-4 col-xs-12 control-label">Latitude</label>

								<div class="col-md-6 col-xs-12 ">
									<input id="_map_x-0" type="text" class="form-control" name="_map_x" value="" disabled>
									<input id="map_x-0" type="hidden" class="form-control" name="map_x" value="" >

									@if ($errors->has('map_x'))
										<span class="help-block">
											<strong>{{ $errors->first('map_x') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('map_y') ? ' has-error' : '' }}">
								<label for="map_y" class="col-md-4 col-xs-12 control-label">Longititude</label>

								<div class="col-md-6 col-xs-12">
									<input id="_map_y-0" type="text" class="form-control" name="_map_y" value="" disabled>
									<input id="map_y-0" type="hidden" class="form-control" name="map_y" value="" >


									@if ($errors->has('map_y'))
										<span class="help-block">
											<strong>{{ $errors->first('map_y') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
					</div>


					<div class="form-group">
						<div class="row text-center">
							<button type="submit" class="btn btn-primary">
								Add meeting point
							</button>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>
