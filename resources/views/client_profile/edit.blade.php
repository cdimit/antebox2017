@extends('layouts.boilerplate')

@section('head')
	<title>My dashboard - Shopguin</title>
	@include('partials.head')
	<link rel="stylesheet" type="text/css" href="/css/assets/pikaday.css">
	<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
<div class="container">
	<div class="row dashboard">
		@include('client_profile.sidebar')
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 main-content">
			<h2>
				Edit your profile
			</h2>
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
					<form class="form-horizontal" role="form" method="POST" action="/client/profile/edit" enctype="multipart/form-data">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
						<label for="first_name" class="col-md-4 control-label">First name*</label>

						<div class="col-md-6">
						<input id="first_name" type="text" class="form-control" name="first_name" value="{{ $client->first_name }}" required autofocus>

						@if ($errors->has('first_name'))
						<span class="help-block">
						<strong>{{ $errors->first('first_name') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
						<label for="last_name" class="col-md-4 control-label">Last name*</label>

						<div class="col-md-6">
						<input id="last_name" type="text" class="form-control" name="last_name" value="{{ $client->last_name }}" required autofocus>

						@if ($errors->has('last_name'))
						<span class="help-block">
						<strong>{{ $errors->first('last_name') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
						<label for="picture" class="col-md-4 control-label">Profile picture</label>

						<div class="col-md-6">
						<label  class="file-upload">
							<i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload file
							<input id="picture" type="file" name="picture">
						</label>
						<br>
						<img class="col-md-4" style="margin: 20px 0px;" src="/storage/{{$client->picture}}" >

						@if ($errors->has('picture'))
						<span class="help-block">
						<strong>{{ $errors->first('picture') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
						<label for="gender" class="col-md-4 control-label">Gender</label>

						<div class="col-md-6">
						<select id="gender" name="gender">
							<option value="male" {{$client->gender == 'male' ? 'selected' : ''}}>Male</option>
							<option value="female" {{$client->gender == 'female' ? 'selected' : ''}}>Female</option>

						</select>

						@if ($errors->has('gender'))
						<span class="help-block">
						<strong>{{ $errors->first('gender') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
						<label for="birthday" class="col-md-4 control-label">Date of Birth</label>

						<div class="col-md-6">
						<input id="birthday" type="text" class="form-control" name="birthday" value="{{ $client->birthday }}">

						@if ($errors->has('birthday'))
						<span class="help-block">
						<strong>{{ $errors->first('birthday') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
						<label for="country" class="col-md-4 control-label">Country</label>

						<div class="col-md-6">
						<input id="country" type="text" class="form-control" name="country" value="{{$client->country}}">
						<input type="hidden" id="country_code" name="country_code" value=""  />
						<input type="hidden" id="savedCountry" name="savedCountry" value="{{$client->country}}"/>
						@if ($errors->has('country'))
						<span class="help-block">
						<strong>{{ $errors->first('country') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						<label for="phone" class="col-md-4 control-label">Phone</label>

						<div class="col-md-6">
						<input id="phone" type="text" class="form-control" name="phone" value="{{ $client->phone }}">

						@if ($errors->has('phone'))
						<span class="help-block">
						<strong>{{ $errors->first('phone') }}</strong>
						</span>
						@endif
						</div>
						</div>

						<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
						Update
						</button>
						</div>
						</div>
					</form>
					<p>
						*Required
					</p>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 side-info">
					<h3>
						Instructions
					</h3>
					<p>
						This is your account details,  your name and picture will be shown when you are communicating with the businesses or the deliverers.
					</p>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
	<script type="text/javascript" src="/js/assets/pikaday.js"></script>
	<script src="/js/assets/countrySelect.min.js"></script>
	<script type="text/javascript">
		var picker = new Pikaday({
			field: document.getElementById('birthday'),
			format: 'YYYY-MM-DD',
			yearRange: [1900,2005]
			 });
		$(document).ready(function(){
			var countryCode = $("#savedCountry").val();
			$("#country").countrySelect();
			if (countryCode != '') {
				$("#country").countrySelect("selectCountry", countryCode);
			} else {
				console.log("Error");
			};
		});

	</script>
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection
