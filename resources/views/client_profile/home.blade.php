@extends('layouts.boilerplate')

@section('head')
	<title>My dashboard - Shopguin</title>
	@include('partials.head')
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
<div class="container">
	<div class="row dashboard">
		@include('client_profile.sidebar')
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 main-content">
			<h2>
				Hi there {{Auth::user()->profile->first_name}}!
				<p>
					Welcome to your dashboard!
				</p>
			</h2>
			<p>
				This is your personalised dashboard, where you can customise your profile, as well as change important account settings such as email, password and payout methods.
			</p>
		</div>
	</div>
</div>
</body>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
