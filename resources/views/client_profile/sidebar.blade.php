{{-- Sidebar --}}
<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 sidebar">
	<h4>
		<a href="/">
			<i class="fa fa-chevron-left" aria-hidden="true"></i> Back to Shopguin
		</a>
	</h4>
	<ul class="nav nav-pills nav-stacked"  role="tablist">
		<li class="{{ $title == 'Home' ? 'active' : ''}}" ><a href="/client/dashboard"><i class="pe-7s-home pe-lg pe-va"></i> &nbsp;Home</a></li>
		<li class="{{ $title == 'Profile' ? 'active' : ''}}"><a href="/client/profile/edit"><i class="pe-7s-user pe-lg pe-va"></i> &nbsp;My profile</a></li>
		<li class="{{ $title == 'MeetingPoint' ? 'active' : ''}}"><a href="/client/meeting_points"><i class="pe-7s-users pe-lg pe-va"></i> &nbsp;My meeting points</a></li>
		<li class="{{ $title == 'Payout' ? 'active' : ''}}"><a href="/client/profile/payout"><i class="pe-7s-cash pe-lg pe-va"></i> &nbsp; Payout methods</a></li>
		<li class="dropdown {{ $title == 'Settings' ? 'active' : ''}}" role="tab" id="headingOne">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
				<i class="pe-7s-settings pe-lg pe-va"></i> &nbsp;Account settings <span class="caret"></span>
			</a>
		</li>
		<ul id="collapseOne" class="collapse " role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"> 
			<li class="{{ $title == 'Password' ? 'active' : ''}}"><a href="/client/profile/pwd">Change my password</a></li>
			<li class="{{ $title == 'Email' ? 'active' : ''}}"><a href="/client/profile/email">Change my email</a></li>
		</ul>
	</ul>
</div>