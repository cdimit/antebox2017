@include('partials.head')
<link rel="stylesheet" type="text/css" href="/css/assets/pikaday.css">
<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
</head>


<body>
@include('partials.navbar-slim')

<div class="container">

	@if (session('status'))
		<div class="alert alert-success">
			<strong>{{ session('status') }}</strong>
		</div>
	@endif


@foreach($mpoints as $mpoint)
	{{$mpoint->country}}<br>
	{{$mpoint->state}}<br>
	{{$mpoint->city}}<br>
	{{$mpoint->address}}<br>
	{{$mpoint->zip_code}}<br>
	{{$mpoint->map_x}}<br>
	{{$mpoint->map_y}}<br>
	<a href="/client/meeting_points/{{$mpoint->id}}/delete" class="btn btn-danger">Delete</a>
	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalEdit{{$mpoint->id}}">Edit</button>
	<hr>


	<div class="modal fade" id="modalEdit{{$mpoint->id}}" role="dialog">
	    <div class="modal-dialog">

	      <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Edit Meeting Point</h4>
	        </div>
	        <div class="modal-body">
						<form class="form-horizontal" role="form" method="POST" action="/client/meeting_points/{{$mpoint->id}}/edit" >
						{{ csrf_field() }}


							<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
								<label for="country" class="col-md-4 control-label">Country</label>
								<div class="col-md-6">
									<input id="country" type="text" class="form-control" name="country" value="{{ $mpoint->country }}" required>

									@if ($errors->has('country'))
									<span class="help-block">
									<strong>{{ $errors->first('country') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
								<label for="city" class="col-md-4 control-label">City</label>
								<div class="col-md-6">
									<input id="city" type="text" class="form-control" name="city" value="{{ $mpoint->city }}" required>

									@if ($errors->has('city'))
									<span class="help-block">
									<strong>{{ $errors->first('city') }}</strong>
									</span>
									@endif
								</div>
							</div>


							<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
								<label for="address" class="col-md-4 control-label">Address</label>
								<div class="col-md-6">
									<input id="address" type="text" class="form-control" value="{{ $mpoint->address}}" name="address" required>

									@if ($errors->has('address'))
									<span class="help-block">
									<strong>{{ $errors->first('address') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
								<label for="state" class="col-md-4 control-label">State</label>
								<div class="col-md-6">
									<input id="state" type="text" class="form-control" name="state" value="{{ $mpoint->state }}">

									@if ($errors->has('state'))
									<span class="help-block">
									<strong>{{ $errors->first('state') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
								<label for="zip_code" class="col-md-4 control-label">Zip Code</label>
								<div class="col-md-6">
									<input id="zip_code" type="text" class="form-control" name="zip_code" value="{{ $mpoint->zip_code }}">

									@if ($errors->has('zip_code'))
									<span class="help-block">
									<strong>{{ $errors->first('zip_code') }}</strong>
									</span>
									@endif
								</div>
							</div>



							<div class="form-group">
								<div class="col-md-6 col-xs-12 ">
									<div id="map-canvas" style="width:400px;height:300px;">
									</div>
								</div>
							</div>

							<div class="form-group{{ $errors->has('map_x') ? ' has-error' : '' }}">
								<label for="map_x" class="col-md-4 col-xs-12 control-label">Latitude</label>

								<div class="col-md-6 col-xs-12 ">
									<input id="_map_x" type="text" class="form-control" name="_map_x" value="{{$mpoint->map_x}}" disabled>
									<input id="map_x" type="hidden" class="form-control" name="map_x" value="{{$mpoint->map_y}}" >

									@if ($errors->has('map_x'))
									<span class="help-block">
										<strong>{{ $errors->first('map_x') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('map_y') ? ' has-error' : '' }}">
								<label for="map_y" class="col-md-4 col-xs-12 control-label">Longititude</label>

								<div class="col-md-6 col-xs-12">
									<input id="_map_y" type="text" class="form-control" name="_map_y" value="" disabled>
									<input id="map_y" type="hidden" class="form-control" name="map_y" value="" >


									@if ($errors->has('map_y'))
									<span class="help-block">
										<strong>{{ $errors->first('map_y') }}</strong>
									</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-8 col-md-offset-4">
									<button type="submit" class="btn btn-primary">
									Create Meeting Point
									</button>
								</div>
							</div>
						</form>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>
	      </div>

	    </div>
	  </div>

@endforeach

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modalCreate">Add Meeting Point</button>


<div class="modal fade" id="modalCreate" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Meeting Point</h4>
        </div>
        <div class="modal-body">
					<form class="form-horizontal" role="form" method="POST" action="/client/meeting_points/create" >
					{{ csrf_field() }}


						<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
							<label for="country" class="col-md-4 control-label">Country</label>
							<div class="col-md-6">
								<input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}" required>

								@if ($errors->has('country'))
								<span class="help-block">
								<strong>{{ $errors->first('country') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
							<label for="city" class="col-md-4 control-label">City</label>
							<div class="col-md-6">
								<input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required>

								@if ($errors->has('city'))
								<span class="help-block">
								<strong>{{ $errors->first('city') }}</strong>
								</span>
								@endif
							</div>
						</div>


						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<label for="address" class="col-md-4 control-label">Address</label>
							<div class="col-md-6">
								<input id="address" type="text" class="form-control" value="{{ old('address') }}" name="address" required>

								@if ($errors->has('address'))
								<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
							<label for="state" class="col-md-4 control-label">State</label>
							<div class="col-md-6">
								<input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}">

								@if ($errors->has('state'))
								<span class="help-block">
								<strong>{{ $errors->first('state') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
							<label for="zip_code" class="col-md-4 control-label">Zip Code</label>
							<div class="col-md-6">
								<input id="zip_code" type="text" class="form-control" name="zip_code" value="{{ old('zip_code') }}">

								@if ($errors->has('zip_code'))
								<span class="help-block">
								<strong>{{ $errors->first('zip_code') }}</strong>
								</span>
								@endif
							</div>
						</div>



						<div class="form-group">
							<div class="col-md-6 col-xs-12 ">
								<div id="map-canvas" style="width:400px;height:300px;">
								</div>
							</div>
						</div>

						<div class="form-group{{ $errors->has('map_x') ? ' has-error' : '' }}">
							<label for="map_x" class="col-md-4 col-xs-12 control-label">Latitude</label>

							<div class="col-md-6 col-xs-12 ">
								<input id="_map_x" type="text" class="form-control" name="_map_x" value="" disabled>
								<input id="map_x" type="hidden" class="form-control" name="map_x" value="" >

								@if ($errors->has('map_x'))
								<span class="help-block">
									<strong>{{ $errors->first('map_x') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('map_y') ? ' has-error' : '' }}">
							<label for="map_y" class="col-md-4 col-xs-12 control-label">Longititude</label>

							<div class="col-md-6 col-xs-12">
								<input id="_map_y" type="text" class="form-control" name="_map_y" value="" disabled>
								<input id="map_y" type="hidden" class="form-control" name="map_y" value="" >


								@if ($errors->has('map_y'))
								<span class="help-block">
									<strong>{{ $errors->first('map_y') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
								Create Meeting Point
								</button>
							</div>
						</div>
					</form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

</div>

</body>
@include('partials.footer')
@include('partials.scripts')
<script type="text/javascript" src="/js/assets/pikaday.js"></script>
<script src="/js/assets/countrySelect.min.js"></script>
<script type="text/javascript">
	var picker = new Pikaday({
		field: document.getElementById('birthday'),
		yearRange: [1900,2005]
		 });
	$(document).ready(function(){
		var countryCode = $("#savedCountry").val();
		$("#country").countrySelect();
		if (countryCode != '') {
			$("#country").countrySelect("selectCountry", countryCode);
		} else {
			console.log("Error");
		};
	});

</script>
</html>
