@extends('layouts.boilerplate')

@section('head')
	<title>My dashboard - Shopguin</title>
	@include('partials.head')
	<link rel="stylesheet" type="text/css" href="/css/assets/pikaday.css">
	<link rel="stylesheet" href="/css/assets/countrySelect.min.css">
@endsection


@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
<div class="container">
	<div class="row dashboard">
		@include('client_profile.sidebar')
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 main-content">
			<h2>
				Change account settings
			</h2>
			<h3>Change email</h3>
			<p>
				Change your email here.
			</p>
			<div class="row">
				<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
					
					@if (session('status_email'))
					<div class="alert alert-success">
					<strong>{{ session('status_email') }}</strong>
					</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="/account/settings/email/update" >
					{{ csrf_field() }}

					<div class="form-group">
					<label class="col-md-4 control-label">Current Email</label>

					<div class="col-md-6">
					<input type="text" class="form-control" value="{{$user->email}}" readonly>
					</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email" class="col-md-4 control-label">New Email</label>

					<div class="col-md-6">
					<input id="email" type="text" class="form-control" name="email" required>

					@if ($errors->has('email'))
					<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
					</div>
					</div>

					<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="btn btn-primary">
					Change Email
					</button>
					</div>
					</div>
					</form>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 side-info">
					<h3>
						Instructions
					</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/products/products.js"></script>
	<script type="text/javascript" src="/js/assets/pikaday.js"></script>
	<script src="/js/assets/countrySelect.min.js"></script>
	<script type="text/javascript">
		var picker = new Pikaday({ 
			field: document.getElementById('birthday'),
			yearRange: [1900,2005]
			 });
		$(document).ready(function(){
			var countryCode = $("#savedCountry").val();
			$("#country").countrySelect();
			if (countryCode != '') {
				$("#country").countrySelect("selectCountry", countryCode);
			} else {
				console.log("Error");
			};
		});
		
	</script>
	<script type="text/javascript" src="/js/products/products.js"></script>
@endsection