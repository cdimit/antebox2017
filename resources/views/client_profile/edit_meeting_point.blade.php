{{-- Edit meeting point --}}
<div class="modal fade meeting-point-modal" id="meetingPointEdit-{{$mpoint->id}}" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Meeting Point</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="/client/meeting_points/{{$mpoint->id}}/edit" >
					{{ csrf_field() }}

					<div class="row">
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
								<label for="country" class="col-md-4 control-label">Country</label>
								<div class="col-md-6">
									<input id="country" type="text" class="form-control" name="country" value="{{ $mpoint->country }}" required>

									@if ($errors->has('country'))
										<span class="help-block">
											<strong>{{ $errors->first('country') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
								<label for="city" class="col-md-4 control-label">City</label>
								<div class="col-md-6">
									<input id="city" type="text" class="form-control" name="city" value="{{ $mpoint->city }}" required>

									@if ($errors->has('city'))
										<span class="help-block">
										<strong>{{ $errors->first('city') }}</strong>
										</span>
									@endif
								</div>
							</div>


							<div class="form-group{{ $errors->has('address1') ? ' has-error' : '' }}">
								<label for="address1" class="col-md-4 control-label">Address1</label>
								<div class="col-md-6">
									<input id="address1" type="text" class="form-control" value="{{ $mpoint->address1}}" name="address1" required>

									@if ($errors->has('address1'))
										<span class="help-block">
										<strong>{{ $errors->first('address1') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('address2') ? ' has-error' : '' }}">
								<label for="address2" class="col-md-4 control-label">Address2</label>
								<div class="col-md-6">
									<input id="address2" type="text" class="form-control" value="{{ $mpoint->address2}}" name="address2" required>

									@if ($errors->has('address2'))
										<span class="help-block">
										<strong>{{ $errors->first('address2') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
								<label for="state" class="col-md-4 control-label">State</label>
								<div class="col-md-6">
									<input id="state" type="text" class="form-control" name="state" value="{{ $mpoint->state }}">

									@if ($errors->has('state'))
										<span class="help-block">
											<strong>{{ $errors->first('state') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('post_code') ? ' has-error' : '' }}">
								<label for="post_code" class="col-md-4 control-label">Post Code</label>
								<div class="col-md-6">
									<input id="post_code" type="text" class="form-control" name="post_code" value="{{ $mpoint->post_code }}">

									@if ($errors->has('post_code'))
										<span class="help-block">
											<strong>{{ $errors->first('post_code') }}</strong>
										</span>
									@endif
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-xs-12 ">
									<div class="googleMaps" id="meetingPointMap-{{$mpoint->id}}" style="width:100%;height:300px;">
									</div>
								</div>
							</div>

							<div class="form-group{{ $errors->has('map_x') ? ' has-error' : '' }}">
								<label for="map_x" class="col-md-4 col-xs-12 control-label">Latitude</label>

								<div class="col-md-6 col-xs-12 ">
									<input id="_map_x-{{$mpoint->id}}" type="text" class="form-control" name="_map_x" value="{{$mpoint->map_x}}" disabled>
									<input id="map_x-{{$mpoint->id}}" type="hidden" class="form-control" name="map_x" value="{{$mpoint->map_y}}" >

									@if ($errors->has('map_x'))
										<span class="help-block">
											<strong>{{ $errors->first('map_x') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('map_y') ? ' has-error' : '' }}">
								<label for="map_y" class="col-md-4 col-xs-12 control-label">Longititude</label>

								<div class="col-md-6 col-xs-12">
									<input id="_map_y-{{$mpoint->id}}" type="text" class="form-control" name="_map_y" value="" disabled>
									<input id="map_y-{{$mpoint->id}}" type="hidden" class="form-control" name="map_y" value="" >


									@if ($errors->has('map_y'))
										<span class="help-block">
											<strong>{{ $errors->first('map_y') }}</strong>
										</span>
									@endif
								</div>
							</div>

						</div>
						<div class="row text-center">
							<div class="form-group">
								<div class="row text-center">
									<button type="submit" class="btn btn-primary">
										Edit meeting point
									</button>
								</div>
							</div>

						</div>
					</div>


				</form>
			</div>
		</div>
	</div>
</div>
