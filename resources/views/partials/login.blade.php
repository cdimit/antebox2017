{{-- Log in modal --}}
<div class="modal fade" id="loginModal" role="dialog" aria-labelledby="LoginModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<img src="/img/logo-and-text.png" class="img-responsive" style="display:inline; height: 45px; width: auto;">
				<h4 class="modal-title">Welcome back to Shopguin!</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
				{{ csrf_field() }}

				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email" class="col-md-4 control-label">E-Mail Address</label>
					<input  id="emailError" type="hidden" name="" value="{{$errors->has('email')}}">

					<div class="col-md-6">
						<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

						@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password" class="col-md-4 control-label">Password</label>
					<div class="col-md-6">
						<input id="password" type="password" class="form-control" name="password" required>
						<input id="pwdError" type="hidden" name="" value="{{$errors->has('password')}}">
						@if ($errors->has('password'))
						<span class="help-block">
						<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<div class="checkbox">
							<label>
							<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-8 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
						Login
						</button>

						<a class="btn btn-link" href="{{ url('/password/reset') }}">
						Forgot Your Password?
						</a>
					</div>
				</div>
				</form>
				
				<h4 class="w300 c-med-gray">
					Or why not login via:
				</h4>
				<div class="text-center">
					<a href="/auth/facebook" class="btn btn-primary  social-login fb">
						<i class="fa fa-facebook-official" aria-hidden="true"></i> &nbsp;
						Facebook
					</a>
					
				</div>
				
			</div>
			<div class="modal-footer">
				<p>
					First time on Shopguin? <br>
					<a href="/register" class="btn btn-register">Register here</a>
					<br>
					Are you a business? <br>
					<a href="/business/register" class="btn btn-register">Register your business</a>
				</p>
			</div>
		</div>
	</div>
</div>
