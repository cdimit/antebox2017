<div class="icon-btn" id="deliveries-btn">
				<i class="icon icon-delivery-icon" style="font-size: 2.3em;">
					<div class="unread text-center" style="font-size: 0.34em">
						{{Auth::user()->profile->orders}}
					</div>
				</i>
				<div class="icon-dropdown-box" id="deliveries">
					<div class="close-btn text-right" id="close-notifications">
						<i class="fa fa-times-circle fa-1x" aria-hidden="true"></i>
					</div>
					<h4 class="subdued">Deliveries</h4>
					<ul>
				
						<li>
							<a href="/client/request//create">
								<div class="row">
									<div class="col-sm-10 col-md-10">
										<b>
											Apple Ltd. (4 Items)
										</b>
										<br>
										<div class="c-med-gray">
											<i class="icon icon-small-calendar" style="font-size: 0.9em"></i> 27/04/18
											<br>
											<b>Delivery to:</b>	 London, GB
											<br>
											<b>Pickup from:</b>	Manchester, GB
										</div>
									</div>
									<div class="col-sm-2 col-md-2">
										<i class="fa fa-chevron-circle-right" aria-hidden="true" style="font-size: 1em;"></i>
									</div>
								</div>
							</a>
						</li>
							
						
					</ul>
					<div class="text-center">
						<a href="/client/deliveries" class="btn btn-primary btn-xs">
							View all
						</a>
					</div>
				</div>
			</div>