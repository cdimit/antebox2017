<div class="m-search-overlay visible-xs">
	<h2 class="w300">
		Search
	</h2>
	<form method="POST" action="/search" class="navbar-search" role="search">
		{{ csrf_field() }}

		<input name="search_query" type="search" class="input-search" placeholder="Search for products">
		<br>
		<div class="text-center">
			<button type="submit" class="btn btn-primary btn-lg">
				Search <i class="fa fa-search" aria-hidden="true"></i> 
			</button>
			
		</div>
	</form>
</div>