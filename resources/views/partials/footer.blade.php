<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<img src="/img/logo-and-text.png" class="img-responsive footer-logo">
				&copy; 2017, Shopguin Ltd.
				<br>
				All rights reserved.
				<ul>
					<li>
						<a href="/business">
							Sell with us
						</a>
					</li>
					<li>
						<a href="">
							Privacy & Cookies
						</a>
					</li>
					<li>
					<a href="/tsandcs">
							Terms & Conditions
						</a>
					</li>
					<li>
						<a href="/aboutus">
							About us
						</a>
					</li>
					<li>
						<a href="/howto">
							How to guides
						</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<h4>
					Popular categories
				</h4>
				<ul>
					<li>
						<a href="">
							Food
						</a>
					</li>
					<li>
						<a href="">
							Technology
						</a>
					</li>
					<li>
						<a href="">
							Groceries
						</a>
					</li>
					<li>
						<a href="">
							Clothes
						</a>
					</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<h4>
					Follow us on social media
				</h4>
				<ul class="social-media">
					<li>
						<a href="http://www.facebook.com/shopguincyprus">
							<i class="fa fa-facebook-square" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/shopguin">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com/shopguin/">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
				</br>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<h4>
					Get in touch
				</h4>
				<ul>
					<li>
						<span>E</span>
						<a href="mailto:contact@shopguin.com" style="display: inline-block;">
							contact@shopguin.com
						</a>
					</li>
					<li>
						<span style="display: block;">
							<i class="fa fa-map-marker" aria-hidden="true"></i> Address
						</span>
						Omonoias 19 <br>
						Limassol Grind <br>
						Limassol 3052 <br>
						Cyprus
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>