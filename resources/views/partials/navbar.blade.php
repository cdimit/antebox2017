<nav class="navbar navbar-default hidden-xs">
	@include('partials.navbar-upper')

	@include('partials.navbar-lower')

</nav>

@include('partials.m-navbar')

@include('partials.login')