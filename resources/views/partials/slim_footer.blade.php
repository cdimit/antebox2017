<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
				<img src="/img/logo-and-text.png" class="img-responsive footer-logo" style="opacity:0.2;">
				&copy; 2017, Shopguin Ltd.
				<br>
				All rights reserved.
				
			</div>
		
		</div>
	</div>
</footer>