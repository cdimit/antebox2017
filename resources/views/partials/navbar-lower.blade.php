<!-- Lower navbar tabs and links -->
<div class="navbar-lower">
	<div class="container">
		<ul class="nav nav-lower-tabs">
			<div class="nav-option">
				I want to:
			</div>
			<li class="active"><a href="#buy" data-toggle="tab" aria-expanded="false">Buy</a></li>
			<li class=""><a href="#deliver" data-toggle="tab" aria-expanded="true">Deliver</a></li>
		</ul>
	</div>
	<div class="bar">
		<div class="container">
			<div id="myTabContent" class="tab-content">
				<div class="tab-pane fade active in" id="buy">
					<ul class="nav nav-lower-pills">
						<li><a href="/products">All products</a></li>
						{{-- <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
								Departments <span class="caret">&nbsp;&nbsp;</span>
							</a>
							<div class="dropdown-menu clearfix fade" role="menu">
								<div class="row">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 side">
										<a href="" class="main-link">All departments <i class="pe-7s-angle-right-circle"></i></a>
										<h4 class="subdued">
											Popular
										</h4>
										<ul>
											<li><a href="">Bakery</a></li>
											<li><a href="">Confectionery</a></li>
											<li><a href="">Pasta and noodles</a></li>
										</ul>
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 main">
										Hello
									</div>
								</div>
							</div>
						</li> --}}
						<li><a href="/retailers">Retailers</a></li>
						@if(Auth::check())
							<li><a href="/shopping_cart">My shopping carts</a></li>
						@endif

					</ul>
				</div>
				<div class="tab-pane fade " id="deliver">
					<ul class="nav nav-lower-pills">
						<li><a href="/delivery">Find a delivery to make</a></li>
						@if(Auth::check())
							{{-- <li><a href="/client/deliveries">My deliveries</a></li> --}}
							{{-- <li><a href="/client/bids">My bids</a></li> --}}
						@endif
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>