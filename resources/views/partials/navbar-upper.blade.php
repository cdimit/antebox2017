{{-- Upper navbar - Search bar and account links --}}
<div class="container">
	<div class="navbar-upper">
		<a href="/">
			<img src="/img/logo-and-text-sm-beta.png" srcset="/img/logo-and-text.png 2000w" class="img-responsive navbar-logo">
		</a>
		<form method="POST" action="/search" class="navbar-search" role="search">
			{{ csrf_field() }}
			<div class="search-group">
				<input name="search_query" type="search" class="input-search" placeholder="Search">
			</div>
			<button type="submit" class="btn-search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</button>
		</form>
		<div class="account-info " >
			@if(Auth::check())
			<span id="profile-name">
				@if(Auth::user()->isClient())
					{{Auth::user()->profile->first_name}} &nbsp;
				@elseif(Auth::user()->isBusiness())
					{{Auth::user()->profile->name}} &nbsp;
				@endif
			</span>
			<div class="profile-name-btn" id="profile-btn">
				<i class="pe-7s-user pe-2x pe-va" ></i>
				<div class="icon-dropdown-box text-left" id="profile-links">
					<div class="close-btn text-right" id="close-profile">
						<i class="fa fa-times-circle fa-1x" aria-hidden="true"></i>
					</div>
					<h4 class="subdued">My account</h4>
					<ul>
						<li>

							@if(Auth::user()->isClient())
							<a href="/client/dashboard">
								<i class="pe-7s-home pe-2x pe-va" ></i> &nbsp; Dashboard
							</a>
							@elseif (Auth::user()->isBusiness())
							<a href="/dashboard">
								<i class="pe-7s-user pe-2x pe-va" ></i> &nbsp; Dashboard
							</a>
							@endif
						</li>
						<li>
							@if(Auth::user()->isClient())
							<a href="/client/profile/edit">
								<i class="pe-7s-user pe-2x pe-va" ></i> &nbsp; My profile
							</a>
							@elseif (Auth::user()->isBusiness())
							<a href="/business/profile/edit">
								<i class="pe-7s-user pe-2x pe-va" ></i> &nbsp; Business profile
							</a>
							@endif

						</li>

						<li>
							<a href="/conversations">
								<i class="pe-7s-chat pe-2x pe-va"></i> &nbsp;
								Conversations
							</a>
						</li>
						<li>
							<a href="/wishlist">
								<i class="pe-7s-magic-wand pe-2x pe-va" ></i> &nbsp; Wish list
							</a>
						</li>
						<li>
							<a href="/account/dashboard">
								<i class="pe-7s-settings pe-2x pe-va" ></i> &nbsp; Account Settings
							</a>
						</li>
						<li>
							<a href="{{url('/logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
								<i class="pe-7s-close-circle pe-2x pe-va" ></i> &nbsp; Log out
							</a>
							<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</div>
			</div>
			<div class="icon-btn" id="notif-btn">
				<i class="pe-7s-bell pe-2x pe-va" >
					@if(($numNot = Auth::user()->unreadNotifications->count()) != 0 )
					<div class="unread text-center">{{$numNot}}</div>
					@endif
				</i>
				<div class="icon-dropdown-box" id="notifications">
					<div class="close-btn text-right" id="close-notifications">
						<i class="fa fa-times-circle fa-1x" aria-hidden="true"></i>
					</div>
					<h4 class="subdued">Notifications</h4>
					<ul>
						@foreach(Auth::user()->unreadNotifications as $n)
						<li>
							<a href="/notifications/read/{{$n->id}}">
								<div class="row">
									<div class="col-md-10 col-sm-10">
										{{$n['data']['message']}}
									</div>
									<div class="col-md-2 col-sm-2">
										<i class="fa fa-chevron-circle-right" aria-hidden="true" style="font-size: 1em;"></i>
									</div>
								</div>

							</a>
						</li>
						@endforeach
					</ul>
					<div class="text-center">
						<a href="/notifications/markasread" class="btn btn-primary btn-xs">
							Mark all as read
						</a>
					</div>
				</div>
			</div>
			@if(Auth::user()->isClient())
			<div class="icon-btn" id="shopping-cart-btn">
				<i class="pe-7s-cart pe-2x pe-va" >
					<div class="unread text-center">
						{{Auth::user()->profile->orders}}
					</div>
				</i>
				<div class="icon-dropdown-box" id="shopping-carts">
					<div class="close-btn text-right" id="close-notifications">
						<i class="fa fa-times-circle fa-1x" aria-hidden="true"></i>
					</div>
					<h4 class="subdued">Shopping Carts</h4>
					<ul>
						@if(Auth::user()->profile->shoppingCarts)
							@php ($carts = Auth::user()->profile->shoppingCarts)
							@if($carts)
							@foreach($carts->take(3) as $cart)
								<li>
								<a href="/client/request/{{$cart->id}}/create">
									<div class="row">
										<div class="col-md-10 col-sm-10">
											<b>
												{{$cart->business->name}}
											</b>
											</br>
											€ {{$cart->price}} | {{$cart->items->count()}} item(s)

										</div>
										<div class="col-md-2 col-sm-2">
											<i class="fa fa-chevron-circle-right" aria-hidden="true" style="font-size: 1em;"></i>
										</div>
									</div>
								</a>
								</li>
							@endforeach
							@endif
						@endif
						@if(Auth::user()->profile->requests)
							@php ($req = Auth::user()->profile->requests)
							@foreach($req->take(3) as $req)
								<li>
									<a href="/client/request/{{$req->id}}/view">
										<div class="row">
											<div class="col-md-10 col-sm-10">
												<b>
													{{$req->business->name}}
												</b>
												</br>
												€ {{$req->total_price}} | {{$req->items->count()}} item(s)
												</br>
												<span class="c-med-gray">
													@if($req->bids->count() <= 0)
														<i class="icon icon-waiting" style="font-size: 1em"></i>
														Awaiting bids for delivery
														</br>
													@else
														@if($req->status == 'open')
															<i class="icon icon-auction_2"></i>
															Please accept a bid for delivery
															</br>
														@elseif($req->status == 'close')
															@if($req->invoice->is_paid == 1)
																<i class="icon icon-delivery"></i>
																Items en route.
															@elseif ($req->invoice->is_paid == 0)
																<i class="icon icon-cash"></i>
																Please pay for your items.
															</br>
															@endif

														@endif
													@endif
												</span>
											</div>
											<div class="col-md-2 col-sm-2">
												<i class="fa fa-chevron-circle-right" aria-hidden="true" style="font-size: 1em;"></i>
											</div>
										</div>
									</a>
								</li>
							@endforeach
						@endif
					</ul>
					<div class="text-center">
						<a href="/shopping_cart" class="btn btn-primary btn-xs">
							View all
						</a>
					</div>
				</div>
			</div>
		@endif
			{{--
			<div class="icon-btn" id="deliveries-btn">
			<i class="icon icon-delivery-icon" style="font-size: 2.3em;">
				<div class="unread text-center" style="font-size: 0.34em">
					{{Auth::user()->profile->delivers}}
				</div>
			</i>
			<div class="icon-dropdown-box" id="deliveries">
				<div class="close-btn text-right" id="close-notifications">
					<i class="fa fa-times-circle fa-1x" aria-hidden="true"></i>
				</div>
				<h4 class="subdued">Deliveries</h4>
				<ul>

					<li>
						@foreach(Auth::user()->profile->bids as $bid)
							@if($bid->isAccepted())
								<a href="/client/deliveries/{{$bid->deal->id}}">
							@endif
							<div class="row">
								<div class="col-sm-10 col-md-10">
									<b>
										{{$bid->clientRequest->business->name}} ({{$bid->clientRequest->items->count()}} Items)
									</b>
									<br>
									<div class="c-med-gray">
										<i class="icon icon-small-calendar" style="font-size: 0.9em"></i> {{$bid->delivery_date}}
										<br>
										<b>Delivery to:</b>	 {{$bid->clientRequest->meetingPoint->city}}, {{$bid->clientRequest->meetingPoint->country}}
										<br>
										@foreach ($bid->clientRequest->business->stores as $store)
											<b>Pickup from:</b>	{{$store->city}}, {{$store->country}}
											<br>
										@endforeach
										@if($bid->isOpen())
											<b>Wait for approved</b>
										@endif
									</div>
								</div>
								<div class="col-sm-2 col-md-2">
									<i class="fa fa-chevron-circle-right" aria-hidden="true" style="font-size: 1em;"></i>
								</div>
							</div>
						</a>
					@endforeach
					</li>


				</ul>
				<div class="text-center">
					<a href="/client/deliveries" class="btn btn-primary btn-xs">
						View all
					</a>
				</div>
			</div>
		</div>
			--}}

			@else
			<button type="button" class="btn btn-primary nav-btn" data-toggle="modal" data-target="#loginModal"><i class="pe-7s-users pe-2x pe-va" ></i>&nbsp;Login / Register</button>

			@endif
		</div>
	</div>
</div>
