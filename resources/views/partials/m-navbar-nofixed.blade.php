{{-- Mobile navbar --}}
<nav class="navbar navbar-default visible-xs navbar-mobile">
	<div class="m-navbar">
		<div class="m text-left">
			<button class="m-search" id="m-search">
				<i id="search-open" class="fa fa-search fa-lg" aria-hidden="true"></i>
				<i id="search-close" class="fa fa-times fa-lg hide" aria-hidden="true"></i>
			</button>
		</div>
		<div class="m-logo">
			<a href="/" class="text-center">
				<img src="/img/logo-and-text.png" class="img-responsive navbar-logo">
			</a>
		</div>
		<div class="m-icon-btn">
			<button type="button" class="hamburger hamburger--htx"  aria-expanded="false" id="open-nav">
				<span>Toggle menu</span>
			</button>
		</div>

	</div>
</nav>

{{-- Mobile navbar - Open --}}
<div class="m-nav-overlay visible-xs">
	{{-- Profile links slide down --}}
	@if(Auth::check())
	<h4>
		@if(Auth::user()->isClient())
			Welcome {{Auth::user()->profile->first_name}} &nbsp;
		@elseif(Auth::user()->isBusiness())
			Welcome {{Auth::user()->profile->name}} &nbsp;
		@endif
	</h4>
	<h4 id="m-profile-links-btn" class="m-slidedown-btn">
		<i class="rotate pe-7s-angle-down pe-lg pe-va"></i> &nbsp; <i class="pe-7s-user pe-lg pe-va"></i> &nbsp; My account
	</h4>

	<ul id="m-profile-links" class="m-slidedown-links">
		<li>
			@if(Auth::user()->isClient())
			<a href="/client/dashboard">
				<i class="pe-7s-home pe-lg pe-va" ></i> &nbsp; My dashboard
			</a>
			@elseif (Auth::user()->isBusiness())
			<a href="/dashboard">
				<i class="pe-7s-home pe-lg pe-va" ></i> &nbsp; My dashboard
			</a>
			@endif
		</li>
		<li>	
			@if(Auth::user()->isClient())
			<a href="/client/profile/edit">
				<i class="pe-7s-user pe-lg pe-va"></i> &nbsp; My profile
			</a>
			@elseif(Auth::user()->isBusiness())
			<a href="/business/profile/edit">
				<i class="pe-7s-user pe-lg pe-va"></i> &nbsp; My business account
			</a>
			@endif
		</li>
		<li>
			<a href="/conversations">
				<i class="pe-7s-chat pe-lg pe-va" ></i> &nbsp; Conversations
			</a>
		</li>
		
		<li>
			<a href="/wishlist">
				<i class="pe-7s-magic-wand pe-lg pe-va"></i> &nbsp; My wishlist
			</a>
		</li>
		<li>
			<a href="{{url('/logout')}}" onclick="event.preventDefault();document.getElementById('logout-form-m').submit();">
				<i class="pe-7s-user pe-lg pe-va"></i> &nbsp; Log out
			</a>
			<form id="logout-form-m" action="{{ url('/logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>

		</li>

	</ul>

	<!-- Notifications slide-down -->
	<h4 id="m-notifications-btn" class="m-slidedown-btn">
		<i class="rotate pe-7s-angle-down pe-lg pe-va"></i> &nbsp; <i class="pe-7s-info pe-lg pe-va"></i> &nbsp; Notifications &nbsp;
	</h4>
	{{-- <ul id="m-notifications" class="m-slidedown-links">
		<li>
			<a href="">
				<i class="pe-7s-wine pe-lg pe-va"></i> You have a bid to pay for
			</a>
			<a style="float:right;" href="">
			<i class="pe-7s-angle-right-circle pe-lg pe-va"></i>
			</a>
			&nbsp;
			<a style="float:right;" href="">
			<i class="pe-7s-close-circle pe-lg pe-va"></i>
			</a>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			</p>
		</li>
		<li>
			<a href="">
				<i class="pe-7s-wine pe-lg pe-va"></i> You have a bid to pay for
			</a>
			<a style="float:right;" href="">
			<i class="pe-7s-angle-right-circle pe-lg pe-va"></i>
			</a>
			&nbsp;
			<a style="float:right;" href="">
			<i class="pe-7s-close-circle pe-lg pe-va"></i>
			</a>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			</p>
		</li>
		<li>
			<a href="">
				<i class="pe-7s-wine pe-lg pe-va"></i> You have a bid to pay for
			</a>
			<a style="float:right;" href="">
			<i class="pe-7s-angle-right-circle pe-lg pe-va"></i>
			</a>
			&nbsp;
			<a style="float:right;" href="">
			<i class="pe-7s-close-circle pe-lg pe-va"></i>
			</a>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			</p>
		</li>
	</ul> --}}
	@else
	<ul  class="m-slidedown-links m-open" style="height: auto;">
		<li>
			<a href="/login">
				<i class="pe-7s-user pe-lg pe-va"></i> Log in
			</a>
		</li>
		<li>
			<a href="/register">
				<i class="pe-7s-users pe-lg pe-va"></i> Register
			</a>

		</li>
	</ul>
	@endif

	<h3 >I'm looking to...</h3>
	<ul id="m-tabs" class="nav nav-flex-tabs">
		<li class="active"><a href="#m-buy" data-toggle="tab">Buy</a></li>
		<li><a href="#m-deliver" data-toggle="tab">Deliver</a></li>
	</ul>
	<div id="m-panes" class="tab-content">
		<div class="tab-pane fade active in" id="m-buy">
			<ul>
				<li>
					<a href="/products">
						<i class="pe-7s-angle-right pe-lg pe-va"></i>
						All products
					</a>
				</li>
				@if(Auth::check())
					<li>
						<a href="/shopping_cart">
							<i class="pe-7s-angle-right pe-lg pe-va"></i>
							My shopping carts
						</a>
					</li>
				@endif
				{{-- <li>
					<a href="">
						<i class="pe-7s-angle-right pe-lg pe-va"></i>
						Departments
					</a>
				</li> --}}
				<li>
					<a href="/retailers">
						<i class="pe-7s-angle-right pe-lg pe-va"></i>
						Retailers
					</a>
				</li>

			</ul>
		</div>
		<div class="tab-pane fade" id="m-deliver">
			<ul>
				<li>
					<a href="/delivery">
						<i class="pe-7s-angle-right pe-lg pe-va"></i>
						Make a delivery
					</a>
				</li>
				@if(Auth::check())
					{{-- <li>
						<a href="/client/deliveries">
							<i class="pe-7s-angle-right pe-lg pe-va"></i>
							My deliveries
						</a>
					</li> --}}
					{{-- <li>
						<a href="/client/bids">
							<i class="pe-7s-angle-right pe-lg pe-va"></i>
							My bids
						</a>
					</li> --}}
				@endif
			</ul>
		</div>
	</div>
</div>

@include('partials.m-search')