
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#BDBDBD">
	<meta name="mobile-web-app-capable" content="yes">

	{{-- Google Fonts --}}
	<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,900|Quicksand:500,700|Roboto+Condensed:300,300i,400,400i,700,700i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i|Kreon:300,400,700" rel="stylesheet">

	{{-- Stylesheets --}}
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="/css/assets/flag-icon.min.css">
	<link rel="stylesheet" href="/css/assets/animate.css">
	<link rel="stylesheet" href="/css/assets/pe-icon-7-stroke/css/helper.css">
	<link rel="stylesheet" type="text/css" href="/css/assets/icheck/blue.css">



	{{-- Icon fonts --}}
	<script src="https://use.fontawesome.com/67b15a84b6.js"></script>
	<link rel="stylesheet" href="/css/assets/pe-icon-7-stroke/css/pe-icon-7-stroke.css">

	{{-- JS plugins --}}
	<script type="text/javascript" src="/js/assets/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="/js/assets/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/assets/icheck.min.js"></script>
	<script type="text/javascript" src="/js/assets/bootstrap-notify.min.js"></script>

	{{-- Favicon --}}
	<link rel="icon" type="img/ico" href="/ico/favicon.ico">
	<link rel="icon" sizes="1024x1025" href="/ico/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="1024x1025" href="/ico/ios-favicon.png" />


	<!-- Scripts -->
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>
	
	{{-- Google analytics code --}}
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-97926267-1', 'auto');
	  ga('send', 'pageview');
	</script>

	{{-- Facebook Pixel --}}
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '111427639513337');
		fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1"
		src="https://www.facebook.com/tr?id=111427639513337&ev=PageView
		&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
