@if(Auth::check())
@if(!Auth::user()->isAdmin())

	<div class="quick-action-bar visible-xs">
		<div class="container">
			<div class="row">
				<div class="col-xs-6 action text-center">
					<div class="icon-btn">
						<i class="icon icon-shopping-cart">
							<div class="unread text-center">
								{{Auth::user()->profile->orders}}
							</div>
						</i>
					</div>
					Carts
				</div>
				<div class="col-xs-6 action text-center">
					<div class="icon-btn">
						<i class="icon icon-delivery-truck">
							<div class="unread text-center">
								0
							</div>
						</i>
					</div>
					Deliveries
				</div>
			</div>
			
		</div>
	</div>

	<div class="quick-action-menu">
		<button type="button" class="quick-action-close-btn" id="quick-action-close">
			<i class="pe-7s-angle-down pe-3x pe-va"></i>
		</button>
		<div id="quick-action-panes" class="tab-content">
			@if(Auth::user()->isClient())
			<div class="tab-pane fade active in" id="q-cart">
				<h3><i class="pe-7s-cart pe-lg pe-va"></i>&nbsp; My shopping carts</h3>
				<ul id="m-shopping-cart" class="m-scrollable-links">
					<p style="font-size: 1.2em;">
						On Shopguin, you have a separate cart for every order you make.
					</p>


					@if(Auth::user()->profile->requests)
					@php($carts = Auth::user()->profile->requests)
					@foreach($carts->take(4) as $cart)
						<li>
							<a href="/client/request/{{$cart->id}}/view">

								<i class="pe-7s-cart pe-lg pe-va"></i> {{$cart->business->name}}
								<i class="pe-7s-angle-right-circle pe-lg pe-va align-right"></i>

								<h3 class="cart-total no-vm">
									<span>
										<i class="icon icon-shopping-bags"></i>
										Items
									</span>
									{{$cart->items->count()}}
									<span>
										|
									</span>
									<span>
										<i class="icon icon-euro"></i>
										Total
									</span>
									€{{$cart->total_price}}
								</h3 >
								<ul class="timeline-steps mobile">
									<li class="active">
										<i class="icon icon-shopping-cart"></i>
										<span>Products <br> added</span>
									</li>
									<li class="{{$cart->bids->count() > 0? 'active' : ''}}">
										<i class="icon icon-hammer"></i>
										<span>Bidding <br> stage</span>
									</li>
									@if($cart->status == 'close')
										@if($cart->invoice->is_paid == 1)
											<li class="active">
												<i class="icon icon-delivery"></i>
												<span>In delivery</span>
											</li>
										@else
											<li class="">
												<i class="icon icon-delivery"></i>
												<span>In delivery</span>
											</li>
										@endif
									@else
										<li class="">
											<i class="icon icon-delivery"></i>
											<span>In delivery</span>
										</li>
									@endif
									@if($cart->bids->count() <= 0)
										<div class="progress-bar" style="width: 25%;"></div>
									@else
										@if($cart->status == 'open')
											<div class="progress-bar" style="width: 50%;"></div>
										@elseif($cart->status == 'close')
											@if($cart->invoice->is_paid == 1)
												<div class="progress-bar" style="width: 99%;"></div>
											@else
												<div class="progress-bar" style="width: 75%;"></div>
											@endif
										@endif
									@endif
									<div class="bar"></div>
								</ul>
							</a>
						</li>
					@endforeach
					@endif
					@if(Auth::user()->profile->shoppingCarts)
					@php ($carts = Auth::user()->profile->shoppingCarts)
					@foreach($carts->take(4) as $cart)
						<li>
							<a href="/client/request/{{$cart->id}}/create">

								<i class="pe-7s-cart pe-lg pe-va"></i> {{$cart->business->name}}
								<i class="pe-7s-angle-right-circle pe-lg pe-va align-right"></i>

								<h3 class="cart-total no-vm">
									<span>
										<i class="icon icon-shopping-bags"></i>
										Items
									</span>
									{{$cart->items->count()}}
									<span>
										|
									</span>
									<span>
										<i class="icon icon-euro"></i>
										Total
									</span>
									€{{$cart->price}}
								</h3 >
								<ul class="timeline-steps mobile">
									<li class="active">
										<i class="icon icon-shopping-cart"></i>
										<span>Products <br> added</span>
									</li>
									<li>
										<i class="icon icon-hammer"></i>
										<span>Bidding <br> stage</span>
									</li>
									<li>
										<i class="icon icon-delivery"></i>
										<span>Deliver</span>
									</li>

									<div class="progress-bar" style="width: 0%;"></div>
									<div class="bar"></div>
								</ul>
							</a>
						</li>
					@endforeach
					@endif

					<div class="text-center" style="margin-bottom: 10px;">
						</br>
						<a href="/shopping_cart" class="btn btn-primary">
							View all shopping carts
						</a>
					</div>
				</ul>
			</div>
		@endif
			{{-- <div class="tab-pane fade" id="q-deliveries">
				<h3><i class="icon icon-delivery-icon" style="font-size: 1.6em;"></i>&nbsp; My deliveries</h3>
				<ul id="m-shopping-cart" class="m-scrollable-links">
					<p class="s-12em">
						Here all the deliveries you're due to make.
					</p>
					<li>
						<a href="/client/request//view">

							<i class="pe-7s-cart pe-lg pe-va"></i>
							Delivery from Apple Ltd.
							<i class="pe-7s-angle-right-circle pe-lg pe-va align-right"></i>

							<h3 class="cart-total no-vm">
								<span>
									<i class="icon icon-shopping-bags"></i>
									Items
								</span>
								5
								<br>
								<span>
									<i class="icon icon-place"></i>
								</span>
								London, GB
								<span>
									<i class="icon icon-arrow-left"></i>
								</span>
								<span>
									<i class="icon icon-place"></i>
								</span>
								Manchester, GB
							</h3 >
							<ul class="timeline-steps mobile">
								<li class="active">
									<i class="icon icon-shopping-cart"></i>
									<span>Delivery <br> received</span>
								</li>
								<li>
									<i class="icon icon-shopping-cart"></i>
									<span>Products <br> acquired</span>
								</li>
								<li class="">
									<i class="icon icon-hammer"></i>
									<span>Products  <br> delivered</span>
								</li>

								<div class="bar"></div>
							</ul>
						</a>
					</li>


					<div class="text-center" style="margin-bottom: 10px;">
						</br>
						<a href="/shopping_cart" class="btn btn-primary">
							View all deliveries
						</a>
					</div>
				</ul>
			</div> --}}
			<div class="tab-pane fade" id="q-notifications">
				<h3><i class="pe-7s-bell pe-lg pe-va"></i>&nbsp; Notifications</h3>
				<p>No notifications at the moment.</p>
			</div>
		</div>
		<div class="bottom">
			<ul class="nav quick-action-tabs">
				<li class="active"><a href="#q-cart" data-toggle="tab"><i class="pe-7s-cart pe-3x pe-va"></i></a></li>
				{{-- <li><a href="#q-deliveries" data-toggle="tab"><i class="icon icon-delivery-icon" style="font-size: 3em;"></i></a></li>			 --}}
				<li><a href="#q-notifications" data-toggle="tab"><i class="pe-7s-bell pe-3x pe-va"></i></a></li>
			</ul>
		</div>
	</div>
@endif
@endif
