@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
	<div class="container first">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
					{{ csrf_field() }}
					<div class="text-center">
						<img src="/img/logo-and-text.png" class="img-responsive hidden-xs" style="display:inline; height: 45px; width: auto;">
						<h3 class="w300">
							Welcome back to Shopguin!
						</h3>		
					</div>
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">Email Address</label>

						<div class="col-md-6">
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Password</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="password" required>

							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
							</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
								</label>
							</div>
						</div>
					</div>

					<div class="text-center">
						<div class="form-group">
							<div class="col-md-12 text-center">
								<button type="submit" class="btn btn-primary">
									Login
								</button>
								</br>
								<a class="btn btn-link" href="{{ url('/password/reset') }}">
									Forgot Your Password?
								</a>
							</div>
						</div>
						<p class="body">
							Or why not log in via Facebook:
						</p>
						<a href="/auth/facebook" class="btn btn-primary  social-login fb">
							<i class="fa fa-facebook-official" aria-hidden="true"></i> &nbsp;
							Facebook
						</a>
						
					</div>
					<div class="text-center">
						<p>
							First time on Shopguin? <br>
							<a href="/register" class="btn btn-primary">Register here</a>
							<br> <br>	
							Are you a business? <br>
							<a href="/business/register" class="btn btn-primary">Register your business</a>
						</p>
						
					</div>
				</form>
				
			</div>
		</div>
	</div>

@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
