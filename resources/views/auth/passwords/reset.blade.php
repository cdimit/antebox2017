@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
	<div class="container first">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h3 class="w300">
					Reset your password
				</h3>
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
				{{ csrf_field() }}

					<input type="hidden" name="token" value="{{ $token }}">

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">E-Mail Address</label>

						<div class="col-md-6">
							<input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Password</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="password" required>

							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
						<div class="col-md-6">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

							@if ($errors->has('password_confirmation'))
								<span class="help-block">
								<strong>{{ $errors->first('password_confirmation') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Reset password
							</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>

@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
