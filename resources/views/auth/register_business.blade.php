@extends('layouts.boilerplate')

{{-- Head tags --}}
@section('head')
	@include('partials.head')
	<script>
	 window.Stripe = <?php echo json_encode([
		 'csrfToken' => csrf_token(),
		 'stripeKey' => config('services.stripe.key'),
	 ]); ?>
	</script>
@endsection

{{-- OpenGraph tags --}}
@section('og-scripts')
@endsection

{{-- Head scripts --}}
@section('head-scripts')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/599857bb1b1bed47ceb0583b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
@endsection

{{-- Navbar --}}
@section('navbar')
	@include('partials.navbar-slim')
@endsection

{{-- Body --}}
@section('body')
	<div class="first container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="w-300 s-4em">
					Transform your business today.
				</h1>
				<h3 class="w-300 c-black">
					Create your online store with Shopguin in under 5 minutes!
				</h3>

			</div>
		</div>
		<div class="row">
			@if (session('error'))
				<div class="alert alert-danger">
					{{ session('error') }}
				</div>
			@endif
			{{-- Register form --}}
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
				<hr>
				{{-- Timeline steps --}}
				<div id="top" style="margin-bottom: 75px;">
					<ul class="timeline-steps">
						<li class="active">
							<i class="icon icon-lock"></i>
							<span>Log in details</span>
						</li>
						<li>
							<i class="icon icon-resume"></i>
							<span>Business <br> details</span>
						</li>
						<li>
							<i class="icon icon-trophy"></i>
							<span>Membership <br> plan</span>
						</li>
						<li>
							<i class="icon icon-domain-registration"></i>
							<span>Themes <br>& domain</span>
						</li>

						<div class="progress-bar" style="width: 0%;"></div>
						<div class="bar"></div>
					</ul>
				</div>

				<div id="app">
					<register-form :plans="{{ $plans }}"></register-form>
				</div>


				</br>
			</div>
			{{-- Sidebar info --}}
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 sidebar-info">
				<h3 class="w-300">
					Welcome to Shopguin
				</h3>
				<p class="s-13em lh">
					Create an eye-catching online store/website using our themes, without any need for coding knowledge. Everything is included in your membership plan; the payment system, the review system, discount coupons, and even a messaging system. Simply fill in the fields, and with the press of a button, everything will be presented as promised. We will be with you at all times, but with our seamless, system, you probably wont need us!
				</p>
			</div>

	</div>
</div>
@endsection

{{-- Footer --}}
@section('footer')
	@include('partials.footer')
@endsection

{{-- Footer scripts --}}
@section('scripts')
	@include('partials.scripts')
	<script type="text/javascript" src="/js/business/register.js"></script>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script src="/js/app.js"></script>

@endsection
