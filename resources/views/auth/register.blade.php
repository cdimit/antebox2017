@extends('layouts.boilerplate')

@section('head')
	@include('partials.head')
@endsection

@section('og-scripts')
@endsection

@section('head-scripts')
@endsection

@section('navbar')
	@include('partials.navbar-slim')
@endsection

@section('body')
	<div class="container first">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				@if (session('error'))
					<div class="alert alert-danger">
						{{ session('error') }}
					</div>
				@endif
				<h2 class="w300">
					Register with Shopguin
				</h2>
				<div class="well">
					<p class="body">
						Wait! Are you a business? If so, please register here:
					</p>
					<a href="/business/register" class="btn btn-primary">Register your business</a>					
				</div>
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
					{{ csrf_field() }}

					<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
						<label for="first_name" class="col-md-4 control-label">First name</label>

						<div class="col-md-6">
							<input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

							@if ($errors->has('first_name'))
								<span class="help-block">
									<strong>{{ $errors->first('first_name') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
						<label for="last_name" class="col-md-4 control-label">Last name</label>

						<div class="col-md-6">
							<input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

							@if ($errors->has('last_name'))
								<span class="help-block">
									<strong>{{ $errors->first('last_name') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email" class="col-md-4 control-label">Email adddress</label>

						<div class="col-md-6">
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 control-label">Password</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control" name="password" required>

							@if ($errors->has('password'))
								<span class="help-block">
									<strong>{{ $errors->first('password') }}</strong>
								</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<label for="password-confirm" class="col-md-4 control-label">Confirm password</label>

						<div class="col-md-6">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-md-offset-4 text-center">
							<button type="submit" class="btn btn-primary btn-lg">
								Register
							</button>
						</div>
					</div>
				</form>
				<h4 class="w300 c-med-gray">
					Or why not login via:
				</h4>
				<a href="/auth/facebook" class="btn btn-primary  social-login fb">
					<i class="fa fa-facebook-official" aria-hidden="true"></i> &nbsp;
					Facebook
				</a>

			</div>
		</div>
	</div>

@endsection

@section('footer')
	@include('partials.footer')
@endsection

@section('scripts')
	@include('partials.scripts')
@endsection
