<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// //TEST REDIS
// Route::get('/visits', function () {
//
// dd(App\ProductsCategories::find(23)->fields->first()->name);
//
// });





Route::get('/stripe/charge', function () {
    $invoices = App\DealInvoice::all();

    return view('stripe.charge')->withInvoices($invoices);
});
Route::post('/stripe/charge', 'StripeController@charge');



Route::post('stripe/webhook', 'StripeWebhookController@handle');


Route::get('/', 'HomepageController@home');

//TEST
Route::get('/login-test/{id}', function ($id) {
    Auth::loginUsingId($id);
    return redirect()->back();
});

Route::get('/notifications/test', function () {
  Auth::user()->notify(new \App\Notifications\Business\Welcome);
  return redirect()->back();
});

//Authenticate Links
Auth::routes();
Route::get('/join', function () {
  return redirect('business/register');
});
Route::get('business/register', 'Auth\RegisterBusinessController@showRegistrationForm');
Route::post('business/register', 'Auth\RegisterBusinessController@register');
Route::get('/auth/{provider}', 'Auth\SocialController@redirectToProvider');
Route::get('/auth/callback/{provider}', 'Auth\SocialController@handleProviderCallback');

//Verified Email
Route::get('/verify/{token}', 'UserController@verifiedEmail');

//Referral invitation
Route::get('/invite/{ref}', 'ReferController@invite');


//Authenticate users has access
Route::group(['middleware' => 'auth'], function(){

  //Account Settings
  Route::get('account/settings', 'UserController@accountSettings');
  Route::post('account/settings/password/update', 'UserController@updatePassword');
  Route::post('account/settings/email/update', 'UserController@updateEmail');
  Route::post('account/settings/payout/update', 'UserController@updatePayoutMethods');

  //Notifications
  Route::get('notifications/read/{notification_id}', 'NotificationsController@read');
  Route::get('notifications/markasread', 'NotificationsController@markAsRead');

  //Bids
  Route::get('delivery/{request}/bid/create', 'BidController@create');
  Route::post('delivery/{request}/bid/create', 'BidController@store');

  //Conversation
  Route::get('conversations', 'ConversationController@index');
  Route::get('conversations/{conversation}', 'ConversationController@single');
  Route::post('conversations/{conversation}/sendMessage', 'ConversationController@sendMessage');

  //Deal
  Route::get('deal/{invoice}', 'DealController@index');

});

//Only admin has access
// **For now is auth, change it before public in admin**
Route::group(['middleware' => 'admin'], function(){



  //Products Categories
  Route::get('admin/categories', 'ProductsCategoriesController@index');
  Route::get('admin/categories/create', 'ProductsCategoriesController@create');
  Route::post('admin/categories/create', 'ProductsCategoriesController@store');
  Route::get('admin/categories/{productsCategories}/edit', 'ProductsCategoriesController@edit');
  Route::post('admin/categories/{productsCategories}/edit', 'ProductsCategoriesController@update');
  Route::get('admin/categories/{productsCategories}/delete', 'ProductsCategoriesController@destroy');
  Route::get('admin/categories/{productsCategories}/restore', 'ProductsCategoriesController@restore');

  Route::get('admin/businesses/index', 'AdminController@businesses');
  Route::get('admin/clients/index', 'AdminController@users');
  Route::get('admin/payments/index', 'AdminController@payments');
  Route::get('admin/products/index', 'AdminController@products');

  Route::get('admin/products/feature/add/{product}', 'FeaturedProductsController@add');
  Route::get('admin/products/feature/remove/{product}', 'FeaturedProductsController@remove');
  Route::get('feature', 'FeaturedProductsController@getAllFeatured');

});

//Only business has access
Route::group(['middleware' => 'business'], function(){

  //  Dashboard view only for businesses
  Route::get('/dashboard', "BusinessController@dashboard");

  //Themes
  Route::get('business/themes', 'BusinessThemeController@themes');
  Route::post('business/themes', 'BusinessThemeController@updateThemes');
  Route::get('/business/themes/customise', 'BusinessThemeController@customise');
  Route::post('/business/themes/customise/frontpageslider', 'BusinessThemeController@updateFrontPageSlider');
  Route::post('/business/themes/customise/homepage', 'BusinessThemeController@updateHomePage');
  Route::post('/business/themes/customise/aboutpage', 'BusinessThemeController@updateAboutPage');


  //Products
  Route::get('business/products', 'ProductsController@index');
  Route::get('business/products/create', 'ProductsController@create');
  Route::post('business/products/create', 'ProductsController@store');
  Route::get('business/products/{products}/edit', 'ProductsController@edit');
  Route::post('business/products/{products}/edit', 'ProductsController@update');
  Route::get('business/products/{products}/delete', 'ProductsController@destroy');
  Route::get('business/products/{products}/images', 'ProductsController@images');
  Route::post('business/products/{products}/images/add', 'ProductsController@addImages');
  Route::post('business/products/{products}/images/remove', 'ProductsController@removeImages');



  //Stores
  Route::get('business/stores', 'StoresController@index');
  Route::get('business/stores/create', 'StoresController@create');
  Route::post('business/stores/create', 'StoresController@store');
  Route::get('business/stores/{stores}/edit', 'StoresController@edit');
  Route::post('business/stores/{stores}/edit', 'StoresController@update');
  Route::get('business/stores/{stores}/delete', 'StoresController@destroy');


  //ProductsStores
  //store
  Route::get('business/stores/{stores}/products', 'ProductsStoresController@storeProducts');
  Route::get('business/stores/{stores}/products/{product}/add', 'ProductsStoresController@storeAddProduct');
  Route::get('business/stores/{stores}/products/{product}/remove', 'ProductsStoresController@storeRemoveProduct');
  //product
  Route::get('business/products/{products}/stores', 'ProductsStoresController@productStores');
  Route::get('business/products/{products}/stores/{store}/add', 'ProductsStoresController@productAddStore');
  Route::get('business/products/{products}/stores/{store}/remove', 'ProductsStoresController@productRemoveStore');

  //Discount
  Route::get('business/discount/create', 'ProductsDiscountController@create');
  Route::post('business/discount/create', 'ProductsDiscountController@store');
  Route::get('business/discount/{productsDiscount}/remove', 'ProductsDiscountController@destroy');


  //Profile
  Route::get('/business/profile/edit', 'BusinessController@edit');
  Route::post('/business/profile/edit', 'BusinessController@update');
  Route::get('/business/account/payout', 'BusinessController@payout');
  Route::get('/business/account/email', 'BusinessController@changeEmail');
  Route::get('/business/account/password', 'BusinessController@changePwd');

  //Shopping carts
  Route::get('/business/carts', 'ClientRequestController@index');
  Route::get('/business/carts/open', 'ClientRequestController@openCarts');
  Route::get('/business/carts/completed', function(){
    return view('carts.business-closed');
  });
  Route::get('/business/carts/{invoice}/invoice', 'DealController@businessInvoice');
  Route::get('/business/carts/{request}/bid', 'BidController@businessCreate');
  Route::get('/business/carts/{request}/view', 'ShoppingCartController@businessView');

  Route::get('/business/card', 'StripeController@creditCard');
  Route::post('/business/card', 'StripeController@updateCreditCard');


  //Subscription
  Route::get('/business/subscription', 'StripeController@index');
  Route::get('/business/subscription/change', 'StripeController@change_subscription');
  Route::patch('/business/subscription/change', 'StripeController@changePlan');

  Route::delete('/stripe/subscribe', 'StripeController@destroy');
  Route::patch('/stripe/subscribe', 'StripeController@update');

});

Route::get('/stripe/subscribe', function () {
    $plans = App\SubscribePlan::all();

    return view('stripe.subscribe')->withPlans($plans);
});
Route::post('/stripe/subscribe', 'StripeController@register');

//Only clients has access
Route::group(['middleware' => 'client'], function(){
  // Dashboard
  Route::get('/client/dashboard', 'ClientController@dashboard');
  //Profile
  Route::get('/client/profile/edit', 'ClientController@edit');
  Route::post('/client/profile/edit', 'ClientController@update');
  Route::get('/client/profile/settings', 'UserController@accountSettings');
  Route::get('/client/profile/email', 'UserController@changeEmail');
  Route::get('/client/profile/pwd', 'UserController@changePwd');
  Route::get('/client/profile/payout', 'UserController@changePayout');

  //Meeting points
  Route::get('/client/meeting_points', 'MeetingPointsController@index');
  Route::post('/client/meeting_points/create', 'MeetingPointsController@store');
  Route::get('/client/meeting_points/{meeting_points}/delete', 'MeetingPointsController@destroy');
  Route::post('/client/meeting_points/{meeting_points}/edit', 'MeetingPointsController@update');

  //Shopping Cart
  Route::post('/products/{product}/addItemToCart', 'ItemsController@store');
  Route::get('/products/{product}/addItemToCart', 'ItemsController@store');
  Route::post('/products/{product}/addSearchedItemToCart', 'ItemsController@searchAdd');
  Route::get('/item/{item}/remove', 'ItemsController@destroy');
  Route::get('/shopping_cart', 'ShoppingCartController@index');
  Route::get('/shopping_cart/{shopping_cart}/remove', 'ShoppingCartController@destroy');
  Route::get('/shopping_cart/added/success/{shopping_cart}', 'ClientRequestController@continue');

  //Wishlist
  Route::post('/wishlist/{product}/add', 'WishlistController@add');
  Route::get('/wishlist/{product}/add', 'WishlistController@add');
  Route::get('/wishlist/{product}/remove', 'WishlistController@remove');
  Route::get('/wishlist', 'WishlistController@index');

  //Request
  Route::get('/client/request/{shopping_cart}/create', 'ClientRequestController@create');
  Route::post('/client/request/{shopping_cart}/create', 'ClientRequestController@store');
  Route::get('/client/request/{request}/view', 'BidController@view');

  // Deliveries
  Route::get('/client/deliveries', function(){
    return view('deliver.home');
  });
  Route::get('/client/deliveries/{delivery_id}', function(){
    return view('deliver.single');
  });

  // Bids
  Route::get('/client/bids', function(){
    return view('bid.home');
  });
  Route::get('/client/bids/{bid}', function(){
    return view('bid.single');
  });

  //Bid
  Route::post('bid/{bid}/accept', 'BidController@acceptBid');

});

// Products (Client view)
Route::get('/products/{product}/{slug?}', 'ProductsController@single');
Route::get('/products', 'ProductsController@all');


//Show Client Profile
Route::get('/client/{client}', 'ClientController@show');

//Client Requests
Route::get('/delivery', 'ClientRequestController@all');
Route::get('/delivery/{request}', 'ClientRequestController@index');


// Search
Route::get('/search', function(){
  $data = [
    'products' => []
  ];
  return view('products.search', $data);
});
Route::post('/search', 'ProductsController@search');

// Placeholder development routes
Route::get('/dev1', function(){
   $data = [
    'id' => '2'
  ];
  return view('agnolotti.main', $data);
});

Route::get('/dev3', function(){
   $data = [
    'id' => '2'
  ];
  return view('vionnet.main', $data);
});

Route::get('/dev4', function(){
  $data = [
    'url' => 'business/2/'
  ];
  return view('vionnet.products', $data);
});

Route::get('/dev5', function(){
  return view('carts.single-business');
});

// Landing page
Route::get('/business', function(){
  return view('businesses.landing');
});
Route::get('/aboutus', function(){
  return view('misc.aboutus');
});

Route::get('/tsandcs', function(){
  return view('misc.tsandcs');
});

Route::get('/retailers', 'BusinessController@retailers');
Route::get('/themes', 'HomepageController@themes');
Route::get('/howto', 'HomepageController@howto');
Route::get('/howto/account', 'HomepageController@howto_account');
Route::get('/howto/addproduct', 'HomepageController@howto_addproduct');
Route::get('/howto/themes', 'HomepageController@howto_themes');

Route::get('/demo/{theme}', 'BusinessController@demo');

//Always on bottom of the file
Route::get('/{business_slug}', 'BusinessController@show');
