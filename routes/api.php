<?php

use Illuminate\Http\Request;
use App\Business;
use App\Http\Middleware;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//  API routes for AngularJS front-end
Route::get('/products/all', function(){

	// Exclude products from 'Demo' business account
	// $products = App\Products::whereHas('business', function($q) {
	// 	$q->where('slug', '!=', 'demo');
	// })->with(['business', 'category', 'discount'])->get();

	// return $products->toArray();

	return App\Products::whereHas('business', function ($q) {
		$q->where('status', '<>', 'offline');
	})->with(['business', 'category', 'discount'])->get()->toArray(); 
	// return App\Products::with(['business', 'category', 'discount'])->get()->toArray(); 
});

// API routes for AngularJS | Templates
// @christos: Please edit as necessary...
Route::get('/business/{id}', function($id){
	$products = App\Products::where('business_id', $id)->with('business', 'category', 'discount')->get();
	return $products;
});

Route::get('/business/{id}/featured', function($id){
	// $featured = App\Products::where('business_id', $id)->where('category', 'featured')->with('business', 'category')->get();
	$featured = App\Products::where('business_id', $id)->with('business', 'category', 'discount')->get();
	return $featured;
});

Route::get('/business/demo/fields', function(){
	$theme_fields = [
		'ad_1_img' => 'https://source.unsplash.com/B4vMGR3X26E/1600x900',
		'ad_2_img' => 'https://source.unsplash.com/HpiYsNBORAw/1600x900',
		'ad_3_img' => 'https://source.unsplash.com/qrPqGP-SG8w/1600x900',
		'ad_1_text' => 'Buy',
		'ad_2_text' => 'It',
		'ad_3_text' => 'Now',
		'intro' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.',
		'intro-img' => 'https://source.unsplash.com/HpiYsNBORAw/1600x900',
		'intro_heading' => 'Consectetuer adipiscing elit',
		'motto' => 'Lorem ipsum dolor sit amet',
		'openingclosing' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non',
		'slider_1_heading' => 'Donec odio',
		'slider_2_heading' => 'Quisque volutpat',
		'slider_3_heading' => 'Nullam malesuada erat',
		'slider_1_img' => 'https://source.unsplash.com/HpiYsNBORAw/1600x900',
		'slider_2_img' => 'https://source.unsplash.com/qrPqGP-SG8w/1600x900',
		'slider_3_img' => 'https://source.unsplash.com/B4vMGR3X26E/1600x900',
		'slider_1_p' => 'Aliquam tincidunt mauris eu risus',
		'slider_2_p' => 'Vestibulum auctor dapibus neque',
		'slider_3_p' => 'Nunc dignissim risus id metus',
	];

	$stores = [
	];

	$business = [
	  'name' => 'Contosso',
	  'themeFields' => [
	    'motto' => 'Quad consecteur adipscing'
	  ]
	];

	$fields = array_merge([0, $theme_fields], $business, $stores);
	return [$fields];
});

Route::get('/business/{business_id}/fields', function($business_id){
	$business = App\Business::findOrFail($business_id);
	$theme_fields = $business->themeFields;
	$stores = $business->stores->toArray();
	$fields = [$theme_fields, $business, $stores];
	return [$fields];
	// return $business->themeFields;
});

