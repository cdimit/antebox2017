<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'role' => $faker->randomElement(['business', 'client', 'admin']),
        'email' => $faker->unique()->safeEmail,
        'is_verified' =>  $faker->boolean,
        'refer_code'  => $faker->bothify('##??'),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Business::class, function ($faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create(['role' => 'business'])->id;
        },
        'name' => $faker->unique()->words($nb = 2, $asText = true),
        'slug'  => $faker->sentence,
        'country' => strtolower($faker->countryCode),
        'since'      => $faker->year($max = 'now'),
        'phone'       => $faker->e164PhoneNumber,
        'address1'       => $faker->streetAddress,
        'address2'       => $faker->streetAddress,
        'state'      => $faker->state,
        'city'    => $faker->city,
        'zip_code'       => $faker->postcode,
        'map_x'       => $faker->latitude($min = -90, $max = 90),
        'map_y'     => $faker->longitude($min = -180, $max = 180),
        'status' =>  $faker->randomElement(['draft', 'publish', 'live', 'edited']),
        'subscribe_plan_id' =>  1,
    ];
});

$factory->define(App\Client::class, function ($faker) {
    return [
        'user_id' => function () {
            return factory('App\User')->create(['role' => 'client'])->id;
        },
        'first_name' => $faker->unique()->words($nb = 1, $asText = true),
        'last_name' => $faker->unique()->words($nb = 1, $asText = true),
    ];
});

$factory->define(App\ProductsCategories::class, function ($faker) {
    return [
        'name' => $faker->unique()->word,
    ];
});


$factory->define(App\Products::class, function ($faker) {
    return [
        'business_id' => function () {
            return factory('App\Business')->create()->id;
        },
        'category_id' => function () {
            return factory('App\ProductsCategories')->create()->id;
        },
        'name'  => $faker->unique()->words($nb = 3, $asText = true),
        'price' =>  $faker->randomFloat($nbMaxDecimals = NULL, $min = 0.01, $max = 999.99),
        'description'   => $faker->paragraph,
        'bag'   => 'cabin',
        'weight'     => $faker->numberBetween($min = 100, $max = 20000),
        'height'   => $faker->numberBetween($min = 1, $max = 200),
        'width'     => $faker->numberBetween($min = 1, $max = 200),
        'depth'   => $faker->numberBetween($min = 1, $max = 200),
        'has_stock' => $faker->boolean,
        'status' =>  $faker->randomElement(['draft', 'publish', 'live', 'edited']),
    ];
});
