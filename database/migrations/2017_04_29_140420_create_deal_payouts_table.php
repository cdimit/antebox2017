<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealPayoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_payouts', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('deal_invoice_id')->unsigned()->unique();
          $table->string('service_value');
          $table->double('service_fee', 10, 2);
          $table->double('payout_price', 10, 2);
          $table->boolean('is_payout')->default(0);

          $table->foreign('deal_invoice_id')->references('id')->on('deal_invoices');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_payouts');
    }
}
