<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned();
            $table->integer('deal_invoice_id')->unsigned();
            $table->boolean('is_bought')->default(0);
            $table->boolean('is_delivered')->default(0);
            $table->boolean('is_received')->default(0);

            $table->foreign('bid_id')->references('id')->on('bids');
            $table->foreign('deal_invoice_id')->references('id')->on('deal_invoices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_clients');
    }
}
