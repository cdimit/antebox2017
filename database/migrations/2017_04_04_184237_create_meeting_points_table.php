<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('state')->nullable();
            $table->string('post_code')->nullable();
            $table->string('country');
            $table->string('city');
            $table->string('map_x')->nullable();
            $table->string('map_y')->nullable();
            $table->integer('client_id')->unsigned();

            $table->foreign('client_id')->references('id')->on('client');
            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_points');
    }
}
