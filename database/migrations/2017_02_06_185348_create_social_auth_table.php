<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('social_auth', function (Blueprint $table) {
          $table->increments('id');
          $table->string('provider_id')->unique();
          $table->string('provider');
          $table->integer('user_id')->unsigned();

          $table->SoftDeletes();
          $table->timestamps();

          $table->foreign('user_id')->references('id')->on('users');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('social_auth');
    }
}
