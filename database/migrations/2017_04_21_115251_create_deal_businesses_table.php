<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned();
            $table->integer('deal_invoice_id')->unsigned();
            $table->string('tracking_number')->nullable();
            $table->boolean('is_received')->default(0);

            $table->foreign('bid_id')->references('id')->on('bids');
            $table->foreign('deal_invoice_id')->references('id')->on('deal_invoices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_businesses');
    }
}
