<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('business_id')->unsigned();
            $table->integer('meeting_point_id')->unsigned();
            $table->string('message')->nullable();
            $table->double('total_price', 10, 2);
            $table->string('status')->default('open');

            $table->foreign('client_id')->references('id')->on('client');
            $table->foreign('business_id')->references('id')->on('business');
            $table->foreign('meeting_point_id')->references('id')->on('meeting_points');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_requests');
    }
}
