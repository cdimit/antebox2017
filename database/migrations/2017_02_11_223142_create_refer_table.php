<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('refer', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('referred_by')->unsigned();
        $table->integer('user_id')->unsigned();
        $table->boolean('is_buy')->default(0);
        $table->boolean('is_deliver')->default(0);

        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('referred_by')->references('id')->on('users');

        $table->SoftDeletes();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('refer');
    }
}
