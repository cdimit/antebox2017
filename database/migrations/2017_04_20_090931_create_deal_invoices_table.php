<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deal_invoices', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('client_request_id')->unsigned()->unique();
          $table->double('delivery_fee', 10, 2);
          $table->string('service_value');
          $table->double('service_fee', 10, 2);
          $table->double('final_price', 10, 2);
          $table->boolean('is_paid')->default(0);
          $table->string('mode');

          $table->foreign('client_request_id')->references('id')->on('client_requests');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deal_invoices');
    }
}
