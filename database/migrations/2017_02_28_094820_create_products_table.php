<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('price', 10, 2);
            $table->text('description')->nullable();
            $table->string('bag')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('height')->nullable();
            $table->integer('width')->nullable();
            $table->integer('depth')->nullable();
            $table->string('picture')->default('product_picture.jpg');
            $table->string('video')->nullable();
            $table->string('status')->default(0);
            $table->boolean('has_stock')->default(1);
            $table->integer('category_id')->unsigned();
            $table->integer('business_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('products_categories');
            $table->foreign('business_id')->references('id')->on('business');

            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
