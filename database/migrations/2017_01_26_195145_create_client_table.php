<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('client', function (Blueprint $table) {
          $table->increments('id');
          $table->string('first_name');
          $table->string('last_name');
          $table->string('picture')->default('clients/pictures/client_profile.jpg');
          $table->string('gender')->nullable();
          $table->date('birthday')->nullable();
          $table->text('about')->nullable();
          $table->string('country')->nullable();
          $table->string('phone')->nullable();
          $table->integer('user_id')->unsigned();

          $table->SoftDeletes();
          $table->timestamps();

          $table->foreign('user_id')->references('id')->on('users');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
}
