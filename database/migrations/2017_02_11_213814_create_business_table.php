<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
          $table->string('logo')->default('business/logo/business_profile.jpg');
          $table->integer('since')->nullable();
          $table->text('phone')->nullable();
          $table->string('country');
          $table->string('address1')->nullable();
          $table->string('address2')->nullable();
          $table->string('state')->nullable();
          $table->string('city')->nullable();
          $table->string('zip_code')->nullable();
          $table->string('slug')->unique();
          $table->string('map_x')->nullable();
          $table->string('map_y')->nullable();
          $table->string('status')->default(0);

          $table->integer('subscribe_plan_id')->unsigned();
          $table->boolean('stripe_active')->default(false);
          $table->string('stripe_subscription')->nullable();
          $table->timestamp('subscription_end_at')->nullable();

          $table->integer('user_id')->unsigned();
          $table->SoftDeletes();
          $table->timestamps();

          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('subscribe_plan_id')->references('id')->on('subscribe_plans');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business');
    }
}
