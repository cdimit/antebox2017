<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('intro_heading')->nullable();
            $table->string('intro', 2400)->nullable();
            $table->string('intro-img')->nullable();
            $table->string('ad_1_text')->nullable();
            $table->string('ad_2_text')->nullable();
            $table->string('ad_3_text')->nullable();
            $table->string('ad_1_img')->nullable();
            $table->string('ad_2_img')->nullable();
            $table->string('ad_3_img')->nullable();
            $table->string('openingclosing', 800)->nullable();
            $table->string('about_text', 3200)->nullable();
            $table->string('custom-text', 3200)->nullable();
            $table->string('motto')->nullable();
            $table->string('slider_1_img')->nullable();
            $table->string('slider_2_img')->nullable();
            $table->string('slider_3_img')->nullable();
            $table->string('slider_1_heading')->nullable();
            $table->string('slider_2_heading')->nullable();
            $table->string('slider_3_heading')->nullable();
            $table->string('slider_1_p')->nullable();
            $table->string('slider_2_p')->nullable();
            $table->string('slider_3_p')->nullable();
            $table->integer('theme_id')->unsigned();
            $table->integer('business_id')->unsigned();

            $table->foreign('theme_id')->references('id')->on('themes');
            $table->foreign('business_id')->references('id')->on('business');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_themes');
    }
}
