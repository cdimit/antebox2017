<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('biddable_id');
            $table->string('biddable_type', 50);
            $table->integer('client_request_id')->unsigned();
            $table->string('message')->nullable();
            $table->string('delivery_method')->nullable();
            $table->double('delivery_fee', 10, 2);
            $table->string('delivery_date');
            $table->string('status')->default('open');

            $table->foreign('client_request_id')->references('id')->on('client_requests');

            $table->SoftDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
