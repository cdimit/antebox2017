<?php

use Illuminate\Database\Seeder;
use App\SubscribePlan;
use App\ProductsCategories;
use App\Theme;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscribePlan::forceCreate(['code' => 'basic1', 'description' => 'Basic', 'price'=>990, 'category' => 'Basic', 'packet' => 'Monthly']);
        SubscribePlan::forceCreate(['code' => 'professional1', 'description' => 'Professional', 'price'=>1990, 'category' => 'Professional', 'packet' => 'Monthly']);
        SubscribePlan::forceCreate(['code' => 'advance1', 'description' => 'Advance', 'price'=>3990, 'category' => 'Advance', 'packet' => 'Monthly']);
        ProductsCategories::forceCreate(['name' => 'Food']);
        ProductsCategories::forceCreate(['name' => 'Clothes']);
        ProductsCategories::forceCreate(['name' => 'Shoes']);
        ProductsCategories::forceCreate(['name' => 'Electronics']);

        Theme::forceCreate(['name' => 'Agnolotti', 'description' => 'Suitable for businesses selling food items.']);
        Theme::forceCreate(['name' => 'Vionnet', 'description' => 'Modern, minimalistic theme for businesses in the fashion industry.']);

    }
}
