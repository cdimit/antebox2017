const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   


mix.sass('resources/assets/sass/app.scss', 'public/css')
	.sass('resources/assets/sass/agnolotti.scss', 'public/css')
	.sass('resources/assets/sass/vionnet.scss', 'public/css')
	.sass('resources/assets/sass/capsule.scss', 'public/css')
	.sass('resources/assets/sass/belgravia.scss', 'public/css')
	.sass('resources/assets/sass/dashboard.scss', 'public/css')
	.sass('resources/assets/sass/gent-dashboard.scss', 'public/css').sourceMaps();