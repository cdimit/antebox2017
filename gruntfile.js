module.exports = function(grunt) {

	grunt.initConfig({
		webfont: {
			icons: {
				src: 'resources/assets/icons/*.svg',
				dest: 'resources/assets/fonts',
				destScss: 'resources/assets/sass/modules/',
				options: {
					stylesheet: ['scss'],
					hashes: false,
					fontPathVariables: false,
					relativeFontPath: '../../fonts/',
					destHtml: 'resources/assets/fonts',
					templateOptions: {
						baseClass: 'icon',
						classPrefix: 'icon-',
					},
					// template: 'resources/assets/icons/templates/tmpl.css'
				}
			},
			vionnet: {
				src: 'resources/assets/icons/vionnet/*.svg',
				dest: 'resources/assets/fonts/vionnet',
				destScss: 'resources/assets/sass/templates/vionnet/',
				options: {
					font: 'vionnet',
					stylesheet: ['scss'],
					hashes: false,
					fontPathVariables: false,
					relativeFontPath: '../../../fonts/vionnet/',
					destHtml: 'resources/assets/fonts/vionnet/',
					templateOptions: {
						baseClass: 'vionnet',
						classPrefix: 'vionnet-',
					},
					// template: 'resources/assets/icons/templates/tmpl.css'
				}
			}
		},
		watch: {
			files: ['resources/assets/icons/*.svg'],
			task: ['webfont']
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-webfont');
	grunt.registerTask('build', ['webfont']);

}