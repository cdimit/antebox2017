

$(document).ready(function(){

	$("#to-biz").click(function () {
		$("#login_details").css('display', 'none');
		$('.progress-bar').css('width', '33%');
		$('.timeline-steps>li:nth-child(2)').toggleClass('active');
		$("#business_details").css('display', 'block');
		$('html, body').animate({
				scrollTop: $("#top").offset().top
		}, 1000);
	});

	$("#back-to-login").click(function () {
		$("#login_details").css('display', 'block');
		$('.progress-bar').css('width', '0%');
		$('.timeline-steps>li:nth-child(2)').toggleClass('active');
		$("#business_details").css('display', 'none');
		$('html, body').animate({
				scrollTop: $("#top").offset().top
		}, 1000);
	});

	$("#to-plan").click(function () {
		$("#business_details").css('display', 'none');
		$('.progress-bar').css('width', '66%');
		$('.timeline-steps>li:nth-child(3)').toggleClass('active');
		$("#plan").css('display', 'block');
		$('html, body').animate({
				scrollTop: $("#top").offset().top
		}, 1000);
	});

	$("#back-to-biz").click(function () {
		$("#plan").css('display', 'none');
		$('.progress-bar').css('width', '33%');
		$('.timeline-steps>li:nth-child(3)').toggleClass('active');
		$("#business_details").css('display', 'block');
		$('html, body').animate({
				scrollTop: $("#top").offset().top
		}, 1000);
	});

	$("#to-themes").click(function () {
		$("#plan").css('display', 'none');
		$('.progress-bar').css('width', '99%');
		$('.timeline-steps>li:nth-child(4)').toggleClass('active');
		$("#themes").css('display', 'block');
		$('html, body').animate({
				scrollTop: $("#top").offset().top
		}, 1000);
	});

	$("#back-to-plan").click(function () {
		$("#themes").css('display', 'none');
		$('.progress-bar').css('width', '66%');
		$('.timeline-steps>li:nth-child(4)').toggleClass('active');
		$("#plan").css('display', 'block');
		$('html, body').animate({
				scrollTop: $("#top").offset().top
		}, 1000);
	});

});