//  Shopguin 
//	Capsule theme
//  AngularJS 1.6.2

//  Fix for Lodash
//  https://github.com/mgonto/restangular/issues/1225
_.contains = _.includes;

var app = angular.module('anteboxCapsule', ['ngRoute', 'restangular', 'ui.bootstrap', 'angularUtils.directives.dirPagination'], function($interpolateProvider){
	$interpolateProvider.startSymbol('[[').endSymbol(']]');
});

// Business ID for performing REST API call
app.run(function($rootScope){
	$rootScope.config = _config;
});

app.config(function($routeProvider){
	$routeProvider
	.when('/', {
		templateUrl: "/templates/capsule/home.html",
		controller: 'HomeController',
		controllerAs: 'h'
	})
	.when("/about", {
		templateUrl: "/templates/capsule/about.html",
		controller: 'HomeController',
		controllerAs: 'h'
	})
	.when("/products", {
		templateUrl: "/templates/capsule/all.products.html"
	})
	.when("/stores", {
		templateUrl: "/templates/capsule/stores.html",
		controller: 'HomeController',
		controllerAs: 'h'
	});
});


app.directive('mainSlider', function(){
	return {
		templateUrl: '/templates/capsule/main-slider.html',
		link: function(scope, element, attrs) {
			$('.main-slider').owlCarousel({
				loop:true,
				nav:false,
				items:1,
			});
		}
	};
});

app.directive('productsSlider', function(){
	return {
		templateUrl: '/templates/capsule/products-slider.html',
		link: function(scope, element, attrs) {
			$('#featured-products').owlCarousel({
				loop:true,
				center: false,
				nav:false,
				margin: 25,
				autoPlay: true,
				autoWidth: false,
				responsiveClass:true,
				responsive:{
					0:{
						items:1,
						nav:true
					},
					600:{
						items:3,
						nav:false
					},
					1000:{
						items:5,
						nav:true,
						loop:false
					}
				}
			});
		}
	};
});


//  Products grid view
app.directive('productsGrid', function(){
	return {
		restrict: 'E',
		templateUrl: '/templates/capsule/products-grid.html'
	};
});

// Products  filter
app.directive('productsFilter', function(){
	return {
		restrict: 'E',
		templateUrl: '/templates/capsule/products-filter.html'
	};
});

// Home page controller
app.controller('HomeController', ['$scope', '$http', 'Restangular', '$window' ,'$timeout', function($scope, $http, Restangular, $window, $timeout){
	var home = this;
	var allFields = Restangular.all('/api/business/' + $scope.config.id + '/fields');
	var featuredProducts = Restangular.all('/api/business/' + $scope.config.id + '/featured');
	home.fields = [];
	home.business = [];
	home.featuredProducts = [];

	//  REST API call to get fields
	allFields.getList().then(function(data){
		home.fields = data[0][0];
		home.business = data[0][1];
		console.log(data);
	}, function errorCallback(){
	});

	featuredProducts.getList().then(function(data){
		home.featuredProducts = data;
	}, function errorCallback(){
	});

	// Add item to cart
	home.addToCart = function(id){
		var api_url = '/products/' + id + '/addItemToCart';
		$http({
			method: 'POST',
			url: api_url,
			headers: {
           		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		}).then(function(success){
			$.notify({
				message: 'Item successfully added to cart' 
			},{
				type: 'success'
			});
			$timeout(function(){
				$window.location.reload();
			}, 1000);
		}, function(error) {
		});
	}

	// Add item to cart
	home.addToWishlist = function(id){
		var api_url = '/wishlist/' + id + '/add';
		$http({
			method: 'POST',
			url: api_url
		}).then(function(success){
			$.notify({
				message: 'Item successfully added to wishlist' 
			},{
				type: 'success'
			});
		}, function(error) {
		});
	}

}]);


//  All products grid view controller
app.controller('ProductsController', ['$scope', '$http', '$window', 'Restangular', '$location', '$anchorScroll', '$timeout', function($scope, $http, $window, Restangular, $location, $anchorScroll, $timeout){
	var products = this;
	var allProducts = Restangular.all('/api/business/' + $scope.config.id);
	

	//  Variables
	products.Loaded = false;
	products.Error = false;
	products.sortType = '';
	products.sortReverse = false;
	products.currentPage = 1;
	products.itemsPerPage = 10;
	products.maxSize = 5;
	products.startNumber = (products.currentPage - 1)*products.itemsPerPage;
	products.viewByOptions = [1,2,3,5,10];

	

	products.pageChanged = function() {
		$location.hash('top');
		$anchorScroll();
	};

	//  Arrays
	products.productsList = [];
	products.countriesList = [];
	products.countriesFullNameList = [];
	products.categoriesList = [];

	products.categoryFilter = [];
	products.countryFilter = [];


	var getUniqueCategories = function(data){
		_.forEach(data, function(product){
			products.categoriesList.push(product.category.name);
		});
		products.categoriesList = _.uniq(products.categoriesList);
	}

	var getUniqueCountries = function(data){
		_.forEach(data, function(product){
			products.countriesList.push(product.business.country);
		});
		products.countriesList = _.uniq(products.countriesList);
		console.log(products.countriesList);
	}

	products.addToFilter = function (item, list) {
		var index = list.indexOf(item);
		//  If item in list, remove item
		if (index > -1) {
			list.splice(index, 1);
		} else {
			//  Otherwise, add item to list
			list.push(item);
		}
	};

	// Set checkbox as checked only if
	//  item in filter list
	products.exists = function (item, list) {
		return list.indexOf(item) > -1;
	}

	//  REST API call to get all products
	allProducts.getList().then(function(data){
		products.productsList = data;
		products.Loaded = true;

		getUniqueCategories(products.productsList);
		getUniqueCountries(products.productsList);
	}, function errorCallback(){
		products.Loaded = true;
		products.Error = true;
	});

	// Add item to cart
	products.addToCart = function(id){
		var api_url = '/products/' + id + '/addItemToCart';
		$http({
			method: 'POST',
			url: api_url,
			headers: {
           		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		}).then(function(success){
			$.notify({
				message: 'Item successfully added to cart' 
			},{
				type: 'success'
			});
			$timeout(function(){
				$window.location.reload();
			}, 1000);
		}, function(error) {
		});
	}

	// Add item to cart
	products.addToWishlist = function(id){
		var api_url = '/wishlist/' + id + '/add';
		$http({
			method: 'POST',
			url: api_url
		}).then(function(success){
			$.notify({
				message: 'Item successfully added to wishlist' 
			},{
				type: 'success'
			});
		}, function(error) {
		});
	}

	products.revealSidebar = function() {
		$(function(){
			$('.t-sidebar-product-filter').toggleClass('hide-sidebar');
		})
	}

}]);

//  Filter for pagination
app.filter('startFrom', function () {
	return function (input, start) {
		if (input) {
			start = +start;
			return input.slice(start);
		}
		return [];
	};
});

app.filter('filterCompanies', function(){
	return function(products, companyList) {
		var out = [];
		if (companyList.length == 0) {
			return products;
		} else {
			out = _.filter(products, function(product){
				var productCompany = product.business.name;
				if (_.includes(companyList, productCompany)) {
					return true;
				} else {
					return false;
				}
			});
			return out;
		}
	};
});


app.filter('filterCategories', function(){
	return function(products, categoriesList) {
		var out = [];
		if (categoriesList.length == 0) {
			return products;
		} else {
			out = _.filter(products, function(product){
				var productCategory = product.category.name;
				if (_.includes(categoriesList, productCategory)) {
					return true;
				} else {
					return false;
				}
			});
			return out;
		}
	};
});

app.filter('filterCountries', function(){
	return function(products, countriesList) {
		var out = [];
		if (countriesList.length == 0) {
			return products;
		} else {
			out = _.filter(products, function(product){
				var productCountry = product.business.country;
				if (_.includes(countriesList, productCountry)) {
					return true;
				} else {
					return false;
				}
			});
			return out;
		}
	};
});