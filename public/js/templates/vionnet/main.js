$(document).ready(function(){
	function toggleHandler(toggle) {
		toggle.addEventListener( "click", function(e) {
			e.preventDefault();
			(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
		});
	}

	$('.burger').click(function(){
		$('.slide-in-nav').toggleClass('closed');
		var toggles = document.querySelectorAll(".burger");
		toggleHandler(toggles);
	});

	

	var stickyBar = $('.header')
	$(window).scroll(function(){
		var currentScroll = $(window).scrollTop();
		if (currentScroll >= 81) {
			stickyBar.addClass('fixed');
		} else {
			stickyBar.removeClass('fixed');
		}

		if (currentScroll >= 1) {
			$('.t-sidebar-product-filter').addClass('scroll');
		} else {
			$('.t-sidebar-product-filter').removeClass('scroll');
		}
	});
});

//  Hamburger menu
(function() {

	"use strict";
	function toggleHandler(toggle) {
		toggle.addEventListener( "click", function(e) {
			e.preventDefault();
			(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
		});
	}

	var toggles = document.querySelectorAll(".burger");

	for (var i = toggles.length - 1; i >= 0; i--) {
		var toggle = toggles[i];
		toggleHandler(toggle);
	};

})();

