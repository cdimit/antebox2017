// Address autocomplete
// Google Maps API

var placeSearch, autocomplete;
var componentForm = {
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'short_name',
	country: 'long_name',
	postal_code: 'short_name'
};

var positionDoubleClick;

function initAutocomplete() {
	var saved_x = $("#map_x").val();
	var saved_y = $("#map_y").val();

	if (saved_x != '' && saved_y != ''){
		var initialCentre = new google.maps.LatLng(saved_x, saved_y);
	} else {
		var initialCentre = new google.maps.LatLng(50.5, 4.5);
	}

	var map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: initialCentre,
		disableDoubleClickZoom: true,
		zoom: 5
	});
	var marker = new google.maps.Marker({
		position: initialCentre,
		map: map
	});

	google.maps.event.addListener(map, 'dblclick', function(e){
		positionDoubleClick = e.latLng;
		marker.setPosition(positionDoubleClick);
		$("#map_x").val(positionDoubleClick.lat());
		$("#map_y").val(positionDoubleClick.lng());

		$("#_map_x").val(positionDoubleClick.lat());
		$("#_map_y").val(positionDoubleClick.lng());
	});

	autocomplete = new google.maps.places.Autocomplete(
		(document.getElementById('address_search')),
		{types: ['geocode']}
	);

	autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
	var place = autocomplete.getPlace();

	for (var component in componentForm) {
		document.getElementById(component).value = '';
		document.getElementById(component).disabled = false;
	}

	// Get Lat/Long values
	var x = place.geometry.location.lat();
	var y = place.geometry.location.lng();
	document.getElementById('map_x').value = x;
	document.getElementById('_map_x').value = x;

	document.getElementById('map_y').value = y;
	document.getElementById('_map_y').value = y;

	var initialCentre = new google.maps.LatLng(x, y);
	var map = new google.maps.Map(document.getElementById('map-canvas'), {
		center: initialCentre,
		disableDoubleClickZoom: true,
		zoom: 13
	});

	var marker = new google.maps.Marker({
		position: initialCentre,
		map: map
	});

	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		
		if (componentForm[addressType]) {

			console.log(val);
			var val = place.address_components[i][componentForm[addressType]];
			document.getElementById(addressType).value = val;
		}
	}

	if($("#country_code").length > 0) {
		document.getElementById("country_code").value = place.address_components[5]['short_name'];
		$("#country").prop('disabled', true);
	}

	if($("#address2").length > 0) {
		
	} else {
		// Fix to combine street number and street name into same field
		var strt_number = place.address_components[0]['short_name'];
		var street = place.address_components[1]['long_name'];
		document.getElementById('route').value = strt_number + ' ' + street;
		
	}

}

function geolocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var circle = new google.maps.Circle({
				center: geolocation,
				radius: position.coords.accuracy
			});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}