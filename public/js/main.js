$(document).ready(function(){

	$(document).mouseup(function (e)
	{
		var container = $("#profile-links, #notifications, #shopping-carts, #deliveries");
		if (!container.is(e.target) && container.has(e.target).length === 0) 
		{
			container.hide();
			container.unbind('click', document);
		}
	});

	$("#profile-btn, #profile-name").click(function(){
		$("#profile-links").fadeIn('fast').css('display', 'block');
	});

	$("#notif-btn").click(function(){
		$("#notifications").fadeIn('fast').css('display', 'block');
	});

	$("#shopping-cart-btn").click(function(){
		$("#shopping-carts").fadeIn('fast').css('display', 'block');
	});

	$("#deliveries-btn").click(function(){
		$("#deliveries").fadeIn('fast').css('display', 'block');
	});

	$(".close-btn").click(function(e){
		$(this).parent().css('display', 'none').fadeOut('fast');
		e.stopPropagation();
	})

	//  Mobile funtions

	$("#open-nav").click(function () {
		$(".m-nav-overlay").toggleClass("overlay-open");
	});

	$("#m-search").click(function() {
		$(".m-search-overlay").toggleClass("overlay-open");
		$("#search-close").toggleClass("hide");
		$("#search-open").toggleClass("hide");
	});

	$("#m-profile-links-btn").click(function () {
		$("#m-profile-links").toggleClass("m-open");
		$("#m-profile-links-btn").toggleClass("m-open");
	});

	$("#m-notifications-btn").click(function () {
		$("#m-notifications").toggleClass("m-open");
		$("#m-notifications-btn").toggleClass("m-open");
	});

	$(".quick-action-bar").click(function () {
		$(".quick-action-menu").toggleClass("overlay-open");
		$("body").css('overflow', 'hidden');
	});

	$("#quick-action-close").click(function () {
		$(".quick-action-menu").toggleClass("overlay-open");
		$("body").css('overflow', 'auto');
	});

	

	//  Hamburger menu
	(function() {

		"use strict";

		var toggles = document.querySelectorAll(".hamburger");

		for (var i = toggles.length - 1; i >= 0; i--) {
			var toggle = toggles[i];
			toggleHandler(toggle);
		};

		function toggleHandler(toggle) {
			toggle.addEventListener( "click", function(e) {
			e.preventDefault();
			(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
			});
		}
	})();
});
