//  Antebox 2017 website
//  AngularJS 1.6

//  Fix for Lodash
//  https://github.com/mgonto/restangular/issues/1225
_.contains = _.includes;

var app = angular.module('anteboxAllProducts', [ 'restangular', 'ui.bootstrap', 'angularUtils.directives.dirPagination'], function($interpolateProvider){
	$interpolateProvider.startSymbol('[[').endSymbol(']]');
});

//  Products grid view
app.directive('productsGrid', function(){
	return {
		restrict: 'E',
		templateUrl: '/templates/products-grid.html'
	};
});

// Products  filter
app.directive('productsFilter', function(){
	return {
		restrict: 'E',
		templateUrl: '/templates/products-filter.html'
	};
});


//  All products grid view controller
app.controller('ProductsController', ['$scope', '$http', '$window', 'Restangular', '$location', '$anchorScroll', '$timeout', function($scope, $http, $window, Restangular, $location, $anchorScroll, $timeout){
	var products = this;
	var allProducts = Restangular.all('/api/products/all');

	//  Variables
	products.Loaded = false;
	products.Error = false;
	products.sortTypeOptions = [
		{
			label: 'Price (Low to High)',
			order: false,
			slug: 'price'
		},
		{
			label: 'Price (High to Low)',
			order: true,
			slug: 'price'
		},
		{
			label: 'Alphabetically (A-Z)',
			order: false,
			slug: 'name'
		},
		{
			label: 'Alphabetically (Z-A)',
			order: true,
			slug: 'name'
		},
		{
			label: 'Category',
			order: false,
			slug: 'category.name'
		}
	];
	products.sortTypeObj = 0;
	// products.sortType = products.sortTypeObj.slug;
	// products.sortReverse = products.sortTypeObj.order == 'asc' ? true : false;
	products.currentPage = 1;
	products.itemsPerPage = 9;
	products.maxSize = 5;
	products.startNumber = (products.currentPage - 1)*products.itemsPerPage;
	products.viewByOptions = [3,6,9,12,15];


	products.pageChanged = function() {
		console.log("Page changed to " + products.currentPage);
		$location.hash('top');
		$anchorScroll();
	};

	products.getSort = function(sort, index, property) {
		console.log(sort[index][property]);
		return sort[index][property];
	}

	//  Arrays
	products.productsList = [];
	products.companiesList = [];
	products.countriesList = [];
	products.countriesFullNameList = [];
	products.categoriesList = [];

	products.companyFilter = [];
	products.categoryFilter = [];
	products.countryFilter = [];

	var getUniqueCompanies = function(data){
		_.forEach(data, function(product){
			products.companiesList.push(product.business.name);
		});
		products.companiesList = _.uniq(products.companiesList);
	}

	var getUniqueCategories = function(data){
		_.forEach(data, function(product){
			products.categoriesList.push(product.category.name);
		});
		products.categoriesList = _.uniq(products.categoriesList);
	}

	var getUniqueCountries = function(data){
		_.forEach(data, function(product){
			products.countriesList.push(product.business.country);
		});
		products.countriesList = _.uniq(products.countriesList);

	}



	products.addToFilter = function (item, list) {
		var index = list.indexOf(item);
		//  If item in list, remove item
		if (index > -1) {
			list.splice(index, 1);
		} else {
			//  Otherwise, add item to list
			list.push(item);
		}
	};

	// Set checkbox as checked only if
	//  item in filter list
	products.exists = function (item, list) {
		return list.indexOf(item) > -1;
	}

	//  REST API call to get all products
	allProducts.getList().then(function(data){
		products.productsList = data;
		products.Loaded = true;
		console.log(data);
		getUniqueCompanies(products.productsList);
		getUniqueCategories(products.productsList);
		getUniqueCountries(products.productsList);
	}, function errorCallback(){
		products.Loaded = true;
		products.Error = true;
	});

	// Add item to cart
	products.addToCart = function(id){
		var api_url = '/products/' + id + '/addItemToCart';
		$http({
			method: 'POST',
			url: api_url,
			headers: {
           		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
		}).then(function(success){
			$.notify({
				message: 'Item successfully added to cart' 
			},{
				type: 'success'
			});
			$timeout(function(){
				$window.location.reload();
			}, 500);
		}, function(error) {
			console.log('Error!');
		});
	}

	// Add item to cart
	products.addToWishlist = function(id){
		var api_url = '/wishlist/' + id + '/add';
		$http({
			method: 'POST',
			url: api_url
		}).then(function(success){
			console.log('Success');
			// $window.location.reload();
			$.notify({
				message: 'Product added to wishlist'
			},{
				type: 'success',
				timer: 400,
			});
		}, function(error) {
			console.log('Error!');
		});
	}

	products.hideSidebar = function() {
		$(function(){
			$('.sidebar-product-filter').toggleClass('hide-sidebar');
		})
	}

	products.hideSidebarOverlay = function() {
		$(function(){
			$('.product-filter-overlay').toggleClass('slide-out');
		});
	}

}]);

//  Filter for pagination
app.filter('startFrom', function () {
	return function (input, start) {
		if (input) {
			start = +start;
			return input.slice(start);
		}
		return [];
	};
});

app.filter('filterCompanies', function(){
	return function(products, companyList) {
		var out = [];
		if (companyList.length == 0) {
			return products;
		} else {
			out = _.filter(products, function(product){
				var productCompany = product.business.name;
				if (_.includes(companyList, productCompany)) {
					return true;
				} else {
					return false;
				}
			});
			return out;
		}
	};
});


app.filter('filterCategories', function(){
	return function(products, categoriesList) {
		var out = [];
		if (categoriesList.length == 0) {
			return products;
		} else {
			out = _.filter(products, function(product){
				var productCategory = product.category.name;
				if (_.includes(categoriesList, productCategory)) {
					return true;
				} else {
					return false;
				}
			});
			return out;
		}
	};
});

app.filter('filterCountries', function(){
	return function(products, countriesList) {
		var out = [];
		if (countriesList.length == 0) {
			return products;
		} else {
			out = _.filter(products, function(product){
				var productCountry = product.business.country;
				if (_.includes(countriesList, productCountry)) {
					return true;
				} else {
					return false;
				}
			});
			return out;
		}
	};
});
