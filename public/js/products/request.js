$.validator.setDefaults({ ignore: [] });

$(document).ready(function(){
	$("#to-location").click(function () {
		$("#step1").css('display', 'none');
		$('.progress-bar').css('width', '50%');
		$('.timeline-steps>li:nth-child(2)').toggleClass('active');
		$("#step2").css('display', 'block');
	});

	$("#back-to-products").click(function () {
		$("#step1").css('display', 'block');
		$('.progress-bar').css('width', '0%');
		$('.timeline-steps>li:nth-child(2)').toggleClass('active');
		$("#step2").css('display', 'none');
	});

	$("#to-date").click(function () {
		
		$("#step2").css('display', 'none');
		$('.progress-bar').css('width', '66%');
		$('.timeline-steps>li:nth-child(3)').toggleClass('active');
		$("#step3").css('display', 'block');
	});

	$("#back-to-location").click(function () {
		$("#step2").css('display', 'block');
		$('.progress-bar').css('width', '50%');
		$('.timeline-steps>li:nth-child(3)').toggleClass('active');
		$("#step3").css('display', 'none');
	});

	$("#to-final").click(function () {
		if ($("select[name='meeting_point']").val() == "") {
			if ($("#clientRequest").valid()) {
				$("#step2").css('display', 'none');
				$('.progress-bar').css('width', '99%');
				$('.timeline-steps>li:nth-child(3)').toggleClass('active');
				$("#step3").css('display', 'block');
			} else {
				alert("There are errors in your form!");
			}
		} else {
			$("#clientRequest").validate().cancelSubmit = true;
			$("#step2").css('display', 'none');
			$('.progress-bar').css('width', '99%');
			$('.timeline-steps>li:nth-child(3)').toggleClass('active');
			$("#step3").css('display', 'block');
		}
	});

	$("#back-to-date").click(function () {
		$("#step4").css('display', 'none');
		$('.progress-bar').css('width', '66%');
		$('.timeline-steps>li:nth-child(4)').toggleClass('active');
		$("#step3").css('display', 'block');
	});

	$('.datepicker').datepicker();
	$('#time').timepicker({
		step:60,
		show2400: true
	});

	$("#clientRequest").validate({
			rules: {
				address1: 'required',
				address2: 'required',
				city: 'required',
				state: 'required',
				country: 'required',
				post_code: 'required'
			},
			messages: {
				address1: 'Please fill in the address field',
				address2: 'Please fill in the address field',
				city: 'Please fill in the city field',
				state: 'Please fill in the state field',
				country: 'Please fill in the country field',
				post_code: 'Please fill in the post code field'
			},
			submitHandler: function(form) {
				form.submit();
			}
		});

});



// Address autocomplete
// Google Maps API

var placeSearch, autocomplete;
var componentForm = {
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'short_name',
	country: 'long_name',
	postal_code: 'short_name'
};

function initAutocomplete() {

	autocomplete = new google.maps.places.Autocomplete(
		(document.getElementById('address')),
		{types: ['geocode']}
	);

	autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
	var place = autocomplete.getPlace();

	for (var component in componentForm) {
		document.getElementById(component).value = '';
		document.getElementById(component).disabled = false;
	}

	for (var i = 0; i < place.address_components.length; i++) {
		var addressType = place.address_components[i].types[0];
		if (componentForm[addressType]) {
			var val = place.address_components[i][componentForm[addressType]];
			document.getElementById(addressType).value = val;
		}
	}
}

function geolocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			var geolocation = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var circle = new google.maps.Circle({
				center: geolocation,
				radius: position.coords.accuracy
			});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}