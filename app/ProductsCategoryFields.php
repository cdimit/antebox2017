<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsCategoryFields extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_category_fields';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['products_category_id', 'name'];

    /**
    * Return the relationship with Product model.
    */
    public function category()
    {
      return $this->belongsTo('App\ProductsCategories');
    }
}
