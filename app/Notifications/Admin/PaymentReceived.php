<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;

class PaymentReceived extends Notification
{
    use Queueable;

    protected $payment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($payment)
    {
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
      return (new SlackMessage)
          ->success()
          ->attachment(function ($attachment) {
            $attachment->title('Payment Received!', url('/admin/dashboard'))
            ->fields([
                'Amount' =>  '$'.$this->payment->formatAmount(),
                'From'  => $this->payment->user->profile->name
            ]);
          });
    }
}
