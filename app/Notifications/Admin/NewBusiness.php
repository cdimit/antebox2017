<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;

class NewBusiness extends Notification
{
    use Queueable;

    protected $business;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($business)
    {
        $this->business = $business;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
      return (new SlackMessage)
          ->success()
          ->attachment(function ($attachment) {
            $attachment->title('A new business has register!', url('/admin/business/index'))
            ->fields([
                'Name' =>  $this->business->name,
                'Plan'  => $this->business->subscribePlan->category
            ]);
          });
    }
}
