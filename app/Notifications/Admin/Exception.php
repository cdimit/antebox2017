<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;

class Exception extends Notification
{
    use Queueable;

    protected $exception;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
      return (new SlackMessage)
        ->error()
        ->content('Exception Handler!')
        ->attachment(function ($attachment) {
          $attachment->title('Details')
          ->fields([
              'Message' =>  $this->exception->getMessage(),
              'File'  => $this->exception->getFile(),
              'Line' => $this->exception->getLine(),
              'Code' => $this->exception->getCode()
          ]);
        });
    }
}
