<?php

namespace App\Notifications\Client;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReceivedOffer extends Notification
{
    use Queueable;

    protected $client_request, $bidder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($client_request, $bidder)
    {
        $this->client_request = $client_request;
        $this->bidder = $bidder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      return (new MailMessage)
                  ->subject('You have received a delivery offer')
                  ->line($this->bidder->profile->name.' has made you a delivery offer')
                  ->line('Accept the delivery to receive your order')
                  ->action('View the offer', url('/client/request/'.$this->client_request->id.'/view'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
          'message' => "New Delivery Offer!",
          'redirect'  => '/client/request/'.$this->client_request->id.'/view',
        ];
    }
}
