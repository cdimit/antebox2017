<?php

namespace App\Notifications\Client;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class Welcome extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Welcome to Shopguin!')
            ->greeting('Welcome to Shopguin!')
            ->line('Now you can easily shop products from every business selling on the Shopguin network.')
            ->line('Shopguin works a bit differently from other stores.')
            ->line('1) Add products to your shopping carts')
            ->line('2) Checkout to send your order to the business')
            ->line('3) Receive a delivery bid by the business')
            ->line('4) Accept it and pay for your items to be delivered')
            ->action('Find Products', url('products'))
            ->line('After you checkout a shopping cart, you can message the business for any questions or special requests you may have!')
            ->line('Enjoy!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => "Welcome to Shopguin!!!",
            'redirect'  => "client/profile/edit"
        ];
    }
}
