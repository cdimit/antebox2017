<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ConversationMessage extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'conversation_messages';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['message', 'is_seen', 'is_note', 'conversation_id', 'user_id'];

  /**
  * Return the relationship with model.
  */
  public function user()
  {
    return $this->belongsTo('App\User');
  }

  /**
  * Return the relationship with model.
  */
  public function conversation()
  {
    return $this->belongsTo('App\Conversation');
  }

  public function isNote()
  {
    return $this->is_note;
  }

  public function isAuth()
  {
    return $this->user_id==Auth::user()->id;
  }


}
