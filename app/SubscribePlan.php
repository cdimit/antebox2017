<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscribePlan extends Model
{
  /**
   * Don't auto-apply mass assignment protection.
   *
   * @var array
   */
  protected $guarded = [];

  protected $casts = [
    'price' => 'integer'
  ];

  public function getHumanpriceAttribute(): string
  {
    return number_format($this->price/100, 2, '.', '');
  }
}
