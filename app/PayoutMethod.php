<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayoutMethod extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'payout_methods';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['method', 'data', 'user_id'];


  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

}
