<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;
use App\Bid;
use App\DealInvoice;

class ClientRequest extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'client_requests';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['client_id', 'business_id', 'meeting_point_id', 'message', 'total_price', 'status'];


  public function items()
  {
    return $this->morphMany('App\Items', 'itemable');
  }

  public function bids()
  {
    return $this->hasMany('App\Bid');
  }


  public function meetingPoint()
  {
    return $this->belongsTo('App\MeetingPoints', 'meeting_point_id');
  }

  /**
  * Return the relationship with User model.
  */
  public function client()
  {
    return $this->belongsTo('App\Client');
  }

  /**
  * Return the relationship with model.
  */
  public function business()
  {
    return $this->belongsTo('App\Business');
  }

  public function isOwner(User $user)
  {
    if($user->isClient()){
      return $this->client_id==$user->profile->id;
    }

    return false;
  }

  /**
  * update the status value to close.
  */
  public function closed()
  {
    return $this->update([
      'status' => 'close'
    ]);
  }

  public function rejectOpenBids()
  {
    foreach($this->bids->where('status', 'open') as $bid){
      $bid->rejected();
    }

    return $this->closed();
  }


  public function scopeOpen($query)
  {
    return $query->where('status', 'Open')->get();
  }

  public function invoice()
  {
    return $this->hasOne('App\DealInvoice', 'client_request_id');
  }

  public function acceptedBid()
  {
      return $this->bids->where('status', 'accepted')->first();
  }
}
