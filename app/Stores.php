<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stores extends Model
{

  use SoftDeletes;

  protected $softDelete = true;
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'stores';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'name',
     'phone',
     'address',
     'state',
     'zip_code',
     'country',
     'city',
     'map_x',
     'map_y',
     'business_id'
     ];


     /**
     * Return the relationship with Business model.
     */
     public function business()
     {
       return $this->belongsTo('App\Business', 'business_id');
     }

     public function products()
     {
       return $this->belongsToMany('App\Products' , 'products_stores', 'store_id', 'product_id');
     }

     public function hasProduct($product)
     {
        return $this->products()->where('product_id', $product->id)->first()!=null;
     }

     public function addAllProducts()
     {
       foreach($this->business->products as $product){
         if(!$this->hasProduct($product)){
           $this->products()->attach($product->id);
         }
       }

     }

}
