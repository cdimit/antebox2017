<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\VerifiedEmail;
use App\Business;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'email', 'password', 'refer_code', 'stripe_id', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_verified'
    ];

    /**
    * Check if a user is admin
    *
    * return boolean
    */
    public function isAdmin()
	  {
      	return $this->attributes['role'] == 'admin';
    }

    /**
    * Check if a user is client
    *
    * return boolean
    */
    public function isClient()
    {
        return $this->attributes['role'] == 'client';
    }

    /**
    * Check if a user is business
    *
    * return boolean
    */
    public function isBusiness()
    {
        return $this->attributes['role'] == 'business';
    }

    /**
    * update the is_verified value to true.
    */
    public function verified()
    {
      return $this->update([
        'is_verified' => true
      ]);
    }

    /**
    * update the is_verified value to false.
    */
    public function unverified()
    {
      return $this->update([
        'is_verified' => false
      ]);
    }

    /**
    * Return the relationship with ClientProfile model.
    */
    public function verificationToken()
   	{
   		return $this->hasOne('App\VerificationToken');
   	}

    /**
    * Return the relationship with ClientProfile model.
    */
    public function profile()
    {
      if($this->isClient()){
        return $this->hasOne('App\Client');
      }
      if($this->isBusiness()){
        return $this->hasOne('App\Business');
      }

      return null;
    }

    /**
    * Clients
    * Return the relationship with ClientProfile model.
    */
    public function socialAuth()
   	{
   		return $this->hasMany('App\SocialAuth');
   	}

    /**
    * Clients & Business
    * My Refers
    */
    public function myRefers()
    {
      return $this->hasMany('App\Refer', 'referred_by');
    }

    /**
    * Clients
    * Whose refer me
    */
    public function referredBy()
    {
      return $this->hasOne('App\Refer', 'user_id');
    }

    public function payoutMethods()
    {
        return $this->hasMany('App\PayoutMethod');
    }

    public function payout($method)
    {
      if($method=='paypal'){
        return $this->payoutMethods->where('method', 'paypal')->first();
      }
      if($method=='iban'){
        return $this->payoutMethods->where('method', 'iban')->first();
      }

      return null;
    }

    public function routeNotificationForSlack()
    {
      return "https://hooks.slack.com/services/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";
    }

    public function requestConversations()
    {
      return $this->hasMany('App\Conversation', 'requester_id');
    }

    public function bidConversations()
    {
      return $this->hasMany('App\Conversation', 'bidder_id');
    }

    public function payments()
    {
      return $this->hasMany('App\Payment');
    }

    /**
     * Fetch a user by their Stripe id.
     *
     * @param  string $stripeId
     * @return User
     */
    public static function byStripeId($stripeId)
    {
        return static::where('stripe_id', $stripeId)->firstOrFail();
    }
}
