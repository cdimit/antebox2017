<?php

namespace App;

use Stripe\Customer;
use Stripe\Subscription as StripeSubscription;
use Carbon\Carbon;

class Subscription
{
  protected $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }

  public function create(SubscribePlan $plan, $token)
  {
    $trial_end = Carbon::now()->addMonth()->timestamp;

    $customer = Customer::create([
      'email' => $this->user->email,
      'source' => $token,
      'plan'  => $plan->code,
      "trial_end" => $trial_end
    ]);

    $subscriptionId = $customer->subscriptions->data[0]->id;

    $this->user->profile->activate($customer->id, $subscriptionId);

  }

  /**
   * Cancel the user's Stripe subscription.
   *
   * @param  boolean $atPeriodEnd
   * @return void
   */
  public function cancel($atPeriodEnd = true)
  {
      $customer = $this->retrieveStripeCustomer();
      $subscription = $customer->cancelSubscription(['at_period_end' => $atPeriodEnd]);
      $endDate = Carbon::createFromTimestamp($subscription->current_period_end);

      $this->user->profile->deactivate($endDate);
  }

  /**
   * Cancel the user's Stripe subscription immediately.
   *
   * @return void
   */
  public function cancelImmediately()
  {
      return $this->cancel(false);
  }

  public function change($plan)
  {

    $plan = SubscribePlan::findOrFail($plan);

    $subscription = $this->retrieveStripeSubscription();
    $subscription->plan = $plan->code;
    // $subscription->prorate = false;
    $subscription->save();

    $this->user->profile->changePlan($plan);

  }

  public function resume()
  {
    $subscription = $this->retrieveStripeSubscription();

    $subscription->plan = $this->user->profile->subscribePlan->code;

    $subscription->save();

    $this->user->profile->activate();
  }


  /**
   * Retrieve a user's Stripe-specific customer instance.
   *
   * @return \Stripe\Customer
   */
  public function retrieveStripeCustomer()
  {
      return Customer::retrieve($this->user->stripe_id);
  }

  /**
 * Retrieve a user's Stripe-specific subscription.
 *
 * @return \Stripe\SubscriptionItem
 */
public function retrieveStripeSubscription()
{
    return StripeSubscription::retrieve($this->user->profile->stripe_subscription);
}


}
