<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bid;
use App\Invoice;

class DealClient extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'deal_clients';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
     'bid_id',
     'deal_invoice_id',
     'is_bought',
     'is_delivered',
     'is_received'
   ];

   /**
   * Return the relationship with model.
   */
   public function bid()
   {
     return $this->belongsTo('App\Bid');
   }

   public function getDelivererAttribute()
   {
     return $this->bid->biddable;
   }

   public function isClientDeal()
   {
     return true;
   }

   /**
   * Return the relationship with model.
   */
   public function invoice()
   {
     return $this->belongsTo('App\DealInvoice');
   }

   public static function acceptBid(Bid $bid)
   {

     $deal_invoice = DealInvoice::CreateRecord($bid, 'client');

     return self::create(['bid_id' => $bid->id, 'deal_invoice_id' => $deal_invoice->id]);
   }
}
