<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
  /**
   * Don't auto-apply mass assignment protection.
   *
   * @var array
   */
  protected $guarded = [];

  /**
  * Return the relationship with User model.
  */
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

  public function formatAmount()
  {
      return number_format($this->amount / 100, 2);
  }


  // public static function byChargeId($chargeId)
  // {
  //     return static::where('charge_id', $chargeId)->firstOrFail();
  // }
  //
  // public function setToInvocie($invoiceId)
  // {
  //   return $this->update([
  //     'paymentable_type'  => 'App\DealInvoice',
  //     'paymentable_id'  => $invoiceId
  //   ]);
  // }


}
