<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
  /**
   * Don't auto-apply mass assignment protection.
   *
   * @var array
   */
  protected $guarded = [];

  public function getSlugAttribute(): string
  {
    return strtolower($this->name).".main";
  }

}
