<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Cache;
use URL;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      // $request->session()->put('redirect', \URL::full());
      // session(['redirect' => \URL::full()]);


        if (is_null($request->user()) || !$request->user()->isClient())
        {
          $expiresAt = Carbon::now()->addMinutes(1);
          Cache::put('redirect', URL::full(), $expiresAt);

            return redirect('/login')->with('status', 'Please login first');
        }

        return $next($request);
    }
}
