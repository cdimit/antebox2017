<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckReferral
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guest()) {
            if( $request->ref || $request->query('ref') ) {
                return redirect('/register')->withCookie(cookie('referral', $request->ref, 1440)); //queue($name, $value, $minutes);
            }
        }

        return $next($request);
    }
}
