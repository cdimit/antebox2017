<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\SubscribePlan;
use Stripe\Customer;

class SubscribeForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !! $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stripeEmail' => 'required|email',
            'stripeToken' => 'required',
            'plan'        => 'required'
        ];
    }

    public function save()
    {
      $plan = SubscribePlan::findOrFail($this->plan);


      $this->user()->profile->subscription()->create($plan, request('stripeToken'));

    }
}
