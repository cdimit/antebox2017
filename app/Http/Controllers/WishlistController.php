<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Auth;

class WishlistController extends Controller
{

    public function index()
    {
      $wishlist = Auth::user()->profile->wishlist;
      return view('wishlist')->withWishlist($wishlist);
    }

    public function add(Products $product)
    {
      $client = Auth::user()->profile;

      if(!$client->isOnWishlist($product)){
        $client->addOnWishlist($product);
      }
      return back()->with('status', 'Item successfully added to wishlist');
    }

    public function remove(Products $product)
    {
      $client = Auth::user()->profile;

      if($client->isOnWishlist($product)){
        $client->removeFromWishlist($product);
      }
      return back();
    }
}
