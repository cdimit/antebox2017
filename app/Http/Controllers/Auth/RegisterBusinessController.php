<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Business;
use App\VerificationToken;
use App\Mail\Verification;
use Mail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ReferController;
use App\SubscribePlan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterBusinessController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'business_name' => 'required|max:255|min:4',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'country' => 'required',
            'subscribe_plan_id' => 'required',
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
      $plans = SubscribePlan::all();

        return view('auth.register_business')->withPlans($plans);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        //Complete all queries before commited
        DB::beginTransaction();

        //Create User credentials.
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role'  => 'business',
            'refer_code' => ReferController::generateReferCodeForBusiness($data['business_name']),
        ]);

        //Create Business Profile.
        Business::create([
          'user_id' => $user->id,
          'name'  =>  $data['business_name'],
          'slug'  =>  str_slug($data['business_name']),
          'address1'  => $data['address1'],
          'address2'  => $data['address2'],
          'city'  => $data['city'],
          'state'  => $data['state'],
          'country'  => $data['country'],
          'post_code'  => $data['post_code'],
          'subscribe_plan_id' => $data['plan']
        ]);

        //Create Verification Token for Email
        VerificationToken::create([
          'user_id' => $user->id,
          'token' => str_random(100),
        ]);


        //Commit the transaction
        DB::commit();

        //Send verification email
        Mail::to($user->email)->send(new Verification($user));

        //Send welcome notification
        $user->notify(new \App\Notifications\Welcome);
        $user->notify(new \App\Notifications\Admin\NewBusiness);

        return $user;
    }

}
