<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Client;
use App\VerificationToken;
use App\Mail\Verification;
use Mail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ReferController;
use Cache;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo ;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $redirect = Cache::get('redirect');

        if($redirect){
          $this->redirectTo = $redirect;
        }else{
          $this->redirectTo = '/client/dashboard';
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //Complete all queries before commited
        DB::beginTransaction();

        //Create User credentials.
        $user = User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'refer_code' => ReferController::generateReferCodeForClient(), //1500625 unique codes
        ]);

        //Create User Profile.
        Client::create([
          'user_id' => $user->id,
          'first_name' => $data['first_name'],
          'last_name' => $data['last_name'],
        ]);

        //Create Verification Token for Email
        VerificationToken::create([
          'user_id' => $user->id,
          'token' => str_random(100),
        ]);

        //Commit the transaction
        DB::commit();

        ReferController::createRefer($user);



        //Send verification email
        Mail::to($user->email)->send(new Verification($user));

        //Send welcome notification
        $user->notify(new \App\Notifications\Client\Welcome);
        $user->notify(new \App\Notifications\Admin\NewClient);

        return $user;
    }

}
