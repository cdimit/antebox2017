<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Client;
use App\VerificationToken;
use App\SocialAuth;
use App\Mail\Verification;
use Auth;
use Mail;
use Socialite;
use Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ReferController;

use Config;

class SocialController extends Controller
{

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/client/dashboard';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('guest');
  }

  /**
   * Redirect the user to the Facebook authentication page.
   *
   * @return Response
   */
  public function redirectToProvider($provider)
  {
      return Socialite::driver($provider)->redirect();
  }

  /**
   * Obtain the user information from Facebook.
   *
   * @return Response
   */
  public function handleProviderCallback($provider, Request $request)
  {

    if (!$request->has('code') || $request->has('denied')) {
        return redirect('register')->withError("You did not share your profile data with us.");
    }


    $user = Socialite::driver($provider)->user();

    if($provider=="facebook"){
      $handle = $this->handleFacebookCallback($user);
    }
    elseif($provider=="google"){
      $handle = $this->handleGoogleCallback($user);
    }

    if($handle){
      return redirect('register')->withError($handle);
    }


    return redirect($this->redirectTo);

  }

  /**
   * Obtain the user information from Facebook and create or login the user.
   *
   * @param $social_user
   * @return Response
   */
  public function handleFacebookCallback($social_user)
  {
    //Check if facebook account already exist.
    $social = SocialAuth::where('provider_id', $social_user->getId())->first();
    if($social){
     Auth::login($social->user, true);
     return;
    }

    //Check if facebook account has email.
    $email = $social_user->getEmail();
    if(!$email){
      return "There is no email on this Facebook Account.";//->withErrors('There was an error communicating with Facebook');
    }else{
      //Check if email already exist in database.
      $email_exist = User::where('email', $email)->first();
      if($email_exist){
        return "Account email already exist!";
      }
    }



    //Complete all queries before commited
    DB::beginTransaction();

    //Create User credentials.
    $user = User::create([
        'email' => $social_user->getEmail(),
        'password' => bcrypt(env('SOCIAL_PASSWORD')),
        'refer_code' => ReferController::generateReferCodeForClient(), //1500625 unique codes
        'is_verified' => true,
    ]);

    //User Profile.
    //Get Picture
    $picture = file_get_contents($social_user->getAvatar());
    $path = 'clients/pictures/'.uniqid().'.jpg';
    Storage::put($path, $picture);

    //Create Profile
    Client::create([
      'user_id' => $user->id,
      'first_name' => $social_user->user['first_name'],
      'last_name' => $social_user->user['last_name'],
      'gender'    => $social_user->user['gender'],
      'picture'   => $path,
    ]);

    //Create Social Auth.
    SocialAuth::create([
      'user_id' => $user->id,
      'provider_id' =>$social_user->getId(),
      'provider' => "facebook",
    ]);

    //Commit the transaction
    DB::commit();

    ReferController::createRefer($user);

    $user->notify(new \App\Notifications\Client\Welcome);
    $user->notify(new \App\Notifications\Admin\NewClient);

    //Login the new user.
    Auth::login($user, true);

    return;

  }

  /**
   * Obtain the user information from Google and create or login the user.
   *
   * @param $social_user
   * @return Response
   */
  public function handleGoogleCallback($social_user)
  {

    //Check if google account already exist.
    $social = SocialAuth::where('provider_id', $social_user->getId())->first();
    if($social){
     Auth::login($social->user, true);
     return;
    }

    //Check if email already exist in database.
    if(User::where('email', $social_user->getEmail())->first()){
      return "Account email already exist in database";
    }

    //Complete all queries before commited
    DB::beginTransaction();

    //Create User credentials.
    $user = User::create([
        'email' => $social_user->getEmail(),
        'password' => bcrypt(env('SOCIAL_PASSWORD')),
        'refer_code' => ReferController::generateReferCodeForClient(), //1500625 unique codes
        'is_verified' => true,
    ]);

    //User Profile.
    //Get Picture
    $picture = file_get_contents($social_user->getAvatar());
    $path = 'users/pictures/'.uniqid().'.jpg';
    Storage::put($path, $picture);

    //Create Profile
    Client::create([
      'user_id' => $user->id,
      'first_name' => $social_user->user['name']['givenName'],
      'last_name' => $social_user->user['name']['familyName'],
      'picture'   => $path,
    ]);

    //Create Social Auth.
    SocialAuth::create([
      'user_id' => $user->id,
      'provider_id' =>$social_user->getId(),
      'provider' => "google",
    ]);

    //Commit the transaction
    DB::commit();

    ReferController::createRefer($user);

    $user->notify(new \App\Notifications\Client\Welcome);
    $user->notify(new \App\Notifications\Admin\NewClient);

    //Login the new user.
    Auth::login($user, true);

    return;
  }

}
