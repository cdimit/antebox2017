<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductsDiscount;
use App\Products;
use Auth;


class ProductsDiscountController extends Controller
{
  /**
   * Where to redirect
   *
   * @var string
   */
  protected $redirectTo = '/business/products';


  private $form_rules = [
          'product_id'  => 'required',
          'off_method'  => 'required',
          'value'   => 'required|integer',
          'valid_until'     => 'date|nullable',
  ];

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $business_products = Auth::user()->profile->products;

    return view('products_discount.create')->withProducts($business_products);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    if (Auth::check()){
            $v = \Validator::make($request->all(), $this->form_rules);
    }

    if ($v->fails()) {
            return redirect()->back()->withErrors($v);
    }

    $product = Products::find($request['product_id']);

    if($request['off_method']=='percent'){
      $new_price = $product->price - ($product->price * ($request['value']/100));
    }else if($request['off_method']=='amount'){
      $new_price = $product->price - $request['value'];
    }

    if($new_price<=0){
      return redirect()->back()->withErrors(["The price of product is less than zero with this discount!"]);
    }

    ProductsDiscount::create([
      'product_id'  => $request['product_id'],
      'value' => $request['value'],
      'off_method'  => $request['off_method'],
      'valid_until'  => $request['valid_until'],
      'new_price' => $new_price
    ]);

    return redirect($this->redirectTo)->withStatus("Discount was successfully created!");
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\ProductsDiscount  $ProductsDiscount
   * @return \Illuminate\Http\Response
   */
  public function show(ProductsDiscount $productsDiscount)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\ProductsDiscount  $ProductsDiscount
   * @return \Illuminate\Http\Response
   */
  public function edit(ProductsDiscount $productsDiscount)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\ProductsDiscount  $ProductsDiscount
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, ProductsDiscount $productsDiscount)
  {

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\ProductsDiscount  $ProductsDiscount
   * @return \Illuminate\Http\Response
   */
  public function destroy(ProductsDiscount $productsDiscount)
  {
    $productsDiscount->delete();
    return redirect($this->redirectTo)->withStatus("Discount was successfully removed!");
  }

  /**
   * Restore the specified resource from storage.
   *
   * @param  \App\ProductsDiscount  $ProductsDiscount
   * @return \Illuminate\Http\Response
   */
  public function restore($productsDiscount)
  {

  }
}
