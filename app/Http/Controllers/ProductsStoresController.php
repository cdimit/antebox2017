<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stores;
use App\Products;
use Auth;

class ProductsStoresController extends Controller
{

  //Stores

  public function storeProducts(Stores $stores)
  {

    $products = Auth::user()->profile->products;
    $diff = $products->diff($stores->products);

    return view('stores.products')->with('store', $stores)
                ->with('products', $diff);
  }

  public function storeAddProduct(Stores $stores, Products $product)
  {
    if(Auth::user()->profile!=$product->business){
      abort(404);
    }

    if(!$stores->hasProduct($product)){
      $stores->products()->attach($product->id);
    }

    return redirect()->back();
  }

  public function storeRemoveProduct(Stores $stores, Products $product)
  {
    if(Auth::user()->profile!=$product->business){
      abort(404);
    }

    $stores->products()->detach($product->id);

    return redirect()->back();
  }


  //Products

  public function productStores(Products $products)
  {

    $stores = Auth::user()->profile->stores;
    $diff = $stores->diff($products->stores);

    return view('stores.stores')->with('product', $products)
                ->with('stores', $diff);
  }

  public function productAddStore(Products $products, Stores $store)
  {
    if(Auth::user()->profile!=$store->business){
      abort(404);
    }

    if(!$products->hasStore($store)){
      $products->stores()->attach($store->id);
    }

    return redirect()->back();
  }

  public function productRemoveStore(Products $products, Stores $store)
  {
    if(Auth::user()->profile!=$store->business){
      abort(404);
    }

    $products->stores()->detach($store->id);

    return redirect()->back();
  }
}
