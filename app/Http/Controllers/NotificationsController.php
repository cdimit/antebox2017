<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class NotificationsController extends Controller
{

    public function read($notification_id){
      $user = Auth::user();
      $notification = $user->unreadNotifications->where("id", $notification_id)->first();

      if($notification){
        $notification->markAsRead();

        return redirect($notification->data['redirect']);
      }else{
        abort(404);
      }

    }

    public function markAsRead(){

      Auth::user()->unreadNotifications->markAsRead();

      return redirect()->back();
    }
}
