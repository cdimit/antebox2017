<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use Auth;
use App\ProductsCategories;
use App\Tags;
use Illuminate\Support\Facades\DB;
use Redis;
use Carbon\Carbon;

class ProductsController extends Controller
{

	/**
	 * Where to redirect
	 *
	 * @var string
	 */
	protected $redirectTo = '/business/products';

	private $form_rules = [
		'name'  => 'required',
		'description'   => '',
		'long_description' => '',
		'picture'     => 'mimes:png,jpeg',
		'video'   => 'mimetypes:video/avi,video/mpeg,video/mp4',
		'price'  => 'required|numeric',
		'bag'   => '',
		'weight'     => 'numeric|nullable',
		'height'   => 'numeric|nullable',
		'width'     => 'numeric|nullable',
		'depth'   => 'numeric|nullable',
		'category_id' => 'required',
		'tags'	=> ''
	];


	/**
   * Business access only
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$products =  Auth::user()->profile->products;
		return view('products.index')->withProducts($products);
	}



	/**
   * Business access only
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$categories = ProductsCategories::all();
		return view('products.create')->withCategories($categories);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// For check
		$business = Auth::user()->profile;

		if (Auth::check()){
			$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}

		if (!empty($request['picture'])) {
			$picture = $request['picture']->store('products/'.$business->slug);
		} else {
			$picture = "product_picture.jpg";
		}

		if (!empty($request['video'])) {
			$video = $request['video']->store('products/'.$business->slug);
		} else {
			$video = null;
		}

		//Complete all queries before commited
		DB::beginTransaction();

		$product = Products::create([
			'name'  => $request['name'],
			'description' => $request['description'],
			'long_description' => $request['long_description'],
			'price'  => $request['price'],
			'bag' => $request['bag'],
			'weight'  => $request['weight'],
			'height' => $request['height'],
			'width'  => $request['width'],
			'depth' => $request['depth'],
			'picture'  => $picture,
			'video' => $video,
			'category_id' => $request['category_id'],
			'business_id' => $business->id
		]);


		$tags = explode(',', $request['tags']);
		foreach($tags as $tag_string){
			if($tag_string!=' ' && $tag_string!=''){
				$tag = Tags::findOrCreate($tag_string);
				$product->tags()->attach($tag->id);
			}
		}

		$product->syncStores($request['stores']);

		//Commit the transaction
		DB::commit();

		return redirect($this->redirectTo)->withStatus("Product was successfully created!");
	}

	/**
	* Business access only
	* Show the form for editing the specified resource.
	*
	* @param  \App\Products  $products
	* @return \Illuminate\Http\Response
	*/
	public function edit(Products $products)
	{
		$categories = ProductsCategories::all();
		return view('products.edit')->withProduct($products)->withCategories($categories);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Products  $products
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Products $products)
	{
		// For check
		$business = Auth::user()->profile;

		if (Auth::check()){
			$v = \Validator::make($request->all(), $this->form_rules);
		}

		if ($v->fails()) {
			return redirect()->back()->withErrors($v);
		}

		if (!empty($request['picture'])){
			$picture = $request['picture']->store('products/'.$business->slug);
		} else {
			$picture = $products->picture;
		}

		if (!empty($request['video'])){
			$video = $request['video']->store('products/'.$business->slug);
		} else {
			$video = $products->video;
		}

		$products->name  = $request['name'];
		$products->description = $request['description'];
		$products->long_description = $request['long_description'];
		$products->price  = $request['price'];
		$products->bag = $request['bag'];
		$products->weight  = $request['weight'];
		$products->height = $request['height'];
		$products->width  = $request['width'];
		$products->depth = $request['depth'];
		$products->picture  = $picture;
		$products->video = $video;
		$products->category_id = $request['category_id'];
		$products->save();

		$tags = explode(',', $request['tags']);
		$products->tags()->detach();
		foreach($tags as $tag_string){
			if($tag_string!=' ' && $tag_string!=''){
				$tag = Tags::findOrCreate($tag_string);
				$products->tags()->attach($tag);
			}
		}

		return redirect($this->redirectTo)->withStatus("Product was successfully edited!");
	}

	/**
	 * Business access only
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Products  $products
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Products $products)
	{
		$products->getClientWish()->detach();
		$products->items->each(function ($item) { $item->remove(); });
		$products->delete();
		return redirect($this->redirectTo)->withStatus("Product was successfully deleted!");
	}

	/**
	* Display the specified resource public.
	* Single products view for client.
	*
	* @param  \App\Products  $products, String $slug
	* @return view 'products.single'
	*/
	public function single(Products $product, $slug = '')
	{
		if ($slug !== $product->slug) {
			return redirect()->to($product->url);
		}

		$ip = \Request::getClientIp();

		if($date = Redis::hget("{$ip}", "business.{$product->business->id}.product.{$product->id}")){
			if(!$date>=Carbon::now()->subDay()){
				Redis::hset("{$ip}", "business.{$product->business->id}.product.{$product->id}", Carbon::now());

				Redis::hincrby("business.{$product->business->id}.visits", "total", 1);
				Redis::hincrby("business.{$product->business->id}.visits", Carbon::now()->toDateString(), 1);
				Redis::hincrby("business.{$product->business->id}.visits", "product.{$product->id}", 1);
			}
		}else{
			Redis::hset("{$ip}", "business.{$product->business->id}.product.{$product->id}", Carbon::now());

			Redis::hincrby("business.{$product->business->id}.visits", "total", 1);
			Redis::hincrby("business.{$product->business->id}.visits", Carbon::now()->toDateString(), 1);
			Redis::hincrby("business.{$product->business->id}.visits", "product.{$product->id}", 1);
		}

		return view('products.single')->withProduct($product);
	}


	// Return view for all products
	public function all()
	{
		return view('products.all');
	}

	public function search(Request $search_query)
	{
		$keywords = $search_query->search_query;

		if($keywords) {
			$query = Products::query();


			$query->where(function ($q) use($keywords) {
				$q->where("name", "LIKE", "%$keywords%");
			});

			$query->orWhereHas('tags', function ($q)  use($keywords) {
				$q->where('name', 'LIKE', "%$keywords%");
			});


			$products = $query->get();

			$data = [
				'products' => $products
			];

			return view('products.search', $data);
		}
	}

	public function images(Products $products)
	{

		return view('products.images')->withProduct($products);
	}

	public function addImages(Products $products, Request $request)
	{

		if (!empty($request['picture'])){
			$image = $request['picture']->store('products/'.$products->id.'/images');
		} else {
			return back();
		}


		Redis::sadd('product'.$products->id.'images', $image);


		return back();
	}

	public function removeImages(Products $products)
	{

		Redis::srem('product'.$products->id.'images', request('image'));


		return back();

	}

}
