<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientRequest;
use App\ShoppingCart;
use Auth;
use Illuminate\Support\Facades\DB;

class ClientRequestController extends Controller
{

    /**
     * Where to redirect
     *
     * @var string
     */
    protected $redirectTo = '/';

    private $form_rules = [
      'meeting_point'  => '',
      'address1'  => 'required_if:meeting_point,null',
      'country' => 'required_if:meeting_point,null',
      'city'  => 'required_if:meeting_point,null'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClientRequest $request)
    {
      $business_id = Auth::user()->profile->id;

      $allRequests = ClientRequest::whereHas('business', function($q) use ($business_id) {
        $q->where('id', '=', $business_id);
      })->get();

      $openRequests = ClientRequest::whereHas('business', function($q) use ($business_id) {
        $q->where('id', '=', $business_id);
      })->where('status', '=', 'open')->get();

      $closedRequests = ClientRequest::whereHas('business', function($q) use ($business_id) {
        $q->where('id', '=', $business_id);
      })->where('status', '=', 'close')->get();

      $data = [
        'allRequests' => $allRequests,
        'openRequests' => $openRequests,
        'closedRequests' => $closedRequests
      ];

      return view('carts.index', $data);
    }

    public function openCarts(ClientRequest $request)
    {
      $business_id = Auth::user()->profile->id;
      $openRequests = ClientRequest::whereHas('business', function($q) use ($business_id) {
        $q->where('id', '=', $business_id);
      })->where('status', '=', 'open')->get();
      return view('carts.business-open')->with('requests', $openRequests);
    }

    public static function permissionCheck(ShoppingCart $shopping_cart)
    {
      $client = Auth::user()->profile;

      if(!$shopping_cart->belongsToClient($client)){
        abort(404);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ShoppingCart $shopping_cart)
    {
      $client = Auth::user()->profile;

      $this->permissionCheck($shopping_cart);

      return view('client_request.create')->with('shopping_cart', $shopping_cart);
    }

    public function continue(ShoppingCart $shopping_cart)
    {
      $client = Auth::user()->profile;

      $this->permissionCheck($shopping_cart);

      return view('carts.addtocart-success')->with('shopping_cart', $shopping_cart);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ShoppingCart $shopping_cart)
    {
      //for check
      $client = Auth::user()->profile;

      $this->permissionCheck($shopping_cart);

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      //Complete all queries before commited
      DB::beginTransaction();


      if(!$meeting_point_id = $request['meeting_point']){

        $meeting_point_id = $client->meetingPoints()->create([
          'address1'  => $request['address1'],
          'address2'  => $request['address2'],
          'state'  => $request['state'],
          'post_code' => $request['post_code'],
          'country'  => $request['country'],
          'city'  => $request['city'],
          'map_x' => $request['map_x'],
          'map_y'  => $request['map_y'],
        ])->id;
      }

      $client_request = ClientRequest::create([
        'client_id'   => $client->id,
        'business_id'  => $shopping_cart->business->id,
        'meeting_point_id' => $meeting_point_id,
        'message'   =>  $request['message'],
        'total_price' => $shopping_cart->price
      ]);

      $shopping_cart->createRequest($client_request);

      $shopping_cart->business->user->notify(new \App\Notifications\Business\NewOrder);

      //Commit the transaction
      DB::commit();

      return redirect($this->redirectTo);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function all()
    {
      $requests = ClientRequest::open();

      return view('deliver.requests')->withRequests($requests);
    }


}
