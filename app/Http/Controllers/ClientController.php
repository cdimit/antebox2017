<?php

namespace App\Http\Controllers;

use App\Client;
use Auth;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Storage;


class ClientController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
      return $client;
    }
    /**
      * Show the client diashboard
    */
    public function dashboard()
    {

      return view('client_profile.home')->with('title', 'Home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $client = Auth::user()->profile;

        return view('client_profile.edit')->withClient($client)->with('title', 'Profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $client = Auth::user()->profile;

      $form_rules = [
              'first_name'  => 'required',
              'last_name'   => 'required',
              'picture'     => 'mimes:png,jpeg',
              'gender'      => '',
              'birthday'    => '',
              'about'       => '',
              'country_code'     => '',
              'phone'       => '',
      ];

      if (Auth::check()){
              $v = \Validator::make($request->all(), $form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      // $default_pic = "clients/pictures/client_profile.jpg";

      if(!empty($request['picture'])){
        $pic = $request['picture']->store('clients/pictures');
        // if($client->picture!=$default_pic){
        //   Storage::delete('/'.$client->picture); //delete the previous image from server
        //   Storage::move($client->picture, 'trash/'.$user->id.'/'.$client->picture);
        // }
      }else{
        $pic=$client->picture;
      }

      $client->first_name = $request['first_name'];
      $client->last_name = $request['last_name'];
      $client->picture = $pic;
      $client->gender = $request['gender'];
      $client->birthday = $request['birthday'];
      $client->about = $request['about'];
      $client->country = $request['country_code'];
      $client->phone = $request['phone'];
      $client->save();

      return redirect()->back()->withStatus("Profile was successfully edited!");

    }

}
