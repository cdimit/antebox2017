<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Business;


class HomepageController extends Controller
{
    public function home()
    {
    	// $query = Products::query();
    	// $query->whereHas('category', function ($q) {
    	// 	$q->where('name', 'Featured');
    	// });
      //
    	// $featured_products = $query->get();
      //
    	// $data = [
    	// 	'featured_products' => $featured_products
    	// ];

      $featured_products = Products::features();

    	return view('main')->with('featured_products', $featured_products);
    }

    public function themes()
    {
        return view('misc.themes');
    }

    public function howto()
    {
        return view('misc.howto');
    }

    public function howto_account()
    {
        return view('misc.howto-account');
    }

    public function howto_addproduct()
    {
        return view('misc.howto-addproduct');
    }

    public function howto_themes()
    {
        return view('misc.howto-themes');
    }
}
