<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Business;
use Auth;
use Redis;
use Carbon\Carbon;

class BusinessController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($business_slug)
    {
      $business = Business::findOrFailWithSlug($business_slug);

      if($business->isOffline()){
        abort(404);
      }

      $theme = $business['themeFields']['theme_id'];
      $stores = $business->stores();

      $themeDb = [
        1 => 'agnolotti.main',
        2 => 'vionnet.main',
        3 => 'capsule.main',
        4 => 'belgravia.main'
      ];

      $ip = \Request::getClientIp();

  		if($date = Redis::hget("{$ip}", "business.{$business->id}.website")){
  			if(!$date>=Carbon::now()->subDay()){
  				Redis::hset("{$ip}", "business.{$business->id}.website", Carbon::now());

          Redis::hincrby("business.{$business->id}.visits", Carbon::now()->toDateString(), 1);
          Redis::hincrby("business.{$business->id}.visits", "total", 1);
          Redis::hincrby("business.{$business->id}.visits", "website", 1);
  			}
  		}else{
  			Redis::hset("{$ip}", "business.{$business->id}.website", Carbon::now());

        Redis::hincrby("business.{$business->id}.visits", Carbon::now()->toDateString(), 1);
        Redis::hincrby("business.{$business->id}.visits", "total", 1);
        Redis::hincrby("business.{$business->id}.visits", "website", 1);
  		}


      return view($themeDb[$theme], ['id' => $business->id, 'business' => $business]);
    }

    public function demo($theme)
    {
      $business = Business::findOrFailWithSlug('demo');
      return view($theme.'.main', ['id' => $business->id, 'business' => $business]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $business_slug
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $business = Auth::user()->profile;
        $url = Storage::url($business->logo);
// dd($business->logo);


        return view('business_profile.edit')->withBusiness($business);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

      $form_rules = [
              'name'  => 'required',
              'about'   => '',
              'logo'     => 'mimes:png,jpeg',
              'since'      => 'nullable|digits:4',
              'phone'       => '',
              'country'     => '',
              'address1'       => '',
              'address2'       => '',
              'state'      => '',
              'city'    => '',
              'zip_code'       => '',
              'map_x'       => '',
              'map_y'     => '',
      ];

      $business = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }



      // $default_pic = "business/logo/business_profile.jpg";

      if(!empty($request['logo'])){
        $pic = $request['logo']->store('business/logo');
        // if($business->picture!=$default_pic){
        //   Storage::delete('/'.$business->picture); //delete the previous image from server
        //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
        // }
      }else{
        $pic=$business->logo;
      }

      $business->name = $request['name'];
      $business->logo = $pic;
      $business->since = $request['since'];
      $business->phone = $request['phone'];
      $business->country = $request['country_code'];
      $business->address1 = $request['address1'];
      $business->address2 = $request['address2'];
      $business->state = $request['state'];
      $business->city = $request['city'];
      $business->zip_code = $request['zip_code'];
      $business->map_x = $request['map_x'];
      $business->map_y = $request['map_y'];
      $business->save();

      return redirect()->back()->withStatus("Profile was successfully edited!");

    }

    public function retailers()
    {
      $businesses = Business::live();
      return view('businesses.retailers', ['businesses' => $businesses]);
    }

    public function dashboard()
    {
      $business = Auth::user()->profile;

      $visits = Redis::hgetall("business.{$business->id}.visits");

      return view('dashboard')->withVisits($visits);
    }

    public function payout()
    {
      return view('business_profile.payout')->withUser(Auth::user());
    }

    public function changeEmail()
    {
      return view('business_profile.email')->with('title', 'Email')->withUser(Auth::user());
    }

    public function changePwd()
    {
      return view('business_profile.password')->with('title', 'Password')->withUser(Auth::user());
    }

}
