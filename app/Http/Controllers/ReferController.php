<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Refer;
use Cookie;

class ReferController extends Controller
{
  /**
   * Create a new unique refer code for a client user.
   * for inner use.
   * Register and Social Controller use it.
   *
   * @param  none
   * @return string
   */
  public static function generateReferCodeForClient()
  {
    $refer_code = strtoupper(str_random(4));
    $check = User::where('refer_code', $refer_code)->first();

    //Until find code, generate new
    while($check){
      $refer_code = strtoupper(str_random(4));
      $check = User::where('refer_code', $refer_code)->first();
    }

    return $refer_code;
  }

  /**
   * Create a new unique refer code for a business user.
   * for inner use.
   * RegisterBusinessController use it.
   *
   * @param  String $name
   * @return string
   */
  public static function generateReferCodeForBusiness($name)
  {
    $split = preg_split("/(\s|&|,|\"|\'|-|_|\.)/", $name); //split symbols and whitespaces
    $filter = array_filter($split, function($value) { return strlen($value) >3; }); //remove element has less than 4 characters.
    $value = array_values($filter)[0]; //Get first element
    $refer_code = strtoupper($value); //set string to uppercase

    $check = User::where('refer_code', $refer_code)->first();

    //Find refer code from name
    $i = 1;
    while($check && $i<count($filter)){
      $value = array_values($filter)[$i];
      $refer_code = strtoupper($value);
      $i++;
      $check = User::where('refer_code', $refer_code)->first();
    }

    //If previues while failure find refer code from name and add an integer.
    $j = 1;
    while($check){
      $value = array_values($filter)[0];
      $refer_code = strtoupper($value).$j;
      $j++;
      $check = User::where('refer_code', $refer_code)->first();
    }

    return $refer_code;
  }

  /**
   * Create a new refer buddies.
   * for inner use.
   * Register and Social Controller use it.
   *
   * @param  App\User $user
   * @return boolean
   */
  public static function createRefer($user)
  {
    //Get cookie
    $refer_code = Cookie::get('referral');
    $refer_user = Refer::getUser(strtoupper($refer_code));

    //Check if refer user exist
    if($refer_user){
      Refer::create([
        'user_id'   => $user->id,
        'referred_by' => $refer_user->id,
      ]);

      return true;
    }

    return false;
  }

  public function invite(){

    return redirect('register');

  }

}
