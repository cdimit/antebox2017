<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Items;
use App\ShoppingCart;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{

    /**
     * Persist a new item.
     *
     * @param  Products $product
     * @return \Illuminate\Http\RedirectResponse
     */
      public function store(Products $product)
      {

          $shopping_cart = ShoppingCart::findOrCreate($product->business);

          if($item = $shopping_cart->getItem($product)){
            $item->incrQty();
          }else{
            $shopping_cart->addItem([
                'product_id' => $product->id
            ]);
          }
          // return back()->with('status', 'Item successfully added to cart');
          return redirect('/shopping_cart/added/success/'.$shopping_cart->id);

      }

      public function searchAdd(Products $product)
      {

          $shopping_cart = ShoppingCart::findOrCreate($product->business);

          if($item = $shopping_cart->getItem($product)){
            $item->incrQty();
          }else{
            $shopping_cart->addItem([
                'product_id' => $product->id
            ]);
          }
          return redirect('/products');
      }

      /**
       * Remove the specified resource from storage.
       *
       * @param  \App\Stores  $store
       * @return \Illuminate\Http\Response
       */
      public function destroy(Items $item)
      {
        $item->remove();

        if($item->itemable->items->count()>0){
          return back();
        }else{
          return redirect('/products');
        }
      }
}
