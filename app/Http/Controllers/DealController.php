<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DealInvoice;

class DealController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(DealInvoice $invoice)
  {
    return view('deal.view')->withInvoice($invoice);
  }

  public function businessInvoice(DealInvoice $invoice)
  {
    return view('deal.business-invoice')->withInvoice($invoice);
  }

  public function pay(DealInvoice $invoice)
  {

  }
}
