<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class BusinessThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function themes()
    {
      $fields = Auth::user()->profile->themeFields;

        return view('business_profile.themes')->withFields($fields);;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customise()
    {
        $fields = Auth::user()->profile->themeFields;

        return view('business_profile.theme_customisations')->withFields($fields);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateFrontPageSlider(Request $request)
    {
        $form_rules = [
          'slider_1_img' => 'mimes:png,jpeg',
      		'slider_2_img' => 'mimes:png,jpeg',
      		'slider_3_img' => 'mimes:png,jpeg',
      		'slider_1_heading' => '',
      		'slider_2_heading' => '',
      		'slider_3_heading' => '',
      		'slider_1_p' => '',
      		'slider_2_p' => '',
      		'slider_3_p' => ''
        ];

        $business = Auth::user()->profile;

        if (Auth::check()){
                $v = \Validator::make($request->all(), $form_rules);
        }

        if ($v->fails()) {
                return redirect()->back()->withErrors($v);
        }

        // $default_pic = "business/logo/business_profile.jpg";

        if(!empty($request['slider_1_img'])){
          $slider1 = $request['slider_1_img']->store('business/'.$business->id);
          // if($business->picture!=$default_pic){
          //   Storage::delete('/'.$business->picture); //delete the previous image from server
          //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
          // }
        }else{
          $slider1=$business->themeFields->slider_1_img;
        }

        if(!empty($request['slider_2_img'])){
          $slider2 = $request['slider_2_img']->store('business/'.$business->id);
          // if($business->picture!=$default_pic){
          //   Storage::delete('/'.$business->picture); //delete the previous image from server
          //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
          // }
        }else{
          $slider2=$business->themeFields->slider_2_img;
        }

        if(!empty($request['slider_3_img'])){
          $slider3 = $request['slider_3_img']->store('business/'.$business->id);
          // if($business->picture!=$default_pic){
          //   Storage::delete('/'.$business->picture); //delete the previous image from server
          //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
          // }
        }else{
          $slider3=$business->themeFields->slider_2_img;
        }

        $business->themeFields->slider_1_img = $slider1;
        $business->themeFields->slider_2_img = $slider2;
        $business->themeFields->slider_3_img = $slider3;
        $business->themeFields->slider_1_p = $request['slider_1_p'];
        $business->themeFields->slider_2_p = $request['slider_2_p'];
        $business->themeFields->slider_3_p = $request['slider_3_p'];
        $business->themeFields->slider_1_heading = $request['slider_1_heading'];
        $business->themeFields->slider_2_heading = $request['slider_2_heading'];
        $business->themeFields->slider_3_heading = $request['slider_3_heading'];

        $business->themeFields->save();

        return redirect()->back()->withStatus("FrontPage Slider was successfully updated!");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateHomePage(Request $request)
    {
      $form_rules = [
        'intro_heading' => '',
        'intro' => '',
        'ad_1_text' => '',
        'ad_2_text' => '',
        'ad_3_text' => '',
        'ad_1_link' => '',
        'ad_2_link' => '',
        'ad_3_link' => '',
        'ad_1_img' => 'mimes:png,jpeg',
        'ad_2_img' => 'mimes:png,jpeg',
        'ad_3_img' => 'mimes:png,jpeg',
        'openingclosing' => ''
      ];

      $business = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      // $default_pic = "business/logo/business_profile.jpg";

      if(!empty($request['ad_1_img'])){
        $ad1 = $request['ad_1_img']->store('business/'.$business->id);
        // if($business->picture!=$default_pic){
        //   Storage::delete('/'.$business->picture); //delete the previous image from server
        //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
        // }
      }else{
        $ad1=$business->themeFields->ad_1_img;
      }

      if(!empty($request['ad_2_img'])){
        $ad2 = $request['ad_2_img']->store('business/'.$business->id);
        // if($business->picture!=$default_pic){
        //   Storage::delete('/'.$business->picture); //delete the previous image from server
        //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
        // }
      }else{
        $ad2=$business->themeFields->ad_2_img;
      }

      if(!empty($request['ad_3_img'])){
        $ad3 = $request['ad_3_img']->store('business/'.$business->id);
        // if($business->picture!=$default_pic){
        //   Storage::delete('/'.$business->picture); //delete the previous image from server
        //   Storage::move($business->picture, 'trash/'.$user->id.'/'.$business->picture);
        // }
      }else{
        $ad3=$business->themeFields->ad_3_img;
      }

      $business->themeFields->ad_1_img = $ad1;
      $business->themeFields->ad_2_img = $ad2;
      $business->themeFields->ad_3_img = $ad3;
      $business->themeFields->intro_heading = $request['intro_heading'];
      $business->themeFields->intro = $request['intro'];
      $business->themeFields->openingclosing = $request['openingclosing'];
      $business->themeFields->ad_1_text = $request['ad_1_text'];
      $business->themeFields->ad_2_text = $request['ad_2_text'];
      $business->themeFields->ad_3_text = $request['ad_3_text'];
      $business->themeFields->ad_1_link = $request['ad_1_link'];
      $business->themeFields->ad_2_link = $request['ad_2_link'];
      $business->themeFields->ad_3_link = $request['ad_3_link'];

      $business->themeFields->save();

      return redirect()->back()->withStatus("HomePage was successfully updated!");
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAboutPage(Request $request)
    {
      $form_rules = [
        'about_text' => '',
        'motto' => ''
      ];

      $business = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }


      $business->themeFields->about_text = $request['about_text'];
      $business->themeFields->motto = $request['motto'];

      $business->themeFields->save();

      return redirect()->back()->withStatus("About Page was successfully updated!");
    }


    public function updateThemes(Request $request)
    {
      $form_rules = [
        'theme' => '',
      ];

      $business = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }


      $business->themeFields->theme_id = $request['theme'];

      $business->themeFields->save();

      return redirect()->back()->withStatus("About Page was successfully updated!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
