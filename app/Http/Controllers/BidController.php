<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientRequest;
use Auth;
use App\Bid;
use Illuminate\Support\Facades\DB;
use App\Conversation;
use App\DealInvoice;

class BidController extends Controller
{

    /**
     * Where to redirect
     *
     * @var string
     */
    protected $redirectTo = '/';

    private $form_rules = [
      'delivery_method'  => 'required',
      'delivery_fee'  => 'required',
      'delivery_date' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     *  View all bids on client's request
     */
    public function view(ClientRequest $request)
    {
      // $bidder = $request->acceptedBid()->biddable;

      // dd($bidder->user->id);

      // $conversation = $request->client->user->requestConversations->where('bidder_id', $bidder->user->id)->first();

      // dd($conversation->messages);

      return view('carts.view')->withRequest($request);
      // ->withConversation($conversation);

    }



    /**
     *  View all bids on a client's request from business dashboard
    */
    public function businessView(ClientRequest $request)
    {
        $invoices = DealInvoice::all();
        return view('carts.single-business')->withRequest($request)->withInvoices($invoices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ClientRequest $request)
    {
      return view('bid.create')->withRequest($request);
    }

    public function businessCreate(ClientRequest $request)
    {
      return view('bid.business-create')->withRequest($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req, ClientRequest $request)
    {
      //for check
      $user = Auth::user();
      if($user->isClient() && ($request->client==$user->profile)){
        return redirect($this->redirectTo);
      }elseif($user->isBusiness() && $request->business!=$user->profile){
        return redirect($this->redirectTo);
      }

      if (Auth::check()){
              $v = \Validator::make($req->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      //Complete all queries before commited
      DB::beginTransaction();

      $bid = $user->profile->bids()->create([
        'client_request_id' => $request->id,
        'delivery_method' => $req['delivery_method'],
        'message'   =>  $req['message'],
        'delivery_fee' => $req['delivery_fee'],
        'delivery_date' => $req['delivery_date']
      ]);

      $conversation = Conversation::findOrCreate($request->client->user, $user);
      $conversation->addBidNote($bid);
      $conversation->addMessage($user, $req['message']);

      $request->client->user->notify(new \App\Notifications\Client\ReceivedOffer($request, $user));

      //Commit the transaction
      DB::commit();

      return back();
    }

    public function acceptBid(Bid $bid)
    {
      $bid->makeDeal();

      return back();
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
