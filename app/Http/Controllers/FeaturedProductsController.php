<?php

namespace App\Http\Controllers;

use Redis;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Products;


class FeaturedProductsController extends Controller
{

    public function add(Products $product)
    {

      Redis::zadd('featured', time(), $product->id);

      return back();
    }

    public function remove(Products $product)
    {
      Redis::zrem('featured', $product->id);

      return back();
    }

}
