<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Business;
use App\Payment;
use App\Products;

class AdminController extends Controller
{

    public function businesses()
    {
      $businesses = Business::all();

      return view('admin.businesses-index')->withBusinesses($businesses);
    }

    public function users()
    {

      $clients = Client::all();

      return view('admin.clients-index')->withClients($clients);
    }

    public function payments()
    {
      $payments = Payment::all();

      return view('admin.payments-index')->withPayments($payments);
    }

    public function products()
    {
      $products = Products::all();

      return view('admin.products-index')->withProducts($products);
    }

}
