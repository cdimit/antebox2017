<?php

namespace App\Http\Controllers;

use App\Stores;
use App\Products;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class StoresController extends Controller
{

    /**
  	 * Where to redirect
  	 *
  	 * @var string
  	 */
  	protected $redirectTo = '/business/stores';

    private $form_rules = [
  		'name'  => 'required',
  		'phone'   => '',
  		'address'     => 'required',
  		'state'   => '',
  		'zip_code'  => '',
  		'country'   => 'required',
  		'city'     => 'required',
  		'map_x'   => '',
  		'map_y'     => ''
  	];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $stores =  Auth::user()->profile->stores;
      return view('stores.index')->withStores($stores);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //for check
      $business = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      //Complete all queries before commited
      DB::beginTransaction();

      $store = Stores::create([
        'name'  => $request['name'],
        'phone' => $request['phone'],
        'address'  => $request['address'],
        'state'  => $request['state'],
        'zip_code' => $request['zip_code'],
        'country'  => $request['country'],
        'city'  => $request['city'],
        'map_x' => $request['map_x'],
        'map_y'  => $request['map_y'],
        'business_id'  => $business->id
      ]);

      $store->addAllProducts();

      //Commit the transaction
      DB::commit();
      
      return redirect($this->redirectTo)->withStatus("Store was successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Stores $stores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stores  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Stores $stores)
    {
        return view('stores.edit')->withStore($stores);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stores $stores)
    {
      //for check
      $business = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

        $stores->name = $request['name'];
        $stores->phone = $request['phone'];
        $stores->address  = $request['address'];
        $stores->state = $request['state'];
        $stores->zip_code = $request['zip_code'];
        $stores->country  = $request['country'];
        $stores->city  = $request['city'];
        $stores->map_x = $request['map_x'];
        $stores->map_y  = $request['map_y'];
        $stores->save();

      return redirect($this->redirectTo)->withStatus("Store was successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stores  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stores $stores)
    {
      $stores->delete();
      $stores->products()->detach();
      return redirect($this->redirectTo)->withStatus("Store was successfully deleted!");
    }

}
