<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\MeetingPoints;

class MeetingPointsController extends Controller
{

    /**
     * Where to redirect
     *
     * @var string
     */
    protected $redirectTo = '/client/meeting_points';

    private $form_rules = [
      'address1'     => 'required',
      'address2'     => '',
      'state'   => '',
      'post_code'  => '',
      'country'   => 'required',
      'city'     => 'required',
      'map_x'   => '',
      'map_y'     => ''
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mpoints = Auth::user()->profile->meetingPoints;

        return view('client_profile.meeting_points')->withMpoints($mpoints)->with('title', 'MeetingPoint');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $client = Auth::user()->profile;

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      MeetingPoints::create([
        'address1'  => $request['address1'],
        'address2'  => $request['address2'],
        'state'  => $request['state'],
        'post_code' => $request['post_code'],
        'country'  => $request['country'],
        'city'  => $request['city'],
        'map_x' => $request['map_x'],
        'map_y'  => $request['map_y'],
        'client_id'  => $client->id
      ]);

      return redirect($this->redirectTo)->withStatus("Meeting Point was successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MeetingPoints $meeting_points)
    {

      $client = Auth::user()->profile;
      if(!$meeting_points->client==$client){
        abort(404);
      }

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

        $meeting_points->address1  = $request['address1'];
        $meeting_points->address2  = $request['address2'];
        $meeting_points->state = $request['state'];
        $meeting_points->post_code = $request['post_code'];
        $meeting_points->country  = $request['country'];
        $meeting_points->city  = $request['city'];
        $meeting_points->map_x = $request['map_x'];
        $meeting_points->map_y  = $request['map_y'];
        $meeting_points->save();

      return redirect($this->redirectTo)->withStatus("Meeting Point was successfully updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MeetingPoints $meeting_points)
    {
      $meeting_points->delete();
      return redirect($this->redirectTo)->withStatus("Meeting Point was successfully deleted!");
    }
}
