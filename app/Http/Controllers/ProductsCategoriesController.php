<?php

namespace App\Http\Controllers;

use App\ProductsCategories;
use App\ProductsCategoryFields;
use Illuminate\Http\Request;

use Auth;

class ProductsCategoriesController extends Controller
{

    /**
     * Where to redirect
     *
     * @var string
     */
    protected $redirectTo = '/admin/categories';


    private $form_rules = [
            'name'  => 'required',
            'description'   => '',
            'icon'     => '',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductsCategories::all();
        $trashed_categories = ProductsCategories::onlyTrashed()->get();

        return view('products_categories.index')->withCategories($categories)
                                                ->withTrashedCategories($trashed_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('products_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //for check
      $admin = Auth::user();

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      $category = ProductsCategories::create([
        'name'  => $request['name'],
        'description' => $request['description'],
        'icon'  => $request['icon']
      ]);

      $fields = explode(',', $request['fields']);
  		foreach($fields as $field_string){
  			if($field_string!=' ' && $field_string!=''){
          $lower = strtolower($field_string);
          $trim = trim($lower, ' ');
          $format = ucwords($trim);

  				ProductsCategoryFields::create([
            'products_category_id' => $category->id,
            'name' => $format
          ]);
  			}
  		}


      return redirect($this->redirectTo)->withStatus("Category was successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductsCategories  $productsCategories
     * @return \Illuminate\Http\Response
     */
    public function show(ProductsCategories $productsCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductsCategories  $productsCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductsCategories $productsCategories)
    {
        return view('products_categories.edit')->withCategory($productsCategories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductsCategories  $productsCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductsCategories $productsCategories)
    {
      //for check
      $admin = Auth::user();

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }


        $productsCategories->name = $request['name'];
        $productsCategories->description = $request['description'];
        $productsCategories->icon = $request['icon'];
        $productsCategories->save();

      return redirect($this->redirectTo)->withStatus("Category was successfully edited!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductsCategories  $productsCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductsCategories $productsCategories)
    {
        $productsCategories->delete();
        return redirect($this->redirectTo)->withStatus("Category was successfully deleted!");

    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  \App\ProductsCategories  $productsCategories
     * @return \Illuminate\Http\Response
     */
    public function restore($productsCategories)
    {
        $category = ProductsCategories::onlyTrashed()->findOrFail($productsCategories);

        $category->restore();
        return redirect($this->redirectTo)->withStatus("Category was successfully restored!");

    }
}
