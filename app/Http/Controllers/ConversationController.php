<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Conversation;
use Auth;

class ConversationController extends Controller
{

    private $form_rules = [
      'message'  => 'required'
    ];

    public function index()
    {
      $user = Auth::user();

      return view('conversation.index')->withUser($user);
    }

    public static function permissionCheck(Conversation $conversation)
    {
      $user = Auth::user();
      if(!$conversation->hasUser($user)){
        abort(403);
      }
    }

    public function single(Conversation $conversation)
    {
      $this->permissionCheck($conversation);

      $user = Auth::user();

      return view('conversation.single')->withUser($user)->withConversation($conversation);
    }

    public function sendMessage(Request $request, Conversation $conversation)
    {
      $this->permissionCheck($conversation);

      $user = Auth::user();

      if (Auth::check()){
              $v = \Validator::make($request->all(), $this->form_rules);
      }

      if ($v->fails()) {
              return redirect()->back()->withErrors($v);
      }

      $conversation->addMessage($user, $request['message']);

      return back();
    }
}
