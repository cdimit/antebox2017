<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\ClientRequest;
use Auth;
use App\ShoppingCart;

class ShoppingCartController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $shopping_carts =  Auth::user()->profile->shoppingCarts;

    return view('carts.all')->with('shopping_carts', $shopping_carts);
  }

  public function businessView(ClientRequest $request)
  {
    return view('carts.single-business')->withRequest($request);
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Stores  $store
   * @return \Illuminate\Http\Response
   */
  public function destroy(shopping_cart $shopping_cart)
  {
    $shopping_cart->remove();

    return back();
  }



}
