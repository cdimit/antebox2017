<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VerificationToken;
use App\PayoutMethod;
use Auth;
use Hash;

class UserController extends Controller
{

    /**
    * Give a token and verify the belong user.
    * After complete the process delete the token.
    * Return true if its complete corectly.
    *
    * @param string $token
    * @return boolean
    */
    public function verifiedEmail($token)
    {
      $user = VerificationToken::findUserOrFail($token);
      $user->verified();
      $user->verificationToken->delete();

      return redirect("/");
    }

    public function accountSettings()
    {
      return view('client_profile.settings')->with('title', 'Settings')->withUser(Auth::user());
    }

    public function changeEmail()
    {
      return view('client_profile.email')->with('title', 'Email')->withUser(Auth::user());
    }

    public function changePwd()
    {
      return view('client_profile.pwd')->with('title', 'Password')->withUser(Auth::user());
    }

    public function changePayout()
    {
      return view('client_profile.payout')->with('title', 'Payout')->withUser(Auth::user());
    }

    public function updatePassword(Request $request)
    {
      $form_rules = [
        'old_password'		=> 'required',
        'password'	     	=> 'required|confirmed',
      ];

      $v = \Validator::make($request->all(), $form_rules);

      if ($v->fails()) {
         return redirect()->back()->withErrors($v)->withInput();
      }

      $user = Auth::user();
      if(Hash::check($request->old_password, $user->password)){
        $user->password = bcrypt($request->password);
        $user->save();
      }
      else{
        return redirect()->back()->with('error_password', 'Wrong Password!!');
      }

        return redirect()->back()->with('status_password', 'Password was successfully updated!');

    }

    public function updateEmail(Request $request)
    {
        $form_rules = [
          'email'		=> 'required|email|max:255|unique:users',
        ];

        $v = \Validator::make($request->all(), $form_rules);

        if ($v->fails()) {
           return redirect()->back()->withErrors($v)->withInput();
        }

        $user = Auth::user();

        $user->email = $request['email'];
        $user->is_verified = 0;
        $user->save();

        //Delete if verification token exist
        if($user->verificationToken){
          $user->verificationToken->delete();
        }

        //Create Verification Token for Email
        VerificationToken::create([
          'user_id' => $user->id,
          'token' => str_random(100),
        ]);

        //Send verification email
        // Mail::to($user->email)->send(new Verification($user));


        return redirect()->back()->with('status_email', 'Email was successfully updated! A verification email send!');

    }

    public function updatePayoutMethods(Request $request)
  	{

      $form_rules = [
        'paypal'  => 'email|max:255|nullable',
        'iban'    => 'max:34|min:15|nullable',
      ];

  		$user = Auth::user();

      $v = \Validator::make($request->all(), $form_rules);
      if ($v->fails()) {
        return redirect()->back()->withErrors($v)->withInput();
      }

      //Paypal Method
      if($user->payout('paypal')){ //if user has already data saved
        if($request['paypal']==null){ //if user removed data
          $user->payout('paypal')->delete();
        }
        if($user->payout('paypal')!=$request['paypal']){ //if user change data
          $user->payout('paypal')->data = $request['paypal'];
          $user->payout('paypal')->save();
        }
      }else{
        if($request['paypal']){ //if user add data
          PayoutMethod::create([
              'method' => 'paypal',
              'data'  => $request['paypal'],
              'user_id' => $user->id,
          ]);
        }
      }

      //IBAN Method
      if($user->payout('iban')){ //if user has already data saved
        if($request['iban']==null){ //if user removed data
          $user->payout('iban')->delete();
        }
        if($user->payout('iban')!=$request['iban']){ //if user change data
          $user->payout('iban')->data = $request['iban'];
          $user->payout('iban')->save();
        }
      }else{
        if($request['iban']){ //if user add data
          PayoutMethod::create([
              'method' => 'iban',
              'data'  => $request['iban'],
              'user_id' => $user->id,
          ]);
        }
      }


  		return redirect()->back()->with('status_payout', 'Method was successfully updated!');
  	}

}
