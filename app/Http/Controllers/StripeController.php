<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Stripe\{Charge, Customer};
use App\DealInvoice;
use App\SubscribePlan;
use App\Http\Requests\SubscribeForm;
use \Exception;
use App\User;
use App\Business;
use App\BusinessTheme;
use App\VerificationToken;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use App\Mail\Verification;

class StripeController extends Controller
{
    public function charge()
    {
      $invoice = DealInvoice::findOrFail(request('invoice'));

      try {
        $customer = Customer::create([
          'email' => request('stripeEmail'),
          'source' => request('stripeToken')
        ]);

        $charge = Charge::create([
          'customer' => $customer->id,
          'amount' => $invoice->final_price*100,
          'currency' => 'eur'
        ]);

        $invoice->paid($charge);

      } catch (Exception $e) {
        return response()->json(['status' => $e->getMessage()], 422);
      }



      return [
        'status' => 'Success!'
      ];

    }

    public function index()
    {
      $plans = SubscribePlan::all();

      return view('businesses.subscription')->withPlans($plans);
    }

    public function creditCard()
    {

      $source = Customer::retrieve(Auth::user()->stripe_id)->sources->data[0];

      // dd($source);

      return view('businesses.credit_card')->withSource($source);
    }

    public function updateCreditCard()
    {
      try {
        $customer = Customer::retrieve(Auth::user()->stripe_id);
        $customer->source = request('stripeToken');
        $customer->save();

      } catch (Exception $e) {
        return response()->json(['status' => $e->getMessage()], 422);
      }



      return [
        'status' => 'Success!'
      ];

    }

    public function change_subscription()
    {
      $plans = SubscribePlan::all();

      return view('businesses.subscription_update')->withPlans($plans);
    }

    public function register()
    {

      DB::beginTransaction();

      $user = User::create([
          'email' => request('email'),
          'password' => bcrypt(request('password')),
          'role'  => 'business',
          'refer_code' => ReferController::generateReferCodeForBusiness(request('business_name')),
      ]);

      //Create Business Profile.
      $business = Business::create([
        'user_id' => $user->id,
        'name'  =>  request('business_name'),
        'slug'  =>  str_slug(request('slug')),
        'country'  => request('country'),
        'subscribe_plan_id' => request('plan'),
        'address1'  => request('address1'),
        'address2'  => request('address2'),
        'city'  => request('city'),
        'state'  => request('state'),
        'post_code'  => request('post_code')
      ]);

      BusinessTheme::create([
        'theme_id' => request('theme'),
        'business_id' => $business->id
      ]);

      //Create Verification Token for Email
      VerificationToken::create([
        'user_id' => $user->id,
        'token' => str_random(100),
      ]);

      try {
        $plan = SubscribePlan::findOrFail(request('plan'));


        $user->profile->subscription()->create($plan, request('stripeToken'));
      } catch (Exception $e) {
        return response()->json(['status' => $e->getMessage()], 422);
      }

      //Commit the transaction
      DB::commit();

      //Send verification email
      Mail::to($user->email)->send(new Verification($user));

      //Send welcome notification
      $user->notify(new \App\Notifications\Business\Welcome);
      $user->notify(new \App\Notifications\Admin\NewBusiness($business));

      Auth::login($user, true);

      return [
        'status' => 'Success!'
      ];
    }

    public function subscribe(SubscribeForm $form)
    {

      try {
        $form->save();
      } catch (Exception $e) {
        return response()->json(['status' => $e->getMessage()], 422);
      }

      return [
        'status' => 'Success!'
      ];
    }

    public function update()
    {
      if (request('resume')) {
        auth()->user()->profile->subscription()->resume();
      }

      return back();
    }

    public function destroy()
    {
      auth()->user()->profile->Subscription()->cancel();

      return back();
    }

    public function changePlan()
    {
      auth()->user()->profile->Subscription()->change(request('plan'));

      return back();
    }

}
