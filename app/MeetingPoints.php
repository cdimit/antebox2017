<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingPoints extends Model
{
  use SoftDeletes;

  protected $softDelete = true;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'meeting_points';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'address1',
     'address2',
     'state',
     'post_code',
     'country',
     'city',
     'map_x',
     'map_y',
     'client_id'
     ];


     /**
     * Return the relationship with Client model.
     */
     public function client()
     {
       return $this->belongsTo('App\Client', 'client_id');
     }

     public function clientRequests()
     {
       return $this->hasMany('App\ClientRequest');
     }
}
