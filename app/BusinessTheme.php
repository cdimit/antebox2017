<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessTheme extends Model
{
  /**
   * Don't auto-apply mass assignment protection.
   *
   * @var array
   */
  protected $guarded = [];

  /**
  * Return the relationship with model.
  */
  public function business()
  {
    return $this->belongsTo('App\Business', 'business_id');
  }

  /**
  * Return the relationship with model.
  */
  public function theme()
  {
    return $this->belongsTo('App\Theme', 'theme_id');
  }
}
