<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Log;

class VerificationToken extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'verification_token';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['user_id', 'token'];

  public function user()
  {
      return $this->belongsTo('App\User', 'user_id');
  }

  /**
  * Give as parameter the token and get back the owner user
  * or fail with 404 error
  *
  * @param $token
  * @return_success: User
  * @return_failure: abort 404
  */
  public static function findUserOrFail($token)
  {
      $token = self::where('token', $token)->first();
      if(!$token){
        Log::warning("Someone try an invalid verification link. [Model: App\VerificationToken.php]");
        abort(404);
      }
      return $token->user;
  }

}
