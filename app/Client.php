<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FollowBusiness;
use App\Traits\Wishlist;

class Client extends Model
{
  use SoftDeletes;
  use FollowBusiness;
  use Wishlist;


  protected $table = "client";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'first_name',
    'last_name',
    'picture',
    'gender',
    'birthday',
    'about',
    'country',
    'user_id',
    'phone',
    'stripe_id',
  ];

  public function getNameAttribute(): string
  {
    return $this->first_name.' '.$this->last_name;
  }

  public function getOrdersAttribute(): string
  {
    return $this->shoppingCarts()->count() + $this->requests()->count();
  }

  public function getDeliversAttribute(): string
  {
    return $this->bids()->count();
  }

  /**
  * Return the relationship with User model.
  */
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

  /**
  * Return the meeting points than client create.
  */
  public function meetingPoints()
  {
    return $this->hasMany('App\MeetingPoints');
  }

  /**
  * Return the wishlist than client create.
  */
  public function shoppingCarts()
  {
    return $this->hasMany('App\ShoppingCart');
  }

  /**
  * Return the requests than client create.
  */
  public function requests()
  {
    return $this->hasMany('App\ClientRequest');
  }

  public function bids()
  {
    return $this->morphMany('App\Bid', 'biddable');
  }


}
