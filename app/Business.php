<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FollowBusiness;
use App\Traits\BusinessSubscribe;

class Business extends Model
{
  use SoftDeletes;
  use FollowBusiness;
  use BusinessSubscribe;


  protected $table = "business";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'logo',
    'since',
    'phone',
    'country',
    'address1',
    'address2',
    'state',
    'city',
    'zip_code',
    'slug',
    'map_x',
    'map_y',
    'status',
    'user_id',

    'stripe_id',
    'stripe_active',
    'subscription_end_at',
    'stripe_subscription',
    'subscribe_plan_id'
  ];

  protected $dates = [
    'subscription_end_at'
  ];

  public function getTransactionfeeAttribute(): string
  {
    return 0.005;
  }

  /**
  * Return the relationship with User model.
  */
  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

  /**
  * Return the relationship with User model.
  */
  public function subscribePlan()
  {
    return $this->belongsTo('App\SubscribePlan', 'subscribe_plan_id');
  }

  /**
  * Return the business who has the slug or abort.
  *
  * @param String $slug
  * @return Business
  */
  public static function findOrFailWithSlug($slug)
  {
    $business = self::where('slug', $slug)->with('stores')->first();

    if(!$business){
      abort(404);
    }

    return $business;
  }

  /**
  * Return the products than business create.
  */
  public function products()
  {
    return $this->hasMany('App\Products');
  }

  /**
  * Return the products than business create.
  */
  public function stores()
  {
    return $this->hasMany('App\Stores');
  }

  public function bids()
  {
    return $this->morphMany('App\Bid', 'biddable');
  }

  public function openCarts()
  {
    $openRequests = ClientRequest::whereHas('business', function($q){
      $q->where('id', '=', $this->id);
    })->where('status', '=', 'open')->get();

    return $openRequests->count();
  }

  public function themeFields()
  {
    return $this->hasOne('App\BusinessTheme');
  }

  public function isOffline()
  {
    return $this->status=='offline';
  }

  public function scopeLive($query)
  {
    return $query->where('status','<>', 'offline')->orderBy('name', 'asc')->get();
  }



}
