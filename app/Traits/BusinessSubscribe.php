<?php

namespace App\Traits;

use App\Subscription;
use Carbon\Carbon;
use Stripe\Customer;

trait BusinessSubscribe
{

  /**
   * Activate the user's subscription.
   *
   * @param  string $customerId
   * @param  string $subscriptionId
   * @return bool
   */
  public function activate($customerId = null, $subscriptionId = null)
  {
      $this->user->forceFill([
          'stripe_id' => $customerId ?? $this->user->stripe_id,
          // 'stripe_id' => 'cus_00000000000000',

      ])->save();

      $this->forceFill([
          'stripe_active' => true,
          'stripe_subscription' => $subscriptionId ?? $this->stripe_subscription,
          'subscription_end_at' => null
      ])->save();
  }

  /**
   * Deactivate the user's subscription.
   *
   * @param  mixed $endDate
   * @return bool
   */
  public function deactivate($endDate = null)
  {
      $endDate = $endDate ?: \Carbon\Carbon::now();
      return $this->forceFill([
          'stripe_active' => false,
          'subscription_end_at' => $endDate
      ])->save();
  }


  public function subscription()
  {
    return new Subscription($this->user);
  }

  public function isSubscribed()
  {
    return !! $this->stripe_active;
  }

  public function isActive()
  {
    return $this->isSubscribed() || $this->isOnGracePeriod();
  }

  public function isOnGracePeriod()
  {
    if(! $endAt = $this->subscription_end_at){
      return false;
    }

    return Carbon::now()->lt(Carbon::instance($endAt));
  }

  public function isTrial()
  {
    if($this->user->stripe_id){
      $trial_end = $this->subscription()->retrieveStripeCustomer()->subscriptions->data['0']->trial_end;
      $end = Carbon::createFromTimestamp($trial_end);

      if(Carbon::now()->lt($end)){
        return $end;
      }
    }



    return false;

  }

  public function trialEndAt()
  {
    if($end = $this->isTrial()){
      return $end;
    }
    return null;
  }

  public function changePlan($plan)
  {
    return $this->forceFill([
      'subscribe_plan_id' => $plan->id
    ])->save();
  }

}
