<?php

namespace App\Traits;

trait FollowBusiness
{

    //Clients
    public function getFollowing()
    {
      return $this->belongsToMany('App\Business' , 'follow_businesses', 'client_id', 'business_id');
    }

    public function isFollow($business)
    {
      return $this->getFollowing->where('id', $business->id)->first()!=null;
    }

    public function follow($business)
    {
      return $this->getFollowing()->attach($business->id);
    }

    public function unfollow($business)
    {
      return $this->getFollowing()->detach($business->id);
    }


    //Business
    public function getFollowers()
    {
      return $this->belongsToMany('App\Client' , 'follow_businesses', 'business_id', 'client_id');
    }

    public function isFollowBy($client)
    {
      return $this->getFollowers->where('id', $client->id)->first()!=null;
    }
}
