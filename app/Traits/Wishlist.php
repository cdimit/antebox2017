<?php

namespace App\Traits;

trait Wishlist
{

    //Clients
    public function wishlist()
    {
      return $this->belongsToMany('App\Products' , 'wishlists', 'client_id', 'product_id');
    }

    public function isOnWishlist($product)
    {
      return $this->wishlist->where('id', $product->id)->first()!=null;
    }

    public function addOnWishlist($product)
    {
      return $this->wishlist()->attach($product->id);
    }

    public function removeFromWishlist($product)
    {
      return $this->wishlist()->detach($product->id);
    }


    //Product
    public function getClientWish()
    {
      return $this->belongsToMany('App\Client' , 'wishlists', 'product_id', 'client_id');
    }

    public function isClientWish($client)
    {
      return $this->getClientWish()->where('id', $client->id)->first()!=null;
    }
}
