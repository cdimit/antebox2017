<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsFields extends Model
{

  /**
  * Fields
  * -------
  * price: item->price * qty
  */


  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'items_fields';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['item_id', 'type', 'value'];

  /**
  * Return the relationship with Product model.
  */
  public function item()
  {
    return $this->belongsTo('App\Items');
  }
}
