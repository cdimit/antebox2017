<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsDiscount extends Model
{
  use SoftDeletes;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'products_discounts';

  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['off_method', 'value', 'new_price', 'valid_until', 'product_id'];

  /**
  * Return the relationship with Category model.
  */
  public function product()
  {
    return $this->belongsTo('App\Products', 'product_id');
  }
}
