<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ClientRequest;

class Items extends Model
{

    public function getPriceAttribute(): string
    {
      //Before Request
      if($this->itemable_type=='App\ShoppingCart'){
          $price = $this->qty * $this->product->currentprice;
        //After Request
      }elseif($this->itemable_type=='App\ClientRequest'){
          $price = $this->fields->where('type', 'price')->first()->value;
      }

      return number_format($price, 2, '.', '');
    }

    /**
     * Don't auto-apply mass assignment protection.
     *
     * @var array
     */
    protected $guarded = [];

    public function itemable()
    {
      return $this->morphTo();
    }

    /**
    * Return the relationship with model.
    */
    public function product()
    {
      return $this->belongsTo('App\Products');
    }

    /**
    * Relationship with model
    */
    public function fields()
    {
      return $this->hasMany('App\ItemsFields', 'item_id');
    }

    //increase quantity for 1
    public function incrQty()
    {
      return $this->update([
        'qty' => $this->qty+1
      ]);
    }

    //decrease quantity for 1
    public function decrQty()
    {
      return $this->update([
        'qty' => $this->qty+1
      ]);
    }

    //set quantity to $num
    public function setQty($num)
    {
      return $this->update([
        'qty' => $num
      ]);
    }

    /**
     *update quantity for $num
     *if qty<=0 remove it from shopping_cart
     */
    public function updateQty($num)
    {
      $qty = $this->qty+$num;
      if($qty<=0){
        $this->remove();
      }

      return $this->update([
        'qty' => $qty
      ]);
    }

    /**
     * remove item from shopping_cart
     *if shopping_cart items is 0 remove shopping_cart
     */
    public function remove()
    {
      $this->delete();

      if($this->itemable->items->count()==0){
        $this->itemable->delete();
      }

    }

    public function moveToRequest(ClientRequest $client_request)
    {
      $this->fields()->create(['type' => 'price', 'value' => $this->price]);

      return $this->update([
              'itemable_type' =>  'App\ClientRequest',
              'itemable_id'   =>  $client_request->id
            ]);
    }
}
