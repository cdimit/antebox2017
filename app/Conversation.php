<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Bid;
use App\ConversationMessage;

class Conversation extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'conversations';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['requester_id', 'bidder_id'];

  /**
  * Return the relationship with User model.
  */
  public function requester()
  {
    return $this->belongsTo('App\User', 'requester_id');
  }

  /**
  * Return the relationship with User model.
  */
  public function bidder()
  {
    return $this->belongsTo('App\User', 'bidder_id');
  }

  /**
  * Relationship with model
  */
  public function messages()
  {
    return $this->hasMany('App\ConversationMessage', 'conversation_id');
  }

  public function hasUser(User $user)
  {
    return $this->requester==$user || $this->bidder==$user;
  }

  public function getOtherUser(User $user)
  {
    if($this->hasUser($user)){
      if($this->requester==$user){
        return $this->bidder;
      }else{
        return $this->requester;
      }
    }

    return null;
  }

  public static function findOrCreate(User $requester, User $bidder)
  {
    $conversation = self::where('requester_id', $requester->id)->where('bidder_id', $bidder->id)->first();

    if($conversation){
      return $conversation;
    }else{
      return self::create(['requester_id' => $requester->id, 'bidder_id' => $bidder->id]);
    }
  }

  public function addBidNote(Bid $bid)
  {
     $this->messages()->create(['message' => $bid->biddable->name.' made a €'.$bid->delivery_fee.' bid for your request', 'user_id' => $this->requester_id, 'is_note' => true]);
    return $this->messages()->create(['message' => 'You made a €'.$bid->delivery_fee.' bid for '.$bid->clientRequest->client->first_name.' request', 'user_id' => $this->bidder_id, 'is_note' => true]);

  }

  public function addMessage(User $user, $message)
  {
    if($message){
      return $this->messages()->create(['message' => $message, 'user_id' => $user->id]);
    }

    return null;
  }
}
