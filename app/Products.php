<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Wishlist;
use Redis;

class Products extends Model
{
  use SoftDeletes;
  use Wishlist;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'products';

  protected $softDelete = true;


  public function getSlugAttribute(): string
  {
    return str_slug($this->name);
  }

  public function getUrlAttribute(): string
  {
      return action('ProductsController@single', [$this->id, $this->slug]);
  }

  public function getCurrentpriceAttribute(): string
  {
    if($this->discount){
      $price = $this->discount->new_price;
    }else {
      $price = $this->price;
    }

    return number_format($price, 2, '.', '');
  }

  public function getTagsstringAttribute(): string
  {
    $tags_string = $this->tags()->pluck('name')->implode(', ');

    return $tags_string;
  }

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'description',
    'long_description',
    'price',
    'bag',
    'weight',
    'height',
    'width',
    'depth',
    'picture',
    'video',
    'category_id',
    'business_id',
    'status',
    'has_stock'
  ];

  /**
  * Return the relationship with Business model.
  */
  public function business()
  {
    return $this->belongsTo('App\Business', 'business_id');
  }

  /**
  * Return the relationship with Category model.
  */
  public function category()
  {
    return $this->belongsTo('App\ProductsCategories', 'category_id');
  }

  public function tags()
  {
    return $this->BelongsToMany('App\Tags', 'products_tags', 'product_id', 'tag_id');
  }

  public function stores()
  {
    return $this->BelongsToMany('App\Stores', 'products_stores', 'product_id', 'store_id');
  }

  public function hasStore($store)
  {
    return $this->stores()->where('store_id', $store->id)->first()!=null;
  }

  public function syncStores($stores)
  {
    return $this->stores()->sync($stores);
  }

  /**
   * Get the discount record associated with the product.
   */
  public function discount()
  {
      return $this->hasOne('App\ProductsDiscount', 'product_id', 'id');
  }

  public function scopeLive()
  {

    return \DB::table('products')
                ->whereExists(function ($query1) {
                    $query1->select(\DB::raw(1))
                          ->from('business')
                          ->whereRaw('business.id = products.business_id')
                          ->whereRaw('business.status <> "offline"');
                })
                ->get();

    // return $query->where('status', 'live')->orWhere('status', 'edited')->get();
  }

  public function items()
  {
    return $this->hasMany('App\Items', 'product_id');
  }

  public function isFeatured()
  {
    return Redis::zscore('featured', $this->id);
  }

  public function scopeFeatures($query)
  {

    $ids = Redis::zrange('featured', 0, -1);

    // $features = Products::hydrate(
    //   array_map('json_decode', $features)
    // );

    $features = self::whereIn('id', $ids)->get();

    // dd($features);
    return $features;
  }

  public function images()
  {
    $images = Redis::smembers('product'.$this->id.'images');

    return $images;

  }


}
