<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Business;
use App\Products;
use App\Client;
use App\ClientRequest;

class ShoppingCart extends Model
{

  public function getPriceAttribute(): string
  {
    $total_price = 0;
    foreach($this->items as $item){
      $total_price = $total_price + $item->price;
    }

    return number_format($total_price, 2, '.', '');
  }

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'shopping_cart';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['client_id', 'business_id'];

  public function items()
  {
    return $this->morphMany('App\Items', 'itemable');
  }

  /**
  * Return the relationship with User model.
  */
  public function client()
  {
    return $this->belongsTo('App\Client');
  }

  /**
  * Return the relationship with User model.
  */
  public function business()
  {
    return $this->belongsTo('App\Business');
  }

  /**
 * Add an item to the shopping cart.
 *
 * @param $item
 */
  public function addItem($item)
  {
      $this->items()->create($item);
  }

  public static function findOrCreate(Business $business)
  {
    $client = auth()->user()->profile;

    $shopping_cart = self::where('business_id', $business->id)->where('client_id', $client->id)->first();

    if($shopping_cart){
      return $shopping_cart;
    }else{
      return self::create(['business_id' => $business->id, 'client_id' => auth()->user()->profile->id ]);
    }
  }

  public function hasItem(Products $product)
  {
    return $this->items()->where('product_id', $product->id)->first()!=null;
  }

  public function getItem(Products $product)
  {
    return $this->items()->where('product_id', $product->id)->first();
  }

  public function remove()
  {
    return $this->items->each->remove();
  }

  public function createRequest(ClientRequest $client_request)
  {
    $this->items->each->moveToRequest($client_request);

    return $this->delete();
  }

  public function belongsToClient(Client $client)
  {
    return $this->client_id==$client->id;
  }

}
