<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Bid extends Model
{
  use SoftDeletes;

  protected $softDelete = true;

  /**
   * Don't auto-apply mass assignment protection.
   *
   * @var array
   */
  protected $guarded = [];

  /**
  * Return the relationship with model.
  */
  public function clientRequest()
  {
    return $this->belongsTo('App\ClientRequest', 'client_request_id');
  }

  public function biddable()
  {
    return $this->morphTo();
  }

  /**
  * update the value.
  */
  public function rejected()
  {
    return $this->update([
      'status' => 'rejected'
    ]);
  }

  /**
  * update the value.
  */
  public function accepted()
  {
    $this->update([
      'status' => 'accepted'
    ]);

    return $this->clientRequest->rejectOpenBids();
  }

  public function isAccepted()
  {
      return $this->status == 'accepted';
  }

  public function isOpen()
  {
      return $this->status == 'open';
  }

  public function deal()
  {
    if($this->biddable_type=='App\Client'){
      return $this->hasOne('App\DealClient');
    }elseif($this->biddable_type=='App\Business'){
      return $this->hasOne('App\DealBusiness');
    }
  }

  public function makeDeal()
  {

    //Complete all queries before commited
    DB::beginTransaction();

    if($this->biddable_type=='App\Client'){
      $deal = DealClient::acceptBid($this);
    }elseif($this->biddable_type=='App\Business'){
      $deal = DealBusiness::acceptBid($this);
    }

    $this->accepted();

    //Commit the transaction
    DB::commit();


    return $deal;
  }
}
