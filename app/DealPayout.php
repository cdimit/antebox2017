<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealPayout extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'deal_invoices';

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
     'deal_invoice_id',
     'service_value',
     'service_fee',
     'payout_price',
     'is_payout'
   ];
}
