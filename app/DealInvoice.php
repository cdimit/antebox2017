<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bid;

class DealInvoice extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'deal_invoices';

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
     'client_request_id',
     'delivery_fee',
     'service_value',
     'service_fee',
     'final_price',
     'is_paid',
     'mode'
   ];

     /**
   * Return the relationship with model.
   */
   public function clientRequest()
   {
    return $this->belongsTo('App\ClientRequest');
   }

   public function deal()
   {

     if($this->mode=='client'){
       return $this->hasOne('App\DealClient', 'deal_invoice_id');
     }
     else{
       return $this->hasOne('App\DealBusiness', 'deal_invoice_id');
     }

   }

   public function payment()
   {
     return $this->morphOne('App\Payment', 'paymentable');
   }


   public function items()
   {
     return $this->request()->items;
   }

   public static function calculateServiceValue(Bid $bid)
   {
     return 0;
   }

   public static function calculateServiceFee(Bid $bid)
   {
     return ($bid->clientRequest->total_price + $bid->delivery_fee) * self::calculateServiceValue($bid);
   }

   public static function calculateFinalPrice(Bid $bid)
   {
     return $bid->clientRequest->total_price + $bid->delivery_fee + self::calculateServiceFee($bid);
   }

   public static function createRecord(Bid $bid, $mode)
   {
     return self::create([
         'client_request_id' => $bid->clientRequest->id,
         'delivery_fee'  => $bid->delivery_fee,
         'service_value' => self::calculateServiceValue($bid),
         'service_fee' => self::calculateServiceFee($bid),
         'final_price' => self::calculateFinalPrice($bid),
         'mode' =>  $mode
     ]);
   }

   public function paid($charge)
   {

     $this->clientRequest->client->user->forceFill([
       'stripe_id' => $charge->customer
     ])->save();

     $this->payment()->create([
       'user_id'  => $this->clientRequest->client->user->id,
       'charge_id'  => $charge->id,
       'amount' => $charge->amount
     ]);

     return $this->update([
       $this->is_paid = 1
     ]);

   }

}
