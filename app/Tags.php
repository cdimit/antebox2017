<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'tags';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name'];


  public function products()
  {
    return $this->belongsToMany('App\Products' , 'products_tags', 'tag_id', 'product_id');
  }

  /**
  * Return tag Object from string.
  *
  * @param String $tags
  * @return Integer tag->id
  */
  public static function findOrCreate($tag_string)
  {
    $lower = strtolower($tag_string);
    $trim = trim($lower, ' ');
    $format = ucwords($trim);

    $tag = self::where('name', $format)->first();

    if($tag){
      return $tag;
    }else{
      return self::create(['name' => $format]);
    }

  }

}
