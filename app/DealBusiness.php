<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DealBusiness extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'deal_businesses';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
     'bid_id',
     'deal_invoice_id',
     'tracking_number',
     'is_received'
   ];

   /**
   * Return the relationship with model.
   */
   public function bid()
   {
     return $this->belongsTo('App\Bid');
   }

   public function getDelivererAttribute()
   {
     return $this->bid->biddable;
   }

   public function isClientDeal()
   {
     return false;
   }

   /**
   * Return the relationship with model.
   */
   public function invoice()
   {
     return $this->belongsTo('App\DealInvoice');
   }

   public static function acceptBid(Bid $bid)
   {

     $deal_invoice = DealInvoice::CreateRecord($bid, 'business');

     return self::create(['bid_id' => $bid->id, 'deal_invoice_id' => $deal_invoice->id]);
   }

}
