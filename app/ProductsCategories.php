<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductsCategories extends Model
{
  use SoftDeletes;

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'products_categories';

  protected $softDelete = true;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'description', 'icon'];

  /**
  * Return the products with this category.
  */
  public function products()
  {
    return $this->hasMany('App\Products', 'category_id');
  }

  /**
  * Return the products with this category.
  */
  public function fields()
  {
    return $this->hasMany('App\ProductsCategoryFields', 'products_category_id');
  }


}
